<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => \UriLocalizer::localeFromRequest()], function () {

  Route::group(['middleware' => ['analytic']], function () {
    Route::get('/', 'PageController@index')->name('index');
    Route::get('/logo', 'PageController@logo')->name('logo');

    Route::prefix('/about-us')->group(function () {
      Route::get('/company-profile', 'PageController@companyProfile')->name('companyProfile');
      Route::get('legality', 'PageController@legality')->name('legality');
      Route::get('management-structure', 'PageController@managementStructure')->name('managementStructure');
      Route::get('broker-representative', 'PageController@brokerRepresentative')->name('brokerRepresentative');
      Route::get('office-location', 'PageController@officeLocation')->name('officeLocation');

      Route::get('our-history', 'PageController@company')->name('aboutCompany');
      Route::get('vision-mission', 'PageController@visionMission')->name('aboutVisionMission');
      Route::get('our-value', 'PageController@value')->name('aboutValue');
      Route::get('why-melandas', 'PageController@whyMelandas')->name('aboutWhyMelandas');
      Route::get('career', 'PageController@career')->name('aboutCareer');
      Route::get('career/experienced_professionals', 'PageController@career_experienced_professional')->name('aboutCareerExperienced');
      Route::get('career/recent_graduated', 'PageController@career_recent_graduated')->name('aboutCareerGraduated');
      Route::get('our-stores', 'PageController@ourStore')->name('ourStores');
      Route::get('our-brands', 'PageController@ourBrands')->name('ourBrands');
    });


    Route::prefix('futures')->group(function () {
      Route::get('trading', 'PageController@futuresTrading')->name('futuresTrading');
      Route::get('rules', 'PageController@futuresRules')->name('futuresRules');
    });

    Route::prefix('platform')->group(function () {
      Route::get('download', 'PageController@platformDownload')->name('platformDownload');
      Route::get('introductions', 'PageController@platformIntroduction')->name('platformIntroduction');
      Route::get('how-to-use', 'PageController@platformHowToUse')->name('platformHowToUse');
      Route::get('maxco-mobile', 'PageController@maxcoMobile')->name('maxcoMobile');
    });

    Route::prefix('market')->group(function () {
      Route::get('forex', 'PageController@marketForex')->name('marketForex');
      Route::get('komoditi', 'PageController@marketKomoditi')->name('marketKomoditi');
      Route::get('index', 'PageController@marketIndex')->name('marketIndex');
    });

    Route::prefix('trading-account')->group(function () {
      Route::get('account-type', 'PageController@tradingAccountTtype')->name('tradingAccountTtype');
      // Route::get('komoditi','PageController@marketKomoditi')->name('marketKomoditi');
      // Route::get('index','PageController@marketIndex')->name('marketIndex');
      Route::get('trading-hours', 'PageController@tradinghours')->name('tradinghours');
    });

    Route::prefix('promo')->group(function () {
      Route::get('bounty-program', 'PageController@bountyprogram')->name('bountyprogram');
      // Route::get('komoditi','PageController@marketKomoditi')->name('marketKomoditi');
      // Route::get('index','PageController@marketIndex')->name('marketIndex');
    });

    Route::get('redirect', 'PageController@redirect')->name('redirect');

    Route::prefix('expert-area')->group(function () {
      Route::get('how-to-analyze-market', 'PageController@expertAnalyzeMarket')->name('expertAnalyzeMarket');
      Route::get('how-to-read-candlestick', 'PageController@expertCandlestick')->name('expertCandlestick');
      Route::get('technical-analysis', 'PageController@expertTechnicalAnalysis')->name('expertTechnicalAnalysis');
      Route::get('fundamental-analysis', 'PageController@expertFundamentalAnalysis')->name('expertFundamentalAnalysis');
      Route::get('market-news', 'PageController@expertMarketNews')->name('expertMarketNews');
      Route::get('todays-expert-tips', 'PageController@expertTips')->name('expertTips');
      Route::get('trading-tricks', 'PageController@tradingTricks')->name('tradingTricks');
      Route::get('news', 'PageController@news')->name('news');
    });

    Route::get('/privacy-policy', 'PageController@privacyPolicy')->name('privacyPolicy');
    Route::get('tac', 'PageController@termCondition')->name('termCondition');
    Route::get('faq', 'PageController@faq')->name('faq');

    Route::get('blogs', 'PageController@blogIndex')->name('blogIndex');
    Route::get('blogs/single/{slug}', 'PageController@blogSingle')->name('blogSingle');

    Route::get('trading-rules', 'PageController@tradingrules')->name('tradingrules');

    Route::prefix('contact')->group(function () {
      Route::get('customer-service', 'PageController@customerService')->name('customerService');
      Route::get('contact-you', 'PageController@contactYou')->name('contactYou');
    });

    Route::group(['middleware' => ['login']], function () {
      Route::prefix('trading-tools')->group(function () {
        Route::get('profile', 'PageController@tradingToolProfile')->name('profile-trading-tool');
        Route::get('robot-trading', 'PageController@robotTrading')->name('trading-tool-robot');
      });
      Route::get('logout','PageController@logout')->name('logout');
    });

    Route::get('login', 'PageController@login')->name('login');
    Route::post('login', 'PageController@postlogin')->name('postlogin');
    Route::get('forget-password', 'PageController@forgetpassword')->name('forgetpassword');
    Route::post('send-verify-code','PageController@sendVerificationCodeForgotPass')->name('sendVerificationCodeForgotPass');
    Route::get('forget-password-change', 'PageController@forgetpasswordchange')->name('forgetpasswordchange');
    Route::post('forget-password-new', 'PageController@forgetpasswordnew')->name('forgetpasswordnew');

    // ARTIKEL 1
    Route::get('get-smart-maxco-app', 'PageController@getsmartmaxcoapps')->name('getsmartmaxcoapps');
    // ARTIKEL 2
    Route::get('easy-way-access-trading', 'PageController@easywayaccesstrading')->name('easywayaccesstrading');
    // ARTIKEL 3
    Route::get('legal-broker', 'PageController@legalbroker')->name('legalbroker');
    // artikel 4
    Route::get('prestigious', 'PageController@prestigious')->name('prestigious');
    // artikel 5
    Route::get('commission-80-percent', 'PageController@commission80percent')->name('commission80percent');
    // artikel 6
    Route::get('simple-marketing', 'PageController@simplemarketing')->name('simplemarketing');
    // artikel 7
    Route::get('gue-juga-invest', 'PageController@guejugainvest')->name('guejugainvest');
    // artikel 8
    Route::get('bantu-suami-dari-rumah', 'PageController@bantusuamidarirumah')->name('bantusuamidarirumah');
    // artikel 9
    Route::get('accept-all-eas', 'PageController@acceptalleas')->name('acceptalleas');

    // artikel 10
    Route::get('robot-trading-emas-online', 'PageController@artikel10')->name('acceptalleas');
    Route::get('robot-trading-demo', 'PageController@robotDemo')->name('robotDemo');
    Route::post('save-data-register', 'PageController@savedataregister')->name('apisavedataregister');
    Route::post('api-register-verification', 'PageController@apiadddemouser')->name('apiadddemouser');

    Route::get('thankyou', 'PageController@thankyou')->name('thankyou');

    Route::prefix('api')->group(function () {
      Route::post('insertcrm', 'PageController@apiinsertcrm')->name('postinsertcrm');
    });

    Route::post('registermaxco', 'PageController@apiregistermaxco')->name('postapiregistermaxco');
    Route::post('register-verification', 'PageController@adddemouser')->name('postRegisterVerification');
    Route::get('/register-verification', 'PageController@sendVerificationCodeRegistration')->name('getRegisterVerification');
    Route::get('thank-you', 'PageController@thankyou2')->name('thank-you');
    Route::get('accept-all-eas-thankyou', 'PageController@acceptalleasthankyou')->name('acceptalleasthankyou');

    Route::get('sitemap','PageController@sitemaphtml');

    /* CAMPAIGN MADE BY YOKE ENDARTO*/

    //Route::get('redeem-bonus-500-dolar','PageController@redeembonus500dolarV2')->name('redeembonus500dolarV2');
    //Route::get('robot-trading-tester','PageController@robotTradingTester')->name('robotTradingTester');

    /* END OF CAMPAIGN MADE BY YOKE ENDARTO*/

    /* SUBMIT CAMPAIGN FORM MADE BY YOKE ENDARTO*/
    Route::post('funnel-campaign','PageController@registerCRM')->name('funnelCampaignPost');
    /* END OF SUBMIT CAMPAIGN FORM MADE BY YOKE ENDARTO*/

    // Route::get('battle-of-the-robots-2020/promo-page','CampaignController@battleOfTheRobotsPromo')->name('battleOfTheRobotsPromo');

  });


  Route::get('register', 'PageController@register')->name('register');
  Route::get('lost-password', 'PageController@lostPassword')->name('lostPassword');

  Route::prefix('process')->group(function () {
    Route::post('register', 'FormController@register')->name('processRegister');
    Route::post('login', 'FormController@login')->name('processLogin');
  });

  Route::get('cookie', 'FormController@cookie')->name('cookie');
  Route::get('lab/email', 'LabController@email')->name('labEmail');

});

Route::get('sitemap.xml','PageController@sitemap');

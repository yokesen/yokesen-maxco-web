$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#maxco_contact_form").validate({
        ignore: [],
        // errorElement: "div",
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            company: {
                required: true
            },
            message: {
                required: true
            }
        },
        messages: {
            name: "Please fill your name.",
            email: "Please use valid email.",
            company: "Please fill subject.",
            message: "Please fill message.",
        },
        submitHandler: function (form) {
            $('#maxco_contact_form').fadeOut(1000);
            $('#contact_message').fadeIn(1000);
            // $.ajax({
            //     type: "POST",
            //     url: "php/submit.php",
            //     data: $("#contact_form").serialize(),
            //     success: function (msg) {
            //         //alert(msg);
            //         if (msg) {
            //             $('#contact_form').fadeOut(1000);
            //             $('#contact_message').fadeIn(1000);
            //             document.getElementById("contact_message");
            //             return true;
            //         }
            //     }
            // });
        }
    });

    $("#maxco_blog_comment_form").validate({
        ignore: [],
        // errorElement: "div",
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            comment: {
                required: true
            }
        },
        messages: {
            name: "Please fill your name.",
            email: "Please use valid email.",
            comment: "Leave your comment.",
        },
        submitHandler: function (form) {
            // $.ajax({
            //     type: "POST",
            //     url: "php/submit.php",
            //     data: $("#contact_form").serialize(),
            //     success: function (msg) {
            //         //alert(msg);
            //         if (msg) {
            //             $('#contact_form').fadeOut(1000);
            //             $('#contact_message').fadeIn(1000);
            //             document.getElementById("contact_message");
            //             return true;
            //         }
            //     }
            // });
        }
    });

});
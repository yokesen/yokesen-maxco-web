(function () {
    "use strict";
    if (document.getElementsByClassName("ts-full-screen").length) {       
        var addheight = window.innerHeight;
        if (window.innerHeight < 768) {
            addheight += window.innerHeight * 0.3;
        }
        document.getElementsByClassName("ts-full-screen")[0].style.height =  addheight + "px";
    }
})();
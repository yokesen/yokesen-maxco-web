@extends('page.template.master_withoutdisclaimer')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<div id="content">
  <section class="portfolio padding-top-20 padding-bottom-20">
    <div class="container" style="min-height: 352px;">
      <div class="text-center">
        <h1>500</h1>
        <h5 class="text-muted">Ups.. servernya error !</h5>
      </div>
      <div class="col-md-12 text-center">
        <img src="{{url('/')}}/web/images/webpage/errors/500.png" class="img-responsive">
      </div>
    </div>
  </section>
</div>
@endsection
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NHQE658KYS"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-NHQE658KYS');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Maxcofutures">
    <title>@yield('title')</title>

    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="@yield('og-title')" />
    <meta property="og:image" content="@yield('og-image')" />
    <meta property="og:description" content="@yield('og-description')">

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/favicon.ico">

    @yield('csslist')
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{url('/')}}/web/css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link href="{{url('/')}}/web/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/web/css/ionicons.min.css" rel="stylesheet">
    <link href="{{url('/')}}/web/css/main.css" rel="stylesheet">
    <link href="{{url('/')}}/web/css/style.css" rel="stylesheet">
    <link href="{{url('/')}}/web/css/responsive.css" rel="stylesheet">

    <!-- Online Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800,200,500&display=swap' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,400italic,300,300italic,600,700,700italic,800,800italic&display=swap' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400italic,400,700&display=swap' rel='stylesheet' type='text/css'>

    <!-- COLORS -->
    <link rel="stylesheet" id="color" href="{{url('/')}}/web/css/colors/blue.css">

    @yield('cssonpage')


    <!-- JavaScripts -->
    <script src="{{url('/')}}/web/js/modernizr.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- Wrap -->
    <div id="wrap">
        <div id="content">
            <section class="portfolio padding-top-20 padding-bottom-20">
                <div class="container" style="min-height: 352px;">
                    <div class="text-center">
                        <h1>404</h1>
                        <h5 class="text-muted">wah..kamu tersesat</h5>
                    </div>
                    <div class="col-md-12 text-center">
                        <img src="{{url('/')}}/web/images/webpage/errors/404.png" class="img-responsive">
                    </div>
                    <div class="col-md-12 text-center">
                        <br>
                        <br>
                        <a class="btn-openacc" rel="nofollow" style="cursor:pointer;padding-left: 55px;" onclick="window.history.back();">
                        Kembali ke halaman sebelumnya
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script src="{{url('/')}}/web/js/jquery-1.11.0.min.js"></script>
    <script src="{{url('/')}}/web/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/web/js/own-menu.js"></script>
    @yield('jsonpage')

</body>

</html>
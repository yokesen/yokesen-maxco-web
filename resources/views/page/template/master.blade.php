<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="google-site-verification" content="1wkQa3KyE7s1SPZC5i9NawpHSae3Y9tGlUnvYDVDj9o" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-NHQE658KYS"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-NHQE658KYS');
  </script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Maxcofutures">
  <title>@yield('title')</title>
  <meta name="robots" content="index,follow">
  <meta name="googlebot" content="index,follow">

  <!-- Favicon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/favicon.ico">

  <meta property="og:title" content="@yield('og-title')" />
  <meta property="og:url" content="{{URL::current()}}" />
  <meta property="og:image" content="{{url('/')}}@yield('og-image')" />
  <meta property="og:description" content="@yield('og-description')">

  <!-- @yield('csslist') -->
  <!-- Bootstrap Core CSS -->
  <link href="{{url('/')}}/web/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="{{url('/')}}/web/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="{{url('/')}}/web/css/ionicons.min.css" rel="stylesheet">
  <link href="{{url('/')}}/web/css/main.css" rel="stylesheet">
  <link href="{{url('/')}}/web/css/style.css" rel="stylesheet">
  <link href="{{url('/')}}/web/css/responsive.css" rel="stylesheet">

  <!-- Online Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800,200,500&display=swap' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,400italic,300,300italic,600,700,700italic,800,800italic&display=swap' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400italic,400,700&display=swap' rel='stylesheet' type='text/css'>

  <!-- COLORS -->
  <link rel="stylesheet" id="color" href="{{url('/')}}/web/css/colors/blue.css">

  @yield('cssonpage')


  <!-- JavaScripts -->
  <script src="{{url('/')}}/web/js/modernizr.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
  <style>
    .filter-option {
      margin-top: -5px;
    }

    .top-info .personal-info li {
      padding: 1px 0px !important;
    }
  </style>
  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '242949443388541');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=242949443388541&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->
</head>

<body>
  <!-- Wrap -->
  <div id="wrap">

    <!-- header -->
    @include('page.template.header')

    @yield('content')

    @include('page.template.disclaimerprivacyrisk')

    <!-- FOOTER -->
    @include('page.template.footer')

  </div>
  <!-- <div class="modal fade" id="introModal" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 10000;">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p>Sehubungan dengan maraknya website palsu untuk melakukan penipuan demi keuntungan pribadi yang mengatasnamakan PT MAXCO FUTURES dalam hal menawarkan instrumen investasi di bidang Perdagangan Berjangka (Forex), Berikut adalah nama2 website ILEGAL sbb: www.ptmaxcofutures.com, www.forexmaxco.com, www.pt-maxco.com, www.ptmaxco.com, www.pt-maxcofutures.com, www.profitptmaxco.id, www.pt-maxcofutures.id, www.maxcofuture.com, www.maxco-futures.com,www.ptmaxco-futures.com, agar masyarakat lebih berhati hati atas penawaran yang dilakukan oleh website ilegal tersebut, dengan demikian PT MAXCO FUTURES tidak bertanggung jawab atas penyalahgunaan website ilegal tersebut.</p>
            </div>
        </div>
    </div>
  </div> -->
  <script src="{{url('/')}}/web/js/jquery-1.11.0.min.js"></script>
  <script src="{{url('/')}}/web/js/bootstrap.min.js"></script>
  <script src="{{url('/')}}/web/js/own-menu.js"></script>
  <script src="{{url('/')}}/web/js/jquery.isotope.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.flexslider-min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.countTo.js"></script>
  <script src="{{url('/')}}/web/js/owl.carousel.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.cubeportfolio.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.colio.min.js"></script>
  <script src="{{url('/')}}/web/js/main.js"></script>

  @yield('jslist')
  @yield('jsonpage')

  <script src="{{url('/')}}/web/js/lazysizes.min.js" async></script>
</body>

</html>

<a href="{{env('CABINET_URL')}}login?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&{{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" class="cbp-l-loadMore-link btn-clientarea"
    rel="nofollow" style="min-width: 190px;margin-bottom: 10px;">
    <span class="cbp-l-loadMore-defaultText">{{trans('page.menu-client-area')}} <i
        class="fa fa-caret-right"></i></span>
</a>

<a href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&{{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" class="cbp-l-loadMore-link btn-openacc"
    rel="nofollow" style="min-width: 190px;margin-bottom: 10px;">
    <span class="cbp-l-loadMore-defaultText">{{trans('page.menu-open-account')}} <i
        class="fa fa-caret-right"></i></span>
</a>

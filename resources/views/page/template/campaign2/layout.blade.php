<!doctype html>
<html lang="en">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-NHQE658KYS"></script>
  <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
          dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'G-NHQE658KYS');
  </script>
  @yield('gtag-event')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="yokesen">

    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600"> -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;1,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{url('/')}}/campaign2/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{url('/')}}/campaign2/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="{{url('/')}}/campaign2/css/magnific-popup.css">
    <link rel="stylesheet" href="{{url('/')}}/campaign2/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/')}}/campaign2/css/style.css">
    <link rel="stylesheet" href="{{url('/')}}/campaign2/css/custome.css">
    <title>@yield('title')</title>
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:image" content="@yield('og-image')" />


      @yield('cssonpage')

    
    @yield('csslist')
    @yield('cssonpage')
    @yield('zendesk')
    @yield('facebookPixel')

</head>

<body data-spy="scroll" data-target=".navbar">
@yield('fbtrack')
@yield('googletagmanagerbody')
    <div class="ts-page-wrapper" id="page-top">

        <!--*********************************************************************************************************-->
        <!--************ CONTENT ************************************************************************************-->
        <!--*********************************************************************************************************-->
        @yield('content')
        <!--end #content-->

        <!--*********************************************************************************************************-->
        <!--************ FOOTER *************************************************************************************-->
        <!--*********************************************************************************************************-->
        <footer id="ts-footer">

            <section id="" class="ts-separate-bg-element" data-bg-color="#12264f">
                <div class="container">
                    <div class="row py-4 text-white ">

                        <div class="col-md-12">
                            <h4 class="mb-1rem">Disclaimer</h4>
                            <p class="text-white mb-5 ts-opacity__50">Informasi yang disediakan dalam situs web ini dimaksudkan secara eksklusif untuk tujuan informatif dan diperoleh dari sumber yang dapat dipercaya. PT. Maxco Futures tidak menjamin kelengkapan dan keakuratan informasi dan tidak akan bertanggung jawab atas kerugian langsung atau tidak langsung karena menggunakan informasi yang disediakan di situs web ini.</p>
                        </div>
                        <div class="col-md-12">
                            <h4 class="mb-1rem">Kebijakan Privasi</h4>
                            <p class="text-white mb-5 ts-opacity__50"> Untuk keperluan internal, PT. Maxco Futures memerlukan informasi pribadi bagi mereka yang mendaftar demo dan akun live Maxco. PT. Maxco Futures dan karyawan diharuskan untuk menjaga kerahasiaan informasi klien dan dilarang untuk berbagi kepada pihak ketiga. Namun, jika diwajibkan oleh hukum, PT. Maxco Futures dapat memberikan informasi kepada otoritas publik.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <h4 class="mb-1rem">Peringatan Resiko Tentang Perdagangan</h4>
                            <p class="text-white mb-5 ts-opacity__50">Transaksi melalui margin adalah produk dengan penggunaan mekanisme leverage, memiliki risiko tinggi dan mungkin tidak cocok untuk semua orang. TIDAK ADA JAMINAN KEUNTUNGAN atas investasi Anda dan oleh karena itu berhati-hatilah terhadap pihak yang memberi jaminan keuntungan pada perdagangan. Anda disarankan untuk tidak menggunakan dana jika Anda tidak siap untuk menanggung kerugian. Sebelum memustuskan untuk berdagang, pastikan Anda memahami risiko yang terjadi dan pertimbangkan juga pengalaman Anda.
                            </p>
                        </div>
                    </div>
                    <div class="text-center text-white py-4">
                        <small>© All Rights Reserved Maxco</small>
                    </div>
                </div>
                <!--end container-->
            </section>

        </footer>
        <!--end #footer-->

    </div>
    <script src="{{url('/')}}/campaign2/js/custom.hero.js"></script>
    <script src="{{url('/')}}/campaign2/js/jquery-3.3.1.min.js"></script>
    <script src="{{url('/')}}/campaign2/js/popper.min.js"></script>
    <script src="{{url('/')}}/campaign2/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/campaign2/js/imagesloaded.pkgd.min.js"></script>
    <!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script> -->
    <script src="{{url('/')}}/campaign2/js/isInViewport.jquery.js"></script>
    <script src="{{url('/')}}/campaign2/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('/')}}/campaign2/js/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/campaign2/js/scrolla.jquery.min.js"></script>
    <script src="{{url('/')}}/campaign2/js/jquery.validate.min.js"></script>
    <!-- <script src="{{url('/')}}/campaign2/js/jquery-validate.bootstrap-tooltip.min.js"></script> -->
    <script src="{{url('/')}}/campaign2/js/custom.js"></script>
    @yield('jsonpage')

</body>

</html>

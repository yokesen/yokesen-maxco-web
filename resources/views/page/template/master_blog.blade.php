<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="google-site-verification" content="1wkQa3KyE7s1SPZC5i9NawpHSae3Y9tGlUnvYDVDj9o" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-NHQE658KYS"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-NHQE658KYS');
  </script>
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta name="robots" content="index,follow">
  <meta name="googlebot" content="index,follow">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="title" content="@yield('og-title')">
  <meta name="description" content="@yield('og-description')">
  <meta name="keywords" content="@yield('meta-keyword')">
  <meta name="author" content="Maxcofutures">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <meta property="og:url" content="{{URL::current()}}" />
  <meta property="og:type" content="article" />
  <meta property="og:title" content="@yield('og-title')" />
  <meta property="og:image" content="@yield('og-image')" />
  <meta property="og:description" content="@yield('og-description')">

  <!-- Favicon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/favicon.ico">

  @yield('csslist')
  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="{{url('/')}}/web/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link href="{{url('/')}}/web/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="{{url('/')}}/web/css/ionicons.min.css" rel="stylesheet">
  <link href="{{url('/')}}/web/css/main.css" rel="stylesheet">
  <link href="{{url('/')}}/web/css/style.css" rel="stylesheet">
  <link href="{{url('/')}}/web/css/responsive.css" rel="stylesheet">

  <!-- Online Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800,200,500&display=swap' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,400italic,300,300italic,600,700,700italic,800,800italic&display=swap' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400italic,400,700&display=swap' rel='stylesheet' type='text/css'>

  <!-- COLORS -->
  <link rel="stylesheet" id="color" href="{{url('/')}}/web/css/colors/blue.css">

  @yield('cssonpage')


  <!-- JavaScripts -->
  <script src="{{url('/')}}/web/js/modernizr.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
  @yield('facebookPixel')
</head>

<body>
  @yield('fbtrack')
  @yield('googletagmanagerbody')

  <!-- Wrap -->
  <div id="wrap">
    <header>
      <!-- Top bar -->
      <div class="top-bar">
        <div class="top-info">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-xs-12 personal-info">
                <ul class="personal-info" style="margin-left: 16px;">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $currentLanguage->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      @foreach ($altLocalizedUrls as $alt)
                      <li><a href="{{ $alt['url'] }}" hreflang="{{ $alt['locale'] }}">{{ $alt['name'] }}</a></li>
                      @endforeach
                    </ul>
                  </li>
                  <li>
                    @include('page.template.sosmedlink')
                  </li>
                </ul>
              </div>
              <div class="col-md-6 col-xs-12">
                <div class="right-sec" style="min-width: 286px;">
                  <div class="col-md-6 col-xs-6">
                    <a class="btn br-0 bg-paninred" href="{{env('CABINET_URL')}}login?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" style="width:100%;">{{trans('page.menu-client-area')}}</a>
                  </div>
                  <div class="col-md-6 col-xs-6">
                    <a class="btn br-0 bg-paninblue" href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" style="width:100%;">{{trans('page.menu-open-account')}}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Navigation -->
      <nav class="navbar">
        <div class="sticky">
          <div class="container">

            <!-- LOGO -->
            <div class="logo" style="top: 8px;">
              <a href="{{route('index')}}">
                <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/web/logo_300.png" alt="" style="<?php if (session('isMobile')) {
                                                                                                            echo 'height:46px;';
                                                                                                          } else {
                                                                                                            echo 'height:49px;';
                                                                                                          } ?>">
              </a>
            </div>

            <!-- Nav -->
            <ul class="nav ownmenu" style="height: 35px;">

            </ul>
            <!-- Search -->
            <div class="search-icon">
              <a style="<?php
                        if (session('isMobile')) {
                          echo 'top:0px;';
                        }
                        ?>">
                <i class="fa fa-search"></i>
              </a>
              <form action="{{route('blogIndex')}}" method="get">
                <input class="form-control" type="search" name="search" placeholder="Type Here" value="{{$_GET['search']}}">
                <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div>
          </div>
        </div>
      </nav>
    </header>
    @yield('content')
  
  </div>
  <script src="{{url('/')}}/web/js/jquery-1.11.0.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.validate.min.js"></script>

  <script src="{{url('/')}}/web/js/bootstrap.min.js"></script>
  <script src="{{url('/')}}/web/js/own-menu.js"></script>
  <script src="{{url('/')}}/web/js/jquery.isotope.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.flexslider-min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.countTo.js"></script>
  <script src="{{url('/')}}/web/js/owl.carousel.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.cubeportfolio.min.js"></script>
  <script src="{{url('/')}}/web/js/jquery.colio.min.js"></script>
  <script src="{{url('/')}}/web/js/main.js"></script>
  <script src="{{url('/')}}/web/js/vendor.js"></script>
  @yield('jsonpage')


  <script src="{{url('/')}}/js/sweetalert.min.js"></script>
  <!-- Include this after the sweet alert js file -->
  @include('sweet::alert')
  <script src="{{url('/')}}/web/js/lazysizes.min.js" async></script>

</body>

</html>

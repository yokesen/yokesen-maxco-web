@if(Session::get('robot') < 100 )

  <div class="iklan-atas" data-stellar-background-ratio="0.5">
    <video controls class="box-iklan-atas img-chat-sales-bounce-atas" id="iklanRobot">
      <source src="{{url('/')}}/web/videos/campaign/battle-of-the-robot/battle-of-the-robot.mp4" type="video/mp4" />
    </video>
    <div class="row margin-top-10 margin-bottom-10">
      <div class="col-md-12 text-center">
        <a href="{{route('battleOfTheRobotsPromo')}}" class="btn btn-lg btn-1 bg-paninred">Daftar Sekarang!<i class="fa fa-caret-right"></i></a>
      </div>
    </div>
    <div class="tombol-close-iklan-atas text-center" onclick="closeIklanAtasFunc()"> <small>close</small> x (tutup iklan klik disini)</div>
  </div>
@endif

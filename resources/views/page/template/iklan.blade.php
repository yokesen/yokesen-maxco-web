@if(Session::get('robot') < 100 )
  @if (!session('isMobile'))
    <div class="iklan">
      <div class="tombol-close-iklan" onclick="closeIklanFunc()"> <small>close</small> x</div>
      <a href="{{route('battleOfTheRobotsPromo')}}">
        <video controls class="box-iklan img-chat-sales-bounce" id="iklanRobot">
          <source src="{{url('/')}}/web/videos/campaign/battle-of-the-robot/battle-of-the-robot.mp4" type="video/mp4">
          </video>
        </a>
      </div>

    @endif
  @endif

var vid = document.getElementById("iklanRobot");

$(document).ready(function() {

  setTimeout(function() {
    $(".iklan").css("display", "block");
  }, 1000);

  setTimeout(function() {
    $(".box-iklan").css("display", "block");
    $(".box-iklan").width(440);

  }, 1300);

  setTimeout(function() {
    $(".iklan-atas").css("display", "block");
  }, 1000);

  setTimeout(function() {
    $(".box-iklan-atas").css("display", "block");
    $(".box-iklan-atas").width(440);

  }, 1300);


  setTimeout(function() {
    vid.play();
  }, 2700);

  setTimeout(function() {
    $(".livechat").css("display", "block");
  }, 5000);


  setTimeout(function() {
    $(".box-chat").css("display", "block");
    $(".box-chat").width(235);
  }, 2300);

  setTimeout(function() {
    $(".text-chat-1").css("display", "block");
  }, 2500);

  setTimeout(function() {
    $(".text-chat-2").css("display", "block");
  }, 2700);
});

function closeIklanFunc() {
  $(".iklan").css("display", "none");
  vid.pause();
}

function closeIklanAtasFunc() {
  $(".iklan-atas").css("display", "none");
  vid.pause();
}


function closeChatFunc() {
  $(".livechat").css("display", "none");
}

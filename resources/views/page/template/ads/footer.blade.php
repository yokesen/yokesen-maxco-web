@if( !isset($viewVerifiCode))
<div class="pix_section pix-padding-v-20 ">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-xs-12">
                <div class="pix-content">
                    <h6 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>DISCLAIMER</strong></span></h6>
                    <p class="pix-black-gray-light extra-small-text text-justify">
                        <span class="pix_edit_text">Informasi yang disediakan dalam situs web ini dimaksudkan secara eksklusif untuk tujuan informatif dan diperoleh dari sumber yang dapat dipercaya. PT. Maxco Futures tidak menjamin kelengkapan dan keakuratan informasi dan tidak akan bertanggung jawab atas kerugian langsung atau tidak langsung karena menggunakan informasi yang disediakan di situs web ini. @yield('addtional-disclaimer')</span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content">
                    <h6 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>KEBIJAKAN PRIVASI</strong></span></h6>
                    <p class="pix-black-gray-light extra-small-text text-justify">
                        <span class="pix_edit_text">Untuk keperluan internal, PT. Maxco Futures memerlukan informasi pribadi bagi mereka yang mendaftar demo dan akun live Maxco. PT. Maxco Futures dan karyawan diharuskan untuk menjaga kerahasiaan informasi klien dan dilarang untuk berbagi kepada pihak ketiga. Namun, jika diwajibkan oleh hukum, PT. Maxco Futures dapat memberikan informasi kepada otoritas publik.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content">
                    <h6 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>PERINGATAN RISIKO TENTANG PERDAGANGAN</strong></span></h6>
                    <p class="pix-black-gray-light extra-small-text text-justify">
                        <span class="pix_edit_text">Transaksi melalui margin adalah produk dengan penggunaan mekanisme leverage, memiliki risiko tinggi dan mungkin tidak cocok untuk semua orang. TIDAK ADA JAMINAN KEUNTUNGAN atas investasi Anda dan oleh karena itu berhati-hatilah terhadap pihak yang memberi jaminan keuntungan pada perdagangan. Anda disarankan untuk tidak menggunakan dana jika Anda tidak siap untuk menanggung kerugian. Sebelum memustuskan untuk berdagang, pastikan Anda memahami risiko yang terjadi dan pertimbangkan juga pengalaman Anda.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="pix_section pix-padding-v-20">
    <div class="container">
        <div class="row">
            <div class="pix-inner-col col-md-8">
                <div class="pix-padding-v-30">
                    <a class="pix-inline-block" href="#"><img src="{{url('/')}}/web/images/web/logo_300.png" alt="" class="img-responsive" style="max-width: 183px !important;"></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pix-content pix-padding-v-30 text-right">
                    <span class="pix-black-gray-light pix-padding-v-10 pix-inline-block pix-margin-fix-top"><span class="pix_edit_text">© All Rights Reserved Maxco</span></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-xs-12">
                <div class="pix-content">
                    <h5 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>TAUTAN</strong></span></h5>
                    <div class="pix-padding-v-5">
                        <a class="pix-black-gray-light" href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}"><span class="pix_edit_text">Buka Akun</span></a>
                    </div>
                    <div class="pix-padding-v-5">
                        <a class="pix-black-gray-light" href="{{url('/')}}/{{App::getLocale()}}/trading-account/account-type"><span class="pix_edit_text">Pilihan Produk</span></a>
                    </div>
                    <div class="pix-padding-v-5">
                        <a href="#{{url('/')}}/{{App::getLocale()}}/expert-area/how-to-analyze-market" class="pix-black-gray-light"><span class="pix_edit_text">Area Pakar</span></a>
                    </div>
                    <div class="pix-padding-v-5">
                        <a href="https://www.facebook.com/smartmaxco" class="small-social" target="_blank">
                            <i class="fa fa-facebook color-fb"></i>
                        </a>
                        <a href="https://www.instagram.com/smartmaxco" class="small-social" target="_blank">
                            <i class="fa fa-instagram big-icon-50 color-ig"></i>
                        </a>
                        <a href="https://www.youtube.com/channel/UC69yjQ0hynhMXffy4K-XpWw" class="small-social" target="_blank">
                            <i class="fa fa-youtube big-icon-50 color-yt"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <h5 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>TENTANG KAMI</strong></span></h5>
                <div class="pix-padding-v-5">
                    <a href="{{url('/')}}/{{App::getLocale()}}/about-us/company-profile" class="pix-black-gray-light"><span class="pix_edit_text">Broker Terbaik</span></a>
                </div>
                <div class="pix-padding-v-5">
                    <a href="{{url('/')}}/{{App::getLocale()}}/about-us/office-location" class="pix-black-gray-light"><span class="pix_edit_text">Hubungi Kami</span></a>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <h5 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>LEGALITY</strong></span></h5>
                <div class="pix-padding-v-5 pix-black-gray-light">
                    <span class="pix_edit_text">Izin Usaha Pialang Berjangka</span>
                </div>
                <div class="pix-padding-v-5 pix-black-gray-light ">
                    <span class="pix_edit_text">Surat Persetujuan Anggota Bursa (SPAB)</span>
                </div>
                <div class="pix-padding-v-5 pix-black-gray-light">
                    <span class="pix_edit_text">Perubahan Nama PT. Panin Futures</span>
                </div>
                <div class="pix-padding-v-5 pix-black-gray-light">
                    <span class="pix_edit_text">Certificate of Membership</span>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <h5 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>JAKARTA</strong></span></h5>
                <div class="pix-padding-v-10">
                    <span class="pix-black-gray-light pix_edit_text">Panin Bank Centre - Ground Floor, Jl. Jend. Sudirman Kav-1 Senayan <br> Jakarta Selatan 10270, DKI Jakarta, Indonesia</span>
                </div>
                <h5 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>SURABAYA</strong></span></h5>
                <div class="pix-padding-v-10">
                    <span class="pix-black-gray-light pix_edit_text">Jln. Dharmahusada No.60A, Mojo, Gubeng<br>
                        Surabaya 60285, Jawa Timur, Indonesia</span>
                </div>
            </div>
            <!-- <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-v-10">
                    <a href="https://www.facebook.com/smartmaxco" class="small-social" target="_blank">
                        <i class="fa fa-facebook pix-slight-white"></i>
                    </a>
                    <a href="https://www.instagram.com/smartmaxco" class="small-social" target="_blank">
                        <i class="fa fa-instagram big-icon-50 pix-slight-white"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UC69yjQ0hynhMXffy4K-XpWw" class="small-social" target="_blank">
                        <i class="fa fa-youtube big-icon-50 pix-slight-white"></i>
                    </a>
                </div>
            </div> -->
        </div>
        <div class="pix-content">
            <h5 class="pix-black-gray-dark"><span class="pix_edit_text"><strong>DOWNLOAD</strong></span></h5>
            <a href="https://apps.apple.com/id/app/smart-maxco/id1516790315" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png" class="lazyload" alt=""></a>
            <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.forex" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png" class="lazyload" alt=""></a>
        </div>
    </div>
</div>
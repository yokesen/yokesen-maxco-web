<div class="pix_section pix_nav_menu pix_scroll_header normal pix-padding-v-10" data-scroll-bg="#fff" data-main-bg="rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 pix-inner-col col-sm-8">
                <div class="pix-content">
                    <nav class="navbar navbar-default pix-no-margin-bottom pix-navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#pix-navbar-collapse" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button> -->
                                <a class="navbar-brand logo-img logo-img-a pix-adjust-height" href="{{url('/')}}" style="margin-top: 2px;">
                                    <img src="{{url('/')}}/web/images/web/logo_300.png" alt="" class="img-responsive pix-logo-img">
                                </a>
                            </div>
                            <div class="collapse navbar-collapse" id="pix-navbar-collapse">
                                <!-- <ul class="nav navbar-nav navbar-right media-middle pix-header-nav pix-adjust-height" id="pix-header-nav" style="margin-top: 7px;">
                                        <li class="dropdown"><a href="{{url('/')}}" class="pix-gray">FEATURES</a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="{{url('/')}}" class="dropdown-toggle pix-gray" data-toggle="dropdown">
                                                DROPDOWN
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-left">
                                                <li><a href="{{url('/')}}" class="null" data-toggle="null">Menu Builder</a>
                                                </li>
                                                <li><a href="{{url('/')}}" class="null" data-toggle="null">Custom Link</a>
                                                </li>
                                                <li><a href="{{url('/')}}" class="null" data-toggle="null">Integrations</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="{{url('/')}}" class="pix-gray">CONTACT</a></li>
                                    </ul> -->
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2 col-xs-12 pix-inner-col col-sm-2">
                <!-- <div class="pix-content pix-adjust-height text-center" style="margin-top: 4px;">
                        <div class="pix-header-item pix-padding-v-10">
                            <a href="{{url('/')}}" class="pix-link-icon">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="{{url('/')}}" class="pix-link-icon pix-padding-h-10">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                    </div> -->
            </div>
        </div>
    </div>
</div>
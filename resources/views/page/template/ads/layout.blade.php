<!DOCTYPE html>
<!-- saved from url=(0043)https://pixfort.com/item/megapack/marketing -->
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NHQE658KYS"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-NHQE658KYS');
    </script>
    @yield('gtag-event')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta property="og:url" content="{{URL::current()}}" />
    <!-- <meta property="og:type"               content="article" /> -->
    <meta property="og:title" content="@yield('og-title')" />
    <meta property="og:image" content="@yield('og-image')" />
    <meta property="og:description" content="@yield('og-description')">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/favicon.ico">
    <!-- CSS dependencies -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/ads/css/bootstrap.css">
    <link href="{{url('/')}}/web/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/ads/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/ads/css/pix_style.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/ads/css/main.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/ads/css/font-style.css">

    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="https://pixfort.com/items/1/css/ie-fix.css" />
    <![endif]-->

    <link href="{{url('/')}}/ads/css/animations.min.css" rel="stylesheet" type="text/css" media="all">
    <title>Maxco Futures | Prestigious Global Brokerage House</title>
    <style>
        .video-container {
            position: relative;
            padding-bottom: 50%;
            /* padding-bottom: 56.25%; */
            padding-top: 30px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .bg-img {
            /* The image used */
            background-image: url("{{url('/')}}/web/images/campign/getsmartmaxcoapps.png");

            /* Control the height of the image */
            min-height: 530px;
            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            /* position: relative; */
        }

        .form-header {
            position: absolute;
            left: 91px;
            bottom: 6px;
        }

        .container-form {
            margin: 20px;
            width: 90%;
            padding: 16px;
            border-radius: 5px;
            background-color: #017dc7;
        }

        .title-fitur {
            text-align: center;
            min-height: 64px;
        }

        .content-fitur {
            min-height: 100px;
        }

        .content-fly:hover {
            opacity: 1;
            border: 2px solid #e1e0e0;
            /* box-shadow: 0 1px 5px rgba(0,0,0,0.1), 0 1px 3px rgba(0,0,0,0.2); */
            box-shadow: 5px 10px rgba(0, 0, 0, 0.1);
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @yield('csslist')
    @yield('cssonpage')
    @yield('zendesk')
    @yield('facebookPixel')
</head>

<body>
    @yield('fbtrack')
    @yield('googletagmanagerbody')

    @include('page.template.ads.navbar')

    @yield('content')

    @include('page.template.ads.footer')

    <!-- Javascript -->
    <script src="{{url('/')}}/ads/js/jquery-1.11.2.js"></script>
    <script src="{{url('/')}}/ads/js/jquery-ui.js"></script>
    <script src="{{url('/')}}/web/js/jquery.validate.min.js"></script>


    <script src="{{url('/')}}/ads/js/bootstrap.js"></script>
    <script src="{{url('/')}}/ads/js/velocity.min.js"></script>
    <script src="{{url('/')}}/ads/js/velocity.ui.min.js"></script>
    <script src="{{url('/')}}/ads/js/appear.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/ads/js/animations.min.js" type="text/javascript"></script>
    <!--<script src="-->
    <!--items/1/js/scroll.js" type="text/javascript"></script>-->
    <script src="{{url('/')}}/ads/js/plugins.js"></script>
    <script src="{{url('/')}}/ads/js/jquery.fancybox.min.js"></script>
    <script src="{{url('/')}}/ads/js/custom-demo.js"></script>
    <!-- <script src="{{url('/')}}/js/sweetalert.min.js"></script> -->

    <div class="pix_scroll_menu pix_menu_hidden" style="padding-top: 10px; padding-bottom: 10px; background: rgb(255, 255, 255); top: 0px; visibility: visible;">
        <div class="container">
            <div class="row">
                <div class="pix-inner-col col-md-8">
                    <div class="pix-content">
                        <nav class="navbar navbar-default pix-no-margin-bottom pix-navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <a class="navbar-brand logo-img pix-adjust-scroll" href="{{url('/')}}" style="margin-top: 7px;">
                                        <img src="{{url('/')}}/web/images/web/logo_300.png" alt="" class="img-responsive scroll_logo_img">
                                    </a></div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @yield('jsonpage')
    @include('sweet::alert')
</body>

</html>

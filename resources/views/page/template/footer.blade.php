<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <h5 style="color:#fff">{{trans('page.menu-quick-inks')}}</h5>
        <div class="footerMenu">
          <a href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}">{{trans('page.menu-open-account')}}</a>
          <a href="{{url('/')}}/{{App::getLocale()}}/trading-account/account-type">{{trans('page.menu-products')}}</a>
          <a href="{{url('/')}}/{{App::getLocale()}}/expert-area/how-to-analyze-market">{{trans('page.menu-expert-area')}}</a>
          <a href="{{url('/')}}/{{App::getLocale()}}/trading-rules">Peraturan Perdagangan</a>
        </div>
        <!-- <a href="https://www.facebook.com/smartmaxco" class="small-social" target="_blank">
                            <i class="fa fa-facebook color-fb"></i>
                        </a>
                        <a href="https://www.instagram.com/smartmaxco" class="small-social" target="_blank">
                            <i class="fa fa-instagram big-icon-50 color-ig"></i>
                        </a>
                        <a href="https://www.youtube.com/channel/UC69yjQ0hynhMXffy4K-XpWw" class="small-social" target="_blank">
                            <i class="fa fa-youtube big-icon-50 color-yt"></i>
                        </a> -->
      </div>
      <div class="col-md-2">
        <h5 style="color:#fff">{{trans('page.menu-about-us')}}</h5>
        <div class="footerMenu">
          <a href="{{url('/')}}/{{App::getLocale()}}/about-us/company-profile">{{trans('page.menu-best-broker')}}</a>
          <a href="{{url('/')}}/{{App::getLocale()}}/about-us/office-location">{{trans('page.menu-contact-us')}}</a>
        </div>
      </div>
      <div class="col-md-4">
        <h5 style="color:#fff">{{trans('page.menu-legality')}}</h5>
        <div class="footerMenu">
          <span>{{trans('page.achievements-detail-1')}}</span>
          <span>{{trans('page.achievements-detail-2')}}</span>
          <span>{{trans('page.achievements-detail-3')}}</span>
          <span>{{trans('page.achievements-detail-5')}}</span>
        </div>
      </div>
      <div class="col-md-4 text-justify">
        <h5 style="color:#fff">Jakarta</h5>
        <p>Panin Bank Centre - Ground Floor, Jl. Jend. Sudirman Kav-1 Senayan Jakarta Selatan 10270, DKI Jakarta, Indonesia</p>
        <h5 style="color:#fff">Surabaya</h5>
        <p>Jln. Dharmahusada No.60A, Mojo, Gubeng</p>
        <p>Surabaya 60285, Jawa Timur, Indonesia </p>
      </div>
    </div>
    {{-- <h5 style="color:#fff">Download</h5>
          <a href="https://apps.apple.com/id/app/smart-maxco/id1516790315" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png" class="lazyload" alt=""></a>
          <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.forex" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png" class="lazyload" alt=""></a> --}}
  </div>
</footer>





<!-- RIGHTS -->
<div class="rights">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <p> © All Rights Reserved <span class="primary-color">Maxco </span></p>
      </div>
      <div class="col-md-6 text-right">
        <a href="{{url('/')}}/{{App::getLocale()}}/privacy-policy">{{trans('page.home-privacy-policy')}}</a>
        <!-- <a href="{{url('/')}}/{{App::getLocale()}}/terms-conditions">Terms & Conditions</a> -->
      </div>
    </div>
  </div>
</div>
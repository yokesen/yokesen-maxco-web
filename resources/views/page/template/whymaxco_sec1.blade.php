<!-- LIST LEFT -->
<div class="col-md-4 no-padding">
    <ul class="text-right">
        <li>
            <div class="media">
                <div class="media-body">
                    <h6>{{trans('page.home-maxco-mobile-connect')}}</h6>
                    <p>{{trans('page.home-maxco-mobile-connect-desc')}}</p>
                </div>
                <div class="media-right">
                    <div class="icon"> <i class="fa fa-retweet"></i> </div>
                </div>
            </div>
        </li>
        <li>
            <div class="media">
                <div class="media-body">
                    <h6>{{trans('page.home-maxco-mobile-account')}}</h6>
                    <p>{{trans('page.home-maxco-mobile-account-desc')}}</p>
                </div>
                <div class="media-right">
                    <div class="icon"> <i class="fa fa-user-plus"></i> </div>
                </div>
            </div>
        </li>
        <li>
            <div class="media">
                <div class="media-body">
                    <h6>{{trans('page.home-maxco-mobile-deposit')}}</h6>
                    <p>{{trans('page.home-maxco-mobile-deposit-desc')}}</p>
                </div>
                <div class="media-right">
                    <div class="icon"> <i class="fa fa-money"></i> </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<!-- LIST IMAGE -->
<div class="col-md-4 text-center no-padding"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/home-slide/iphone2.png" height="300px" alt="">
<!-- <a href="https://release.stage.bull-b.com/rh" class="btn btn-1 margin-top-10" style="min-width: 180px;margin-bottom:5px;" target="_blank">{{trans('page.menu-download')}}<i class="fa fa-download fa-lg"></i></a> -->

</div>

<!-- LIST ICON RIGHT -->
<div class="col-md-4 no-padding">
    <ul>
        <li>
            <div class="media">
                <div class="media-left">
                    <div class="icon"> <i class="fa fa-bar-chart"></i> </div>
                </div>
                <div class="media-body">
                    <h6>{{trans('page.home-maxco-mobile-market')}}</h6>
                    <p>{{trans('page.home-maxco-mobile-market-desc')}}</p>
                </div>
            </div>
        </li>
        <li>
            <div class="media">
                <div class="media-left">
                    <div class="icon"> <i class="fa fa-mobile-phone"></i> </div>
                </div>
                <div class="media-body">
                    <h6>{{trans('page.home-maxco-mobile-trading')}}</h6>
                    <p>{{trans('page.home-maxco-mobile-trading-desc')}}</p>
                </div>
            </div>
        </li>
        <li>
            <div class="media">
                <div class="media-left">
                    <div class="icon"> <i class="fa fa-binoculars"></i> </div>
                </div>
                <div class="media-body">
                    <h6>{{trans('page.home-maxco-mobile-news')}}</h6>
                    <p>{{trans('page.home-maxco-mobile-news-desc')}}</p>
                </div>
            </div>
        </li>
    </ul>
</div>
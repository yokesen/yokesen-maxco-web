  @if (session('isMobile'))
    @if (Route::currentRouteName() != 'index')
      @include('page.template.iklan-atas')
    @endif
  @endif
<header>
  <!-- Top bar -->
  <div class="top-bar">
    <div class="top-info">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12 personal-info margin-bottom-20">
            <ul class="personal-info" style="margin-left: 16px;">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $currentLanguage->name }} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  @foreach ($altLocalizedUrls as $alt)
                    <li><a href="{{ $alt['url'] }}" hreflang="{{ $alt['locale'] }}">{{ $alt['name'] }}</a></li>
                  @endforeach
                </ul>
              </li>
              <li>
                @include('page.template.sosmedlink')
              </li>
            </ul>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="right-sec" style="min-width: 286px;">
              <div class="col-md-6 col-xs-6">
                <a class="btn br-0 bg-paninred" href="{{env('CABINET_URL')}}login?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" style="width:100%;">{{trans('page.menu-client-area')}}</a>
              </div>
              <div class="col-md-6 col-xs-6">
                <a class="btn br-0 bg-paninblue" href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" style="width:100%;">{{trans('page.menu-open-account')}}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Navigation -->
  <nav class="navbar">
    <div class="sticky">
      <div class="container">

        <!-- LOGO -->
        <div class="logo" style="top: 8px;">
          <a href="{{route('index')}}">
            <img  class="img-responsive lazyload" data-src="{{url('/')}}/web/images/web/logo_300.png" alt="" style="<?php if(session('isMobile')){echo 'height:46px;';}else{echo 'height:49px;';}?>">
          </a>
        </div>

        <!-- Nav -->
        <ul class="nav ownmenu">
          <li class="<?php if(isset($navMenu) && $navMenu ==="index" ){echo "active";}else{ echo "";} ?>"> <a href="{{route('index')}}">{{trans('page.menu-home')}} </a> </li>
          <li> <a class="<?php if(isset($navMenu) && $navMenu ==="about-us" ){echo "link-active";}else{ echo "";} ?>">{{trans('page.menu-about-us')}} </a>
            <ul class="dropdown">
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/about-us/company-profile">{{trans('page.menu-company-profile')}}</a> </li>
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/about-us/legality">{{trans('page.menu-legality')}}</a> </li>
              <!-- <li> <a href="/about-us/management-structure">{{trans('page.menu-management-structure')}}</a> </li>
              <li> <a href="{{url('/')}}/about-us/broker-representative">{{trans('page.menu-broker-representatives')}}</a> </li> -->
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/about-us/office-location">{{trans('page.menu-our-location')}}</a> </li>
            </ul>
          </li>
          <li> <a class="<?php if(isset($navMenu) && $navMenu ==="futures" ){echo "link-active";}else{ echo "";} ?>">{{trans('page.menu-about-futures')}}</a>
            <ul class="dropdown">
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/futures/trading">{{trans('page.menu-futures-trading')}}</a> </li>
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/futures/rules">{{trans('page.menu-regulation')}}</a> </li>
            </ul>
          </li>
          <li> <a class="<?php if(isset($navMenu) && $navMenu ==="market" ){echo "link-active";}else{ echo "";} ?>">{{trans('page.menu-trading')}}</a>
            <ul class="dropdown">
              <li> <a>{{trans('page.menu-products')}}</a>
                <ul class="dropdown">
                  <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/market/forex">{{trans('page.menu-forex-trading')}}</a> </li>
                  <!-- <li> <a href="/market/gold">{{trans('page.menu-gold-trading')}}</a> </li>
                  <li> <a href="/market/silver">{{trans('page.menu-silver-trading')}}</a> </li>
                  <li> <a href="/market/oil">{{trans('page.menu-oil-trading')}}</a> </li> -->
                  <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/market/index">{{trans('page.menu-index-trading')}}</a> </li>
                  </ul>
              </li>
              <li> <a>{{trans('page.menu-trading-account')}}</a>
                <ul class="dropdown">
                  <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/trading-account/account-type">{{trans('page.menu-account-type')}}</a> </li>
                  <!-- <li> <a href="#">{{trans('page.menu-trading-costs')}}</a> </li>
                  <li> <a href="#">{{trans('page.menu-margin-and-leverage')}}</a> </li>
                   -->
                                    <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/trading-account/trading-hours">{{trans('page.menu-market-trading-hours')}}</a> </li>

                </ul>
              </li>
            </ul>
          </li>
          <li> <a class="<?php if(isset($navMenu) && $navMenu ==="platform" ){echo "link-active";}else{ echo "";} ?>">{{trans('page.menu-transaction-platform')}}</a>
            <ul class="dropdown">
              {{-- <li> 
                <a href="{{url('/')}}/ --}}
                <?php 
                // echo App::getLocale();
                ?>
                {{-- /platform/maxco-mobile">{{trans('page.menu-maxco-mobile')}}
                </a> 
              </li> --}}
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/platform/download">{{trans('page.menu-metatrader')}}</a>  </li>
            </ul>
          </li>

          <li>
            @if(session()->has('user.token'))
              <a class="" href="{{route('profile-trading-tool')}}">Tools</a>
            @else
              <a class="" href="{{route('login')}}">{{trans('page.menu-expert-area')}}</a>
            @endif

            <!-- <ul class="dropdown">
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/expert-area/how-to-analyze-market">{{trans('page.menu-analyze-market')}}</a> </li>
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/expert-area/how-to-read-candlestick">{{trans('page.menu-read-candlestick')}}</a> </li>
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/expert-area/technical-analysis">{{trans('page.menu-technical-analysis')}}</a> </li>
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/expert-area/fundamental-analysis">{{trans('page.menu-fundamental-analysis')}}</a> </li>
              <li> <a href="{{url('/')}}/<?php echo App::getLocale();?>/expert-area/news">News</a> </li>

            </ul> -->
          </li>
        </ul>
        <!-- Search -->
        <!-- <div class="search-icon">
          <a href="#." style="<?php
          //  if(session('isMobile')){echo 'top:0px;';}
           ?>
          ">
            <i class="fa fa-search"></i>
          </a>
          <form>
            <input class="form-control" type="search" placeholder="Type Here">
            <button type="submit"><i class="fa fa-search"></i></button>
          </form>
        </div> -->
      </div>
    </div>
  </nav>
</header>

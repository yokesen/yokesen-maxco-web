<section class="features padding-top-70 padding-bottom-70">
  <!-- <section class="features light-gray-bg padding-top-70 padding-bottom-70"> -->
  <div class="container">
    <!-- Heading -->
    {{-- <div class="heading text-center">
      <h3>SMART MAXCO APPS</h3>
    </div> --}}

    <!-- Features -->
    <div class="list-style-featured">
      <div class="row no-margin">
        {{-- @include('page.template.whymaxco_sec1') --}}
        {{-- <div class="col-md-12 text-center">
          <br>
          <br>
          @include('page.template.whymaxco_sec2')
        </div> --}}
        @include('page.template.whymaxco_sec3')
      </div>
    </div>
  </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12">
                <h6>{{trans('page.home-disclaimer')}}</h6>
                <p class="text-justify">{{trans('page.home-disclaimer-desc')}}</p>
                <h6>{{trans('page.home-privacy-policy')}}</h6>
                <p class="text-justify">{{trans('page.home-privacy-policy-desc')}}</p>
                <h6>{{trans('page.home-risk-warning')}}</h6>
                <p class="text-justify">{{trans('page.home-risk-warning-desc')}}</p>
            </div>
        </div>
    </div>
    <br>
</section>
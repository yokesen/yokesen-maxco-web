@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h4>Daftar Wakil Pialang</h4>
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Daftar Wakil Pialang</li>
        </ol>
      </div>
    </div>
  </section>

  <!-- Content -->
  <div id="content">

    <!-- WHO WE ARE -->
    <section class="team team-wrap padding-top-70 padding-bottom-70">
      <div class="container">

        <!-- Work Filter -->
        <!-- <ul class="filter team-filter">
          <li class="tab-title filter-item"><a class="active" href="#." data-filter="*">All</a></li>
          <li class="filter-item"><a href="#." data-filter=".gd">graphic designers</a></li>
          <li class="filter-item"><a href="#." data-filter=".html">html coders</a></li>
          <li class="filter-item"><a href="#." data-filter=".market">marketing</a></li>
        </ul> -->
        <div class="">
          <ul class="row items">

            <!-- Member -->
            <li class="col-md-3 item market">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/rudi_anto.jpg" alt="">
                <h5>Rudi Anto </h5>
                <span>Compliance Director</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>

            <!-- Member -->
            <li class="col-md-3 item gd market">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/ferucha.jpg" alt="">
                <h5>Ferucha</h5>
                <span>Account Executive</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>

            <!-- Member -->
            <li class="col-md-3 item gd ">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/prayoga_subagja.jpg" alt="">
                <h5>Prayoga Subagja </h5>
                <span>Consultant</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>

            <!-- Member -->
            <li class="col-md-3 item gd html">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/martha_barker.jpg" alt="">
                <h5>Martha Barker </h5>
                <span>Marketing</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>


          </ul>
        </div>
      </div>
    </section>
  </div>
@endsection

@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>
.close{
  /* display: contents; */
}
/* .close::before {
    content: '\f2d7';
    font-family: "Ionicons";
    color: #fff;
    font-size: 30px;
} */
</style>
@endsection

@section('content')

<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/platform/metatrader_banner.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
  </section>

  <!-- Content -->
   <div id="content">
     <section style="
     background: #f5f5f5;
     padding: 2px;
     ">
       <div class="container">
         <ol class="breadcrumb">
           <li><i class="fa fa-home"></i> <a href="/">{{trans('page.menu-home')}}</a></li>
           <li class="active">{{trans('page.menu-download')}}</li>
         </ol>
       </div>
     </section>
     <div class="top-detail cbp-lightbox">
         <a style="display:none;" href="{{url('/')}}/web/images/webpage/aboutus/perijinan/izin_usaha_pialang_berjangka.png"
             class="cbp-lightbox" data-title="">
             <i class="icon-magnifier"></i></a>
     </div>

     <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h6 class="modal-title">Cara Instal</h6>
                </div>
                <div class="modal-body">
                    <!-- <iframe id="cartoonVideo"
                  src="//www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen ></iframe> -->
                  <iframe style="width:100%;" src="https://www.youtube.com/embed/1rbME9ItbXY?list=PLh35_KqiBmXPIz9mcmBNsjnVEKdePQw_e" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

     <!-- Revenues -->
     <section class="revenues padding-top-70 padding-bottom-70">
       <div class="container">

         <div class="row">
           <!-- Stories -->
           <div class="col-md-offset-1 col-md-10">
             <div class=" text-center">
               <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/mt4-logo-350x200.png" alt="">
             </div>
           </div>

           <div class="col-md-4 text-center" style="
                      margin: 0;
                      /* position: absolute; */
                      /* top: 50%; */
                      left: 50%;
                      transform: translate(-50%);
                  ">
             <div class="list-style-featured media">
                <div class="media-left">
                  <div class="icon" style="padding: 8px;"> <i class="fa fa-laptop" style="font-size: 44px;"></i> </div>
                </div>
                <div class="media-body" >
                  <h4 class="text-left">{{trans('page.download-dekstop-version')}}</h4>
                </div>
              </div>
           </div>

           <div class="col-md-12 margin-top-15">
             <div class="plan">
                <ul class="row">
                  <!-- Basic -->
                  <li class="col-md-3" style="
                      margin: 0;
                      /* position: absolute; */
                      /* top: 50%; */
                      left: 50%;
                      transform: translate(-50%);
                  ">
                    <article>
                      <div class="price" style="background: #2f4a57;padding:15px;">
                        <i class="fa fa-windows" style="font-size: 100px;"></i>
                        <h6>Windows</h6>
                        <!-- <span class="currency">$</span>19<span class="period">/monthy</span> -->
                      </div>
                      <!-- <p class="light"><span>250MB</span> Memory</p>
                      <p><span>10</span> Projects</p>
                      <p class="light"><span>10GB </span>amount of space</p> -->
                      <div class="light" style="padding:10px;background:#f9f9f9">
                        <p style="text-align: justify !important;line-height: 25px;    min-height: 130px;">
                          {{trans('page.download-dekstop-version-windows-detail')}}
                        </p>
                     </div>
                     <div style="display: inline-grid;">
                       <a href="http://files.metaquotes.net/pt.maxco.futures/mt4/maxcofutures4setup.exe" class="btn btn-1 margin-top-10" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download fa-lg"></i></a>
                       <!-- <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play-circle-o fa-lg"></i></a> -->
                     </div>

                    </article>
                  </li>
                </ul>
              </div>
           </div>

           <div class="col-md-offset-4 col-md-4 text-center">
             <div class="list-style-featured media">
                <div class="media-left">
                  <div class="icon" style="padding: 8px;"> <i class="fa fa-mobile-phone" style="font-size: 44px;"></i> </div>
                </div>
                <div class="media-body" style="padding: 16px;">
                  <h4 class="text-left">{{trans('page.download-mobile-version')}}</h4>
                </div>
              </div>
           </div>

           <div class="col-md-12 margin-top-15">
             <div class="plan">
                <ul class="row">
                  <!-- starter -->
                  <li class="col-md-offset-3 col-md-3">
                    <article>
                      <div class="price" style="background: #2f4a57;padding:15px;">
                        <i class="fa fa-apple" style="font-size: 100px;"></i>
                        <h6>Iphone</h6>
                        <!-- <span class="currency">$</span>19<span class="period">/monthy</span> -->
                      </div>
                      <!-- <p class="light"><span>250MB</span> Memory</p>
                      <p><span>10</span> Projects</p>
                      <p class="light"><span>10GB </span>amount of space</p> -->
                      <div class="light" style="padding:10px;background:#f9f9f9">
                        <p style="text-align: justify !important;line-height: 25px;    min-height: 130px;">
                          {{trans('page.download-mobile-version-iphone-detail')}}
                       </p>
                     </div>
                     <div style="display: inline-grid;">
                       <a href="http://itunes.apple.com/en/app/metatrader-4/id496212596?mt=8" class="btn btn-1 margin-top-10" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download fa-lg"></i></a>
                       <!-- <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play-circle-o fa-lg"></i></a> -->
                     </div>

                    </article>
                  </li>

                  <!-- Basic -->
                  <li class="col-md-3">
                    <article>
                      <div class="price" style="background: #2f4a57;padding:15px;">
                        <i class="fa fa-android" style="font-size: 100px;"></i>
                        <h6>Android</h6>
                        <!-- <span class="currency">$</span>19<span class="period">/monthy</span> -->
                      </div>
                      <!-- <p class="light"><span>250MB</span> Memory</p>
                      <p><span>10</span> Projects</p>
                      <p class="light"><span>10GB </span>amount of space</p> -->
                      <div class="light" style="padding:10px;background:#f9f9f9">
                        <p style="text-align: justify !important;line-height: 25px;min-height: 130px;">
                          {{trans('page.download-mobile-version-android-detail')}}
                       </p>
                     </div>
                     <div style="display: inline-grid;">
                      <a href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader4" class="btn btn-1 margin-top-10" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download fa-lg"></i></a>
                       <!-- <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play-circle-o fa-lg"></i></a> -->
                     </div>

                    </article>
                  </li>
                </ul>
              </div>
           </div>

         </div>
       </div>
     </section>
   </div>

 <!-- always on -->
 @include('page.template.always_on')

@endsection

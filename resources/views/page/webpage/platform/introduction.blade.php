@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/bg/sub-bnr-bg-2.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>Introduction</h1>
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Introduction</li>
        </ol>
      </div>
    </div>
  </section>

  <!-- Content -->
  <div id="content">

    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-1 col-md-10">
            <div class="text-center">
              <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/mt4-logo-350x200.png" alt="">
              <br>
            </div>
          </div>

          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Pendaftaran</h3>
              </div>
          </div>

          <div class="col-md-offset-1 col-md-10">
            <p class="text-justify">
              Akun demo adalah cara terbaik bagi pendatang baru untuk menjelajahi perdagangan. Fungsionalitas akun demo mirip dengan akun riil dengan pengecualian Anda berdagang dengan uang virtual. Bekerja pada akun demo memberi Anda pengalaman perdagangan Forex menggunakan semua fitur perdagangan yang disediakan oleh platform MetaTrader 4.
            </p>
            <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/demo_account.jpg" alt="">
            <p class="text-justify">
              Anda dapat berdagang secara real time dan belajar bagaimana menganalisis pasar menggunakan indikator teknis tanpa mempertaruhkan uang Anda. Menyalin perdagangan dengan langganan Sinyal dan pembelian Expert Advisors dari Pasar MetaTrader juga tersedia untuk akun demo.
              <br>
              Akun demo dapat dibuka di platform desktop, serta di aplikasi seluler MetaTrader 4:
            </p>
            <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/metatrader_register.jpg" alt="">
            <p class="text-justify">
              Membuat akun demo adalah cara terbaik untuk mulai menguasai platform MetaTrader 4 dan berdagang di pasar mata uang. Mulai sekarang!
            </p>
            <br>
          </div>
          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Server</h3>
              </div>
          </div>
          <div class="col-md-offset-1 col-md-10">
            <p class="text-justify">Hosting virtual menyediakan operasi sepanjang waktu dari terminal klien (VPS Forex). Ini sangat nyaman jika Anda menggunakan robot perdagangan atau menyalin penawaran menggunakan sinyal perdagangan. Layanan hosting virtual memungkinkan perdagangan Anda dieksekusi kapan saja dengan penundaan minimum!</p>
            <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/metatrader_4_virtual_hosting_en.jpg" alt="">

            <p class="text-justify">
              menemukan server virtual langsung dari terminal MetaTrader 4 adalah cara optimal untuk memastikan operasi robot perdagangan Anda dan langganan sinyal yang tidak terputus. Pada dasarnya, ini adalah analog dari layanan seperti halnya VPS, meskipun ini adalah layanan yang unggul dan lebih cocok untuk menangani kebutuhan dan tantangan yang dihadapi pedagang. Server dapat disewa langsung dari MetaTrader 4. Anda hanya perlu beberapa klik mouse untuk Penasihat Pakar, indikator, skrip bersama dengan langganan Sinyal dan pengaturan lainnya untuk ditransfer ke server virtual.
              <br>
              Keuntungan utama dari virtual hosting bawaan termasuk latensi jaringan minimum ke server broker, kemudahan penggunaan dan harga yang wajar. Ini adalah solusi optimal untuk pedagang yang ingin menggunakan robot dan sinyal 24 jam sehari.
            </p>
          </div>


            <div class="col-md-12">
                <div class="heading text-center">
                    <h3>Login</h3>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10">
              <p class="text-justify">Bagi trader forex yang sering keluar rumah / kantor, yang tidak memungkinan membuka komputer, jangan khawatir, kini Anda bisa menggunakan MetaTrader di smartphone Anda, Apilkasi ini sangat ringan dan simpel jadi Anda dapat membuka MT4 dimanapun, kapanpun tanpa harus menggunakan komputer berikut adalah Cara Login MetaTrader Android.</p>
              <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50 text-justify">
                  <li>
                      <p>Tekan menu yang paling bawah “LOGIN TO AN EXISTING ACCOUNT” untuk masukan akun REAL</p>
                      <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/login-step-1.jpg" alt="">
                  </li>
                  <li>
                      <p>Pilih server trading sesuai dengan akun trading Anda, jika lupa bisa di cek di email.</p>
                      <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/login-step-2.jpg" alt="">
                  </li>
                  <li>
                      <p>Pada bagian ini silahkan masukan nomor akun trading Anda (login) dan password trading Anda, disini diperlukan kehati-hatian karena besar kecilnya huruf sangat diperhatikan agar bisa berhasil login.</p>
                      <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/login-step-3.jpg" alt="">
                  </li>
                  <li>
                      <p>Jika akun sudah berhasil login maka akan muncul jumlah balance / saldo akun trading Anda, dan perbedaa antara akun REAL dan DEMO juga bisa di perhatikan di sudut kanan atas akun trading.</p>
                      <!-- <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/login-step-4.jpg" alt=""> -->
                  </li>
                  <li>
                      <p>Anda dapat melihat menu lengkap menu aplikasidengan tekan menu di pojok kiri atas</p>
                        <!-- <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/login-step-5.jpg" alt=""> -->
                  </li>
              </ul>
            </div>


            <div class="col-md-12">
                <div class="heading text-center">
                    <h3>Ganti Password</h3>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5">
              <p class="text-justify"> Dari platform seluler MetaTrader 5 untuk Android, pengguna dapat mengubah kata sandi akun perdagangan. Buka bagian "Akun" dan ketuk tombol Menu di sudut kanan atas jendela atau tekan tombol "Menu" di perangkat Anda, lalu ketuk "Ubah Kata Sandi".</p>
              <p class="text-justify">Semua perubahan kata sandi berlaku untuk akun saat ini (akun yang saat ini terhubung dengan Anda).</p>
              <br>
                <p>Data berikut harus ditentukan di sini:</p>
                <ul class="dt-sc-fancy-list red arrow padding-left-50 text-justify">
                    <li> Kata sandi utama saat ini - tentukan kata sandi utama saat ini untuk akun</li>
                    <li> Ubah kata sandi master / Ubah kata sandi investor - pilih kata sandi yang ingin Anda ubah - master atau investor.</li>
                    <li>New Password – specify the new password for your account;</li>
                    <li>Password confirmation – re-enter the new password to avoid typing errors;</li>
                    <li>Ketuk "Next" untuk mengubah kata sandi.</li>
                </ul>
                <br>
              <p class="text-justify">                <b  >Pastikan untuk memasukkan kata sandi yang rumit, mengandung tidak kurang dari 5 karakter dan setidaknya dua dari tiga jenis karakter - huruf kecil, huruf besar dan angka </b>
</p>

            </div>

            <div class="col-md-5">
              <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/change-password-step-1.jpg" alt="">

            </div>


            <div class="col-md-12">
                <div class="heading text-center">
                    <h3>Pengenalan Menu</h3>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10">
                <p class="text-justify">Menu MT4 ini dapat dilihat pada bagian bawah dan samping. Jika Anda geser layar ke kanan, maka menu akan muncul seperti pada gambar berikut:<p>
                  <div class="text-center">
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu.jpg" alt="">
                  </div>
                <p class="text-justify">Beberapa menu juga terdapat pada bagian bawah berupa tombol-tombol navigasi yang memudahkan Anda melihat menu utama lainnya. Berikut adalah penjelasan singkat tombol menu pada bagian bawah:<p>
                  <br>
                  <br>
                <h5>1. Quotes</h5>
                <p class="text-justify">Menu ini menampilkan pairing secara keseluruhan. Anda dapat merubahnya menjadi advance view mode begitu juga sebaliknya dari advance ke simple view mode. Advance mode menampilkan kuotasi harga lengkap dengan spread, low, high dan juga waktu.
                  Sedangkan simple mode hanya menampakkan kuotasi harga  lima digit biasa saja.</p>
                  <div class="text-center">
                  <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-1.jpg" alt="">
                </div>
                <br>
                <br>
                  <h5>2. Chart</h5>
                  <p class="text-justify">Tampilan menu chart memiliki beberapa option tersendiri yang dapat anda lihat di bagian atas. Sebagai berikut:</p>
                  <div class="row">
                  <div class="col-md-4">
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-2.jpg" alt="">
                  </div>
                  <div class="col-md-8">
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-sub-2.jpg" alt="">
                    <br>
                    <br>
                    <ul class="dt-sc-fancy-list red arrow padding-left-50 text-justify">
                        <li> <b>Crosshair</b> untuk memunculkan fungsi crosshair pada chart. Crosshair berfungsi sebagai alat bantu analisa market.</li>
                        <li> <b>Indicators</b> Untuk melihat daftar indicators, klik logo ini. Meskipun tidak semua indikator MT4 yang ada di PC ada pada aplikasi ini, namun Metatrader 4 memiliki indikator yang cukup lengkap dan meliputi juga indikator-indikator paling populer seperti MA, MACD, RSI, dan OBV. Namun, Anda tidak bisa memasukkan custom indicators ke dalam versi android ini.</li>
                        <li> <b>Pairs</b> ini digunakan untuk merubah pairs secara langsung. Terdapat 6 pairs yang tersedia dalam aplikasi ini. Anda dapat menambahkan pairs atau symbols hingga 24 symbols yang disediakan oleh MT4</li>
                        <li> <b>Timeframes</b> Terdapat 9 timeframes, yaitu M1, M5, M15, M30, H1, H4, D1, W1 dan MN. Cara merubah timeframes sangatlah mudah cukup klik icon ini, kemudian pilihlah timframe sesuai dengan keinginan Anda, maka tampilan chart akan berubah sesuai dengan timframes yang anda pilih.</li>
                        <li> <b>Order</b> untuk melakukan order.</li>
                    </ul>
                  </div>
                </div>
                <br>
                <br>
                  <h5>3. Trade</h5>
                  <p class="text-justify">Dalam menu ini anda dapat melihat balance, equity dan free margin yang Anda miliki. Tampilan dapat Anda ubah sesuai dengan urutan yang diingikan. Dan terdapat pilihan order, sehingga Anda dapat langsung melakukan order tanpa harus berpindah menu</p>
                  <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-3.jpg" alt="">
                  <br>
                  <br>

                <h5>4. History</h5>
                <p class="text-justify">Anda dapat merubah tampilannya history sesuai dengan yang Anda inginkan. Dengan klik menu yang ada di bagian kanan atas anda dapat menyesuaikan tampilan history sesuai dengan symbols, urutan, dan waktu.</p>

                <div class="row">
                <div class="col-md-4">
                  <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-4.jpg" alt="">
                </div>
                <div class="col-md-8">
                  <div class="col-md-4">
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-4-1.jpg" alt="">
                  </div>
                  <div class="col-md-4">
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-4-2.jpg" alt="">
                  </div>
                  <div class="col-md-4">
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/platform/pengenalan-menu-4-3.jpg" alt="">
                  </div>
                </div>
              </div>
              <br>
              <br>


                  <h5>5. News</h5>
                  <p class="text-justify">Sama seperti kolom news pada MT4 untuk desktop (PC). Menu News dalam MT4 Android ini juga untuk melihat berita berita terbaru dari server. Kolom ini memudahkan anda untuk terus update dengan berita berita terbaru dari perusahaan Metaquotes.</p>

            </div>

          </div>
        </div>
      </div>
    </section>
  </div>

<!-- always on -->
@include('page.template.always_on')

@endsection

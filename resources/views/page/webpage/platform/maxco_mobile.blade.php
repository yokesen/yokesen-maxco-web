@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>
    .video-container {
        position: relative;
        padding-bottom: 56.25%;
        padding-top: 30px;
        height: 0;
        overflow: hidden;
    }

    .video-container iframe,
    .video-container object,
    .video-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: auto;
        /* min-height: 400px; */
    }

    .title-fitur {
        text-align: center;
        min-height: 50px;
    }

    .content-fitur {
        text-align: justify;
        min-height: 100px;
    }

    .content-whymaxco {
        text-align: justify;
        height: 200px;
    }
</style>
@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/web/smartmaxcoapps_1920x1080.png) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;"
  data-stellar-background-ratio="0.5">

</section>

<!-- Content -->
<div id="content">
    <section style="
     background: #f5f5f5;
     padding: 2px;
     ">
        <div class="container">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
                <li class="active">Maxco Mobile</li>
            </ol>
        </div>
    </section>
    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <!-- Post -->
                    <article>
                        <div class="post-detail">
                            <div class="text-justify">
                                <h5 class="text-center">Tentang Maxco Futures </h5>
                                <p>Maxco Futures adalah perusahaan pialang yang memberikan fasilitas bagi para nasabah untuk mengeksekusi transaksi forex dan index. Demi kenyamanan dan kemudahan client dalam berinvestasi, Maxco Futures
                                    mempersembahkan Smart Maxco App, sebuah aplikasi Android all-in-one yang memberikan akses tanpa batas ke pasar dan dengan biaya transaksi yang rendah.
                                </p>
                                <p>
                                    Install Smart Maxco App sekarang dan dapatkan akses ke pasar dengan dukungan berita & Analisa dari tim konsultan kami, pembukaan rekening trading sesuai dengan peraturan Bappebti, serta setoran dan penarikan dana
                                    langsung dari genggaman tangan Anda.
                                </p>
                                <div class="text-center video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/X4mZ308hgW8?rel=0" frameborder=" 0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <h5 class="text-center">Fitur</h5>
                                <div class="list-style-featured">
                                    <div class="row no-margin">
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-retweet"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Always Connected </h6>
                                                    <p class="content-fitur">Selalu terhubung dengan akses pasar dan transaksi melalui Smart Maxco App.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-user-plus"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Pembukaan Akun Transaksi </h6>
                                                    <p class="content-fitur">Mulai dari pendaftaran profil, kelengkapan dokumen, agreement, sampai dengan persetujuan.</p>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-money"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Deposit & Withdrawal </h6>
                                                    <p class="content-fitur">Konfirmasi deposit dan permintaan withdrawal langsung di tangan anda kapanpun dan dimanapun.</p>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-bar-chart"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Pasar </h6>
                                                    <p class="content-fitur">Akses pantauan langsung ke harga pasar secara real time langsung di satu platform. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-bar-chart"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Trading </h6>
                                                    <p class="content-fitur">Kemudahan transaksi dalam satu platform, Open Posisi, Update TP/SL, Close posisi.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-binoculars"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Berita dan Analisa </h6>
                                                    <p class="content-fitur">Notifikasi berita dan analisis terbaru sebagai bahan pertimbangan anda dalam mengelola transaksi. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-12 no-padding">
                      <ul>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-retweet"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Always Connected </h6>
                              <p>Selalu terhubung dengan akses pasar dan transaksi melalui Smart Maxco App</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-user-plus"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Pembukaan Akun Transaksi </h6>
                              <p>Mulai dari pendaftaran profil, kelengkapan dokumen, agreement, hingga dengan persetujuan.</p>
                            </div>

                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-money"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Deposit & Withdrawal </h6>
                              <p>Konfirmasi deposit dan permintaan withdrawal langsung di tangan anda kapanpun dan dimanapun.</p>
                            </div>

                          </div>
                        </li>
                      </ul>
                      <ul>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-bar-chart"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Pasar </h6>
                              <p>Akses pantauan langsung ke harga pasar secara real time di satu platform.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-mobile-phone"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Trading </h6>
                              <p>Kemudahan transaksi dalam satu platform, untuk Open Posisi, Update TP/SL, & Close posisi.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-binoculars"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Berita dan Analisa </h6>
                              <p>Notifikasi berita dan analisa terbaru sebagai bahan pertimbangan anda dalam mengelola transaksi.</p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div> -->
                                    </div>
                                </div>
                                <h5 class="text-center">Why Maxco</h5>
                                <div class="list-style-featured">
                                    <div class="row no-margin">
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-user"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Customer First </h6>
                                                    <p class="content-whymaxco">Fokus kepada kebutuhan nasabah secara detail untuk memberikan pengalaman layanan nasabah kelas dunia. Kami disini untuk membantu kesuksesan nasabah.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-money"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Komisi rendah </h6>
                                                    <p class="content-whymaxco">Di Maxco, kami hanya menghasilkan uang dari komisi trading, bukan dari kegagalan nasabah. Kami disini untuk membantu nasabah mendapatkan kentungan dan membantu dengan
                                                        komisi rendah hanya $10 per standard lot.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-balance-scale"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Integritas</h6>
                                                    <p class="content-whymaxco">Kami melakukan apa yang benar bagi nasabah, mitra, karyawan dan seluruh pemangku kepentingan. Kami berpegang teguh pada prinsip dan bertanggung jawab untuk mencapai
                                                        tingkat integritas tertinggi. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-money"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Accept all EA </h6>
                                                    <p class="content-whymaxco acceptallea">Seluruh perdagangan di Maxco dikirim langsung kepada penyedia likuiditas sehingga memungkinkan klien untuk menggunakan Pakar Trading sesuai yang diinginkan,
                                                        termasuk scalping EA. <a data-toggle="collapse" data-target="#demo" onclick="$('.acceptallea').hide();">Selegkapnya ...</a>
                                                    </p>
                                                    <div id="demo" class="collapse">
                                                        <p>Seluruh perdagangan di Maxco dikirim langsung kepada penyedia likuiditas sehingga memungkinkan klien untuk menggunakan Pakar Trading sesuai yang diinginkan, termasuk scalping EA. Kami
                                                            melakukan ini karena ingin memberikan perluang terbaik bagi klien untuk mendapatkan keuntungan, Kami hanya dikompensasi melalui komisi, bukan dari kegagalan klien atau spread. Apakah broker
                                                            Anda sekarang sudah mengizinkan hal ini?</p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-lightbulb-o"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Transparan </h6>
                                                    <p class="content-whymaxco">Eksekusi dan harga berdasarkan standarisasi dunia.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="media panel panel-default">
                                                <div class="media-body panel-body">
                                                    <div style="width:100%;">
                                                        <div class="icon" style="margin-left: calc(50% - 35px);"> <i class="fa fa-money"></i></div>
                                                    </div>
                                                    <br>
                                                    <h6 class="title-fitur">Unggul dan Terpercaya </h6>
                                                    <p class="content-whymaxco">Maxco adalah perusahaan afilisasi dari Panin Group Indonesia, salah satu Lembaga Keuangan terbesar di Asia Tenggara. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-12 no-padding">
                      <ul>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-user"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Customer First </h6>
                              <p>Fokus terhadap kebutuhan nasabah secara detail untuk memberikan pengalaman layanan nasabah kelas dunia. Kami hadir disini untuk membantu agar nasabah mendapatkan pengalaman transaksi secara optimal.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-balance-scale"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Integritas </h6>
                              <p>Kami melakukan apa yang benar bagi nasabah, mitra, karyawan dan seluruh pemangku kepentingan. Kami berpegang teguh pada prinsip dan bertanggung jawab untuk mencapai tingkat integritas tertinggi.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-lightbulb-o"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Transparan </h6>
                              <p>Eksekusi dan harga berdasarkan standarisasi dunia.</p>
                            </div>

                          </div>
                        </li>
                      </ul>
                      <ul>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-money"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Komisi rendah </h6>
                              <p>Maxco menawarkan komisi rendah hanya $10 per standard lot.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-users"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Accept all EA </h6>
                              <p>Seluruh perdagangan di Maxco dikirim langsung kepada penyedia likuiditas sehingga memungkinkan klien untuk menggunakan Pakar Trading sesuai yang diinginkan, termasuk scalping EA. Apakah broker Anda sekarang sudah menawarkan hal yang sama?</p>
                              <p>Kami melakukan ini karena ingin memberikan perluang dan pengalaman terbaik terhadap client dalam melakukan transaksi forex & Index. Kami hanya dikompensasi melalui komisi, bukan dari spread atau kerugian klien dalam bertransaksi.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-left">
                              <div class="icon"> <i class="fa fa-check-square-o"></i> </div>
                            </div>
                            <div class="media-body">
                              <h6>Unggul dan Terpercaya </h6>
                              <p>Maxco adalah strategic partner dari Panin Group Indonesia.</p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div> -->
                                    </div>
                                </div>
                                <br>
                                <h5 class="text-center">Maxco Futures adalah strategic partner dari Panin Group Indonesia</h5>
                                <p>Maxco Futures adalah strategic partner dari Panin Group Indonesia. Dengan legalitas dan diawasi oleh Pemerintah, kami ingin agar anda bertransaksi dengan perasaan aman dan nyaman. </p>
                                <br>
                                <h5 class="text-center">Unrivaled Financial Strength</h5>
                                <p>MaxcoFutures adalah perusahaan pialang berjangka dengan kekuatan keuangan terbesar di Indonesia, dimana kami merupakan perusahaan afiliasi Panin Group. Dengan legalitas dan diawasi langsung oleh Pemerintah,
                                    keinginan kami agar anda dapat bertransaksi dengan perasaan aman dan nyaman, tanpa perlu kuatir tidak dibayar. Berapapun profit anda, pasti kami bayar.
                                </p>
                                <br>
                                <!--
                                <div class="text-center">
                                    <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.forex" class="btn btn-1 margin-top-10 linkdownload" style="min-width: 180px;margin-bottom:5px;">Download Sekarang<i
                                          class="fa fa-download fa-lg"></i></a>
                                </div>
                                -->
                            </div>
                        </div>
                    </article>
                </div>

            </div>
            <!-- <div class="row">
        <div class="col-md-offset-1 col-md-10">
          <div class=" text-center">
          </div>
        </div>
        <div class="col-md-offset-4 col-md-4 text-center">
          <div class="list-style-featured media">
            <div class="media-left">
              <div class="icon" style="padding: 8px;"> <i class="fa fa-mobile-phone" style="font-size: 44px;"></i> </div>
            </div>
            <div class="media-body" style="padding: 16px;">
              <h4 class="text-left">Android App</h4>
            </div>
          </div>
        </div>

        <div class="col-md-12 margin-top-15">
          <div class="plan">
            <ul class="row">
              <li class="col-md-3" style="left: 50%;transform: translate(-50%);">
                <article>
                  <div class="price" style="background: #2f4a57;padding:15px;">
                    <i class="fa fa-android" style="font-size: 100px;"></i>
                    <h6>Android</h6>
                  </div>
                  <div class="light" style="padding:10px;background:#f9f9f9">
                    <p style="text-align: justify !important;line-height: 25px;min-height: 130px;">
                    {{trans('page.download-maxco-apps-mobile-version-android-detail')}}
                    </p>
                  </div>
                  <div style="display: inline-grid;">
                    <a href="https://release.stage.bull-b.com/rh" target="_blank" class="btn btn-1 margin-top-10" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download fa-lg"></i></a>
                  </div>
                </article>
              </li>
            </ul>
          </div>
        </div>

      </div> -->
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection

@section('jsonpage')
<script>
    function resizeEmbedYT() {
        var screenWidth = $('body').width();
        if (screenWidth > 0 && screenWidth < 576) {
            // $('.video-container').css('padding-bottom','100%');
            $('.video-container iframe').css('min-height', "");
        } else {
            // $('.video-container').css('padding-bottom','56.25%')
            $('.video-container iframe').css('min-height', "400px");
        }
    }

    resizeEmbedYT();

    window.addEventListener("resize", function() {
        resizeEmbedYT();
    });
</script>
@endsection
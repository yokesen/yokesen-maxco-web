@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>
.close{
  /* display: contents; */
}
/* .close::before {
    content: '\f2d7';
    font-family: "Ionicons";
    color: #fff;
    font-size: 30px;
} */
</style>
@endsection

@section('content')

<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/platform/metatrader_banner.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
    <!-- <div class="position-center-center">
      <div class="container">
        <h1>Metatrader 4</h1>
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Metatrader 4</li>
        </ol>
      </div>
    </div> -->
  </section>

  <!-- Content -->
   <div id="content">
     <section style="
     background: #f5f5f5;
     padding: 2px;
     ">
       <div class="container">
         <ol class="breadcrumb">
           <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
           <li class="active">{{trans('page.menu-download')}}</li>
         </ol>
       </div>
     </section>
     <div class="top-detail cbp-lightbox">
         <a style="display:none;" href="{{url('/')}}/web/images/webpage/aboutus/perijinan/izin_usaha_pialang_berjangka.png"
             class="cbp-lightbox" data-title="">
             <i class="icon-magnifier"></i></a>
     </div>

     <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h6 class="modal-title">Cara Instal</h6>
                </div>
                <div class="modal-body">
                    <!-- <iframe id="cartoonVideo"
                  src="//www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen ></iframe> -->
                  <iframe style="width:100%;" src="https://www.youtube.com/embed/1rbME9ItbXY?list=PLh35_KqiBmXPIz9mcmBNsjnVEKdePQw_e" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

     <!-- Revenues -->
     <section class="revenues padding-top-70 padding-bottom-70">
       <div class="container">

         <div class="row">
           <!-- Stories -->
           <div class="col-md-offset-1 col-md-10">
             <div class=" text-center">
               <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/mt4-logo-350x200.png" alt="">
             </div>
            <div>

             <!-- Stories -->
             <div class="stories">
               <div class="story">
                 <ul class="row">
                   <li class="col-md-5"> <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/windows_square.jpg" alt=""> </li>
                   <li class="col-md-7">
                     <article>
                       <h5>Metatrader 4 for Windows</h5>
                       <p class="text-justify">
                        Download the installation file to your PC, launch it and install the program, checking for instructions appearing on your monitor.
                        When launching the program for the first time, you will see a window with the registration form; after you have filled it in, you will automatically get a demo account.
                       </p>
                       <div class="row margin-top-20">
                         <div class="col-md-6">
                           <a href="https://download.mql5.com/cdn/web/metaquotes.software.corp/mt4/mt4setup.exe?utm_source=www.metatrader4.com&amp;utm_campaign=download" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download"></i></a>
                         </div>
                         <div class="col-md-6">
                           <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play"></i></a>
                         </div>
                       </div>
                       </article>
                   </li>
                 </ul>
               </div>

               <div class="story">
                 <ul class="row">
                   <li class="col-md-5"> <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/apple_square.jpg" alt="" style="height: 332px !important;"> </li>
                   <li class="col-md-7">
                     <article>
                       <h5>Metatrader 4 for Mac OS</h5>
                       <p class="text-justify">
                         The application is designed for use on iPhone, iPod Touch and iPad with iOS 4.0 and higher.
                        The terminal requires a cellular network or WiFi connection.
                       </p>
                       <div class="row margin-top-20">
                         <div class="col-md-6">
                           <a href="https://www.mql5.com/en/articles/1356?utm_source=www.metatrader4.com&utm_campaign=download.mt4.macos" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download"></i></a>
                         </div>
                         <div class="col-md-6">
                           <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play"></i></a>
                         </div>
                       </div>
                       </article>
                   </li>
                 </ul>
               </div>
               <div class="story">
                 <ul class="row">
                   <li class="col-md-5"> <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/apple_square.jpg" alt="" style="height: 332px !important;"> </li>
                   <li class="col-md-7">
                     <article>
                       <h5>Metatrader 4 for iPhone</h5>
                       <p class="text-justify">
                         Trading logos for smartphones.
                          For internet trading MetaTrader 4 for iPhone requires a cellular network or WiFi connection.
                       </p>
                       <div class="row margin-top-20">
                         <div class="col-md-6">
                           <a href="https://download.mql5.com/cdn/mobile/mt4/ios?utm_source=www.metatrader4.com&utm_campaign=download" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download"></i></a>
                         </div>
                         <div class="col-md-6">
                           <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play"></i></a>
                         </div>
                       </div>
                     </article>
                   </li>
                 </ul>
               </div>
               <div class="story">
                 <ul class="row">
                   <li class="col-md-5"> <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/android_square.jpg" alt="" style="height: 332px !important;"> </li>
                   <li class="col-md-7">
                     <article>
                       <h5>Metatrader 4 for Android</h5>
                       <p class="text-justify">
                         Trading platform for smartphones and tablet PCs powered by Android OS 2.1 and higher.
                        For internet trading MetaTrader 4 for Android requires a cellular network or WiFi connection.
                      </p>
                      <div class="row margin-top-20">
                        <div class="col-md-6">
                         <a href="https://download.mql5.com/cdn/mobile/mt4/android?utm_source=www.metatrader4.com&utm_campaign=download" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">{{trans('page.menu-download')}}<i class="fa fa-download"></i></a>
                       </div>
                       <div class="col-md-6">
                         <a data-toggle="modal" data-target="#myModal" class="btn btn-1" style="min-width: 180px;margin-bottom:5px;">Cara Install<i class="fa fa-play"></i></a>
                       </div>
                     </div>
                   </article>
                   </li>
                 </ul>
               </div>
             </div>
           </div>
         </div>
       </div>
     </section>
   </div>

 <!-- always on -->
 @include('page.template.always_on')

@endsection

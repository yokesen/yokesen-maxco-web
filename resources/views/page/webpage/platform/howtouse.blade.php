@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/bg/sub-bnr-bg-2.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>how to use</h1>
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">How To Use</li>
        </ol>
      </div>
    </div>
  </section>

  <!-- Content -->
  <div id="content">

    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-1 col-md-10">
            <div class="text-center">
              <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/logos/mt4-logo-350x200.png" alt="">
              <br>
            </div>
          </div>

          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Menampilkan Grafik</h3>
              </div>
          </div>
          <!-- Stories -->
          <div class="col-md-offset-1 col-md-10">
              <span>
                <h5 class="text-capitalize">Line Chart</h5>
                <p class="text-justify">
                  Line chart adalah tampilan grafik paling sederhana yang ada di MT4. Grafik ini menggambarkan garis yang menghubungkan harga-harga penutupan. Contohnya: di satu jam terakhir, perdagangan mata uang ditutup di harga 200, 100, 150, 300. Harga-harga ini kemudian dihubungkan dengan garis lurus dan jadilah line chart. Grafik ini kita bisa melihat pergerakan harga secara umum di sebuah periode waktu tertentu.
                  <br>                <br>

                  Berikut contoh dari line chart:
                </p>
                <br>
                <div class="text-center">
                  <img class="img-responsive"
                      src="{{url("/")}}\web\images\webpage\expertarea\berita_pasar_2.jpg">
                </div>
                <br><br>
                <h5 class="text-capitalize">Bar Chart</h5>
                <p class="text-justify">
                Grafik forex dalam bentuk bar sedikit lebih kompleks dibandingkan dengan grafik garis atau line chart. Namun dengan grafik ini, kita bisa mendapatkan lebih banyak informasi dibandingkan line chart, antara lain harga pembukaan, penutupan, harga tertinggi dan terendah di dalam satu periode tertentu. Nama lain dari grafik ini adalah OHLC atau Open-High-Low-Close berdasarkan informasi yang diberikannya. Ini adalah bentuk dari bar chart:
                </p>
                <div class="text-center">
                <img class="img-responsive"
                    src="{{url("/")}}\web\images\webpage\expertarea\berita_pasar_1.jpg">
                </div>
                <h5 class="text-capitalize">Candlestick Chart</h5>
                Dibanding dengan dua grafik sebelumnya, candlestick chart adalah grafik yang paling banyak digunakan oleh trader. Dinamakan demikian karena memang bentuknya menyerupai lilin. Informasi yang diberikan sama dengan grafik bar, namun tampilannya saja yang sedikit berbeda.
                <div class="text-center">
                  <img class="img-responsive"
                      src="{{url("/")}}\web\images\webpage\expertarea\berita_pasar_2.jpg">
                </div>
              </span>
              <!-- <span>
                  Grafik pergerakan harga bisa anda kostumisasi sesuai dengan selera anda. Klik kanan di layar grafik dan pilih properties untuk masuk ke layar seting grafik. Ada beberapa tema warna yang bisa anda gunakan di menu drop-down di bagian atas tab “Colors”. Anda juga bisa mengubah masing-masing warna dari variabel, termasuk latar belakang, grid dan warna grafik.
                  <br>
                  Di tab “common” tersedia pilihan-pilihan untuk sejumlah fitur untuk dimunculkan di grafik. Berikut ini tampilan layar pop-up “Properties”.
                  <br>
                  Setelah anda menentukan tema warna yang ingin anda gunakan, anda bisa menyimpannya sebagai template. Klik tombol template lalu pilih “Save Template” untuk menyimpan tema warna yang sudah anda pilih. Jangan lupa berikan nama untuk template anda. Nama template yang sudah anda buat akan muncul di drop-down list dan bisa anda gunakan pada grafik mana saja yang anda inginkan dengan cara mengklik ikon template dan memilih template yang sudah anda simpan.
                  <div class="hr-invisible-very-small"> </div>

                  <h5>Price bars</h5>
                  Anda bisa memilih tampilan bar grafik yang anda inginkan. Ada tiga pilihan tampilan bar, yakni tampilan garis, tampilan candlestick, serta tampilan line atau garis. Kebanyakan trader menggunakan tampilan candlestick karena kelengkapan informasi yang diberikannya. MRG Trade akan bahas mengenai tampilan candlestick pada artikel lainnya.
                  <div class="hr-invisible-very-small"> </div>

                  <h5>Indikator</h5>
                  Ada banyak indikator yang bisa anda tambahkan sebagai analisis teknikal di grafik pair anda. Klik tombol “Add Indicators” di bagian kanan atas layar MT4 anda dan pilihlah indikator yang anda inginkan. Ada indikator tren, oscillator dan volume. Saat anda sudah memilih indikator, anda bisa memilih input apa saja yang anda ingin ubah, atau anda juga bisa menggunakan setingan standar. Baru setelah itu indikator akan muncul di grafik pair anda.
                  <div class="hr-invisible-very-small"> </div>

                  <h5>Ukuran grafik</h5>
                  Setiap grafik layarnya bisa anda ”close”, “maximized”, “minimized”, atau “restored” seperti layar windows pada umumnya. Anda juga bisa mengatur agar dua hingga empat pair muncul di layar MetaTrader 4 anda bila anda melakukan banyak trading dalam satu waktu.
                  <div class="hr-invisible-very-small"> </div>

                  <h5>Zoom</h5>
                  Fitur ini bisa anda gunakan untuk melihat grafis secara lebih mendetail. Anda bisa klik tombol “+”untuk zoom in dan “-“ untuk zoom out yang terdapat di bagian atas layar MT4.
                  <div class="hr-invisible-very-small"> </div>

                  <h5>Jangka waktu</h5>
                  Anda bisa menampilkan berbagai jangka waktu untuk grafis di MT4. Mulai dari pergerakan harga dalam hitungan menit hingga bulanan bisa anda tampilkan untuk membantu anda saat trading. Anda bisa mengganti jangka waktu dengan meng-klik tombol “Periods” di bagian atas layar MT4 atau memilih jangka waktu yang anda inginkan yang sudah tersedia di toolbar, seperti yang ditampilkan pada gambar di bawah. M1 artinya jangka waktu 1 menit, M5 lima menit, H1 satu jam, dan seterusnya.
                  <div class="hr-invisible-very-small"> </div>


                  <h5>step7-metatrader4</h5>

                  <h5>Drawing tools</h5>
                  Anda bisa menggunakan drawing tools atau alat gambar untuk meningkatkan keakuratan analisis anda. Ada berbagai objek yang bisa anda gambar, yakni garis vertikal, horizontal, trendlines, channels, dan Finabocci Retracements. Masing-masing objek bisa anda temukan di bagian kiri atas layar MT4.
                  <div class="hr-invisible-very-small"> </div>

                  <h5>Status koneksi</h5>
                  Di bagian kanan bawah layar MT4 terdapat ikon yang menggambarkan apakah layar MT4 anda terhubung dengan internet atau tidak untuk menerima update terbaru dari pasar forex. Bar dengan warna hijau mengindikasikan bahwa MT4 anda terkoneksi dengan internet sedangkan bar dengan warna merah berarti tidak terdapat koneksi internet dan anda tidak mendapatkan update dari pasar forex. Angka yang berada di samping bar menampilkan kecepatan koneksi internet anda"
                </span> -->
            </div>
            <div class="col-md-12">
                <div class="heading text-center">
                    <h3>Membaca Harga</h3>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10">

            <span>
              Sesuaikan pola pergerakan harga dengan jenis order yang akan Anda lakukan. Jika ingin melakukan buy, fokuskan pada pair mata uang yang grafiknya sedang mengalami kenaikan. Sebaliknya, jika ingin sell, carilah mata uang yang grafiknya sedang mengalami penurunan.
            <span>

            </div>
            <div class="col-md-12">
                <div class="heading text-center">
                    <h3>Bertransaksi</h3>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10">
            <span>
                  Untuk mulai bertrading, tekan tombol F9 atau dengan mengklik kanan pada layar Market Watch lalu pilih New Order. Setelah itu akan muncul layar seperti dibawah ini :
                  <br>
                  (gambar)
                  <br>
                  Pada menu order terdapat beberapa macam pilihan, seperti :
                  <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                      <li>  Kolom Symbol: Yaitu jenis pair (pasangan mata uang) yang ingin anda tradingkan.</li>
                      <li>  Kolom Volume: Kolom untuk menempatkan jumlah lot yang ingin anda input. Apabila input volumenya dengan 1.00, maka itu berarti Anda bertransaksi 1 lot. Dalam forex, margin yang diperlukan untuk 1 lot = Rp.6.000.000 (enam juta rupiah) atau sama dengan $1000, dengan asumsi $1 = Rp.6.000 (Fix rate). Account yang diterapkan yaitu Regular Account dengan contract size = 100.000.</li>
                      <li>  Kolom Stop Loss & Take Profit: Yakni kolom untuk pengisian Stop Loss atau untuk membatasi kerugian Anda, dan Take Profit yaitu target untuk profit.</li>
                      <li>  Kolom Comment: Apabila ingin memberikan sebuah catatan kecil untuk posisi order saat ini, disinilah tempatnya.</li>
                      <li>  Kolom Type: Jenis order, apakah ingin dieksekusi secara langsung saat itu juga (instant execution) atau dengan pending order (ditunda hingga mencapai level harga tertentu).</li>
                      <li>  Tombol Buy & Sell: Apakah ingin order dengan Buy atau Sell? (untuk instant execution). Bila order Buy maka akan dieksekusi pada harga Ask, sedangkan untuk Sell adalah di harga Bid.</li>
                      <li>  Maximum Deviation: Yakni untuk batasan toleransi slippage apabila harga tiba-tiba melompat. Anda bisa menentukan jarak lompatan harga yang bisa anda toleransi untuk eksekusi order anda. (default: 0)</li>
                  </ul>
                  <br>
                  Dengan menggunakan MT4, order dapat dibuka dengan Instant Execution (order dengan harga saat sekarang) ataupun dengan Pending Order (order yang akan terlaksana jika menyentuh suatu titik harga tertentu yang sebelumnya telah dipesan terlebih dahulu).
                  <br>
                  Untuk tampilan Pending Order dalam MT4 dapat dilihat sebagai berikut:
                  <br>
                  (gambar)
                  <br>
                  <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                      <li>Kolom type untuk pending order: Yaitu jenis pending order apa yang diinginkan</li>
                      <li>Buy Limit: Yaitu memasang order Buy dengan harga dibawah harga running sekarang, dengan harapan jika harga yang sedang turun dapat bergerak naik lagi dari titik dimana posisi harga Pending Buy Limit tersebut.</li>
                      <li>Sell Limit: Yaitu memasang order Sell dengan harga diatas harga running sekarang, dengan harapan jika harga yang sedang naik dapat bergerak turun lagi dari titik posisi harga Pending Sell Limit tersebut.</li>
                      <li>Buy Stop: Yaitu memasang order Buy dengan harga diatas harga running sekarang, dengan harapan jika harga bergerak naik lagi dari titik posisi harga pending Buy Stop tersebut maka Anda akan mendapatkan profit.</li>
                      <li>Sell Stop: Yaitu memasang order Sell dengan harga dibawah harga running sekarang, dengan harapan jika harga bergerak turun lagi dari titik posisi harga pending Sell Stop tersebut maka anda akan mendapatkan profit.</li>
                      <li>Kolom At Price: Yaitu harga pending order, yang dimana order akan terlaksana jika menyentuh atau melewati titik harga tersebut sesuai dengan jenis “Type” yang sudah dipilih.</li>
                      <li>Kolom Expiry: Yaitu bila order pending Anda tidak terlaksana hingga waktu tertentu, maka akan otomatis dibatalkan oleh system (jam di kolom ini mengikuti jam server dari layar Market Watch).</li>
                  </ul>

                  <br>
                  Apabila sudah berhasil melakukan Open Posisi (Order), maka pada bagian bawah layar terminal MT4 akan muncul keterangan seperti ini:
                  <br>
                  Pada kolom Trade, bisa dilihat status order yang sedang berjalan di market (running). Pada order ini bisa dimodifikasi dengan cara “klik kanan” pada posisi order yang ingin dirubah ataupun ingin di-set perintah ""trailing stop""-nya. Jika di-klik kanan pada order Anda, maka akan muncul menu seperti dibawah ini pada MT4:
                  <br>
                  Apabila memilih Modify or Delete Order pada posisi yang aktif tersebut, maka akan muncul layar menu seperti berikut:
                  <br>
                  Dengan menggunakan MT4, Anda juga bisa memodifikasi Stop Loss, Take Profit, Expiry Time dan juga Price-nya dengan mudah sesuai kehendak. Apabila ingin menghapus pending order, maka Anda cukup memilih "Delete".
                </span>

              </div>
              <div class="col-md-12">
                  <div class="heading text-center">
                      <h3>Membaca Transaksi</h3>
                  </div>
              </div>
              <div class="col-md-offset-1 col-md-10">

            <span>
                 Untuk order buy,tunggu sampai kotak candlestick berubah warna menjadi hitam.
                 <br>
                 (gambar)
                 <br>
                 Pada gambar diatas jika kamu melakukan order buy pair EUR USD ketika harga masih di 1.3750,kemudian kamu close order buy kamu ketika harga sudah di 1.3760,maka kamu akan profit sebesar 10 pips.
                 <br>
                 Untuk order sell,tunggu sampai kotak candlestick berubah warna menjadi putih.
                 Pada gambar 10 di atas,jika kamu order sell ketika harga masih di 1.3755,kemudian kamu close order sell kamu ketika harga sudah di 1.3715,maka kamu akan profit sebesar 40 pips.
                 <br>
                 1 pip nilai dollarnya tergantung dari besar kecilnya lot yang kamu pakai ketika melakukan order.Nilainya bisa $ 0.1 ; $ 1 : $ 10 dst."
               </span>

          </div>
        </div>
      </div>
    </section>
  </div>

<!-- always on -->
@include('page.template.always_on')

@endsection

@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h4>Struktur Pengurus</h4>
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Struktur Pengurus</li>
        </ol>
      </div>
    </div>
  </section>

  <!-- Content -->
  <div id="content">

    <!-- WHO WE ARE -->
    <section class="team team-wrap padding-top-70 padding-bottom-70">
      <div class="container">

        <!-- Work Filter -->
        <!-- <ul class="filter team-filter">
          <li class="tab-title filter-item"><a class="active" href="#." data-filter="*">All</a></li>
          <li class="filter-item"><a href="#." data-filter=".gd">graphic designers</a></li>
          <li class="filter-item"><a href="#." data-filter=".html">html coders</a></li>
          <li class="filter-item"><a href="#." data-filter=".market">marketing</a></li>
        </ul> -->
        <div class="">
          <ul class="row items">

            <!-- Member -->
            <li class="col-md-3 item market">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/management/raisa.jpg" alt="">
                <h5>Raisa</h5>
                <span>Direktur Direktur Utama</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>

            <!-- Member -->
            <li class="col-md-3 item gd market">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/management/christian.jpg" alt="">
                <h5>Christian</h5>
                <span>Direktur Marketing</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>

            <!-- Member -->
            <li class="col-md-3 item gd ">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/management/daniel.jpg" alt="">
                <h5>Daniel</h5>
                <span>Direktur Keuangan</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>

            <!-- Member -->
            <li class="col-md-3 item gd html">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/management/deandra.jpg" alt="">
                <h5>Deandra</h5>
                <span>Direktur Kepatuhan</span>
                <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci.</p>
                <ul class="social">
                  <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#."><i class="fa fa-google"></i></a></li>
                  <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </article>
            </li>


          </ul>
        </div>
      </div>
    </section>
  </div>
@endsection

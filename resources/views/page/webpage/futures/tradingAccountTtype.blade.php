@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/future/perdagangan_berjangka_banner.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

  </section>

  <!-- Content -->
  <div id="content">
    <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
      <div class="container">
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
          <li class="active">Jenis Akun</li>
        </ol>
      </div>
    </section>

    <!-- Revenues -->
    <!-- Plan -->
    <section class="padding-top-70 padding-bottom-70">
      <div class="container">

        <!-- Heading -->
        <div class="heading text-center">
          <h3>Select Your Plan to Start</h3>
        </div>

        <!-- Plan -->
        <div class="plan">
          <div class="row">
            <div class="col-md-3" style="width: 12.5% !important;">
            </div>
            <div class="col-md-6">
              <div class="plan">
                <div class="panel panel-default">
                  <div class="panel-body text-center">
                    <h5>IDR</h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="plan">
                <div class="panel panel-default">
                  <div class="panel-body text-center">
                    <h5>USD</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ul class="row">
            <div class="col-md-3" style="width: 12.5% !important;">
            </div>
              <!-- starter -->
              <li class="col-md-3">
                <article>
                  <div class="price bg-paninblue">
                    <h6>Mini Account</h6>
                    <span class="currency">$</span>500
                    <!-- <span class="period">/monthy</span> -->
                  </div>
                  <p class="light">
                    Forex & Gold & Index
                  </p>
                  <p>
                    IDR Fixed Rate <span>10.000</span>
                  </p>
                  <p class="light">
                    Leverage <span>1:100</span>
                  </p>
                  <p class="light">
                    Contract Size <span>100.000</span>
                  </p>
                  <p class="light">
                    Start from <span>0.1</span> Lot
                  </p>
                  <a href="{{env('CABINET_URL')}}register" class="btn btn-1 margin-top-30 bg-paninblue">buka akun <i class="fa fa-caret-right"></i></a> </article>
              </li>

              <!-- Basic -->
              <li class="col-md-3">
                <article>
                  <div class="price bg-paninred">
                    <h6>Professional Account</h6>
                    <span class="currency">$</span>10.000
                    <!-- <span class="period">/monthy</span> -->
                  </div>
                  <p class="light">
                    Forex & Gold & Index
                  </p>
                  <p>
                    IDR Fixed Rate <span>10.000</span>
                  </p>
                  <p class="light">
                    Leverage <span>1:100</span>
                  </p>
                  <p class="light">
                    Contract Size <span>100.000</span>
                  </p>
                  <p class="light">
                    Start from <span>1</span> Lot
                  </p>
                  <a href="{{env('CABINET_URL')}}register" class="btn btn-1 margin-top-30 bg-paninred">buka akun <i class="fa fa-caret-right"></i></a> </article>
              </li>

              <!-- pro -->
              <li class="col-md-3">
                <article>
                  <div class="price" style="background-color: #d7ab4b">
                    <h6>USD Account</h6>
                    <span class="currency">$</span>10.000
                    <!-- <span class="period">/monthy</span> -->
                  </div>
                  <p class="light">
                    Forex & Gold & Index
                  </p>
                  <p>
                    USD Floating Rate
                  </p>
                  <p class="light">
                    Leverage <span>1:100</span>
                  </p>
                  <p class="light">
                    Contract Size <span>100.000</span>
                  </p>
                  <p class="light">
                    Start from <span>1</span> Lot
                  </p>
                  <a href="{{env('CABINET_URL')}}register" class="btn btn-1 margin-top-30 bg-paningold">buka akun <i class="fa fa-caret-right"></i></a> </article>
              </li>

          </ul>
        </div>
      </div>
    </section>
  </div>

  <!-- always on -->
  @include('page.template.always_on')

@endsection

@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/future/peraturan_header.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

  </section>

  <!-- Content -->
  <div id="content">
    <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
      <div class="container">
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
          <li class="active">Peraturan Berjangka</li>
        </ol>
      </div>
    </section>

    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Peraturan - peraturan</h3>
              </div>
          </div>
          <div class="col-md-12">
            <row>
              <div class="col-md-4">
                <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/future/peraturan_1.jpg" alt="">
              </div>
              <div class="col-md-8">
                <ul class="dt-sc-fancy-list red arrow padding-left-50 text-justify">
                  <li><a href="http://website.bappebti.go.id/id/regulation/laws.html" target="_blank">Undang-Undang Republik Indonesia Nomor 32 Tahun 1997</a><br>
                  Tentang Perdagangan Berjangka Komoditi
                  </li>
                  <li><a href="http://website.bappebti.go.id/id/regulation/laws.html" target="_blank">Undang-Undang Republik Indonesia Nomor 10 Tahun 2011</a><br>
                  Tentang Perubahan atas Undang-Undang Nomor 32 tahun 1997<br>
                  Tentang Perdagangan Berjangka Komoditi
                  </li>
                  <li><a href="https://www.bphn.go.id/data/documents/99ip010.pdf" target="_blank">Peraturan Pemerintah Nomor 10 Tahun 1999</a><br>
                  Tentang Tata Cara Pemeriksaan di Bidang Perdagangan Berjangka Komoditi
                  </li>
                  <li><a href="http://dprd.jatimprov.go.id/produkhukum/98cf2-PP-NOMOR-9-TAHUN-1999-TENTANG-PENYELENGGARAAN-PERDAGANGAN-BERJANGKA-KOMODITI.pdf" target="_blank">Peraturan Pemerintah Nomor 9 Tahun 1999</a><br>
                  Tentang Penyelenggaraan Perdagangan Berjangka Komoditi</li>
                  <li><a href="http://website.bappebti.go.id/id/regulation/ordinance.html" target="_blank">Peraturan Pemerintah Nomor 31 Tahun 2011</a><br>
                  Tentang Pencabutan Peraturan Pemerintah Nomor 17 Tahun 2009 tentang Pajak Penghasilan atas Penghasilan dari Transaksi Derivatif Berupa Kontrak Berjangka yang Diperdagangkan di Bursa
                  </li>
                  <li><a href="https://peraturan.bpk.go.id/Home/Details/5488/pp-no-49-tahun-2014" target="_blank">Peraturan Pemerintah Nomor 49 Tahun 2014<br>
                  Penyelenggaraan Perdagangan Berjangka Komoditi</li>
                </ul>
              </div>
            </row>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

<!-- always on -->
@include('page.template.always_on')

@endsection

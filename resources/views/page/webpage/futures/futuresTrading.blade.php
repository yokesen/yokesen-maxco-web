@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>
.topnav {
  overflow: hidden;
  /* background-color: #333; */
  position: relative;
}

.topnav #myLinks {
  /* display: none; */
}

.topnav a {
  color: #666666;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  display: block;
}

.topnav a.icon {
  background: #017dc7;
  color: white;
  display: block;
  position: absolute;
  left: 0;
  top: 0;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.active {
  /* background-color: #4CAF50; */
  background-color: #f2f2f2;
  color: white;
}
</style>
@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/future/perdagangan_berjangka_banner.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

  </section>

  <!-- Content -->
  <div id="content">
    <section style="
    background: #f5f5f5;
    "
    >
      <div class="container">
        <ol class="breadcrumb <?php if(session('isMobile')){echo 'hidden';}?>">
          <li><i class="fa fa-home"></i> <a href="/">{{trans('page.menu-home')}}</a></li>
          <li class="active">{{trans('page.menu-futures-trading')}}</li>
        </ol>

      </div>
      <div class="topnav <?php if(session('isMobile')){echo 'show';}else{echo 'hidden';}?>">
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
          <i class="fa fa-bars"></i>
        </a>
        <a class="active">
          <span class="titleSubMenu" style="margin-left:43px;">{{trans('page.menu-trading-system')}}</span></a>
        <div id="myLinks" style="display:none;">
          <a data-toggle="collapse" onclick="toggleGhost('system');myFunction();$('.titleSubMenu').text('Sistem');"  aria-expanded="false"><i class="fa fa-building"></i> {{trans('page.menu-trading-system')}}</a>
          <a data-toggle="collapse" onclick="toggleGhost('function');myFunction();$('.titleSubMenu').text('Fungsi');"  aria-expanded="false"><i class="fa fa-eye"></i> {{trans('page.menu-trading-function')}}</a>
          <a data-toggle="collapse" onclick="toggleGhost('structure');myFunction();$('.titleSubMenu').text('Struktur');"  aria-expanded="false"><i class="fa fa-history"></i> {{trans('page.menu-trading-structure')}}</a>
        </div>
      </div>
    </section>
    <!-- Revenues -->


    <section class="revenues padding-top-50 padding-bottom-70">
      <div class="container">
        <div class="row">
          <!-- Sidebar -->

           <div class="col-md-3 <?php if(session('isMobile')){echo 'hidden';}?>">
             <div class="side-bar-revenues">
               <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('system')"  aria-expanded="false"><i class="fa fa-building"></i> {{trans('page.menu-trading-system')}}</a></h6>
               <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('function')"  aria-expanded="false"><i class="fa fa-eye"></i> {{trans('page.menu-trading-function')}}</a></h6>
               <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('structure')"  aria-expanded="false"><i class="fa fa-history"></i> {{trans('page.menu-trading-structure')}}</a></h6>
               <!-- Brochures -->
               @include('page.template.footer_sidebar')
               <!-- <h5 class="side-heading margin-top-40">Brosur</h5> -->
               <!-- Brochures -->
               <!-- <div class="broc"> <img class="img-responsive" src="{{url('/')}}/web/images/down-img-1.jpg" alt="" > <a href="#." class="icon-down"><i class="fa fa-download"></i></a> <a href="#." class="icon-file"><i class="fa fa-file-pdf-o"></i> Portfolio .DOC</a> </div> -->
               <!-- Brochures -->
               <!-- <div class="broc"> <img class="img-responsive" src="{{url('/')}}/web/images/down-img-2.jpg" alt="" > <a href="#." class="icon-down"><i class="fa fa-download"></i></a> <a href="#." class="icon-file"><i class="fa fa-file-pdf-o"></i> Annual Report .PDF</a> </div> -->
             </div>
           </div>
           <div class="col-md-9">
             <div class="col-md-12 toggleGhost system">
                 <div class="heading heading-maxco text-center">
                     <h3>{{trans('page.menu-trading-system')}}</h3>
                 </div>
                 <div class="col-md-5">
                   <img class="img-responsive"
                       src="{{url("/")}}/web/images/webpage/future/Sistem.jpg">
                 </div>
                 <div class="col-md-7">
                   <p class="text-justify">{{trans('page.trading-system-detail')}}</p>
                 </div>
             </div>

             <div class="col-md-12 hidden toggleGhost function">
                 <div class="heading heading-maxco text-center">
                     <h3>{{trans('page.menu-trading-function')}}</h3>
                 </div>
                 <div class="col-md-5">
                   <img class="img-responsive"
                       src="{{url("/")}}/web/images/webpage/future/Fungsi.jpg">
                 </div>
                 <div class="col-md-7">
                   <p>
                     {{trans('page.trading-function-subtitle')}}</p>
                     <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-<?php if(session('isMobile')){echo '10';}else{echo '50';}?> text-justify">
                         <li>
                             <p>{{trans('page.trading-function-detail-1')}}</p>
                         </li>
                         <li>
                             <p>{{trans('page.trading-function-detail-2')}}</p>
                         </li>
                     </ul>

                 </div>
             </div>

             <div class="col-md-12 hidden toggleGhost structure">
                 <div class="heading heading-maxco text-center">
                     <h3>{{trans('page.menu-trading-structure')}}</h3>
                 </div>
                 <div class="col-md-12">
                   <p>{{trans('page.trading-structure-subtitle')}}</p>
                     <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-<?php if(session('isMobile')){echo '10';}else{echo '50';}?> text-justify">
                         <li>
                             <p>{{trans('page.trading-structure-detail-1')}}</p>
                         </li>
                         <li>
                             <p>{{trans('page.trading-structure-detail-2')}}</p>
                         </li>
                         <li>
                             <p>{{trans('page.trading-structure-detail-3')}}</p>
                         </li>
                     <ul>
                 </div>

             </div>

           </div>
          </div>
        </div>

    </section>
  </div>

  <!-- always on -->
  @include('page.template.always_on')

@endsection

@section('jsonpage')
<script>
  function toggleGhost(target){
    $('.toggleGhost').each(function(){
      $(this).hide();
      $(`.${target}`).removeClass('hidden');
      $(`.${target}`).show();

    })
  }

  function myFunction() {
    $("#myLinks").fadeToggle("slow");
  }

</script>
@endsection

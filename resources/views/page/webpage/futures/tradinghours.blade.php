@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/future/perdagangan_berjangka_banner.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

</section>

<!-- Content -->
<div id="content">
    <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
        <div class="container">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i> <a href="{{route('index')}}">Home</a></li>
                <li class="active">Trading Hours</li>
            </ol>
        </div>
    </section>

    <!-- Revenues -->
    <!-- Plan -->
    <section class="padding-top-70 padding-bottom-70">
        <div class="container">

            <!-- Heading -->
            <div class="heading text-center">
                <h3>Trading Hours (WIB)</h3>
            </div>
            <br>
            <div class=" text-center">
                <h6>Index Saham Hong kong (HKJ5U_BBJ)</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center table-hover">
                    <thead class="tbl-header">
                        <tr>
                            <th class="text-center">Hari</th>
                            <th class="text-center">Session I</th>
                            <th class="text-center">Session II</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Senin - Jumat</td>
                            <td>08:15AM – 11:00AM</td>
                            <td>12:00PM – 15:30PM</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class=" text-center">
                <h6>Index Saham Amerika NASDAQ (UNJ20U_BBJ & UNJ240_BBJ)</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center table-hover">
                    <thead class="tbl-header">
                        <tr>
                            <th class="text-center">Hari</th>
                            <th class="text-center">Musim Dingin</th>
                            <th class="text-center">Musim Panas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Senin – Kamis</td>
                            <td>06.00 AM – 04.15 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Jumat </td>
                            <td>06.00 AM – 03.00 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td></td>
                            <td>06.00 AM – 03.15 AM</td>
                        </tr>
                        <tr>
                            <td>Selasa – Jumat </td>
                            <td></td>
                            <td>05.00 AM – 03.15 AM</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class=" text-center">
                <h6>Index Saham Amerika S&P 500 (UPJ50U_BBJ & UPJ600_BBJ)</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center table-hover">
                    <thead class="tbl-header">
                        <tr>
                            <th class="text-center">Hari</th>
                            <th class="text-center">Musim Dingin</th>
                            <th class="text-center">Musim Panas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Senin – Kamis</td>
                            <td>06.00 AM – 04.15 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Jumat </td>
                            <td>06.00 AM – 03.00 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td></td>
                            <td>06.00 AM – 03.15 AM</td>
                        </tr>
                        <tr>
                            <td>Selasa – Jumat </td>
                            <td></td>
                            <td>05.00 AM – 03.15 AM</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class=" text-center">
                <h6>Index Saham Amerika DOW JONES (USJ5U_BBJ & USJ60_BBJ)</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center table-hover">
                    <thead class="tbl-header">
                        <tr>
                            <th class="text-center">Hari</th>
                            <th class="text-center">Musim Dingin</th>
                            <th class="text-center">Musim Panas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Senin – Kamis</td>
                            <td>06.00 AM – 04.15 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Jumat </td>
                            <td>06.00 AM – 03.00 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td></td>
                            <td>06.00 AM – 03.15 AM</td>
                        </tr>
                        <tr>
                            <td>Selasa – Jumat </td>
                            <td></td>
                            <td>05.00 AM – 03.15 AM</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class=" text-center">
                <h6>CFD Berkala Crude Oil (CLS)</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center table-hover">
                    <thead class="tbl-header">
                        <tr>
                            <th class="text-center">Hari</th>
                            <th class="text-center">Musim Dingin</th>
                            <th class="text-center">Musim Panas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Senin – Kamis</td>
                            <td>06.00 AM – 05.00 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Jumat </td>
                            <td>06.00 AM – 03.00 AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td></td>
                            <td>06.00 AM – 04.00 AM</td>
                        </tr>
                        <tr>
                            <td>Selasa – Kamis </td>
                            <td></td>
                            <td>05.00 AM – 04.00 AM</td>
                        </tr>
                        <tr>
                            <td>Jumat </td>
                            <td></td>
                            <td>05.00 AM – 02.00 AM</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class=" text-center">
                <h6>Forex</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center table-hover">
                    <thead class="tbl-header">
                        <tr>
                            <th class="text-center">Sesi Perdagangan</th>
                            <th class="text-center">Musim Dingin</th>
                            <th class="text-center">Musim Panas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Sydney</td>
                            <td>06.00 AM – 03.00 PM</td>
                            <td>05.00 AM – 02.00 PM</td>
                        </tr>
                        <tr>
                            <td>Tokyo </td>
                            <td>08.00 AM – 05.00 PM</td>
                            <td>07.00 AM – 04.00 PM</td>
                        </tr>
                        <tr>
                            <td>London</td>
                            <td>02.00 PM – 11.00 PM</td>
                            <td>01.00 PM – 10.00 PM</td>
                        </tr>
                        <tr>
                            <td>New York</td>
                            <td>09.00 PM – 06.00 AM</td>
                            <td>08.00 PM – 05.00 AM</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection
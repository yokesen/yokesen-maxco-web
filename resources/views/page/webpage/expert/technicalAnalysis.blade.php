@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
        <div class="container">
            <h4>Analisis Teknikal</h4>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Analisis Teknikal</li>
            </ol>
        </div>
    </div>
</section> -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/banner_AREA_PAKAR.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>

<!-- Content -->
<div id="content">
  <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Analisis Teknikal</li>
      </ol>
    </div>
  </section>
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>Analisis Teknikal</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div class="text-justify">
                        <p>
                            Analisa teknikal adalah cara menganalisa pergerakan harga aset di pasar finansial
                            menggunakan
                            perangkat statistik seperti grafik dan rumus matematis. Tujuan belajar analisa teknikal
                            yaitu agar
                            trader dapat menilai kondisi pasar saat ini berdasarkan histori harga di masa lampau,
                            sekaligus
                            memberikan gambaran atau prediksi tentang pergerakan pasar di masa depan.
                            <br><br>
                        </p>
                        <h5>Prinsip Dasar Analisa Teknikal </h5>
                        <p>
                            Analisa teknikal termasuk dalam dua jenis analisa forex yang paling umum digunakan trader.
                            Jenis
                            analisa lainnya adalah analisa fundamental yang berupaya menganalisa nilai suatu mata uang
                            berdasarkan kondisi ekonomi negara asalnya, situasi pasar finansial, atau berita dan rumor
                            lainnya
                            yang beredar. Berbeda dengan analisa fundamental, analisa teknikal didasarkan pada tiga
                            prinsip:
                        </p>

                        <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50 text-justify">
                            <li>
                                <p>Market Price Discount Everything, artinya: harga yang terlihat pada grafik (chart)
                                    telah
                                    menggambarkan semua faktor yang mempengaruhi pasar.</p>
                            </li>
                            <li>
                                <p> Price Moves in Trend, artinya: harga tidak bergerak secara acak,
                                    melainkan selalu membentuk suatu pola tertentu (trend) yang akan terus berlangsung
                                    sampai
                                    ada tanda-tanda bahwa pola pergerakan ini berhenti dan berganti.
                                </p>
                            </li>
                            <li><p>History Repeats Itself, artinya: ada kecenderungan kuat bahwa perilaku para pelaku
                                    pasar
                                    di masa kini akan memberikan reaksi yang sama dengan para pelaku pasar di masa lalu,
                                    dalam
                                    menyikapi berbagai informasi yang mempengaruhi pasar; sehingga motif pergerakan yang
                                    dahulu
                                    pernah terjadi, bisa terulang lagi.
                                </p></li>
                        </ul>
                        <br>
                        <h5>Komponen Analisa Teknikal</h5>
                        <p>Analisa teknikal mengandung sejumlah komponen penting. Komponen-komponen ini wajib
                            diketahui
                            oleh
                            semua trader forex:</p>
                        <br>
                        <h5>1. Grafik Harga (Chart)</h5>
                        <div class="hr-invisible-very-small"> </div>
                        <p>
                            Grafik harga menunjukkan nilai tukar dua mata uang dan terus bergerak dari waktu ke
                            waktu.
                            Ada tiga
                            model grafik harga yang umum digunakan dalam analisa teknikal, yaitu grafik garis
                            (Line
                            Chart),
                            grafik batang (Bar Chart), dan grafik lilin (Candlestick Chart). Diantara ketiganya,
                            yang
                            paling
                            populer di Indonesia adalah grafik Candlestick seperti yang nampak pada gambar
                            pergerakan
                            harga
                            GBP/USD (Pounds/Dolar AS) di bawah.
                        </p>
                        <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/expertarea/analisis_teknikal_945x498.jpg" alt="">
                        <br>
                        <br>
                        <h5> 2. Indikator Teknikal</h5>
                        <br>
                        <p>
                            Agar pergerakan harga yang ditunjukkan oleh grafik di atas dapat dianalisa, maka
                            dibutuhkan
                            indikator teknikal. Indikator teknikal ini banyak sekali jenisnya, tetapi semua
                            memiliki
                            kesamaan,
                            yaitu memungkinkan trader untuk memaknai pergerakan harga saat ini sehingga dapat
                            memunculkan
                            prediksi untuk pergerakan harga di masa depan.
                            <br><br>
                            Karena banyaknya indikator teknikal di dunia, bahkan hingga ratusan, maka setiap
                            trader bisa
                            menggunakan indikator yang berbeda. Namun, ada sejumlah indikator yang umum
                            digunakan,
                            antara lain
                            Moving Average (MA) dan Relative Strenth Index (RSI). Contohnya pada gambar di bawah
                            ini,
                            grafik
                            GBP/USD yang telah dipasang indikator teknikal berupa MA dan RSI.
                        </p>
                        <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/expertarea/cara_membaca_candlestick.jpg" alt="">
                        <br>
                        <br>
                        <p>
                            Dari kondisi indikator teknikal tersebut, dapat dipahami bahwa harga pada GBP/USD
                            sedang
                            mengalami
                            bearish (tren harga menurun) dan trader memiliki peluang untuk ""Sell"". Bagaimana
                            bisa
                            menyimpulkan
                            itu? Karena pergerakan harga berada di bawah garis MA; sedangkan RSI menurun, tetapi
                            belum
                            mencapai
                            titik 30.0. Nantinya, jika RSI sudah mencapai 30.0, maka itu boleh jadi momen untuk
                            ""Buy""
                            GBP/USD,
                            karena menandai perubahan pergerakan di pasar.
                            <br><br>
                            Belajar analisa teknikal berarti harus memahami cara menggunakan indikator-indikator
                            seperti
                            Moving
                            Average dan RSI. Akan tetapi, analisa teknikal dan indikator tidaklah 100% pasti
                            tepat.
                            Kondisi
                            pasar bisa berubah-ubah sewaktu-waktu dan tak seorang pun di dunia dapat mengetahui
                            apa yang
                            terjadi
                            di masa depan. Oleh karena itu, hasil analisa teknikal maupun analisa fundamental
                            merupakan
                            ""perkiraan"", dan trader harus selalu siap untuk menghadapi kemungkinan salah
                            prediksi.
                        </p>
                        <br>
                        <h5> 3. Teknik Atau Metode Analisa Teknikal</h5>
                        <br>
                        <p>

                            Kombinasi antara MA dan RSI untuk menyusun keputusan trading forex seperti dalam
                            contoh di
                            atas,
                            merupakan bagian dari teknik atau metode analisa teknikal. Jadi, dalam analisa
                            teknikal,
                            sekedar ada
                            grafik dan indikator saja tidak cukup. Trader forex bisa membuat teknik atau metode
                            analisa
                            sendiri,
                            berdasarkan satu jenis indikator, kombinasi banyak indikator, atau digabungkan
                            dengan
                            teknik-teknik
                            khusus seperti Fibonacci, Retracement dan Reversal, Elliott Wave, dan lain
                            sebagainya.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection

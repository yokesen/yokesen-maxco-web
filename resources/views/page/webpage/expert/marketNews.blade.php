@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('ogprop')
  <meta property="og:title" content="@yield('og-title','Maxco Futures')" />
  <meta property="og:type" content="@yield('og-type','website')" />
  <meta property="og:url" content="@yield('og-url',URL::current())" />
  <meta property="og:image" content="@yield('og-image',url('/').'/web/images/webpage/expertarea/BERITA_PASAR_BANNER.jpg')" />
@endsection

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
        <div class="container">
            <h4>Berita Pasar</h4>
            Breadcrumbs
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Berita Pasar</li>
            </ol>
        </div>
    </div>
</section> -->

<!-- Content -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/BERITA_PASAR_BANNER.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>

<!-- Content -->
<div id="content">
  <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Berita Pasar</li>
      </ol>
    </div>
  </section>
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>Berita Pasar</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div class="text-justify">
                      <h5 class="text-capitalize">Sentimen Konsumen Australia Kembali Turun, AUD/USD Melemah </h5>
                      <p>Maxcofutures.co.id - Pada hari Rabu (22/Januari),
                         Departemen Statistik Australia merilis data Sentimen Konsumen versi Westpac yang kembali melanjutkan
                         trend buruk dengan penurunan 1.8 persen menjadi 93.4 pada bulan Januari.
                         Pada periode sebelumnya, data ini juga melemah 1.9 persen dari 97 ke 95.1.
                         Kondisi ini semakin menegaskan kondisi perekonomian Australia yang lemah di awal tahun 2020.
                     </p>
                     <br>
                     <img class="img-responsive"
                         src="{{url("/")}}\web\images\webpage\expertarea\berita_pasar_1.jpg">
                     <br>
                     <br>
                     <p>
                         Secara terperinci, sub indeks keyakinan konsumen jangka pendek untuk 12 bulan ke depan turun 5.4
                         persen,
                         sementara indeks keyakinan konsumen untuk 5 tahun ke depan turun sebanyak 3.7 persen.
                     </p>
                     <p>
                         Sentimen konsumen Australia yang terus-menerus mengalami penurunan telah berimbas terhadap trend
                         belanja konsumen.
                         Dalam hal ini, analis Westpac mengecualikan data penjualan ritel yang tumbuh cukup kokoh pada bulan
                         November 2019 karena didukung momen Black Friday (saat banyak produsen menawarkan diskon
                         besar-besaran).
                     </p>
                     <p>
                         Analis Westpac terkait juga mengatakan bahwa kemerosotan lebih lanjut pada sentimen konsumen
                         Australia di bulan Januari sangat masuk akal, mengingat kebakaran hutan dahsyat telah menghanguskan
                         lebih dari 10 juta hektar lahan di sana.
                         Bahkan, bisa dikatakan agak mengejutkan Indeks Sentimen Konsumen hanya turun 1.8 persen, tidak
                         separah penurunan 5.8 persen saat terjadi banjir bandang di Queensland pada tahun 2011 lalu.
                     </p>
                     <h5 class="text-capitalize">AUD/USD Melemah, Nantikan Data Ketenagakerjaan</h5>
                     <p>Buruknya rilis data Sentimen Konsumen versi Westpac sedikit banyak memberi tekanan terhadap Dolar.
                        Kondisi ini tercermin dari pergerakan pair AUD/USD dalam sesi perdagangan pagi ini,
                        yang berada di kisaran 0.6836 atau melemah 0.1 persen dari harga Open harian.
                      </p>
                      <br>
                      <img class="img-responsive"
                          src="{{url("/")}}\web\images\webpage\expertarea\berita_pasar_2.jpg">
                      <br>
                      <br>
                      <p>Pelemahan cukup signifikan AUD juga karena tertekan oleh merebaknya sentimen risk-off dan
                      antisipasi pelaku pasar terhadap data ketenagakerjaan Australia yang dijadwalkan meluncur pada hari
                      Kamis (23/Januari) besok.
                      Apabila data pekerjaan memburuk, maka prospek penurunan suku bunga RBA di bulan Februari mendatang
                      akan semakin meningkat dan menekan sentimen terhadap Dolar Australia.
                      </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection

@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
        <div class="container">
            <h4>Tips Pakar hari ini </h4>
            Breadcrumbs
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Tips Pakar hari ini </li>
            </ol>
        </div>
    </div>
</section> -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/BANNER_TRADING_TRICKS_.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>
<!-- Content -->
<div id="content">
    <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
        <div class="container">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
                <li class="active">News</li>
            </ol>
        </div>
    </section>
    <!-- <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>News</h3>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <section class="blog padding-top-70 padding-bottom-70">
        <div class="container">
            <!-- Heading -->
            <div class="heading text-center">
                <h4>News</h4>
            </div>
            <!-- Blog Row -->
            <div class="row blog-slide">
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/dXuyFZhOqVY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=dXuyFZhOqVY" class="post-tittle">Berita Saham 6 Maret 2020</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/lG3w0IVvSPc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=lG3w0IVvSPc" class="post-tittle">BERITA DOLLAR & EMAS - 5 MARET 2020</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/eeThjT_CUco" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=eeThjT_CUco" class="post-tittle">zerlina 华语财经新闻3。5</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/cFpsrnZ7xxY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=cFpsrnZ7xxY" class="post-tittle">Berita Saham 05 Maret 2020</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/YTMvWU7ag-4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=YTMvWU7ag-4" class="post-tittle">Berita Saham 04 Maret 2020</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/LY_VCd_aDww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=LY_VCd_aDww" class="post-tittle">BERITA DOLLAR & EMAS - 4 MARET 2020</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/h7K3xZaAARk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=h7K3xZaAARk" class="post-tittle">zerlina 华语财经新闻 3。4</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/pwiUUVIytxE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=pwiUUVIytxE" class="post-tittle">zerlina 华语财经新闻 3。3</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/GrammnH79_o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=GrammnH79_o" class="post-tittle">BERITA DOLLAR & EMAS - 3 MARET 2020</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>
                <div class="col-md-12 no-padding">
                    <article>
                        <iframe src="https://www.youtube.com/embed/GYB9zvqGFkU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <!-- Date -->
                        <!-- <div class="date"> 31 <span>Jan</span> </div> -->

                        <!-- Detail -->
                        <div class="post-detail"> <a href="https://www.youtube.com/watch?v=GYB9zvqGFkU" class="post-tittle">zerlina 华语财经新闻 3。2</a> <span></span>
                            <!-- <p class="">Pidato Trump yang ditunggu diperkirakan akan memberikan dampak bagi logam mulia dan aset l...</p> -->
                            <!-- <p>Pidato Trump yang ditunggu diperkirakan akan memberikan damp...</p> -->
                        </div>
                    </article>
                </div>

            </div>
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection
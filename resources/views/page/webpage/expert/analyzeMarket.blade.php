@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/banner_AREA_PAKAR.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>
<!-- Content -->
<div id="content">
  <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">{{trans('page.menu-home')}}</a></li>
        <li class="active">{{trans('page.menu-analyze-market')}}</li>
      </ol>
    </div>
  </section>
    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <!-- Revenues Sidebar -->
                <!-- Stories -->
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>{{trans('page.menu-analyze-market')}}</h3>
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10">
                    <div class="text-justify">
                        <p>{{trans('page.how-to-analyze-market-para-1')}}</p>
                    </div>
                    <div class="text-center">
                        <img class="img-responsive"
                            src="{{url("/")}}\web\images\webpage\expertarea\cara_menganalisis_pasar_2_800x562.jpg">
                        <br>
                        <br>
                    </div>
                    <div class="text-justify">
                        <br><br>
                        <h5 class="text-capitalize">{{trans('page.how-to-analyze-market-subtitle-1')}}</h5>
                        <br>
                        <p>{{trans('page.how-to-analyze-market-subtitle-1-para-1')}}
                            <br>
                            {{trans('page.how-to-analyze-market-subtitle-1-para-2')}}
                        </p>
                        <br><br>
                        <h5 class="text-capitalize">{{trans('page.how-to-analyze-market-subtitle-2')}}</h5>
                        <br>
                        <p>{{trans('page.how-to-analyze-market-subtitle-2-para-1')}}<br>
                            {{trans('page.how-to-analyze-market-subtitle-2-para-2')}}
                        </p>
                        <br><br>
                        <h5 class="text-capitalize">{{trans('page.how-to-analyze-market-subtitle-3')}}</h5>
                        <br>
                        <p>{{trans('page.how-to-analyze-market-subtitle-3-para-1')}}<br>
                        {{trans('page.how-to-analyze-market-subtitle-3-para-2')}}
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection

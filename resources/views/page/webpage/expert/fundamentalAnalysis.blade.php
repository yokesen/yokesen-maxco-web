@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>


.order-list {
list-style: none;
counter-reset: item;
}
.order-list > li {
counter-increment: item;
/* margin-bottom: 5px; */
display: flex;
}
.order-list > li:before {
   margin-right: 10px;
   content: counter(item);
   /* background: lightblue; */
   /* border-radius: 100%; */
   /* color: white; */
   /* width: 1.2em; */
   text-align: center;
   display: inline-block;
   font-weight: 400;
    color: #6f6f6f;
    line-height: 24px;
    font-family: 'Open Sans', sans-serif;
 }

</style>

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
        <div class="container">
            <h4>Analisis Fundamental</h4>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Analisis Fundamental</li>
            </ol>
        </div>
    </div>
</section> -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/banner_AREA_PAKAR.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>

<!-- Content -->
<div id="content">
    <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
        <div class="container">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
                <li class="active">Analisis Fundamental</li>
            </ol>
        </div>
    </section>
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>Analisis Fundamental</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div class="text-justify">
                        <p>Analisa fundamental adalah cara menganalisa pergerakan harga aset di pasar finansial berdasarkan data ekonomi dan berita-berita yang menjadi pusat perhatian pelaku pasar.
                            Trader yang melakukan analisa fundamental forex akan mengamati bagaimana data-data ekonomi seperti inflasi dan pengangguran mempengaruhi penilaian baik atau buruknya prospek ekonomi suatu negara, kemudian meneliti bagaimana efeknya terhadap nilai tukar mata uang.
                        </p>
                        <br>
                        <h6 class="text-capitalize">Contoh Analisa Fundamental Forex</h6>
                        <br>
                        <ol class="dt-sc-fancy-list blue decimal">
                            <li class="maxco-text-justify">
                                <p>1. Rencana kenaikan suku bunga bank sentral AS (Fed Funds
                                    Rate/FFR) biasanya mendorong penguatan Dolar AS. Namun, agar
                                    bank sentral dapat menaikkan suku bunga, dibutuhkan inflasi
                                    sesuai target, tingkat pengangguran rendah, dan pertumbuhan
                                    ekonomi stabil. Apabila ada indikasi inflasi lesu,
                                    pengangguran meningkat, dan pertumbuhan melambat; maka dapat
                                    berdampak pada gagalnya kenaikan suku bunga, sehingga
                                    pengguna analisa fundamental akan memandang Dolar AS berbias
                                    bearish (sell atas Dolar AS).</p><br><br>
                            </li>
                            <li class="maxco-text-justify">
                                <p>2. Yunani termasuk salah satu negara dalam kesatuan Zona
                                    Euro. Setelah keluar rumor krisis utang pada akhir tahun
                                    2009, Yunani menghadapi ancaman gagal bayar pada awal tahun
                                    2010. Berita-berita seputar krisis utang tersebut membuat
                                    banyak pelaku pasar khawatir kalau bakal meluas ke
                                    negara-negara Uni Eropa lainnya, atau bahkan mengakibatkan
                                    Uni Eropa bubar. Akibatnya, Euro tertekan berat, sebagaimana
                                    nampak pada grafik berikut ini. Euro baru mampu beranjak
                                    setelah para debitur Yunani menyetujui bailout dan
                                    pelonggaran jadwal pembayaran utang berkali-kali. Seorang
                                    trader pengguna analisa fundamental akan cenderung
                                    menganggap Euro bearish (sell atas Euro) selama krisis
                                    Yunani dinilai berisiko besar.</p>
                            </li>
                        </ol>
                        <br>
                        <img class="img-responsive lazyload" data-src="{{url("/")}}\web\images\webpage\expertarea\Analisis_Fundamental_1.png">
                        <br>
                        <br>
                        <h6 class="text-capitalize">
                            Dasar-Dasar Analisa Fundamental Dalam Forex
                        </h6>
                        <br>
                        <p>Dalam analisa fundamental, prospek ekonomi bagus biasanya
                            diterjemahkan sebagai peluang penguatan nilai tukar mata
                            uang. Sedangkan jika perekonomian memburuk maka dianggap
                            dapat melemahkannya. Namun, penafsiran mengenai prospek
                            ekonomi dan dampaknya pada mata uang tak bisa dilakukan
                            secara sembarangan.
                            <br>
                            <br>
                            <img class="img-responsive lazyload" data-src="{{url("/")}}\web\images\webpage\expertarea\Analisis_Fundamental_2.png">
                            <br>
                            <br>
                            <br>
                            Umumnya, trader pemula menganggap bahwa asalkan data ekonomi
                            semakin bagus, maka mata uang pasti menguat. Padahal, ini
                            salah kaprah. Dalam analisa fundamental, perlu dipahami
                            bahwa:
                        </p>

                        <ol class="dt-sc-fancy-list blue decimal">
                            <li class="maxco-text-justify">
                                <br>
                                <br>
                                <h6 class="text-capitalize">1. Penggerak yang mendorong naik-turunnya nilai tukar mata
                                    uang bukanlah data ekonomi, melainkan sikap pelaku pasar.</h6>
                                <br>
                                <p>

                                    Pelaku pasar biasanya sudah memiliki ekspektasi sendiri
                                    sebelum data ekonomi dirilis. Apabila ekspektasi tak
                                    terpenuhi, maka data dianggap buruk. Jadi, trader tak bisa
                                    hanya memperhitungkan data terkini dan data periode
                                    sebelumnya saja. Contohnya, kenaikan suku bunga selayaknya
                                    mendorong penguatan mata uang. Namun, jika katakanlah suku
                                    bunga naik dari 1.0% menjadi 1.15% saja, padahal pasar
                                    mengharapkan kenaikan hingga 1.25%; maka mata uang takkan
                                    menguat.
                                    <p>
                                        <br>
                                        <br>
                                        <img class="img-responsive lazyload" data-src="{{url("/")}}\web\images\webpage\expertarea\Analisis_Fundamental_3.png">
                                        <br>
                                        <br>
                                        <p>
                                            Pada Kalender Forex, biasanya telah dicantumkan data periode
                                            sebelumnya (previous), ekspektasi (forecast), dan data
                                            terkini (actual) yang baru akan terisi setelah laporan
                                            terkait dipublikasikan. Oleh karenanya, Kalender Forex
                                            merupakan sebuah ""senjata"" penting bagi pengguna analisa
                                            fundamental.
                                        </p>
                                        <p>
                                            Kalender Forex biasanya menggolongkan peristiwa penting
                                            (event) menjadi tiga, ditampilkan dalam bentuk simbol kepala
                                            banteng, tanda pentung, atau kode warna. Pada gambar di
                                            atas, simbol tersebut artinya:
                                        </p>
                                        <br>
                                        <ul class="dt-sc-fancy-list orange arrow">
                                            <li>
                                                <p>Kepala Banteng 1: Peristiwa berdampak rendah di pasar forex.
                                                    Biasanya, rilisan ini berpengaruh pada pasar obligasi atau
                                                    saham, sehingga efeknya pada forex cenderung minimal.<p>
                                            </li>
                                            <li>
                                                <p>Kepala Banteng 2: Peristiwa berdampak menengah. Event
                                                    bersimbol ini dapat berdampak besar jika terjadi pada
                                                    hari-hari ketika tak ada berita berdampak tinggi.<p>
                                            </li>
                                            <li><p>Kepala Banteng 3: Peristiwa berdampak tinggi di pasar forex.
                                                Saat-saat mendekati waktu yang tercantum, biasanya
                                                pergerakan harga menjadi sulit ditebak, antara flat
                                                (mendatar/stagnan) atau justru sangat fluktuatif (choppy).
                                                Trader pengguna analisa teknikal sering menghindari pasar
                                                pada momen-momen seperti ini.</p>
                                            </li>
                                        </ul>
                                        <br>
                                        <br>

                            </li>
                            <li class="maxco-text-justify">
                                <h6 class="text-capitalize">2. Pergerakan harga merupakan akumulasi dari sikap pelaku
                                    pasar menanggapi berbagai berita dari banyak negara, bukan
                                    hanya satu laporan saja.</h6><br>
                                <p>
                                    Jadi, seandainya Anda trading pasangan EUR/USD, maka harus
                                    memperhitungkan fundamental global, Amerika Serikat, dan
                                    Zona Euro. Sekedar kabar suku bunga AS meningkat saja takkan
                                    cukup untuk membuat Dolar AS menguat. Apalagi, dalam hal
                                    pengumuman suku bunga AS, faktor yang lebih berpengaruh
                                    biasanya justru pidato para pejabat bank sentral AS, bukan
                                    nominal bunga itu sendiri.
                                </p>
                                <div class="small-gap"></div>
                            </li>
                            <li class="maxco-text-justify">
                                <h6 class="text-capitalize">
                                    3. Sehat-tidaknya sebuah perekonomian ditunjukkan oleh
                                    pertumbuhan secara berimbang dan berkelanjutan, bukan oleh
                                    angka-angka semata.
                                </h6><br>
                                <p>Dengan kata lain, dalam menerjemahkan data-data ekonomi,
                                    tidak ada istilah "semakin rendah, semakin baik" ataupun
                                    "semakin tinggi, semakin baik".</p>
                                <div class="small-gap"></div>
                            </li>
                            <li class="maxco-text-justify">
                                <h6 class="text-capitalize">
                                    4. Setiap kebijakan yang diambil oleh pemerintah dan bank
                                    sentral merupakan <i>"trade-off"</i>, yaitu keputusan yang
                                    diambil dengan mengorbankan suatu aspek untuk mencapai
                                    peningkatan dalam aspek yang lain.
                                </h6><br>
                                <p> Oleh karenanya, tak ada kebijakan yang sempurna tanpa efek
                                    negatif sama sekali. Penilaian atas kebijakan harus berpatok
                                    pada <i>apakah ini akan menarik dana investasi atau tidak?"</i></p>                        
                                    
                            </li>
                        </ol>
                        <div class="small-gap"></div>
                        <h6 class="text-capitalize">
                            Cara Mempelajari Analisa Fundamental Forex
                        </h6>
                        
                        <ul class="dt-sc-fancy-list red arrow margin-top-30 text-justify">
                            <li class="maxco-text-justify">
                                <p>Kenali macam-macam indikator ekonomi dan berita yang
                                    berpengaruh dalam trading forex.</p>
                            </li>
                            <li class="maxco-text-justify">
                                <p>Ketahui mana saja berita-berita paling penting dalam
                                    forex yang berdampak paling besar, khususnya data-data dari
                                    Amerika Serikat. Tandai event-event berdampak tinggi pada
                                    Kalender Forex.</p>
                            </li>
                            <li class="maxco-text-justify">
                                <p>Pahami cara trading forex menggunakan berita.</p>
                            </li>
                            <li class="maxco-text-justify">
                                <p>Apabila Anda berminat untuk menggunakan berita-berita
                                    tadi untuk mendapatkan keuntungan cepat, maka pelajari
                                    strategi News Trading. Namun, jika ingin menerapkan analisa
                                    fundamental layaknya para trader kawakan, maka pelajari cara
                                    benar menganalisis fundamental.</p>
                            </li>
                            <li class="maxco-text-justify">
                                <p> Terakhir, banyak-banyaklah membaca artikel-artikel
                                    bertema ekonomi dan ikutilah berita forex. Seorang trader
                                    pengguna analisa fundamental akan kehilangan arah apabila
                                    tak mengikuti berita-berita ekonomi secara rutin.</p>
                            </li>
                        </ul>                            
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection
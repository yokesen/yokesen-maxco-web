@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
        <div class="container">
            <h4>Tips Pakar hari ini </h4>
            Breadcrumbs
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Tips Pakar hari ini </li>
            </ol>
        </div>
    </div>
</section> -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/BANNER_TRADING_TRICKS_.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>
<!-- Content -->
<div id="content">
  <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Tips Pakar hari ini</li>
      </ol>
    </div>
  </section>
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>Tips Pakar hari ini</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div class="text-justify">
                      <span>
                Berikut adalah tips-tips dari para trader berpengalaman untuk memaksimalkan profit dan meminimalkan kerugian :
                <div class="hr-invisible-very-small"> </div>
                <h5>1. Gunakan stop loss</h5>
                <br>
                Jangan pernah trading tanpa menggunakan order Stop Loss, mengingat hal tersebut dapat menghindarkan kita dari hal-hal yang di luar dugaan.
                <div class="hr-invisible-very-small"> </div>
                <h5>2. Analisis secara fundamental dan teknikal</h5>
                <br>

                Sebagian trader hanya menggunakan analisis teknikal, sementara sebagaian lainnya hanya mengamati fundamental. Gunakanlah keduanya, sehingga kita dapat mengerti kondisi pasar dengan baik.
                <div class="hr-invisible-very-small"> </div>

                <h5>3. Jangan melibatkan emosi</h5>
                <br>
                Trading forex dapat menjadi hal yang menyenangkan sekaligus membingungkan. Ketika menahan emosi, trader tidak akan tergesa-gesa dalam mengambil posisi dan dapat terhindar dari kerugian.
                <div class="hr-invisible-very-small"> </div>

                <h5>4. Hindari over trade</h5>
                <br>
                Janganlah menambah posisi hanya karena merasa adanya kesempatan untuk mendapat profit lebih. Dengan jumlah posisi yang tepat, kita akan mampu menghadapi pasar forex, emas, dan minyak mentah yang volatil.
                <div class="hr-invisible-very-small"> </div>
                <h5>5. Menggunakan strategi money management</h5>
                <br>
                Batasi jumlah kerugian, para expert menyarankan untuk membatasi risiko 3 â€“ 5% dari modal awal dalam satu trade. Risiko berlebihan dapat membahayakan dalam volatilitas tinggi.
                <div class="hr-invisible-very-small"> </div>

                <h5>6. Menggunakan beberapa timeframe</h5>
                <br>
                Jangan hanya terpaku hanya pada satu timeframe tertentu. Jika melihat beberapa timeframe, arah pasar akan nampak lebih jelas. Contoh: membandingkan pergerakan H4 (intraday) dengan D1 (medium term).
                <div class="hr-invisible-very-small"> </div>

                <h5>7. Rancang rencana trading anda</h5>
                <br>
                Penting untuk membuat rencana trading (trading plan), sebagai pedoman trading kita sendiri. ""Jika kita gagal untuk merencanakan, berarti kita berencana untuk gagal"". Trading plan akan membantu dalam penentuan langkah selanjutnya.

                Kini anda sudah mendapatkan tips-tips dari expert trader untuk memaksimalkan profit anda dari trading forex. Nantikan tips-tips trading lainnya di post selanjutnya.
              </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection

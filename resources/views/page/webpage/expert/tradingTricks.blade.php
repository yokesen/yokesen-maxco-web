@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
        <div class="container">
            <h4>Trading Tricks</h4>
            Breadcrumbs
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Trading Tricks</li>
            </ol>
        </div>
    </div>
</section> -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/BANNER_TRADING_TRICKS_.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>
<!-- Content -->
<div id="content">
  <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Trading Tricks</li>
      </ol>
    </div>
  </section>
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>Trading Tricks</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div class="text-justify">
                      <span>
               Belajar Forex Tanpa Modal Untuk Pemula
                 Saat masih dalam tahap belajar, sebaiknya manfaatkan akun trading demo (Demo Account) yang disediakan broker secara cuma-cuma. Berikut ini tiga langkah yang dapat diambil:
                 <div class="hr-invisible-very-small"> </div>

                 <h5>1. Siapkan sarana belajar forex</h5>
                 <br>
                 Persiapkanlah PC/laptop atau ponsel pintar berbasis Android/iOS, serta koneksi internet yang cukup baik. Jika belum memiliki PC atau laptop sama sekali, maka dapat memanfaatkan warnet untuk sementara waktu. Namun, untuk jangka panjang, lebih baik bila Anda memiliki perangkat sendiri, karena gadget pinjaman akan sangat rentan diretas pihak-pihak tak bertanggung jawab. Pastikan juga Anda sudah memiliki alamat email yang aktif, karena broker akan sering mengirim konfirmasi dan laporan transaksi via email.
                 <div class="hr-invisible-very-small"> </div>

                 <h5>2. Pilih broker forex</h5>
                 <br>
                 Kunjungi situs salah satu broker forex. Dalam rangka belajar cara bermain forex untuk pemula, sebenarnya Anda bisa memilih broker mana saja. Akan tetapi, ada baiknya memilih broker forex yang tidak menentukan syarat modal minimum terlalu besar. Selain itu, pastikan broker forex telah mengantongi ijin resmi dari lembaga berwenang (broker forex teregulasi).
                 <div class="hr-invisible-very-small"> </div>

                 <h5>3. Buka akun trading demo</h5>
                 <br>
                 Setelah memilih broker forex tertentu, daftarkan diri atau buka akun trading demo (Demo Account). Ini merupakan rekening trading yang berisi uang virtual (bukan uang sungguhan), tetapi memungkinkan kita untuk melihat kondisi pasar yang sesungguhnya. Akun trading demo memang didesain sebagai media belajar para pemula, sehingga pendaftarannya gratis dan tidak membutuhkan syarat macam-macam. Cukup klik tombol daftar akun demo di situs broker, lalu mengisi formulir yang disediakan. Setelah mendaftar, broker akan mengirim email verifikasi dan notifikasi ke email Anda. Ikuti saja prosesnya, hingga mendapatkan link untuk mengunduh software trading yang biasanya bernama Metatrader.
                 <div class="hr-invisible-very-small"> </div>

                 <h5>4. Unduh platform trading forex</h5>
                 <br>
                 Dari link yang diberikan broker, unduh dan install software ke PC/laptop atau gadget Anda. Inilah salah satu alasan mengapa akan lebih baik jika memiliki perangkat sendiri; karena tentu akan repot jika harus unduh dan install setiap kali berganti perangkat, bukan?
                 Gunakan ID dan password yang sudah dikirimkan broker ke alamat email Anda, untuk masuk ke software trading. Nah, melalui software trading itu, kita dapat melihat dan menganalisis pergerakan nilai tukar mata uang yang dihadirkan dalam bentuk grafik. Misalnya grafik pada pasangan mata uang EUR/USD bergerak naik, maka itu artinya Euro menguat terhadap Dolar AS. Sedangkan bila EUR/USD bergerak turun, maka artinya Dolar AS menguat terhadap Euro. Di sini Anda bisa memahami apa itu forex dengan menyaksikan sendiri pergerakan harga di pasar internasional.

                 Umumnya, broker forex memperbolehkan trader untuk memanfaatkan akun demo tanpa batas waktu tertentu. Apabila dana virtual dalam akun demo sudah habis, Anda juga bisa mengajukan permohonan melalui Customer Service (CS) atau Live Chat broker untuk mengisi ulang dana tersebut. Jadi, akun demo merupakan sarana penting belajar forex tanpa modal untuk pemula.
                 <span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection

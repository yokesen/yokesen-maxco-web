@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/expertarea/banner_AREA_PAKAR.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>
<div id="content">
    <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
        <div class="container">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
                <li class="active">Cara Membaca Candlestick</li>
            </ol>
        </div>
    </section>
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>Cara membaca Candlestick</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">+
                    <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/expertarea/analisis_teknikal_945x498.jpg" alt="">
                    <div class="text-justify">
                        <br>
                        <p>Membaca candlestick tidak semata-mata menghafal dan mengenal formasi-formasinya saja. Banyak buku mereferensikan beratus-ratus pola candlestick, dengan setiap pola memiliki informasi dan keterangan untuk mengetahui apa yang akan terjadi berikutnya di pasar Forex.
                        </p>
                        <p>Nyatanya, menghafal ratusan pola candlestick tidak membuat perbedaan signifikan pada performa trading Anda. Sebut saja Three Black Crows, Concealing Baby Swallow, Unique Three River Bottom. Terlalu banyak, bikin pusing, dan tidak praktis.
                        </p>
                        <p>
                            Sebenarnya, Anda tidak perlu menghafal semua pola untuk analisa candlestick. Anda hanya
                            perlu tahu
                            gambaran besar cara membaca candlestick,
                            karena setiap candle pada dasarnya sudah mampu menginformasikan struktur harga, kekuatan
                            tren,
                            dinamika Buyer melawan Seller,
                            dan proyeksi arah harga akan bergerak nantinya.
                        </p>
                        <p>Empat Elemen Dasar Sebagai Panduan Membaca Candlestick</p>
                        <ul class="dt-sc-fancy-list red arrow text-justify">
                            <li>
                                <p>Langkah 1: Perang Candlestick
                                    Sebelum kita mulai mendalami elemen-elemen penting untuk analisa candlestick, kita
                                    harus punya cara
                                    pandang yang benar terlebih dulu. Anggap saja pergerakan harga itu terjadi karena
                                    perang antara
                                    Buyer dan Seller. Setiap candlestick adalah suatu pertempuran selama masa perang,
                                    dan keempat elemen
                                    candlestick menceritakan siapa yang unggul, siapa yang mundur, siapa memegang
                                    kontrol, dan pihak
                                    mana memiliki peluang lebih besar untuk memenangkan pertempuran berikutnya.</p>

                            </li>
                            <li>
                                <p>
                                    Langkah 2: Pahami Konteks (Gambaran Besar) Saat Analisa Candlestick
                                    Perlu digarisbawahi, candlestick tidak dapat diamati dalam satu pola saja, tanpa
                                    mengetahui dinamika
                                    harga sebelumnya. Analisa candlestick harus dicermati dengan memperhitungkan pula
                                    pergerakan harga
                                    lampau. Karena itu, setiap kali kita coba membaca candlestick atau formasi harga,
                                    kita harus
                                    mempertanyakan beberapa hal berikut:
                                </p>
                                <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50 text-justify">
                                    <li>
                                        <p>
                                            Apakah candlestick terkini ukurannya lebih kecil atau besar dari candle
                                            sebelumnya?
                                        </p>
                                    </li>
                                    <li>
                                        <p>Apakah perubahan ukuran tersebut berarti? </p>
                                    </li>
                                    <li>
                                        <p>Apakah perubahan terjadi saat sesi trading tak aktif? Misalnya, candlestick
                                            pada pasangan-pasangan
                                            mata uang EUR sering mengkerut atau mengecil pada sesi Asia karena volume
                                            trading-nya juga kecil.</p>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                        <p>
                            Poin-poin di atas penting untuk dipegang supaya kita terhindar dari pemikiran sempit yang
                            membatasi
                            pemahaman gambaran besarnya. Berbekal pegangan itu, sekarang kita dapat mengeksplor 4 elemen
                            penting
                            untuk membaca candlestick:
                        </p>
                        <br>
                        <h5 class="text-capitalize">Elemen 1: Ukuran Badan Candlestick</h5>
                        <p>Ukuran badan (body) candlestick adalah poin awal yang bagus karena kita bisa dapat banyak
                            informasi
                            darinya. Panjang badan candlestick menunjukkan kekuatan salah satu pihak. Ukuran badan
                            memanjang
                            berarti menampilkan menguatnya momentum. Saat badan mengecil, berarti momentum juga
                            bertambah pelan.
                            Singkatnya, panjang badan menunjukkan seberapa jauh harga telah bergerak selama durasi
                            candle
                            tersebut (timeframe per candle).</p>
                        <br>
                        <h5 class="text-capitalize">Elemen 2: Panjang Sumbu Candlestick (Wick)</h5>
                        <p>
                            Panjang sumbu candlestick menginformasikan volatilitas pergerakan harga. Sumbu panjang
                            mengindikasikan bahwa harga bergerak cepat selama durasi candlestik terkait, tapi mengalami
                            penolakan karena adanya perlawanan. Jika sumbu bertambah panjang, berarti volatilitas
                            semakin
                            meningkat. Hal ini sering terjadi di akhir sebuah tren, sebelum harga berbalik arah, atau
                            ketika
                            harga mendekati Support Resistance penting.
                        </p>
                        <br>
                        <h5 class="text-capitalize">Elemen 3: Rasio Panjang Badan dan Sumbu</h5>
                        <p>
                            Mulai dari sini kita sudah bisa mendapat gambaran besar dalam membaca candlestick. Mana yang
                            lebih
                            panjang, badan atau sumbu candlestick? Pada saat tren dengan momentum tinggi, Anda akan
                            sering
                            mendapati candlestick berbadan panjang dengan sumbu lebih kecil. Saat pasar sedang dilanda
                            ketidakpastian, volatilitas meningkat sehingga badan candlestick mengecil, tapi sumbunya
                            lebih
                            panjang.
                        </p>
                        <br>
                        <h5 class="text-capitalize">Elemen 4: Posisi Badan Candlestick</h5>
                        <p>
                            Elemen ini merupakan pengembangan dari elemen sebelumnya. Apakah Anda menemukan candlestick
                            bersumbu
                            panjang dengan posisi badan berada di salah satu ujungnya? Hal ini menunjukkan perlawanan.
                            Candlestick dengan posisi badan di tengah-tengah sumbu bawah dan atas mengindikasikan
                            keraguan/ketidakpastian di pasar. Dengan memahami semua elemen dasar di atas, kita dapat
                            membaca
                            candlestick tanpa perlu menghafal bentuk atau namanya satu per satu.
                        </p>
                        <p>
                            Penampilan candlestick pada masing-masing trading platform (MT4, cTrader, Tradingview, dsb.)
                            bisa
                            saja berbeda. Namun dengan memahami 4 elemen dasarnya, kita masih dapat membaca candlestick
                            dengan
                            akurat, bagaimanapun tampilannya pada chart.
                        </p>
                        <p>
                            Contoh Analisa Candlestick Pada Chart
                            Sampai di sini, kita sudah mengupas setiap elemen-elemen dasar untuk analisa candlestick.
                            Sekarang,
                            dengan pengetahuan tersebut kita dapat mengunakannya untuk membedah grafik harga.
                        </p>
                        </span>
                        <br>
                    </div>

                    <div class="text-justify">
                        <br>
                        <h5 class="text-capitalize">Contoh #1</h5>
                        <p>
                            Perhatikan arah pergerakan harga pada chart di bawah ini. Berikut adalah uraian mengenai
                            informasi pergerakan harga berdasarkan analisa candlestick:
                            <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50 text-justify">
                                <li>
                                    <p>Selama Downtrend, candlestick menampilkan badan merah panjang dengan sumbu kecil
                                        atau tak bersumbu sama sekali. Artinya, momentum Bearish
                                        masih kuat.</p>
                                </li>
                                <li>
                                    <p>
                                        Di posisi bawah, kita menemukan penolakan. Satu candle saja belum cukup untuk
                                        memastikan sinyal pembalikan arah. Reversal baru terkonfirmasi ketika harga ditutup
                                        lebih tinggi
                                        daripada pembukaan candle.
                                    </p>
                                </li>
                            </ul>
                        </p>
                        <div class="text-center">
                            <br><br>
                            <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/expertarea/candle_stick_contoh_1.jpg" alt="">
                            <br><br>
                        </div>
                        <br>
                        <h5 class="text-capitalize">Contoh #2</h5>
                        <p> Pada contoh di bawah, harga memperlihatkan kondisi pasar sideways. Beginilah cara kita
                        membaca candlestick dalam kondisi tersebut:</p>
                        <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50 text-justify">
                            <li><p>Harga terjun ke bawah di sisi kiri dengan candle Bearish kuat dan tiada satupun Bullish candle di tengah-tengahnya.</p></li>
                            <li><p>Berikutnya, panjang badan candlestick mulai mengerut, tapi ekornya semakin panjang. Artinya, momentum sedang melemah. </p></li>
                            <li><p>Harga kembali mengarah ke titik Support sebelumnya, dan sekarang berubah menjadi Resistance. Candlestick menampilkan penolakan pada titik tersebut.</p></li>
                            <li><p>Saat harga mendekati titik Support di bawah, badan candle semakin mengecil dan ekor semakin sering muncul. Ini merupakan indikasi keraguan pasar. Artinya, kecil kemungkinan harga untuk menembus Support. </p></li>
                            <li><p>Sebelum harga terjun menembus Support, harga tampil membentuk barisan candlestick Bearish saja, yang berarti momentum menurun juga semakin kencang.</p></li>
                        </ul>
                        <div class="text-center">
                            <br><br>
                            <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/expertarea/candle_stick_contoh_2.jpg" alt="">
                            <br><br>
                        </div>
                        <br>
                        <h5 class="text-capitalize">Contoh #3</h5>
                        <p> Pada contoh terakhir, terbentuk pola candlestick klasik di akhir tren. Pola candlestick
                        tersebut
                        merupakan landasan untuk menentukan kapan harga akan berbalik arah. Selama Uptrend, analisa
                        candlestick menyorot badan candle yang terlihat jelas memanjang dan sumbunya mengecil.
                        Berikutnya,
                        muncul dua candlestick dengan ekor memanjang ke bawah. Indikasinya, harga berusaha bergerak
                        ke
                        bawah, tapi tekanan Seller masih belum cukup kuat. Setelah aksi sell-off gagal tadi, badan
                        candlestick semakin mengecil, sehingga mencerminkan bahwa tren sudah mulai kehilangan
                        momentumnya.
                        Dari situ muncullah candlestick Bearish balasan dengan ukuran badan panjang, yang
                        mengonfirmasikan
                        pembalikan arah untuk menurun.</p>
                       
                        <div class="text-center">
                            <br><br>
                            <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/expertarea/candlestick_contoh_3.jpg" alt="">
                            <br><br>
                        </div>
                        <br>
                        <h5>Kesimpulan</h5>
                        <p>
                        Anda Tidak Butuh Menghafal Pola Candlestick Dengan artikel ini, Anda sudah bisa memahami arti dari masing-masing candlestick tanpa perlu menghafal nama dan formasi setiap pola candlestick.
                        Perlu dicatat, untuk menjadi trader profesional kita harus mengembangkan strategi di luar kebiasaan pasar dan menghindari kesalahan-kesalahan umum dalam bertrading.
                        </p>
                        <p>
                        Intinya, dengan mempelajari dasar-dasar elemennya, Anda dapat membaca candlestick dengan lebih sederhana dan akurat, seperti mengetahui perbandingan kekuatan antara Buyer melawan Seller, siapa yang dominan, dan pihak mana yang sedang tertekan.
                        Jika Anda memahami hal-hal tersebut, maka membaca pasar dan memperkirakan arah harga selanjutnya akan jauh lebih mudah, ketimbang harus menghafal satu per satu pola candlestick yang ada.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<!-- always on -->
@include('page.template.always_on')

@endsection
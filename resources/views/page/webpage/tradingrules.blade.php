@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')

<!-- Content -->
<div id="content">
    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <!-- Revenues Sidebar -->
                <!-- Stories -->
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h4>Peraturan dan Ketentuan Perdagangan MAXCO FUTURES</h4>
                        <h4 class="text-muted">Maxco Futures Trading Rules</h4>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10 col-sm-12 text-center">
                    @if($agent->isMobile())
                        <a href="{{url('/')}}/documents/Maxco_CFD_Trading Rules-Indonesia.pdf" class="btn btn-1 margin-top-10" style="min-width: 180px;margin-bottom:5px;" download>Download <i class="fa fa-download fa-lg"></i></a>
                    @else
                    <iframe src="{{url('/')}}/documents/Maxco_CFD_Trading Rules-Indonesia.pdf" width="100%" height="800px">
                    </iframe>
                    @endif
                </div>
            </div>
        </div>

    </section>
</div>

<!-- always on -->
@include('page.template.always_on')

@endsection
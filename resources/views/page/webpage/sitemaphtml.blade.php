@extends('page.template.master_homepage')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('content')
<section class="sub-bnr" style="background-image: url({{url('/')}}/web/images/webpage/smartmaxco_sitemap_red.jpg) !important; background-repeat: no-repeat !important; background-attachment: initial !important; background-origin: initial !important; background-clip: initial !important; background-color: initial !important; background-size: cover !important; background-position: 0% -64px;" data-stellar-background-ratio="0.5">
</section>
<section style="
    background: #f5f5f5;
    padding: 2px;
    ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">{{trans('page.menu-home')}}</a></li>
        <li class="active">Sitemap</li>
      </ol>
    </div>
  </section>
<section class="job padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="col-md-6">
            <div class="job-sider-bar bg-transparent no-border-bottom">
                <h6><a href="{{route('index')}}">{{trans('page.menu-home')}}</a></h6>

                <h6>{{trans('page.menu-about-us')}}</h6>
                <ul class="cate result ml-15">
                    <li> <a href="{{route('companyProfile')}}">{{trans('page.menu-company-profile')}}</a> </li>
                    <li> <a href="{{route('legality')}}">{{trans('page.menu-legality')}}</a> </li>
                    <li> <a href="{{route('officeLocation')}}">{{trans('page.menu-our-location')}}</a> </li>
                </ul>
                <h6>{{trans('page.menu-about-futures')}}</h6>
                <ul class="cate result ml-15">
                    <li> <a href="{{route('futuresTrading')}}">{{trans('page.menu-futures-trading')}}</a> </li>
                    <li> <a href="{{route('futuresRules')}}">{{trans('page.menu-regulation')}}</a> </li>
                </ul>
                <h6>{{trans('page.menu-trading')}}</h6>
                <ul class="cate result ml-15">
                    <!-- By Type-->
                    <h6>{{trans('page.menu-products')}}</h6>
                    <ul class="cate result ml-15">
                        <li> <a href="{{route('marketForex')}}">{{trans('page.menu-forex-trading')}}</a> </li>
                        <li> <a href="{{route('marketIndex')}}">{{trans('page.menu-index-trading')}}</a> </li>
                    </ul>

                    <h6>{{trans('page.menu-trading-account')}}</h6>
                    <ul class="cate result ml-15">
                        <li> <a href="{{route('tradingAccountTtype')}}">{{trans('page.menu-account-type')}}</a> </li>
                        <li> <a href="{{route('tradinghours')}}">{{trans('page.menu-market-trading-hours')}}</a> </li>

                    </ul>
                </ul>
                <h6>{{trans('page.menu-transaction-platform')}}</h6>
                <ul class="cate result ml-15">
                    <li> <a href="{{route('maxcoMobile')}}">{{trans('page.menu-maxco-mobile')}}</a> </li>
                    <li> <a href="{{route('platformDownload')}}">{{trans('page.menu-metatrader')}}</a> </li>
                </ul>
                <h6>
                    @if(session()->has('user.token'))
                    <a class="" href="{{route('profile-trading-tool')}}">Trading Tools</a>
                    @else
                    <a class="" href="{{route('login')}}">{{trans('page.menu-expert-area')}}</a>
                    @endif
                </h6>



            </div>
        </div>
        <div class="col-md-6">
            <div class="job-sider-bar bg-transparent no-border-bottom">
                <h6>Blogs</h6>
                <ul class="cate result ml-15">
                    @foreach($blogs as $blog)
                    <li> <a href="{{url('/').'/'.App::getLocale().'/blogs/single/'. $blog->blogSlug }}">{{$blog->blogTitle}}</a> </li>

                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection
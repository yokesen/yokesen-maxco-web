@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr bnr-2" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h4>Legality</h4>
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li class="active">Legality</li>
        </ol>
      </div>
    </div>
  </section>

  <!-- Content -->
  <div id="content">

    <!-- WHO WE ARE -->
    <section class="team team-wrap padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Pentingnya Legalitas</h3>
              </div>
          </div>
            <div class="col-md-offset-2 col-md-8">
              <p class="text-justify">
                  <!-- <strong> Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus.</strong> <br> -->
                  <br>
                  Sebagai negara hukum tentu keberadaan legalitas trading forex dan pialang di Indonesia menjadi hal yang tidak boleh diabaikan ketika anda akan melakukan sebuah investasi.
                  Tentu jika anda sembarangan dalam mempercayakan investasi, maka ada kemungkinan kehilangan modal Anda karena keamanan modal Anda tidak di jamin oleh pemerintah.
                  Sehingga memastikan legalitasnya sebelum memberikan investasi sangatlah penting untuk anda lakukan.
                  Legalitas yang dimaksudkan disini adalah mengenai izin pialang dan tentunya wakil pialang resmi dari BAPPEBTI (Badan Pengawas Perdagangan Berjangka dan Komoditi),
                  ini menjadi sangat penting karena kalau seorang investor sampai bertransaksi dengan perusahaan pialang yang tidak dijamin keberadaannya oleh pemerintah maka tidak akan ada perlindungan bagi investor.
                  <br>
                  <br>
                  <p>

            </div>
        </div>
        <!-- Work Filter -->

        <ul class="filter team-filter">
          <li class="tab-title filter-item"><a class="active" href="#." data-filter="*">All</a></li>
          <li class="filter-item"><a href="#." data-filter=".perijinan">perijinan</a></li>
          <li class="filter-item"><a href="#." data-filter=".sertifikat">sertifikat</a></li>
        </ul>
        <div class="">
          <ul class="row items">

            <!-- certificat -->
            <li class="col-md-3 item sertifikat">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/aboutus/perijinan/certificate_of_membership.png" alt="">
              </article>
            </li>

            <!-- perijinan -->
            <li class="col-md-3 item perijinan">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/aboutus/perijinan/izin_usaha_pialang_berjangka.jpg" alt="">
              </article>
            </li>

            <!-- perijinan -->
            <li class="col-md-3 item perijinan">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/aboutus/perijinan/perubahan_nama_1.jpg" alt="">
              </article>
            </li>

            <!-- perijinan -->
            <li class="col-md-3 item perijinan">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/aboutus/perijinan/perubahan_nama_2.jpg" alt="">
              </article>
            </li>


            <!-- perijinan -->
            <li class="col-md-3 item perijinan">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/aboutus/perijinan/surat_persetujuan_anggota_bursa_(SPAB).jpg" alt="">
            </article>
          </li>


          </ul>
        </div>

        <div class="row">
          <!-- Revenues Cart -->
         <div class="col-md-12">
           <div class="heading">
             <h4>introducing media relations</h4>
             <br>
             <p>Media Relations is responsible for the relationship with global media when it comes to their editorial content. We are the first point of contact for journalists, reporters and other media workers to provide relevant and timely information.</p>
           </div>

           <!-- Portfolio -->
           <section class="portfolio padding-top-0 padding-bottom-20">
             <!-- PORTFOLIO ROW -->
             <div class="ajax-work">
               <!-- ITEMS -->
               <div class="cbp-item col-sm-3 ana dial growth">
                 <article class="item"><img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/media-img-1.jpg" alt="" >
                   <!-- Hover -->
                   <div class="over-detail">
                     <!-- Link -->
                     <div class="top-detail"> <a href="{{url('/')}}/web/ajax-work/project1.html" class="cbp-singlePage"><i class="fa fa-link"></i> </a> <a href="images/portfolio/img-1.jpg" class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a> </div>
                   </div>
                 </article>
               </div>

               <!-- ITEMS -->
               <div class="cbp-item col-sm-3  ana dial sec storage">
                 <article class="item"><img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/media-img-2.jpg" alt="" >
                   <!-- Hover -->
                   <div class="over-detail">
                     <!-- Link -->
                     <div class="top-detail"> <a href="{{url('/')}}/web/ajax-work/project1.html" class="cbp-singlePage"><i class="fa fa-link"></i> </a> <a href="{{url('/')}}/web/images/media-img-2.jpg" class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a> </div>
                     <!-- TITTLE HEADING -->

                   </div>
                 </article>
               </div>



               <!-- ITEMS -->
               <div class="cbp-item col-sm-3 ana dial direc">
                 <article class="item"><img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/media-img-6.jpg" alt="" >
                   <!-- Hover -->
                   <div class="over-detail">
                     <!-- Link -->
                     <div class="top-detail"> <a href="{{url('/')}}/web/ajax-work/project1.html" class="cbp-singlePage"><i class="fa fa-link"></i> </a> <a href="{{url('/')}}/web/images/media-img-6.jpg" class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a> </div>
                   </div>
                 </article>
               </div>
               <div class="cbp-item col-sm-3 ana dial direc">
                 <article class="item"><img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/media-img-7.jpg" alt="" >
                   <!-- Hover -->
                   <div class="over-detail">
                     <!-- Link -->
                     <div class="top-detail"> <a href="{{url('/')}}/web/ajax-work/project1.html" class="cbp-singlePage"><i class="fa fa-link"></i> </a> <a href="{{url('/')}}/web/images/media-img-7.jpg" class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a> </div>
                   </div>
                 </article>
               </div>
               <div class="cbp-item col-sm-6 ana dial direc">
                 <article class="item"><img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/media-img-8.jpg" alt="" >
                   <!-- Hover -->
                   <div class="over-detail">
                     <!-- Link -->
                     <div class="top-detail"> <a href="{{url('/')}}/web/ajax-work/project1.html" class="cbp-singlePage"><i class="fa fa-link"></i> </a> <a href="{{url('/')}}/web/images/media-img-8.jpg" class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a> </div>
                   </div>
                 </article>
               </div>
               <div class="cbp-item col-sm-6 ana dial direc">
                 <article class="item"><img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/media-img-9.jpg" alt="" >
                   <!-- Hover -->
                   <div class="over-detail">
                     <!-- Link -->
                     <div class="top-detail"> <a href="{{url('/')}}/web/ajax-work/project1.html" class="cbp-singlePage"><i class="fa fa-link"></i> </a> <a href="{{url('/')}}/web/images/media-img-9.jpg" class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a> </div>
                   </div>
                 </article>
               </div>
             </div>
           </section>
         </div>
        </div>
      </div>


    </section>
  </div>
@endsection

@extends('page.template.master_blog')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')

<section class="blog blog-pages padding-top-70 padding-bottom-70">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        @if($_GET['search'])
          <h5 class="side-tittle">{{$_GET['search']}} - search results</h5>
        @endif
        
        @if($news->count()>0)

            @foreach ($news as $key => $isi)
            <!-- Post -->
            <article>
              <img class="img-responsive lazyload" data-src="{{url('/').'/'.$isi->blogImage1}}" alt="">
              <!-- Date -->
              <div class="date"> {{date('d',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}} <span>{{date('M',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}}</span> </div>
              <!-- Detail -->
              <div class="post-detail"> <a href="/blogs/single/{{$isi->blogSlug}}" class="post-tittle">{{$isi->blogTitle}}</a> <span>By Admin / 2 Comments</span>
                <p>{{str_limit($isi->blogMetaDescription,50)}}</p>
                <!-- Tags -->
                <ul class="tags">
                  <?php $keywords = explode(" ", $isi->blogMetaKeywords); ?>
                  @foreach ($keywords as $nes)
                  <li><a href="{{route('blogIndex').'?search='.$nes}}">{{$nes}}</a></li>
                  @endforeach
                </ul>
              </div>
            </article>
            @endforeach

            <!-- Pagination -->
            <ul class="pagination">
              @if($news->previousPageUrl())
                <li><a href="{{$news->previousPageUrl()}}{{$_GET['search']?'&search='.$_GET['search']:''}}"><i class="fa fa-angle-left"></i></a></li>
              @endif
              @for ($i = 1; $i < ($news->lastPage()+1); $i++)
              <li><a href="{{$news->url($i)}}{{$_GET['search']?'&search='.$_GET['search']:''}}"
                class="@if($news->currentPage()==$i) current @endif"
              >{{$i}}</a></li>
              @endfor
              @if($news->nextPageUrl())
                <li><a href="{{$news->nextPageUrl()}}{{$_GET['search']?'&search='.$_GET['search']:''}}"><i class="fa fa-angle-right"></i></a></li>
              @endif
            </ul>
        @else
          @if($_GET['search'])
            <h4>No results for your search</h4>
          @endif
        @endif


      </div>
      <div class="col-md-4">
        <div class="side-bar">
          <!-- Recent Posts -->
          <h5 class="side-tittle">Latest Posts </h5>
          <ul class="papu-post">
          @foreach ($latest as $key => $isi)
          <li class="media">
              <div class="media-left"> <a href="/blogs/single/{{$isi->blogSlug}}"> <img class="media-object" src="{{url('/')}}/{{$isi->blogImage1}}" alt=""></a><span>{{date('d M',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}}</span> </div>
              <div class="media-body"> <a class="media-heading" href="/blogs/single/{{$isi->blogSlug}}">{{$isi->blogTitle}} </a>
              </div>
            </li>
          @endforeach

          </ul>

          <!-- Categories -->
          <!-- <h5 class="side-tittle margin-top-50">Categories </h5>
          <ul class="cate">
            <li><a href="#.">Creative</a></li>
            <li><a href="#."> Design</a></li>
            <li><a href="#."> Development</a></li>
            <li><a href="#."> Search Engine Optimization</a></li>
            <li><a href="#."> Uncategorized</a></li>
          </ul> -->

          <!-- ADD -->
          <!-- <div class="add margin-top-50"> <img class="img-responsive lazyload" data-src="images/add-img.jpg" alt=""> </div> -->

          <!-- Tags -->
          <!-- <h5 class="side-tittle margin-top-50">Tags </h5>
          <ul class="tags">
            <li><a href="#.">Amazing </a></li>
            <li><a href="#.">Envato </a></li>
            <li><a href="#.">Themes </a></li>
            <li><a href="#.">Responsiveness </a></li>
            <li><a href="#.">SEO </a></li>
            <li><a href="#.">Mobile </a></li>
            <li><a href="#.">IOS </a></li>
            <li><a href="#.">Design </a></li>
            <li><a href="#.">Flat </a></li>
          </ul> -->
        </div>

        <!-- Archives -->
        <!-- <h5 class="side-tittle margin-top-50">Archives </h5>
        <ul class="cate">
          <li><a href="#."> May 2015</a></li>
          <li><a href="#."> April 2015</a></li>
          <li><a href="#."> March 2015</a></li>
          <li><a href="#."> February 2015</a></li>
          <li><a href="#."> January 2015</a></li>
        </ul> -->
      </div>
    </div>
  </div>
</section>
<!-- always on -->

@endsection

@section('jsonpage')
<script>

</script>
@endsection
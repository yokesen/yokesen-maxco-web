@extends('page.template.master_blog')

<?php  
  $newdes = str_replace("</p>","|",$isi->blogContent);
  $paragraph = html_entity_decode(strip_tags($newdes));
  $paragraph = explode('|', $paragraph);
?>
@section('title',$isi->blogTitle)
@section('og-title',$isi->blogTitle)
@section('og-image',url('/').'/'.$isi->blogImage1)
@section('og-description',substr($paragraph[0],0,150))
@section('meta-keyword',$isi->blogMetaKeywords)

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')


<section class="blog blog-pages blog-single padding-top-70 padding-bottom-70">
  <div class="container">
    <div class="row">
      <div class="col-md-8">

        <!-- Post -->
        <article> <img class="img-responsive lazyload" data-src="{{url('/').'/'.$isi->blogImage1}}" alt="" decoding="async">
          <!-- Date -->
          <div class="date"> {{date('d',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}} <span>{{date('M',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}}</span> </div <!-- Detail -->
          <div class="post-detail">
            <h5 class="font-normal">{{$isi->blogTitle}}</a> <span>By {{$isi->name}} 
              <!-- / 2 Comments -->
            </h5>
            <div class="text-justify">
            {!!$isi->blogContent!!}
            </div>

            <!-- Tags -->
            <ul class="tags">
              <?php $keywords = explode(" ", $isi->blogMetaKeywords); ?>
              @foreach ($keywords as $nes)
              <li><a href="#.">{{$nes}}</a></li>
              @endforeach

            </ul>
            <img class="img-responsive lazyload" data-src="images/share-img.jpg" alt="">
          </div>
        </article>

        <!--=======  COMMENTS =========-->
        <div class="comments margin-top-80">
          <h5 class="font-normal margin-bottom-40">Comments</h5>


          <!--=======  LEAVE COMMENTS =========-->
          <hr class="main">
          <form id="maxco_blog_comment_form">
            <ul class="row">
              <li class="col-sm-6">
                <label>
                  <input type="text" class="form-control" name="name" placeholder="Name">
                </label>
              </li>
              <li class="col-sm-6">
                <label>
                  <input type="text" class="form-control" name="email" placeholder="Email">
                </label>
              </li>
              <li class="col-sm-12">
                <label>
                  <textarea class="form-control" name="comment" placeholder="Comment"></textarea>
                </label>
              </li>
              <li class="col-sm-12">
                <button type="submit" class="btn btn-1 margin-top-20">Post comment <i class="fa fa-caret-right"></i></button>
              </li>
            </ul>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <div class="side-bar">
          <!-- Recent Posts -->
          <h5 class="side-tittle">Latest Posts </h5>
          <ul class="papu-post">
            @foreach ($list as $key => $isi)
            <li class="media">
              <div class="media-left"> <a href="/blogs/single/{{$isi->blogSlug}}"> <img class="media-object" src="{{url('/').'/'.$isi->blogImage1}}" alt="" decoding="sync"></a><span>{{date('d M',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}}</span> </div>
              <div class="media-body"> <a class="media-heading" href="/blogs/single/{{$isi->blogSlug}}">{{$isi->blogTitle}} </a>
              </div>
            </li>
            @endforeach

          </ul>

          <!-- Categories -->
          <!-- <h5 class="side-tittle margin-top-50">Categories </h5>
          <ul class="cate">
            <li><a href="#.">Creative</a></li>
            <li><a href="#."> Design</a></li>
            <li><a href="#."> Development</a></li>
            <li><a href="#."> Search Engine Optimization</a></li>
            <li><a href="#."> Uncategorized</a></li>
          </ul> -->

          <!-- ADD -->
          <!-- <div class="add margin-top-50"> <img class="img-responsive lazyload" data-src="images/add-img.jpg" alt=""> </div> -->

          <!-- Tags -->
          <!-- <h5 class="side-tittle margin-top-50">Tags </h5>
          <ul class="tags">
            <li><a href="#.">Amazing </a></li>
            <li><a href="#.">Envato </a></li>
            <li><a href="#.">Themes </a></li>
            <li><a href="#.">Responsiveness </a></li>
            <li><a href="#.">SEO </a></li>
            <li><a href="#.">Mobile </a></li>
            <li><a href="#.">IOS </a></li>
            <li><a href="#.">Design </a></li>
            <li><a href="#.">Flat </a></li>
          </ul> -->
        </div>

        <!-- Archives -->
        <!-- <h5 class="side-tittle margin-top-50">Archives </h5>
        <ul class="cate">
          <li><a href="#."> May 2015</a></li>
          <li><a href="#."> April 2015</a></li>
          <li><a href="#."> March 2015</a></li>
          <li><a href="#."> February 2015</a></li>
          <li><a href="#."> January 2015</a></li>
        </ul> -->
      </div>
    </div>
  </div>
</section>
<!-- always on -->

@endsection

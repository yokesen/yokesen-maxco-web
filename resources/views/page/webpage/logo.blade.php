@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/aboutus/header_profil_perusahaan_1.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>


  <!-- Content -->
  <div id="content">

    <!-- WHO WE ARE -->
    <section class="team team-wrap padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <div class="col-md-12 margin-top-15">
            <div class="plan">
               <ul class="row">
               <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/opt1.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/opt2.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/opt3.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/opt4.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/opt5.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/opt6.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>




                 <!-- starter -->
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/logo_1.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <!-- Basic -->
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/logo_2.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <!-- Basic -->
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/logo_3.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
                 <!-- Basic -->
                 <li class="col-md-6">
                   <article>
                     <div class="" style="background:url({{url('/')}}/web/images/webpage/samplelogo/logo_4.png);    background-size: cover;height: 151px;
    background-position: 50% 50%;">
                     </div>
                   </article>
                 </li>
               </ul>
             </div>
          </div>

        </div>
      </div>
    </section>
  </div>
@endsection

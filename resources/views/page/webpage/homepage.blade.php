@extends('page.template.master_homepage')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style media="screen">
  /* .slider-img{
      max-width: 100%;
      width: 100%;
      height:auto;
      float: left;
      margin-right: -100%;
      position: relative;
      opacity: 0;
      display: block;
      z-index: 1;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center;
    }

    .news-box{
      margin-right: 20px !important;
      display:block;
    }

    .cover{
      width: 150px;
      height: 150px;
      object-fit: cover;
    } */
</style>

@if($agent->isMobile())
    <style>
      .owl-controls{
        margin-top: -20px;
      }
    </style>
@endif
@endsection

@section('content')
<!-- HOME MAIN SLIDER -->
<!-- HOME MAIN SLIDER -->
<section class="home-slide <?php if (session('isMobile')) {
                              echo 'home-slide-mobile';
                            } ?>">
  <ul class="slides">

    <!-- SLIDE 1 -->

    {{-- <li style="background: url({{url('/')}}/web/images/webpage/home-slide/easy_trading_back.png) no-repeat!important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" class="slider-img"> --}}
      {{-- <div class=" --}}
      <?php
      // if (session('isMobile')) {
      //  echo 'home-slide-1-mobile';
      //  } else {
      //  echo 'home-slide-1';
      //  }
      ?>
      {{-- ">
      </div>
    </li> --}}
    <!-- End item -->

    <!-- SLIDE 2 -->
    <li style="background: url({{url('/')}}/web/images/webpage/home-slide/low-comm_bg.png) no-repeat!important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" class="slider-img" onclick="window.location.href='{{url('/')}}/{{App::getLocale()}}/trading-account/account-type'">
      <div class="<?php if (session('isMobile')) {
                    echo 'home-slide-2-mobile';
                  } else {
                    echo 'home-slide-2';
                  } ?>">
      </div>
      <!-- <div class="position-center-center">
        <a href="#." class="btn margin-top-30">Read More <i class="fa fa-caret-right"></i></a>
      </div> -->
    </li><!-- End item -->

    <!-- SLIDE 3 -->
    <li style="background: url({{url('/')}}/web/images/webpage/home-slide/lowspread-bg.png) no-repeat!important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" class="slider-img" onclick="window.location.href='{{url('/')}}/{{App::getLocale()}}/trading-account/account-type'">
      <div class="<?php if (session('isMobile')) {
                    echo 'home-slide-3-mobile';
                  } else {
                    echo 'home-slide-3';
                  } ?>">
        <!-- <div style="height: 100%;width: 1155px;margin-right: auto;margin-left: auto;background: url({{url('/')}}/web/images/webpage/home-slide/lowspread-front.png) no-repeat!important;background-size: cover !important; background-position-y: bottom; background-position-x: center;"> -->
      </div>
    </li><!-- End item -->

    <!-- SLIDE 4 -->
    <li style="background: url({{url('/')}}/web/images/webpage/home-slide/bounty-program-bg.png) no-repeat scroll 50% 0% / cover !important" class="slider-img" onclick="window.location.href='{{url('/')}}/{{App::getLocale()}}/promo/bounty-program'">
      <div style="height: 100%;width: auto;margin-right: auto;margin-left: auto;background: url({{url('/')}}/web/images/webpage/home-slide/bounty-program-front.png) no-repeat scroll 50% 0% / cover !important">
      </div>
    </li>
  </ul>

  <ul class="flex-direction-nav">
    <li class="flex-nav-prev"><a class="flex-prev" href="#"></a></li>
    <li class="flex-nav-next"><a class="flex-next" href="#"></a></li>
  </ul>
  </div>

</section>
<!-- Portfolio -->
<!-- <section class="portfolio light-gray-bg padding-bottom-20">
  <div class="text-center margin-top-50 margin-bottom-50 animate fadeInUp" data-wow-delay="0.4s">
    <div id="ajax-loadMore"> <a href="ajax-work/loadMore1.html" class="cbp-l-loadMore-link btn btn-1" rel="nofollow"> <span class="cbp-l-loadMore-defaultText">OPEN ACCOUNT NOW <i class="fa fa-caret-right"></i></span> <span class="cbp-l-loadMore-loadingText">LOADING... <i class="fa fa-caret-down"></i></span> <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS <i class="fa fa-caret-up"></i></span></a> </div>
  </div>
</section> -->

<!-- Content -->

<div id="content">
  {{-- <section class="portfolio padding-bottom-20">
    <div class="container">
      <div class="heading text-center">
        <h3>Smart Maxco Apps</h3>
      </div>
      <div class="list-style-featured">
        <div class="row no-margin">
          @include('page.template.whymaxco_sec1')
        </div>
      </div>
  </section> --}}
  {{-- <section class="light-gray-bg portfolio padding-bottom-20">
    <div class="container">
      <div class="col-md-12 text-center">
        <br>
        <br>
        @include('page.template.whymaxco_sec2')
      </div>
    </div>
  </section> --}}
  <section class="portfolio padding-bottom-20">
    <div class="container">
      @include('page.template.whymaxco_sec3')
    </div>
</div>
</div>
</section>

<!-- SERVICES -->
<section class="services padding-top-70 padding-bottom-70">
  <div class="container">

    <!-- Heading -->
    <div class="heading text-center">
      <h3>{{trans('page.home-why-maxco')}}</h3>
    </div>
  </div>
  <div class="best-services">
    <!-- Row -->
    <div class="container">
      <ul class="row list">
        <!-- Analytics -->
        <li class="col-md-4" data-content="#colio_c1">
          <article class="thumb"> <a class="button colio-link" href="#"> <i class="fa fa-user"></i>
              <h5 style="min-height:35px;">{{trans('page.home-why-maxco-customer-first')}}</h5>
              <p style="min-height:72px;">{{trans('page.home-why-maxco-customer-first-desc')}}</p>
            </a> </article>
        </li>

        <!-- Storage -->
        <li class="col-md-4" data-content="#colio_c2">
          <article class="thumb"><a class="button colio-link" href="#"> <i class="fa fa-balance-scale"></i>
              <h5 style="min-height:35px;">{{trans('page.home-why-maxco-integrity')}}</h5>
              <p style="min-height:72px;">{{trans('page.home-why-maxco-integrity-desc')}}</p>
            </a> </article>
        </li>

        <!-- Security -->
        <li class="col-md-4" data-content="#colio_c3">
          <article class="thumb"><a class="button colio-link" href="#"> <i class="fa fa-lightbulb-o"></i>
              <h5 style="min-height:35px;">{{trans('page.home-why-maxco-transparency')}}</h5>
              <p style="min-height:72px;">{{trans('page.home-why-maxco-transparency-desc')}}</p>
            </a> </article>
        </li>

        <!-- Worth -->
        <li class="col-md-4" data-content="#colio_c4">
          <article class="thumb"><a class="button colio-link" href="#"> <i class="fa fa-money"></i>
              <h5 style="min-height: 35px;">{{trans('page.home-why-maxco-low-cost')}}</h5>
              <p style="min-height: 72px;">{{trans('page.home-why-maxco-low-cost-desc')}}</p>
            </a> </article>
        </li>

        <!-- Worth -->
        <li class="col-md-4" data-content="#colio_c5">
          <article class="thumb"><a class="button colio-link" href="#"> <i class="fa fa-users"></i>
              <h5 style="min-height: 35px;">{{trans('page.home-why-maxco-eas')}}</h5>
              <p style="min-height: 72px;">{{trans('page.home-why-maxco-eas-desc')}}</p>
            </a> </article>
        </li>

        <!-- Worth -->
        <li class="col-md-4" data-content="#colio_c6">
          <article class="thumb"><a class="button colio-link" href="#"> <i class="fa fa-check-square-o"></i>
              <h5 style="min-height:35px;">{{trans('page.home-why-maxco-finance')}}</h5>
              <p style="min-height:72px;">{{trans('page.home-why-maxco-finance-desc')}}</p>
            </a> </article>
        </li>
      </ul>
    </div>
  </div>

  <!-- Analytics Tab Open -->
  <div id="colio_c1" class="colio-content">
    <div class="main">
      <div class="container">
        <div class="inside-colio">
          <div class="row">
            <div class="col-md-8">
              <!-- Heading -->
              <div class="heading heading-maxco text-left margin-bottom-40">
                <h4>{{trans('page.home-why-maxco-customer-first')}}</h4>
              </div>
              <p class="text-justify">
                <!-- <strong> One Stop Trading Management oleh Maxco Mobile.</strong> <br> -->
                <br>
                {{trans('page.home-why-maxco-customer-first-detail')}}
                <!-- <a href="#." class="btn btn-1 margin-top-30 margin-right-20">Contact Now <i class="fa fa-caret-right"></i></a>
                  <a href="#." class="btn btn-1 margin-top-30">View Services <i class="fa fa-caret-right"></i></a>  -->
            </div>
            <div class="col-md-4 text-center"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/whymaxco/utamakan_nasabah.jpg" alt=""> </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Storage Tab Open -->
  <div id="colio_c2" class="colio-content">
    <div class="main">
      <div class="container">
        <div class="inside-colio">
          <div class="row">
            <div class="col-md-4 text-center"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/whymaxco/integritas.jpg" alt=""> </div>
            <div class="col-md-8">
              <!-- Heading -->
              <div class="heading heading-maxco text-left margin-bottom-40">
                <h4>{{trans('page.home-why-maxco-integrity')}}</h4>
              </div>
              <p class="text-justify">
                <!-- <strong> Broker resmi dibawah pengawasan BAPPEBTI.</strong> <br> -->
                <br>
                {{trans('page.home-why-maxco-integrity-detail')}}
              </p>
              <!-- <a href="#." class="btn btn-1 margin-top-30 margin-right-20">Contact Now <i class="fa fa-caret-right"></i></a>
                <a href="#." class="btn btn-1 margin-top-30">View Services <i class="fa fa-caret-right"></i></a>  -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Security Tab Open -->
  <div id="colio_c3" class="colio-content">
    <div class="main">
      <div class="container">
        <div class="inside-colio">
          <div class="row">
            <div class="col-md-8">
              <!-- Heading -->
              <div class="heading heading-maxco text-left margin-bottom-40">
                <h4>{{trans('page.home-why-maxco-transparency')}}</h4>
              </div>
              <p class="text-justify">
                <!-- <strong> Terkenal dengan sebutan segregated account.</strong> <br> -->
                <br>
                {{trans('page.home-why-maxco-transparency-desc')}}
              </p>
              <!-- <a href="#." class="btn btn-1 margin-top-30 margin-right-20">Contact Now <i class="fa fa-caret-right"></i></a>
                <a href="#." class="btn btn-1 margin-top-30">View Services <i class="fa fa-caret-right"></i></a>  -->
            </div>
            <div class="col-md-4 text-center"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/whymaxco/transparan.jpg" alt=""> </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Storage Tab Open -->
  <div id="colio_c4" class="colio-content">
    <div class="main">
      <div class="container">
        <div class="inside-colio">
          <div class="row">
            <div class="col-md-8">
              <!-- Heading -->
              <div class="heading heading-maxco text-left margin-bottom-40">
                <h4>{{trans('page.home-why-maxco-low-cost')}}</h4>
              </div>
              <p class="text-justify">
                <br>
                {{trans('page.home-why-maxco-low-cost-detail')}}
              </p>
              <!-- <a href="#." class="btn btn-1 margin-top-30 margin-right-20">Contact Now <i class="fa fa-caret-right"></i></a>
                <a href="#." class="btn btn-1 margin-top-30">View Services <i class="fa fa-caret-right"></i></a>  -->
            </div>
            <div class="col-md-4 text-center"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/whymaxco/biaya_trading_rendah.jpg" alt=""> </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Security Tab Open -->
  <div id="colio_c5" class="colio-content">
    <div class="main">
      <div class="container">
        <div class="inside-colio">
          <div class="row">
            <div class="col-md-4 text-center"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/whymaxco/accept_all_eas.jpg" alt=""> </div>
            <div class="col-md-8">
              <!-- Heading -->
              <div class="heading heading-maxco text-left margin-bottom-40">
                <h4>{{trans('page.home-why-maxco-eas')}}</h4>
              </div>
              <p class="text-justify">
                <!-- <strong> Pakar Professional Maxco menjawab pertanyaan nasabah.</strong> <br> -->
                <br>
                {{trans('page.home-why-maxco-eas-detail')}}
              </p>
              <!-- <a href="#." class="btn btn-1 margin-top-30 margin-right-20">Contact Now <i class="fa fa-caret-right"></i></a>
                <a href="#." class="btn btn-1 margin-top-30">View Services <i class="fa fa-caret-right"></i></a>  -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Security Tab Open -->
  <div id="colio_c6" class="colio-content">
    <div class="main">
      <div class="container">
        <div class="inside-colio">
          <div class="row">
            <div class="col-md-8">
              <!-- Heading -->
              <div class="heading heading-maxco text-left margin-bottom-40">
                <h4>{{trans('page.home-why-maxco-finance')}}</h4>
              </div>
              <p class="text-justify">
                <!-- <strong> Media berbagi informasi mengenai Trading.</strong> <br> -->
                <br>
                {{trans('page.home-why-maxco-finance-detail')}}
              </p>
              <!-- <a href="#." class="btn btn-1 margin-top-30 margin-right-20">Contact Now <i class="fa fa-caret-right"></i></a>
                <a href="#." class="btn btn-1 margin-top-30">View Services <i class="fa fa-caret-right"></i></a>  -->
            </div>
            <div class="col-md-4 text-center"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/webpage/whymaxco/unggul_terpercaya.jpg" alt=""> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="blog padding-top-70 padding-bottom-70">
  <div class="container">
    <!-- Heading -->
    <div class="heading text-center">
      <h4>Latest from the blog</h4>
    </div>
    <!-- Blog Row -->
    <div class="row blog-slide">
      @foreach ($latest as $key => $isi)

      <div class="col-md-12 no-padding">
        <article> <img class="img-responsive lazyload" data-src="{{url('/').'/'.$isi->blogImage1}}" alt="" style="min-width:360px;min-height:240px;">
          <!-- Date -->
          <div class="date"> {{date('d',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}} <span>{{date('M',strtotime(!empty($isi->release_date)?$isi->release_date:$isi->created_at))}}</span> </div>

          <!-- Detail -->
          <div class="post-detail"> <a href="{{url('/')}}/en/blogs/single/{{$isi->blogSlug}}" class="post-tittle">{{$isi->blogTitle}}</a>
              <!-- <span>By Admin / 2 Comments</span> -->
              <p class="">{{str_limit($isi->blogMetaDescription,90)}}</p>
              <!-- <p>{{str_limit($isi->blogMetaDescription,60)}}</p> -->
          </div>
        </article>
      </div>
      @endforeach
    </div>
  </div>
</section>


</div>

@endsection

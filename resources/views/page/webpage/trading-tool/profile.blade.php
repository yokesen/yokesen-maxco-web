@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')

<!-- Content -->
<div id="content">
  <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </div>
  </section>

  <!-- Shop -->
  <section class="shop padding-top-70 padding-bottom-70">
    <div class="container">
      <div class="row">
        @include('page.webpage.trading-tool.sidebar')

        <!-- Shop Items -->
        <div class="col-md-8">
          <div id="accordion">

            <div class="job-content job-post-page">
              <!-- Job Tittle -->
              <div class="panel-group">
                <div class="panel panel-default">
                  <!-- Save -->
                  <div class="star-save"><a href="#."> <i class="fa fa-star"></i></a></div>
                  <!-- PANEL HEADING -->
                  <div class="panel-heading"> <a data-toggle="collapse" data-parent="" href="#job1">
                      <div class="job-tittle">
                        <div class="media-left">
                          <div class="date"> 1 <span>to do</span> </div>
                        </div>
                        <div class="media-body">
                          <h5>Download Smartmaxco App</h5>
                          <span>All in one Aplikasi Trading</span>
                        </div>
                      </div>
                    </a>
                    <p>Instal Smart Maxco App sekarang dan bertransaksilah dengan aman. Hanya dengan satu aplikasi, akses mudah ke pasar dagang dengan update-an berita terkini, pembukaan rekening trading sesuai peraturan Bappebti, Setoran dan Penarikan dana langsung dari gengaman tangan Anda. Ayo tunggu apalagi?</p>
                  </div>

                  <!-- Content -->
                  <div id="job1" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="job-skills">

                        <!-- Additional Requirements -->
                        <h5 class="margin-top-50">Additional Requirements</h5>

                        <!-- Share -->
                        <div class="row share-info">
                          <div class="col-md-4"> <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.forex" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png" class="lazyload" alt=""></a> </div>
                          <div class="col-md-4"> <a href="https://apps.apple.com/id/app/smart-maxco/id1516790315" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png" class="lazyload" alt=""></a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="job-content job-post-page margin-top-20">
              <div class="panel panel-default">
                <!-- Save -->
                <div class="star-save"><a href="#."><i class="fa fa-star"></i></a></div>
                <!-- PANEL HEADING -->
                <div class="panel-heading"> <a data-toggle="collapse" href="#job2">
                    <div class="job-tittle">
                      <div class="media-left">
                        <div class="date"> 2 <span>To do</span> </div>
                      </div>
                      <div class="media-body">
                        <h5>Download Metatrader</h5>
                        <!-- <span>Auto created on register</span> -->
                      </div>
                    </div>
                  </a>
                  <p>Unduh file instalasi ke PC Anda, instal program, periksa instruksi yang muncul di monitor Anda</p>
                </div>

                <!-- ADD INFO HERE -->
                <div id="job2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="job-skills">

                      <!-- Additional Requirements -->
                      <h5 class="margin-top-50">Additional Requirements</h5>

                      <!-- Share -->
                      <div class="row share-info">
                        <div class="col-md-4"> <a href="http://files.metaquotes.net/pt.maxco.futures/mt4/maxcofutures4setup.exe" target="_blank" class="sm-tags"><img src="{{url('/')}}/web/images/webpage/logos/mt4-logo-350x200.png" alt=""></a> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="job-content job-post-page margin-top-20">
              <div class="panel panel-default">
                <!-- Save -->
                <div class="star-save"><a href="#."><i class="fa fa-star"></i></a></div>
                <!-- PANEL HEADING -->
                <div class="panel-heading"> <a data-toggle="collapse" href="#job3">
                    <div class="job-tittle">
                      <div class="media-left">
                        <div class="date"> 3 <span>To do</span> </div>
                      </div>
                      <div class="media-body">
                        <h5>Demo Account</h5>
                        <span>Auto created on register</span>
                      </div>
                    </div>
                  </a>
                  <p>Silahkan cek email yang Anda pakai untuk mendaftar, kami telah mengirimkan detail akun demo ke email Anda.</p>
                </div>

                <!-- ADD INFO HERE -->
                <div id="job3" class="panel-collapse collapse">

                </div>
              </div>
            </div>

            <div class="job-content job-post-page margin-top-20">
              <div class="panel panel-default">
                <!-- Save -->
                <div class="star-save"><a href="#."><i class="fa fa-star"></i></a></div>
                <!-- PANEL HEADING -->
                <div class="panel-heading"> <a data-toggle="collapse" href="#job4">
                    <div class="job-tittle">
                      <div class="media-left">
                        <div class="date"> 4 <span>To do</span> </div>
                      </div>
                      <div class="media-body">
                        <h5>Real Account</h5>
                        <span>Sesuai peraturan Bappebti</span>
                      </div>
                    </div>
                  </a>
                  <p></p>
                </div>

                <!-- ADD INFO HERE -->
                <div id="job4" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="job-skills">

                      <!-- Additional Requirements -->
                      <h5 class="margin-top-50">Link register</h5>

                      <!-- Share -->
                      <div class="row share-info">
                        <div class="col-md-4"> <a href="{{env('CABINET_URL')}}login-from-web?webtoken={{Session::get('user.tokencabinet')}}&cfduid={{Cookie::get('__cfduid')}}" target="_blank" class="sm-tags">Buka Akun Sekarang</a> </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>



            </div>
          </div>
        </div>
      </div>
  </section>

</div>


<!-- always on -->
@include('page.template.always_on')

@endsection

@section("jsonpage")

@endsection
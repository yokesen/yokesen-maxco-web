@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')

<!-- Content -->
<div id="content">
  <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Trading Tools</li>
      </ol>
    </div>
  </section>

  <!-- Shop -->
  <section class="shop padding-top-70 padding-bottom-70">
    <div class="container">
      <div class="row">
        @include('page.webpage.trading-tool.sidebar')


        <!-- Shop Items -->
        <div class="col-md-8">
          <h4>Metatrader</h4>
          <!-- Products -->
          <div class="products">
            <div class="row">
              <div class="col-sm-4">
                <article>
                  <div class="item-img" style="min-height: 166px;padding: 13px;"> <img src="{{url('/')}}/web/images/webpage/logos/mt4-logo-350x200.png" alt="" style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);"> </div>
                  <a href="#." class="tittle">Metatrader 4</a>

                  <a href="http://files.metaquotes.net/pt.maxco.futures/mt4/maxcofutures4setup.exe" target="_blank" class="btn btn-1 btn-sm margin-top-10">Download<i class="fa fa-caret-right"></i></a>
                </article>
              </div>
            </div>
          </div>
          <h4>Robot Trading</h4>
          <!-- Products -->
          <div class="products">
            <div class="row">
              @foreach ($tools as $tool)
              <div class="col-sm-4">
                <article>
                  <div class="item-img"> <img src="{{url('/')}}{{$tool->toolImage}}" alt=""> </div>
                  <a href="#." class="tittle">{{$tool->toolName}}</a>

                  <a href="{{$tool->toolLink}}" class="btn btn-1 btn-sm margin-top-10">Download<i class="fa fa-caret-right"></i></a>
                </article>
              </div>
              @endforeach

            </div>


          </div>
        </div>
      </div>
    </div>
  </section>

</div>


<!-- always on -->
@include('page.template.always_on')

@endsection

@section("jsonpage")

@endsection
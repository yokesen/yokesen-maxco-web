<div class="col-md-4">

  <!-- Sdie Bar -->
  <div class="job-sider-bar">
    <h5 class="side-tittle">Welcome {{Session::get('user.name')}}</h5>
    <a href="{{env('CABINET_URL')}}login-from-web?webtoken={{Session::get('user.tokencabinet')}}&cfduid={{Cookie::get('__cfduid')}}" class="btn btn-1 btn-sm margin-top-10 bg-paninblue" target="_blank">Client Area (Real Account)<i class="fa fa-caret-right"></i></a>

    <h5 class="side-tittle margin-top-50">Main Menu</h5>
    <li class="media">
      <div class="media-body"> > <a href="{{route('profile-trading-tool')}}">My Profile</a></div>
    </li>
    <li class="media">
      <div class="media-body"> > <a href="{{route('getsmartmaxcoapps')}}">Smart Maxco App</a></div>
    </li>

    <h5 class="side-tittle margin-top-50">Trading Tools</h5>
    <li class="media">
      <div class="media-body"> > <a href="{{route('trading-tool-robot')}}" style="
    background-color: #017dc7;
    padding: -15px;
    color: white;">Robot Trading</a>
      </div>
    </li>
    <li class="media">
      <div class="media-body"> > <a href="#.">Sinyal Trading</a>
      </div>
    </li>
    <li class="media">
      <div class="media-body"> > <a href="#.">Analisa Terkini</a>
      </div>
    </li>
    <h5 class="side-tittle margin-top-50">Promotions</h5>
    <li class="media">
      <div class="media-body"> > <a href="{{route('commission80percent')}}" target="_blank">Discount Commission 80%</a>
      </div>
    </li>
    <li class="media">
      <div class="media-body"> > <a href="{{route('commission80percent')}}" target="_blank">Discount Spread 80%</a>
      </div>
    </li>
    {{-- <li class="media">
      <div class="media-body"> > <a href="#.">Bounty Program</a>
      </div>
    </li> --}}
    <h5 class="side-tittle margin-top-50">News</h5>
    <li class="media">
      <div class="media-body"> > <a href="{{route('blogIndex')}}" target="_blank">Education</a>
      </div>
    </li>
    <li class="media">
      <div class="media-body"> > <a href="{{route('blogIndex')}}" target="_blank">Analysis</a>
      </div>
    </li>
    <hr>
    <a href="{{route('logout')}}" class="btn btn-1 btn-sm margin-top-10 bg-paninred">Logout <i class="fa fa-caret-right"></i></a>
  </div>
</div>

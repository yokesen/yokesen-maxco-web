@extends('page.template.master_homepage')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('content')
<section class="padding-bottom-70">

    <div class="container">

        <!-- Heading -->
        <div class="heading text-center margin-top-70 margin-bottom-30">
            <h4>Change Password</h4>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <!-- CONTACT FORM -->
                <div class="contact-form">
                    <!-- FORM -->
                    <form role="form" id="change-password-form" method="post" action="{{route('forgetpasswordnew')}}">
                        @csrf
                        <ul class="row">
                            <li class="col-sm-12">
                                <div class="place-order bg-lightblue mt-20">
                                    <label class="margin-top-bottom">
                                        <h6>Kode Verifikasi</h6>
                                        <input type="text" class="form-control" name="VerifyCode" id="VerifyCode" placeholder="Kode Verifikasi" required autofocus autocomplete="off">
                                        Masukkan kode verifikasi yang telah dikirimkan melalui email/sms
                                    </label>
                                </div>
                            </li>
                                <li class="col-sm-12">
                                <div class="place-order mt-20">
                                    <label class="">
                                        <h6>Kata Sandi Baru</h6>
                                        <input type="password" class="form-control" name="Password" id="Password" placeholder="Kata Sandi Baru" required value="{{old('login') ? old('login') : ''}}" autofocus>
                                    </label>
                                    <label class="margin-top-bottom">
                                        <h6>Masukkan Ulang Kata Sandi</h6>
                                        <input type="password" class="form-control" name="ConfirmPassword" id="ConfirmPassword" placeholder="Masukkan Ulang Kata Sandi" required value="{{old('login') ? old('login') : ''}}" autofocus>
                                    </label>
                                </div>
                                </li>
                                <li class="col-sm-12">
                                    
                                </li>
                            
                            <li class="col-sm-12 text-right margin-top-bottom">
                                <button type="submit" value="submit" class="btn btn-1" id="btn_submit">Change Password <i class="fa fa-caret-right"></i></button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/web/js/jquery.validate.min.js"></script>
<script>
    $(function() {
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Gunakan minimal 8 karakter dengan campuran huruf kapital dan angka."
        );
        $("#change-password-form").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                VerifyCode: {
                    required: true
                },
                Password: {
                    required: true,
                    // minlength: 8,
                    // maxlength: 30,
                    regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
                },
                ConfirmPassword: {
                    required: true,
                    // minlength: 6,
                    equalTo: "[name='Password']"
                },

            },
            messages: {
                rulesPassword: {
                    // minlength: "Password must be at least {0} characters!"
                },
                ConfirmPassword: {
                    equalTo: "Passwords must match!",
                }
            }
        });
    });
</script>

@endsection
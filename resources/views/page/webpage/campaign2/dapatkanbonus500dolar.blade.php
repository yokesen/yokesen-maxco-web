@extends('page.template.campaign2.layout')
@section('title','Maxco Futures | Prestigious Global Brokerage House')
@section('og-image',url('/').'/web/images/web/logo_300.png')

@section('content')

<!--*********************************************************************************************************-->
<!--************ HERO ***************************************************************************************-->
<!--*********************************************************************************************************-->
<header id="ts-hero" class="ts-full-screen ts-separate-bg-element">
    <!--NAVIGATION ******************************************************************************************-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element" data-bg-color="#141a3a">
        <div class="container">
            <a class="navbar-brand" href="#page-top">
                <img src="{{url('/')}}/web/images/web/logo_300.png" alt="" style="width: 135px;">
            </a>
            <!--end navbar-brand-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!--end navbar-toggler-->
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <!-- <a class="nav-item nav-link active ts-scroll" href="#page-top">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link ts-scroll" href="#how-it-works">How It Works</a>
                    <a class="nav-item nav-link ts-scroll" href="#about-us">About Us</a>
                    <a class="nav-item nav-link ts-scroll" href="#successful-stories">Cases</a>
                    <a class="nav-item nav-link ts-scroll" href="#pricing">Pricing</a>
                    <a class="nav-item nav-link ts-scroll" href="#our-team">Team</a>
                    <a class="nav-item nav-link ts-scroll mr-2" href="#form-contact">Contact</a> -->
                    <!-- <a class="nav-link ts-scroll  px-3 ts-width__auto" href="#form-contact">LOGIN</a> -->
                </div>
                <!--end navbar-nav-->
            </div>
            <!--end collapse-->
        </div>
        <!--end container-->
    </nav>
    <!--end navbar-->

    <!--HERO CONTENT ****************************************************************************************-->

    <div class="container align-self-center align-items-center">
        <div class="row align-items-center">
            <div class="col-sm-12 col-md-7 d-sm-block mb-5">
                <h1 data-animate="ts-fadeInUp">Redeem Bonus <br>hingga <img src="{{url('/')}}/campaign2/images/500usd.png"></h1>
                <div data-animate="ts-fadeInUp" data-delay=".1s">
                    <p class="w-75 text-white mb-5 ts-opacity__50">Hanya dengan mendaftarkan akun mini, dapatkan bonus hingga $500</p>
                </div>
                <a href="#how-it-works" class="btn btn-red btn-lg ts-scroll mr-4" data-animate="ts-fadeInUp" data-delay=".2s">
                    Learn More
                    <i class="fa fa-arrow-down small ml-3 ts-opacity__50"></i>
                </a>
            </div>
            <div class="col-sm-12 offset-lg-1 col-md-4">
                <form id="registerForm" class="ts-form p-4 ts-border-radius__xxl bg-white" data-bg-color="rgba(255,255,255,.2)" data-animate="ts-fadeInUp" data-delay=".2s" style="padding-top: 3rem !important;padding-bottom: 3rem !important;">
                    <h3 class="font-black">Daftar Sekarang</h3>
                    <!-- <p class="text-white d-none d-lg-block">Vivamus fermentum magna non faucibus dignissim. Sed a venenatis </p> -->

                    <div class="form-group">
                        <input type="text" class="form-control" name="Realname" placeholder="Nama Lengkap" required>
                    </div>
                    <!--end form-group -->
                    <div class="form-group">
                        <input type="email" class="form-control" name="Email" placeholder="Email" required>
                    </div>
                    <!--end form-group -->
                    <div class="form-group row">
                        <div class="col-sm-2 col-md-4">
                            <input type="text" class="form-control" value="+62" disabled>
                        </div>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" class="form-control" name="Mobile" placeholder="Nomor Handphone" required>
                            <label id="Mobile-error" class="error" style="display: none;" for="Mobile">This field is required.</label>
                        </div>
                    </div>
                    <input type="hidden" name="IDNO" class="form-control" value="12345678910">
                    <input type="hidden" name="Password" class="form-control" value="Temporary2020">
                    <input type="hidden" name="ConfirmPassword" class="form-control" value="Temporary2020">
                    <input type="hidden" name="Nationality" value="Indonesia">
                    <input type="hidden" name="originRegister" value="acceptallea">
                    <input type="hidden" name="ParentId" class="form-control" placeholder="Code Reference" value="10037">
                    <input class="" type="hidden" id="AreaCode" name="AreaCode" value="62" />
                    <input class="" type="hidden" id="InfoType" name="InfoType" value="0" />
                    <input type="hidden" name="ref" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
                    <input type="hidden" name="so" value="<?php if (isset($_GET['so'])) {
                                                                echo $_GET['so'];
                                                            } else {
                                                                echo '';
                                                            } ?>" />
                    <input type="hidden" name="campaign" value="<?php if (isset($_GET['campaign'])) {
                                                                    echo $_GET['campaign'];
                                                                } else {
                                                                    echo '';
                                                                } ?>" />

                    @if (session('ErrorMessage') || isset($ErrorMessage))
                    <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                    @endif
                    <!--end form-group -->
                    <button type="submit" class="btn btn-red w-100">Daftar</button>
                </form>
                <!--end ts-form-->
            </div>
        </div>
    </div>
    <div class="ts-background" data-bg-color="#141a3a" data-bg-parallax="scroll" data-bg-parallax-speed="3">
        <div class="ts-background-image ts-svg ts-z-index__1 ts-background-position-left d-none d-md-block ts-fadeInLeft animated" data-bg-image="{{url('/')}}/campaign2/svg/shape-mask.svg" data-animate="ts-fadeInLeft" style="background-image: url({{url('/')}}/campaign2/svg/shape-mask.svg); visibility: visible;"></div>
        <div class="ts-background-image ts-svg ts-z-index__1 ts-background-position-left d-none d-md-block ts-fadeInLeft animated" data-bg-image="{{url('/')}}/campaign2/svg/shape-mask.svg" data-animate="ts-fadeInLeft" style="background-image: url({{url('/')}}/campaign2/svg/shape-mask.svg); visibility: visible;left: -577px !important;"></div>
        <div class="ts-background-image ts-parallax-element" data-bg-image="{{url('/')}}/campaign2/images/candlestick.jpg" data-animate="ts-zoomOutIn"></div>
    </div>
    <!--end ts-background-->

</header>
<!--end #hero-->
<main id="ts-content">
    <section id="how-it-works" class="ts-block ts-xs-text-center pb-5">
        <div class="container">
            <div class="ts-title">
                <h2 class="text-center">Yuk! Buka akun real sekarang dan langsung dapatkan bonusnya hingga $500.</h2>
            </div>
            <!--end ts-title-->
            <div class="row">
                <div class="col-sm-12 col-md-4 col-xl-4">
                    <div class="text-right" style="max-width: 276px;margin: -26px 0px 0px 74px;">
                        <img src="{{url('/')}}/campaign2/images/get_bonus.png" class="img-responsive" alt="">

                    </div>
                </div>
                <!--end col-xl-4-->
                <div class="col-sm-12 col-md-8 col-xl-8">
                    <div class="ts-item ts-fadeInUp animated" data-animate="ts-fadeInUp" data-delay="0.1s" style="animation-delay: 0.1s; visibility: visible;">
                        <div class="ts-item-content">
                            <!--end ts-item-header-->
                            <div class="ts-item-body">
                                <h4 class="text-justify ts-opacity__50">Cukup dengan membuka akun real sekarang dengan setoran minimal $500, akumulasi jumlah saldo dihitung sejak mulai deposit sampai dengan melakukan transaksi dalam waktu 30 hari, maka Anda berpeluang mendapatkan bonus dengan ketentuan sebagai berikut: </h4>
                                <h4 class="text-justify mb-2 ts-opacity__50"><span class="" style="margin-right: 17px;">1.</span>Transaksi 2,5 lot akan mendapatkan bonus sebesar $10.</h4>
                                <h4 class="text-justify mb-2 ts-opacity__50">2.<span class="ml-10">Transaksi 10 lot akan mendapatkan bonus sebesar $50.</span></h4>
                                <h4 class="text-justify mb-2 ts-opacity__50">3.<span class="ml-10">Transaksi 55 lot akan mendapatkan bonus sebesar $300.</span></h4>
                                <h4 class="text-justify mb-2 ts-opacity__50">4.<span class="ml-10">Transaksi 88 lot akan mendapatkan bonus sebesar $500 *.</span></h4>

                                <p class="font-italic">* Syarat & ketentuan berlaku</p>
                            </div>
                            <!--end ts-item-body-->
                        </div>
                        <!--end ts-item-content-->
                    </div>
                    <!--end ts-item-->
                </div>
                <!--end col-xl-4-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <hr>
    <!--HOW IT WORKS ****************************************************************************************-->
    <section id="how-it-works" class="ts-block ts-xs-text-center pb-5">
        <div class="container">
            <div class="ts-title">
                <h2 class="text-center">Mengapa Smart Maxco?</h2>
            </div>
            <!--end ts-title-->
            <div class="row">
                <div class="col-12 col-md-4 col-xl-4">
                    <div class="ts-item" data-animate="ts-fadeInUp">
                        <div class="ts-item-content">
                            <div class="row">
                                <div class="col-4 col-md-12">
                                    <div class="ts-item-header">
                                        <figure class="icon">
                                            <span class="step" data-animate="ts-zoomIn">1</span>
                                            <div class="ts-circle__md" data-bg-color="#ebf1fe">
                                                <img src="{{url('/')}}/campaign2/images/smart_maxco_apps.png" alt="">
                                            </div>
                                        </figure>
                                        <!--end icon-->
                                    </div>
                                </div>
                                <div class="col-8 col-md-12">
                                    <!--end ts-item-header-->
                                    <div class="ts-item-body">
                                        <h4 class="text-left">Smart Maxco Apps</h4>
                                        <p class="text-left">
                                            Lebih mudah akses ke pasar dengan berita & analisa terbaru, pembukaan rekening trading sesuai dengan peraturan Bappebti, serta setoran dan penarikan dana langsung dari genggaman Anda.
                                        </p>
                                    </div>
                                    <!--end ts-item-body-->
                                </div>
                            </div>

                        </div>
                        <!--end ts-item-content-->
                    </div>
                    <!--end ts-item-->
                </div>
                <!--end col-xl-4-->
                <div class="col-12 col-md-4 col-xl-4">
                    <div class="ts-item" data-animate="ts-fadeInUp" data-delay="0.1s">
                        <div class="ts-item-content">
                            <div class="row">
                                <div class="col-4 col-md-12">
                                    <div class="ts-item-header">
                                        <figure class="icon">
                                            <span class="step" data-animate="ts-zoomIn">2</span>
                                            <div class="ts-circle__md" data-bg-color="#ebf1fe">
                                                <img src="{{url('/')}}/campaign2/images/kekuatan_finansial.png" alt="">
                                            </div>
                                        </figure>
                                        <!--end icon-->
                                    </div>
                                </div>
                                <div class="col-8 col-md-12">
                                    <div class="ts-item-body">
                                        <h4 class="text-left">Kekuatan Financial Terbesar</h4>
                                        <p class="text-left">
                                            Kami bukan broker bandar, kami adalah pialang bergengsi. Perusahaan afilisasi dari Panin Group Indonesia, salah satu Lembaga Keuangan terbesar di Asia.
                                        </p>
                                    </div>
                                </div>
                            </div>


                            <!--end ts-item-content-->
                        </div>
                        <!--end ts-item-->
                    </div>
                </div>
                <!--end col-xl-4-->
                <div class="col-12 col-md-4 offset-md-0 col-xl-4">
                    <div class="ts-item" data-animate="ts-fadeInUp" data-delay="0.2s">
                        <div class="ts-item-content">
                            <div class="row">
                                <div class="col-4 col-md-12">
                                    <div class="ts-item-header">
                                        <figure class="icon">
                                            <span class="step" data-animate="ts-zoomIn">3</span>
                                            <div class="ts-circle__md" data-bg-color="#ebf1fe">
                                                <img src="{{url('/')}}/campaign2/images/bebas_EA.png" alt="">
                                            </div>
                                        </figure>
                                        <!--end icon-->
                                    </div>
                                    <!--end ts-item-header-->
                                </div>
                                <div class="col-8 col-md-12">
                                    <div class="ts-item-body">
                                        <h4 class="text-left">Bebas EA Apa Saja</h4>
                                        <p class="text-left">
                                            Seluruh perdagangan di Smart Maxco dikirim langsung kepada penyedia likuiditas sehingga memungkinkan Anda untuk menggunakan Pakar Trading sesuai yang diinginkan, termasuk scalping EA.
                                        </p>
                                    </div>
                                    <!--end ts-item-body-->
                                </div>
                            </div>
                        </div>
                        <!--end ts-item-content-->
                    </div>
                    <!--end ts-item-->
                </div>
                <!--end col-xl-4-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--END HOW IT WORKS ************************************************************************************-->

    <section id="our-life-sotry" class="ts-block ts-separate-bg-element" data-bg-image="">
        <img src="{{url('/')}}/campaign2/images/smart_maxco_apps.jpg" alt="" class="img-responsive">
        <div class="row" style="top: calc(28% + 1rem);right: 4%;position: absolute;z-index: 1000;width: 205px;">
            <a href="https://apps.apple.com/id/app/smart-maxco/id1516790315" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png" class="mb-1rem" alt="" src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png"></a>
            <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.forex" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png" class=" lazyloaded" alt="" src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png"></a>
        </div>
    </section>

    <!--LEGALITAS ****************************************************************************************-->
    <section id="our-team" class="ts-block">
        <div class="container">
            <div class="ts-title">
                <h2 class="text-center">Legalitas</h2>
            </div>

            <!--end ts-title-->
            <div class="row">
                <div class="col-sm-12 col-md-4  mb-5">
                    <div style="width:160px;position:relative;top: 50%;left: 50%;transform: translate(-50%, -50%);">
                        <img class="img-responsive" src="{{url('/')}}/campaign2/images/legal-bappebtiImg.png" alt="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4  mb-5">
                    <div style="width:160px;position:relative;top: 50%;left: 50%;transform: translate(-50%, -50%);">
                        <img class="img-responsive" src="{{url('/')}}/campaign2/images/legal-jfxImg.png" alt="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4  mb-5">
                    <div style="width:160px;position:relative;top: 50%;left: 50%;transform: translate(-50%, -50%);">
                        <img class="img-responsive" src="{{url('/')}}/campaign2/images/legal-kliringImg.png" alt="">
                    </div>
                </div>

            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--END LEGALITAS ************************************************************************************-->


</main>
@endsection


@section('jsonpage')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Gunakan minimal 5 karakter dengan campuran, huruf, angka dan simbol."
        );

        setTimeout(function() {
            $("#registerForm").validate({
                ignore: [],
                // errorElement: "div",
                rules: {
                    Nationality: {
                        required: true
                    },
                    Realname: {
                        required: true
                    },
                    // AreaCodeSelect: {
                    //     required: true
                    // },
                    IDNO: {
                        required: false
                    },
                    areaCodeTxt: {
                        required: true
                    },
                    Mobile: {
                        required: true,
                        number: true,
                        minlength: 6,
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    Password: {
                        required: false,
                        minlength: 8,
                        maxlength: 30,
                        regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,30}$"
                    },
                    ConfirmPassword: {
                        required: false,
                        // minlength: 6,
                        equalTo: "[name='Password']"
                    },

                },
                messages: {
                    Nationality: {
                        required: "Harap diisi"
                    },
                    Realname: {
                        required: "Harap diisi"
                    },
                    IDNO: {
                        required: "Harap diisi"
                    },
                    areaCodeTxt: {
                        required: "Harap diisi"
                    },
                    Mobile: {
                        required: "Harap diisi",
                        number: "Harap diisi dengan benar",
                        minlength: "Harap diisi dengan benar",
                    },
                    Email: {
                        required: "Harap diisi",
                        email: "Harap isi dengan email yang valid"
                    },
                    Password: {
                        required: "Harap diisi",
                        minlength: "Masukkan setidaknya 8 karakter"
                    },
                    ConfirmPassword: {
                        required: "Harap diisi",
                        equalTo: "Kata sandi harus sama",
                        // minlength: "Password must be at least {0} characters!"
                    }
                },
                submitHandler: function(form) {
                    // $('.download-btn').hide();
                    // $('.download-spinner-btn').show();
                    // $.ajax({
                    //     /* the route pointing to the post function */
                    //     url: "{{route('apisavedataregister')}}",
                    //     type: 'POST',
                    //     /* send the csrf-token and the input to the controller */
                    //     data: {
                    //         'Password': $('[name="Password"]').val(),
                    //         'ConfirmPassword': $('[name="ConfirmPassword"]').val(),
                    //         'Realname': $('[name="Realname"]').val(),
                    //         'Email': $('[name="Email"]').val(),
                    //         'AreaCode': $('[name="AreaCode"]').val(),
                    //         'Mobile': $('[name="Mobile"]').val(),
                    //         'IDNO': $('[name="IDNO"]').val(),
                    //         'Nationality': $('[name="Nationality"]').val(),
                    //         'ParentId': $('[name="ParentId"]').val(),
                    //         'InfoType': $('[name="InfoType"]').val(),
                    //         'originRegister': $('[name="originRegister"]').val(),
                    //     },
                    //     dataType: 'JSON',
                    //     /* remind that 'data' is the response of the AjaxController */
                    //     success: function(response, data) {
                    //         // if (response.code == 0) {
                    //         //     setTimeout(function() {
                    //         //         $('.download-btn').show();
                    //         //         $('.download-spinner-btn').hide();
                    //         //         $('#divregister').hide();
                    //         //         $('#divcodeverification').show();
                    //         //         initvalidatesendcode();
                    //         //     }, 500);
                    //         // } else {
                    //         //     if (response.code == 21004 || response.code == 21003) {
                    //         //         swal({
                    //         //                 title: "Please Login",
                    //         //                 text: response.message,
                    //         //                 type: "info",
                    //         //                 showCancelButton: false,
                    //         //                 confirmButtonColor: '#7cd1f9',
                    //         //                 confirmButtonText: 'Ok',
                    //         //                 // cancelButtonText: "No, cancel it!",
                    //         //                 closeOnConfirm: false,
                    //         //                 closeOnCancel: false
                    //         //             },
                    //         //             function(isConfirm) {
                    //         //                 if (isConfirm) {
                    //         //                     // swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                    //         //                     setTimeout(function() {
                    //         //                         window.location.href = "{{route('login')}}";
                    //         //                     }, 500);
                    //         //                 }
                    //         //             });
                    //         //     }else{
                    //         //         swal('Info',response.message,"info")
                    //         //     }
                    //         //     setTimeout(function() {
                    //         //         $('.download-btn').show();
                    //         //         $('.download-spinner-btn').hide();
                    //         //         $('[name="Realname"]').focus();
                    //         //         //
                    //         //     }, 500);

                    //         // }
                    //     }
                    // });


                }
            });
        }, 500)


    });
</script>
@endsection

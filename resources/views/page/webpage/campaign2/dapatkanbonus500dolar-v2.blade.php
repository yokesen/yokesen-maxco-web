@extends('page.template.campaign2.layout')
@section('title','Maxco Futures | Prestigious Global Brokerage House')
@section('og-image',url('/').'/web/images/web/logo_300.png')

@section('cssonpage')
<style media="screen">
.slidecontainer {
  width: 100%; /* Width of the outside container */
}

.slider-color {
  -webkit-appearance: none;
  width: 100%;
  height: 15px;
  border-radius: 5px;
  background: #d3d3d3;
  outline: none;
  opacity:0.7;
  -webkit-transition: opacity .15s ease-in-out;
  transition: opacity .15s ease-in-out;
}
.slider-color:hover {
  opacity:1;
}
.slider-color::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: #4CAF50;
  cursor: pointer;
}
.slider-color::-moz-range-thumb {
  width: 25px;
  height: 25px;
  border: 0;
  border-radius: 50%;
  background: #4CAF50;
  cursor: pointer;
}

.slider-pic {
  -webkit-appearance: none;
  width: 100%;
  height: 10px;
  border-radius: 5px;
  background: #d3d3d3;
  outline: none;
  opacity:0.7;
  -webkit-transition: opacity .15s ease-in-out;
  transition: opacity .15s ease-in-out;
}
.slider-pic:hover {
  opacity:1;
}
.slider-pic::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 23px;
  height: 24px;
  border-radius: 50%;
  background: url('contrasticon.png');
  cursor: pointer;
}
.slider-pic::-moz-range-thumb {
  width: 23px;
  height: 24px;
  border: 0;
  border-radius: 50%;
  background: url('contrasticon.png');
  cursor: pointer;
}

.slider-square {
  -webkit-appearance: none;
  width: 100%;
  height: 24px;
  background: #d3d3d3;
  outline: none;
  opacity:0.7;
  -webkit-transition: opacity .15s ease-in-out;
  transition: opacity .15s ease-in-out;
}
.slider-square:hover {
  opacity:1;
}
.slider-square::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 24px;
  height: 24px;
  background: #4CAF50;
  cursor: pointer;
}
.slider-square::-moz-range-thumb {
  width: 24px;
  height: 24px;
  border: 0;
  background: #4CAF50;
  cursor: pointer;
}
</style>



@endsection

@section('gtag-event')
<script>
    function gtag_report_conversion(url) {
        var callback = function() {
            if (typeof(url) != 'undefined') {
                window.location = url;
            }
        };
        gtag('event', 'conversion', {
            'send_to': 'AW-663054807/Sy07CJSw788BENfTlbwC',
            'event_callback': callback
        });
        return false;
    }
</script>
@endsection

@section('facebookPixel')

<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '242949443388541');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=242949443388541&ev=PageView
        &noscript=1" />
</noscript>

@endsection

@section('fbtrack')

<script>
    fbq('track', 'ViewContent');
</script>

@endsection

@section('content')

<!--*********************************************************************************************************-->
<!--************ HERO ***************************************************************************************-->
<!--*********************************************************************************************************-->
<header id="ts-hero" class="ts-full-screen ts-separate-bg-element">
  <!--NAVIGATION ******************************************************************************************-->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element" data-bg-color="#141a3a">
    <div class="container">
      <a class="navbar-brand" href="#page-top">
        <img src="{{url('/')}}/web/images/web/logo_300.png" alt="" style="width: 135px;">
      </a>
      <!--end navbar-brand-->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!--end navbar-toggler-->
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          <!-- <a class="nav-item nav-link active ts-scroll" href="#page-top">Home <span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link ts-scroll" href="#how-it-works">How It Works</a>
          <a class="nav-item nav-link ts-scroll" href="#about-us">About Us</a>
          <a class="nav-item nav-link ts-scroll" href="#successful-stories">Cases</a>
          <a class="nav-item nav-link ts-scroll" href="#pricing">Pricing</a>
          <a class="nav-item nav-link ts-scroll" href="#our-team">Team</a>
          <a class="nav-item nav-link ts-scroll mr-2" href="#form-contact">Contact</a> -->
          <!-- <a class="nav-link ts-scroll  px-3 ts-width__auto" href="#form-contact">LOGIN</a> -->
        </div>
        <!--end navbar-nav-->
      </div>
      <!--end collapse-->
    </div>
    <!--end container-->
  </nav>
  <!--end navbar-->

  <!--HERO CONTENT ****************************************************************************************-->

  <div class="container align-self-center align-items-center">
    <div class="row align-items-center">
      <div class="col-sm-12 col-md-7 d-sm-block mb-5">
        <h1 data-animate="ts-fadeInUp">Redeem Bonus <br>hingga <img src="{{url('/')}}/campaign2/images/500usd.png"></h1>
        <div data-animate="ts-fadeInUp" data-delay=".1s">
          <p class="w-75 text-white mb-5 ts-opacity__50">Hanya dengan mendaftarkan akun mini, dapatkan bonus hingga $500</p>
        </div>
        <a href="#how-it-works" class="btn btn-red btn-lg ts-scroll mr-4" data-animate="ts-fadeInUp" data-delay=".2s">
          Learn More
          <i class="fa fa-arrow-down small ml-3 ts-opacity__50"></i>
        </a>
      </div>
      <div class="col-sm-12 offset-lg-1 col-md-4">
        @if(Session::get('insertsuccess') == 'true')
        <div class="ts-form p-4 ts-border-radius__xxl bg-white">
          <h3 class="font-black">Terima kasih</h3>
          <p>Account Executive kami akan segera menghubungi Anda.</p>
        </div>

        @else
        <form method="post" action="{{route('funnelCampaignPost')}}" class="ts-form p-4 ts-border-radius__xxl bg-white" data-bg-color="rgba(255,255,255,.2)" data-animate="ts-fadeInUp" data-delay=".2s" style="padding-top: 3rem !important;padding-bottom: 3rem !important;">
          <h3 class="font-black">Daftar Sekarang</h3>

          <div class="form-group">
            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" required>
          </div>
          <!--end form-group -->
          <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Email" required>
          </div>
          <!--end form-group -->
          <div class="form-group row">
            <div class="col-sm-2 col-md-4">
              <input type="text" class="form-control" value="+62" disabled>
            </div>
            <div class="col-sm-10 col-md-8">
              <input type="text" class="form-control" name="phone" placeholder="Nomor Handphone" required>
              <label id="Mobile-error" class="error" style="display: none;" for="Mobile">This field is required.</label>
            </div>
          </div>

          <input type="hidden" name="parent" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
          <input type="hidden" name="origin" value="<?php if (isset($_GET['so'])) {
            echo $_GET['so'];
          } else {
            echo 'direct';
          } ?>" />
          <input type="hidden" name="campaign" value="38" />

          @if (session('ErrorMessage') || isset($ErrorMessage))
          <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
          @endif
          <!--end form-group -->
          @csrf()
          <button type="submit" class="btn btn-red w-100">Daftar</button>
        </form>
        <!--end ts-form-->
        @endif
      </div>
    </div>
  </div>
  <div class="ts-background" data-bg-color="#141a3a" data-bg-parallax="scroll" data-bg-parallax-speed="3">
    <div class="ts-background-image ts-svg ts-z-index__1 ts-background-position-left d-none d-md-block ts-fadeInLeft animated" data-bg-image="{{url('/')}}/campaign2/svg/shape-mask.svg" data-animate="ts-fadeInLeft" style="background-image: url({{url('/')}}/campaign2/svg/shape-mask.svg); visibility: visible;"></div>
    <div class="ts-background-image ts-svg ts-z-index__1 ts-background-position-left d-none d-md-block ts-fadeInLeft animated" data-bg-image="{{url('/')}}/campaign2/svg/shape-mask.svg" data-animate="ts-fadeInLeft" style="background-image: url({{url('/')}}/campaign2/svg/shape-mask.svg); visibility: visible;left: -577px !important;"></div>
    <div class="ts-background-image ts-parallax-element" data-bg-image="{{url('/')}}/campaign2/images/candlestick.jpg" data-animate="ts-zoomOutIn"></div>
  </div>
  <!--end ts-background-->

</header>
<!--end #hero-->
<main id="ts-content">
  <section id="how-it-works" class="ts-block ts-xs-text-center pb-5">
    <div class="container">
      <div class="ts-title">
        <h2 class="text-center">Dapatkan Bonus hingga $500</h2>
        <h4 class="text-center">hanya dengan buka akun mini sekarang!</h4>
      </div>
      <!--end ts-title-->
      <div class="row">
        <div class="col-sm-12 col-md-4 col-xl-4">
          <div class="text-right" style="max-width: 276px;margin: -26px 0px 0px 74px;">
            <img src="{{url('/')}}/campaign2/images/calculator.png" class="img-responsive" alt="">

          </div>
        </div>
        <!--end col-xl-4-->
        <div class="col-sm-12 col-md-8 col-xl-8">
          <div class="ts-item ts-fadeInUp animated" data-animate="ts-fadeInUp" data-delay="0.1s" style="animation-delay: 0.1s; visibility: visible;">
            <div class="ts-item-content">
              <!--end ts-item-header-->
              <div class="ts-item-body">
                <h4 class="text-center ts-opacity__50 pt-3">Bonus yang akan Anda terima</h4>
                <h1 class="text-center ts-opacity__50"><span id="lotBonus">$50</span></h1>
                <div class="row">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                    <div class="slidecontainer">
                      <small>Geser ke kanan untuk simulasi bonus >></small>
                      <input type="range" min="0.0" max="100" value="10" step="0.1" class="slider-square" id="myRange" onchange="updateSlider(this.value)">
                    </div>
                  </div>
                  <div class="col-md-2">
                  </div>
                </div>
                <br>
                <h4 class="text-center ts-opacity__50">
                  Jumlah Lot 30 hari pertama <b class="text-blue" id="lot-traded"></b> Lot
                </h4>

              </div>
              <!--end ts-item-body-->
            </div>
            <!--end ts-item-content-->
          </div>
          <!--end ts-item-->
        </div>
        <!--end col-xl-4-->
      </div>
      <!--end row-->
    </div>
    <!--end container-->
  </section>
  <section id="about-us">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="ts-block pr-3">
            <div class="ts-title">
              <h2 class="text-center">Syarat dan Ketentuan</h2>
            </div>
            <!--end ts-title-->
            <p class="text-center">
              Syarat dan ketentuan PROGRAM BONUS 500$ sebagai berikut:
            </p>
            <div class="row">
              <div class="col-md-8">
                <div class="list-group list-group-flush mb-5">
                  <a href="#collapseOne" class="list-group-item list-group-item-action border-top-0 pl-0 font-weight-bold bg-transparent" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseOne">Syarat umum <i class="fa fa-plus" aria-hidden="true"></i></a>
                  <div class="collapse" id="collapseOne">
                    <p class="pt-3 small">
                      1.	PROGRAM BONUS hanya berlaku untuk NASABAH BARU SMART dengan pembukaan akun live periode 1 Juli – 30 September 2020.
                    </p>
                    <p class="small">
                      2.	Nasabah diperbolehkan untuk Inject dana jika merasa perlu, begitu pun untuk withdrawal.
                    </p>
                    <p class="small">
                      3.	Penggunaan Robot / EA untuk trading tidak dilarang.
                    </p>
                  </div>
                  <a href="#collapseTwo" class="list-group-item list-group-item-action pl-0 font-weight-bold bg-transparent" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseTwo">Perhitungan bonus <i class="fa fa-plus" aria-hidden="true"></i></a>
                  <div class="collapse" id="collapseTwo">
                    <p class="pt-3 small">
                      4.	Nasabah SMART berhak mendapatkan BONUS 1 (satu) kali dengan ketentuan sesuai tabel dan tidak berlaku kelipatan.
                    </p>
                    <table border="1">
                      <tr>
                        <th class="text-center" style="padding:5px;">Minimum Deposit<br>(Open New Account)</th>
                        <th class="text-center" style="padding:5px;">Bonus</th>
                        <th class="text-center" style="padding:5px;">Ketentuan Lot Settled (2 ways)</th>
                        <th class="text-center" style="padding:5px;">Expired</th>
                      </tr>
                      <tr>
                        <td rowspan="4" class="text-center">500$</td>
                        <td class="text-center">10$</td>
                        <td class="text-center">2,5 lot settled</td>
                        <td rowspan="4" class="text-center" style="padding:5px;">30 hari dari pembukaan akun</td>
                      </tr>
                      <tr>
                        <td class="text-center">50$</td>
                        <td class="text-center">10 lot settled</td>
                      </tr>
                      <tr>
                        <td class="text-center">300$</td>
                        <td class="text-center">55 lot settled</td>
                      </tr>
                      <tr>
                        <td class="text-center">500$</td>
                        <td class="text-center">88 lot settled</td>
                      </tr>
                    </table>
                    <p class="small">
                      5.	BONUS akan diakumulasikan dan ditotal selama 30 hari.
                    </p>
                    <p class="small">
                      6.	BONUS akan diberikan CASH melalui penambah saldo/balance pada MT4 akun tersebut (Top Up) yang dapat dicairkan (withdrawal) tanpa di lock (bebas kapanpun) di Web Client Area atau SMART MOBILE APP.
                    </p>
                  </div>

                  <a href="#collapseThree" class="list-group-item list-group-item-action pl-0 font-weight-bold bg-transparent" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseThree">Pencairan Bonus <i class="fa fa-plus" aria-hidden="true"></i></a>
                  <div class="collapse" id="collapseThree">
                    <p class="small">
                      7.	Penyelenggara berhak melakukan verifikasi via telepon dan email atas penerimaan BONUS oleh nasabah.
                    </p>
                    <p class="small">
                      8.	Proses Pencairan BONUS dapat memerlukan waktu 5 (lima) sampai dengan 10 (sepuluh) hari kerja.
                    </p>
                    <p class="small">
                      9.	Apabila terjadi transaksi dengan dugaan kecurangan dari pihak nasabah, maka Penyelenggara berhak melakukan diskualifikasi secara sepihak dengan pemberitahuan kepada nasabah melalui email.
                    </p>
                    <p class="small">
                      10.	Program ini tidak berlaku untuk akun SMART lama yang dibuat sebelum tanggal 1 Juli 2020 ataupun akun CLASSIC (fix spread).
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="text-right" style="max-width: 276px;margin: -26px auto;">
                    <img src="{{url('/')}}/campaign2/images/get_bonus.png" class="img-responsive" alt="">

                </div>
              </div>
            </div>

            <a href="#registerForm" class="btn btn-red mr-3 my-2">Daftar Sekarang</a>
          </div>
        </div>

      </div>
      <!--end row-->
    </div>


  </section>
  <hr>
  <!--HOW IT WORKS ****************************************************************************************-->
  <section id="how-it-works" class="ts-block ts-xs-text-center pb-5">
    <div class="container">
      <div class="ts-title">
        <h2 class="text-center">Mengapa Smart Maxco?</h2>
      </div>
      <!--end ts-title-->
      <div class="row">
        <div class="col-12 col-md-4 col-xl-4">
          <div class="ts-item" data-animate="ts-fadeInUp">
            <div class="ts-item-content">
              <div class="row">
                <div class="col-4 col-md-12">
                  <div class="ts-item-header">
                    <figure class="icon">
                      <span class="step" data-animate="ts-zoomIn">1</span>
                      <div class="ts-circle__md" data-bg-color="#ebf1fe">
                        <img src="{{url('/')}}/campaign2/images/smart_maxco_apps.png" alt="">
                      </div>
                    </figure>
                    <!--end icon-->
                  </div>
                </div>
                <div class="col-8 col-md-12">
                  <!--end ts-item-header-->
                  <div class="ts-item-body">
                    <h4 class="text-left">Smart Maxco Apps</h4>
                    <p class="text-left">
                      Lebih mudah akses ke pasar dengan berita & analisa terbaru, pembukaan rekening trading sesuai dengan peraturan Bappebti, serta setoran dan penarikan dana langsung dari genggaman Anda.
                    </p>
                  </div>
                  <!--end ts-item-body-->
                </div>
              </div>

            </div>
            <!--end ts-item-content-->
          </div>
          <!--end ts-item-->
        </div>
        <!--end col-xl-4-->
        <div class="col-12 col-md-4 col-xl-4">
          <div class="ts-item" data-animate="ts-fadeInUp" data-delay="0.1s">
            <div class="ts-item-content">
              <div class="row">
                <div class="col-4 col-md-12">
                  <div class="ts-item-header">
                    <figure class="icon">
                      <span class="step" data-animate="ts-zoomIn">2</span>
                      <div class="ts-circle__md" data-bg-color="#ebf1fe">
                        <img src="{{url('/')}}/campaign2/images/kekuatan_finansial.png" alt="">
                      </div>
                    </figure>
                    <!--end icon-->
                  </div>
                </div>
                <div class="col-8 col-md-12">
                  <div class="ts-item-body">
                    <h4 class="text-left">Kekuatan Financial Terbesar</h4>
                    <p class="text-left">
                      Kami bukan broker bandar, kami adalah pialang bergengsi. Perusahaan afilisasi dari Panin Group Indonesia, salah satu Lembaga Keuangan terbesar di Asia.
                    </p>
                  </div>
                </div>
              </div>


              <!--end ts-item-content-->
            </div>
            <!--end ts-item-->
          </div>
        </div>
        <!--end col-xl-4-->
        <div class="col-12 col-md-4 offset-md-0 col-xl-4">
          <div class="ts-item" data-animate="ts-fadeInUp" data-delay="0.2s">
            <div class="ts-item-content">
              <div class="row">
                <div class="col-4 col-md-12">
                  <div class="ts-item-header">
                    <figure class="icon">
                      <span class="step" data-animate="ts-zoomIn">3</span>
                      <div class="ts-circle__md" data-bg-color="#ebf1fe">
                        <img src="{{url('/')}}/campaign2/images/bebas_EA.png" alt="">
                      </div>
                    </figure>
                    <!--end icon-->
                  </div>
                  <!--end ts-item-header-->
                </div>
                <div class="col-8 col-md-12">
                  <div class="ts-item-body">
                    <h4 class="text-left">Bebas EA Apa Saja</h4>
                    <p class="text-left">
                      Seluruh perdagangan di Smart Maxco dikirim langsung kepada penyedia likuiditas sehingga memungkinkan Anda untuk menggunakan Pakar Trading sesuai yang diinginkan, termasuk scalping EA.
                    </p>
                  </div>
                  <!--end ts-item-body-->
                </div>
              </div>
            </div>
            <!--end ts-item-content-->
          </div>
          <!--end ts-item-->
        </div>
        <!--end col-xl-4-->
      </div>
      <!--end row-->
    </div>
    <!--end container-->
  </section>
  <!--END HOW IT WORKS ************************************************************************************-->

  <section id="our-life-sotry" class="ts-block ts-separate-bg-element" data-bg-image="">
    <img src="{{url('/')}}/campaign2/images/smart_maxco_apps.jpg" alt="" class="img-responsive">
    <div class="row" style="top: calc(28% + 1rem);right: 4%;position: absolute;z-index: 1000;width: 205px;">
      <a href="https://apps.apple.com/id/app/smart-maxco/id1516790315" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png" class="mb-1rem" alt="" src="{{url('/')}}/web/images/webpage/smartmaxco_on_apple_store.png"></a>
      <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.forex" target="_blank"><img data-src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png" class=" lazyloaded" alt="" src="{{url('/')}}/web/images/webpage/smartmaxco_on_google_play.png"></a>
    </div>
  </section>

  <!--LEGALITAS ****************************************************************************************-->
  <section id="our-team" class="ts-block">
    <div class="container">
      <div class="ts-title">
        <h2 class="text-center">Legalitas</h2>
      </div>

      <!--end ts-title-->
      <div class="row">
        <div class="col-sm-12 col-md-4  mb-5">
          <div style="width:160px;position:relative;top: 50%;left: 50%;transform: translate(-50%, -50%);">
            <img class="img-responsive" src="{{url('/')}}/campaign2/images/legal-bappebtiImg.png" alt="">
          </div>
        </div>
        <div class="col-sm-12 col-md-4  mb-5">
          <div style="width:160px;position:relative;top: 50%;left: 50%;transform: translate(-50%, -50%);">
            <img class="img-responsive" src="{{url('/')}}/campaign2/images/legal-jfxImg.png" alt="">
          </div>
        </div>
        <div class="col-sm-12 col-md-4  mb-5">
          <div style="width:160px;position:relative;top: 50%;left: 50%;transform: translate(-50%, -50%);">
            <img class="img-responsive" src="{{url('/')}}/campaign2/images/legal-kliringImg.png" alt="">
          </div>
        </div>

      </div>
      <!--end row-->
    </div>
    <!--end container-->
  </section>
  <!--END LEGALITAS ************************************************************************************-->


</main>
@endsection


@section('jsonpage')
<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("lot-traded");
var lotBonus = document.getElementById("lotBonus");
output.innerHTML = slider.value; // Display the default slider value




// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
  output.innerHTML = this.value;

  var bonus = "$0";

  if(this.value < 2.5){
    bonus = "$0";
  }

  if(this.value > 2.4){
    bonus = "$10";
  }

  if(this.value > 9.9){
    bonus = "$50";
  }

  if(this.value > 54.9){
    bonus = "$300";
  }

  if(this.value > 87.9){
    bonus = "$500";
  }

  lotBonus.innerHTML = bonus;
}

$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
});
</script>
@endsection

@extends('page.template.ads.layout')

@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','Robot Trading Emas Online')
@section('og-image',url('/').'/web/images/campign/robot-loco-london.jpg')
@section('og-description','Trading di Smart Maxco bebas menggunakan segala tipe Expert Advisor. Bonus, Robot Trading Emas Online')

@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('gtag-event')
<script>
    function gtag_report_conversion(url) {
        var callback = function() {
            if (typeof(url) != 'undefined') {
                window.location = url;
            }
        };
        gtag('event', 'conversion', {
            'send_to': 'AW-663054807/Sy07CJSw788BENfTlbwC',
            'event_callback': callback
        });
        return false;
    }
</script>
@endsection

@section('facebookPixel')

<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '242949443388541');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=242949443388541&ev=PageView
        &noscript=1" />
</noscript>

@endsection

@section('fbtrack')

<script>
    fbq('track', 'ViewContent');
</script>

@endsection

@section('cssonpage')
<style>

</style>
@endsection

@section('content')
<div class="pix_section pix-padding-v-30 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/robot-loco-london.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
                <div class="pix-content pix-padding-bottom-30">
                    <!-- <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>DAPATKAN</strong> ROBOT TRADING EMAS ONLINE</span>
                    </h2>
                    <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            Uji coba Robot Trading Golden Profit 2020 ini di Demo Account.
                        </span>
                    </p>
                    <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
                        <span class="pix_edit_text">Coba dulu di akun demo</span>
                    </h2> -->

                    <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
                        <span class="pix_edit_text">SELAMAT DATANG ROBOT TRADER </span>
                    </h2>
                    <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            KAMI MENERIMA SEMUA JENIS ROBOT, EA, COPY TRADE DLL
                        </span>
                    </p>
                    <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"> COBA SEKARANG JUGA DI AKUN DEMO SMARTMAXCO</span>
                    </h2>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/robot-golden-profit-highres.png" alt="" width="100%" class="img-responsive">
                </div>
            </div>
            <div class="col-md-8 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-40">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text"><strong>GOLDEN PROFIT</strong></span>
                    </p>
                    <p>Expert Advisor Metatrader 4 khusus untuk Trading <strong>Loco London Gold (XAUUSD)</strong>.</p>
                    <p>Fitur :</p>
                    <ol>
                        <li>Algoritma mencari trend pergerakan pasar.</li>
                        <li>Masuk pasar berdasarkan Price Action.</li>
                        <li>Take Profit dan Stoploss (golden ratio).</li>
                        <li>Open Posisi sebulan 4-6 kali.</li>
                        <li>Rata-rata tahan posisi 9 jam.</li>
                        <li>Margin Management : lot compound berdasarkan equity.</li>
                        <li>Bukan Martingale dan bukan Averaging.</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/robot-god-save-the-queen-highres.png" alt="" width="100%" class="img-responsive">
                </div>
            </div>
            <div class="col-md-8 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-40">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text"><strong>GOD SAVE THE QUEEN</strong></span>
                    </p>
                    <p>Expert Advisor Metatrader 4 khusus untuk Trading <strong>GBPUSD</strong>.</p>
                    <p>Fitur :</p>
                    <ol>
                        <li>Algoritma mencari trend pergerakan pasar.</li>
                        <li>Masuk pasar berdasarkan Price Action.</li>
                        <li>Take Profit dan Stoploss (golden ratio).</li>
                        <li>Open Posisi sebulan 10-14 kali.</li>
                        <li>Rata-rata tahan posisi 18 jam.</li>
                        <li>Margin Management : lot compound berdasarkan equity.</li>
                        <li>Bukan Martingale dan bukan Averaging.</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-40">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text">Jadilah yang pertama untuk mendapatkan kesempatan mencoba sistem terbaru kami.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-50 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-12 pix-padding-h-30 text-center">
                <div class="pix-content pix-padding-v-40 pix-margin-v-10 content-center">
                    <img src="{{url('/')}}/web/images/campign/double-bonus-robot.png" alt="" class="img-responsive">
                </div>
                <div class="pix-content text-center">
                    <div>
                        <h4 class="h4ix-black-gray-light big-text pix-margin-bottom-20">
                            <span class="pix_edit_text">Dapatkan <span style="font-size: 28px;line-height: 45px;" class="pix-red-panin"><strong>Modal Demo $10.000</strong></span> untuk mencoba Expert Advisor <strong>GOLDEN PROFIT</strong> (XAUUSD) dan <strong>GOD SAVE THE QUEEN</strong> (GBPUSD) berikut di platform handal Smart Maxco.</span>
                        </h4>
                    </div>
                </div>
            </div>
            <div id="divthankyouregister" class="col-md-5 col-xs-12" style="border-radius:3px;display:none;">
                <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
                    <div class="pix-content text-center">
                        <h3 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                            <span class="pix_edit_text"><strong style="color:white;">Klik tombol ini untuk download EA Golden Profit.</strong></span>
                        </h3>
                        <a href="{{url('/')}}/files/Demo-Golden-Profit.ex4" class="btn bg-red-panin pix-white btn-lg small-text btn-block" onclick="gtag_report_conversion()" download>Download Golden Profit</a>
                        <a href="{{url('/')}}/files/Demo-God-Save-the-Queen.ex4" class="btn bg-red-panin pix-white btn-lg small-text btn-block" onclick="gtag_report_conversion()" download>Download God Save The Queen</a>
                        <br>
                        <a href="http://files.metaquotes.net/pt.maxco.futures/mt4/maxcofutures4setup.exe" class="btn bg-red-panin pix-white btn-lg small-text btn-block" onclick="gtag_report_conversion()" download>Download Metatrader4 MaxcoFutures</a>
                        <br>
                        <p style="color:white;">Perhatian! Metarader 4 yang di sini adalah versi Desktop.</p>
                        <p style="color:white;">Untuk menggunakan Robot ini, tidak bisa menggunakan Handphone/Smartphone. Anda bisa instal Metatrader 4 di Laptop atau di VPS (Virtual Private Server).</p>
                        <p style="color:white;">Setelah selesai instalasi VPS atau Laptop, Anda bisa memantau menggunakan Smartphone.</p>
                        <p style="color:white;">Anda akan segera dihubungi oleh representative dari Smartmaxco untuk memandu Anda instalasi dan penggunaan Robot ini.</p>
                    </div>
                </div>

            </div>
            <div id="divregister" class="col-md-5 col-xs-12" style="border-radius:3px">
                <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
                    <div class="pix-content text-center">
                        <h3 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                            <span class="pix_edit_text"><strong style="color:white;">Daftar dulu yuk!</strong></span>
                        </h3>
                    </div>

                    <form id="registerForm" class="pix-form-style pixfort-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="Realname" class="form-control" placeholder="Full Name" value="">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="Email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="IDNO" class="form-control" placeholder="No ID" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="Password" class="form-control" placeholder="Password" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password" required="">
                                </div>
                            </div>
                            <div style="display: inline-flex;">

                                <div class="col-md-3 col-xs-3" style="padding-right:5px;">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone" value="+62" disabled>
                                    </div>
                                </div>
                                <div class="col-md-9 col-xs-9" style="padding-left:5px;">
                                    <div class="form-group">
                                        <input type="text" name="Mobile" class="form-control" placeholder="Phone" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Mobile'):''}}">
                                    </div>
                                    <label id="Mobile-error" class="error" style="display: none;" for="Mobile">This field is required.</label>
                                </div>
                            </div>
                            <input type="hidden" name="Nationality" value="Indonesia">
                            <input type="hidden" name="originRegister" value="acceptallea">
                            <input type="hidden" name="ParentId" class="form-control" placeholder="Code Reference" value="10037">
                            <input class="" type="hidden" id="AreaCode" name="AreaCode" value="62" />
                            <input class="" type="hidden" id="InfoType" name="InfoType" value="0" />
                            <input type="hidden" name="ref" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
                            <input type="hidden" name="so" value="<?php if (isset($_GET['so'])) {
                                                                        echo $_GET['so'];
                                                                    } else {
                                                                        echo '';
                                                                    } ?>" />
                            <input type="hidden" name="campaign" value="<?php if (isset($_GET['campaign'])) {
                                                                            echo $_GET['campaign'];
                                                                        } else {
                                                                            echo '';
                                                                        } ?>" />
                        </div>
                        @if (session('ErrorMessage') || isset($ErrorMessage))
                        <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                        @endif
                        <div id="massageerrorregister" class="alert alert-danger alert-dismissible" style="display:none;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span id="massageregisterdangertext"></span>
                        </div>
                        <button type="submit" class="btn bg-red-panin pix-white btn-lg small-text btn-block download-btn"><strong>Daftarin Saya Sekarang</strong></button>
                        <button disabled="" class="btn red-bg pix-white btn-lg small-text btn-block download-spinner-btn" style="display: none"> <i class="fa fa-spinner fa-spin"></i>
                            Tunggu ya pendaftaran lagi OTW</button>
                    </form>

                </div>
            </div>
            <div id="divcodeverification" class="col-md-5 col-xs-12" style="border-radius:3px;display:none;">
                <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
                    <h4 class="pix-black-gray-dark pix-margin-bottom-15 secondary-font text-center">
                        <span class="pix_edit_text"><strong style="color:white;">Kode Verifikasi telah dikirim ke nomor handphone Anda.</strong></span>
                    </h4>
                    <p style="color:white;">Silahkan masukan di sini :</p>
                    <form id="registercodeform" class="pix-form-style pixfort-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="VerifyCode" class="form-control" placeholder="Kode Verifikasi" autocomplete="off">
                            <label id="VerifyCode-error" class="error" style="display: none;" for="VerifyCode">This field is required.</label>

                            @if (session('ErrorMessage') || isset($ErrorMessage))
                            <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                            @endif
                        </div>
                        <div id="massageerrorsendverification" class="alert alert-danger alert-dismissible" style="display:none;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span id="massagedangertext"></span>
                        </div>
                        <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block register-btn"><strong>Verifikasikan nomor hp saya</strong></button>
                        <button disabled="" class="btn red-bg pix-white btn-lg small-text btn-block register-spinner-btn" style="display: none"> <i class="fa fa-spinner fa-spin"></i>
                            Sedang diverifikasi nih..</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-30 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Selamat datang di Industry 4.0</strong></span>
                    </h2>
                    <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            Era di mana segala sesuatunya bisa dibuat menjadi otomatis.
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <!-- <img src="{{url('/')}}/web/images/campign/era_4.0.png" alt="" class="img-responsive"> -->
                    <img src="{{url('/')}}/web/images/campign/robot_ai.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-40">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text">Termasuk trading forex, bagi Anda yang sudah memiliki sistem atau pola trading yang teruji sekarang bisa juga dibikin otomatis dengan menggunakan Expert Advisor.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Keuntungan menggunakan Expert Advisor</strong></span>
                    </h1>
                    <!-- <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            From logo design to website designers and develope are ready to complete perfect your custom jobs.
                        </span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-5 col-xs-12 text-center">
                <div class="pix-content pix-padding-v-10 pix-margin-v-30">
                    <img src="{{url('/')}}/web/images/campign/advisor_1.png" alt="" class="img-responsive" style="width:75%;margin:5px auto!important;">
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="pix-content">
                    <div class="pix_section inner_section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/24hoours.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>24 jam peluang trading</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Dengan menggunakan EA Anda tidak perlu setiap saat berada di depan layar monitor untuk mencari peluang trading.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/millionaire.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>Asisten terhandal</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Selain mencarikan peluang trading, robot trading juga bisa diperintahkan untuk menutup posisi pada level tertentu (TP/SL).</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/variablespread.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>Canggih dan Cepat</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Segala perhitungan indikator yang mungkin membutuhkan waktu berjam-jam dapat diselesaikan perhitungannya oleh robot dalam hitungan detik.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text">Menyambut hal ini Smart Maxco mempersilahkan trading dengan menggunakan Expert Advisor.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h3 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Trading di Smart Maxco bebas menggunakan segala tipe Expert Advisor</strong></span>
                    </h3>
                    <!-- <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            From logo design to website designers and develope are ready to complete perfect your custom jobs.
                        </span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/expert_advisor2.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/secure.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Anti Kecurangan</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Di saat broker lain melarang penggunaan Expert Advisor, kami justru mendeklarasikan silahkan menggunakan segala tipe Ea dan tidak akan ada kecurangan.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/bank.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Kekuatan Financial Terbesar</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Hal ini tentunya karena kami bukan broker bandar, kami adalah pialang bergengsi dengan kekuatan keuangan terbesar di Indonesia.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/balance.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Regulasi Bappebti</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Selain itu tentunya akan lebih nyaman jika Anda bertransaksi di broker yang terdaftar dan diawasi oleh Pemerintah melalui Bappebti.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Maxco Futures</strong></span>
                    </h2>
                    <p class="pix-black-gray-light big-text text-justify">
                        <span class="pix_edit_text">MaxcoFutures adalah perusahaan pialang berjangka dengan kekuatan keuangan terbesar di Indonesia, dimana kami merupakan perusahaan afiliasi Panin Group. Dengan legalitas dan diawasi langsung oleh Pemerintah, keinginan kami agar anda dapat bertransaksi dengan perasaan aman dan nyaman, tanpa perlu kuatir tidak dibayar. Berapapun profit anda, pasti kami bayar.</span>
                    </p>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6 col-xs-12 padding-countdown">
                <div class="pix-content pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/disc_80_commission_copy.png" alt="" class="img-responsive">
                </div>
                <div class="pix-content text-center">
                    <div>
                        <h4 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                            <span class="pix_edit_text"><strong>KHUSUS UNTUK ANDA</strong></span>
                        </h4>
                        <p class="pix-black-gray-light big-text pix-margin-bottom-20">
                            <span class="pix_edit_text">kami berikan penawaran ini, berlaku jika Anda submit pendaftaran dalam waktu 2 jam.</span>
                        </p>
                    </div>
                    <div class="pix-margin-bottom-40">
                        <div class="row">

                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-days pix-count-num" id="day">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Days</strong></span>
                                </h5>
                            </div>
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-hours pix-count-num" id="hours">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Hours</strong></span>
                                </h5>
                            </div>
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-min pix-count-num" id="minutes">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Minutes</strong></span>
                                </h5>
                            </div>
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-sec pix-count-num" id="second">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Seconds</strong></span>
                                </h5>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="row pix-margin-bottom-10 pix-padding-h-10">
                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                        <div class="row bg-blue-panin" style="color:white;border-radius:3px; padding:10px;">
                            <div class="col-xs-6">
                                <h6 for="max80">KODE PROMO</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6 class="form-control text-center" style="margin-top: 6px;margin-bottom: 0px;"><strong>MAX80</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6 col-xs-12 pix-padding-h-30 text-center">
                <div class="pix-content pix-padding-v-40 pix-margin-v-10 content-center">
                    <img src="{{url('/')}}/web/images/campign/double-bonus-robot.png" alt="" class="img-responsive">
                </div>
                <div class="pix-content text-center">
                    <div>
                        <h4 class="h4ix-black-gray-light big-text pix-margin-bottom-20">
                            <span class="pix_edit_text">Dapatkan <span style="font-size: 28px;line-height: 45px;" class="pix-red-panin"><strong>Modal Demo $10.000</strong></span> untuk mencoba Expert Advisor <strong>GOLDEN PROFIT</strong> (XAUUSD) dan <strong>GOD SAVE THE QUEEN</strong> (GBPUSD) berikut di platform handal Smart Maxco.</span>
                        </h4>
                        <a href="#divregister" type="button" name="button" class="btn red-bg pix-white btn-lg small-text btn-block">Ya, Saya Mau</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsonpage')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="{{url('/')}}/data/nationality.js"></script>

<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Gunakan minimal 5 karakter dengan campuran, huruf, angka dan simbol."
        );

        function initvalidatesendcode() {
            $("#registercodeform").validate({
                ignore: [],
                // errorElement: "div",
                rules: {
                    VerifyCode: {
                        required: true,
                        number: true
                    },
                },
                messages: {

                },
                submitHandler: function(form) {
                    $('.register-spinner-btn').show();
                    $('.register-btn').hide();
                    $.ajax({
                        /* the route pointing to the post function */
                        url: "{{route('apiadddemouser')}}",
                        type: 'POST',
                        /* send the csrf-token and the input to the controller */
                        data: {
                            'VerifyCode': $('[name="VerifyCode"]').val(),
                        },
                        dataType: 'JSON',
                        /* remind that 'data' is the response of the AjaxController */
                        success: function(respon, data) {
                            $('.register-spinner-btn').hide();
                            $('.register-btn').show();
                            if (respon.code == 0) {
                                setTimeout(function() {
                                    window.location.href = "{{route('trading-tool-robot')}}";
                                }, 1000);
                                // $('#divregister').hide();
                                // $('#divcodeverification').hide();
                                // $('#divthankyouregister').show();
                            } else if (respon.code == 12001) {
                                $('#massageerrorsendverification').show();
                                $('#massagedangertext').text(respon.message)
                            } else {
                                if (respon.code == 21004 || respon.code == 21003) {
                                    swal({
                                            title: "Please Login",
                                            text: respon.message,
                                            type: "info",
                                            showCancelButton: false,
                                            confirmButtonColor: '#7cd1f9',
                                            confirmButtonText: 'Ok',
                                            // cancelButtonText: "No, cancel it!",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                // swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                                                setTimeout(function() {
                                                    window.location.href = "{{route('login')}}";
                                                }, 500);
                                            }
                                        });
                                }
                                $('#divregister').show();
                                $('#divcodeverification').hide();
                                $('#massageerrorregister').show();
                                $('#massageregisterdangertext').text(respon.message)
                            }
                        }
                    });
                }
            });
        };


        $("#registerForm").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                Nationality: {
                    required: true
                },
                Realname: {
                    required: true
                },
                // AreaCodeSelect: {
                //     required: true
                // },
                IDNO: {
                    required: true
                },
                areaCodeTxt: {
                    required: true
                },
                Mobile: {
                    required: true,
                    number: true
                },
                Email: {
                    required: true,
                    email: true
                },
                Password: {
                    required: true,
                    minlength: 8,
                    maxlength: 30,
                    regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,30}$"
                },
                ConfirmPassword: {
                    required: true,
                    // minlength: 6,
                    equalTo: "[name='Password']"
                },

            },
            messages: {
                Nationality: {
                    required: "Harap diisi"
                },
                Realname: {
                    required: "Harap diisi"
                },
                IDNO: {
                    required: "Harap diisi"
                },
                areaCodeTxt: {
                    required: "Harap diisi"
                },
                Mobile: {
                    required: "Harap diisi",
                    number: true
                },
                Email: {
                    required: "Harap diisi",
                    email: "Harap isi dengan email yang valid"
                },
                Password: {
                    required: "Harap diisi",
                    minlength: "Masukkan setidaknya 8 karakter"
                },
                ConfirmPassword: {
                    required: "Harap diisi",
                    equalTo: "Kata sandi harus sama",
                    // minlength: "Password must be at least {0} characters!"
                }
            },
            submitHandler: function(form) {
                $('.download-btn').hide();
                $('.download-spinner-btn').show();
                $.ajax({
                    /* the route pointing to the post function */
                    url: "{{route('apisavedataregister')}}",
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {
                        'Password': $('[name="Password"]').val(),
                        'ConfirmPassword': $('[name="ConfirmPassword"]').val(),
                        'Realname': $('[name="Realname"]').val(),
                        'Email': $('[name="Email"]').val(),
                        'AreaCode': $('[name="AreaCode"]').val(),
                        'Mobile': $('[name="Mobile"]').val(),
                        'IDNO': $('[name="IDNO"]').val(),
                        'Nationality': $('[name="Nationality"]').val(),
                        'ParentId': $('[name="ParentId"]').val(),
                        'InfoType': $('[name="InfoType"]').val(),
                        'originRegister': $('[name="originRegister"]').val(),
                    },
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function(response, data) {
                        if (response.code == 0) {
                            setTimeout(function() {
                                $('.download-btn').show();
                                $('.download-spinner-btn').hide();
                                $('#divregister').hide();
                                $('#divcodeverification').show();
                                initvalidatesendcode();
                            }, 500);
                        } else {
                            if (response.code == 21004 || response.code == 21003) {
                                swal({
                                        title: "Please Login",
                                        text: response.message,
                                        type: "info",
                                        showCancelButton: false,
                                        confirmButtonColor: '#7cd1f9',
                                        confirmButtonText: 'Ok',
                                        // cancelButtonText: "No, cancel it!",
                                        closeOnConfirm: false,
                                        closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            // swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                                            setTimeout(function() {
                                                window.location.href = "{{route('login')}}";
                                            }, 500);
                                        }
                                    });
                            }else{
                                swal('Info',response.message,"info")
                            }
                            setTimeout(function() {
                                $('.download-btn').show();
                                $('.download-spinner-btn').hide();
                                $('[name="Realname"]').focus();
                            }, 500);
                        }
                    },

                });


            }
        });


        $('#AreaCodeSelect').on('change', function(event) {
            var value = event.currentTarget.value;
            value = value.replace("+ ", "");
            $('#AreaCode').val(value);
        })
        // Set the date we're counting down to
        var countDownDate = new Date().getTime();
        countDownDate = countDownDate + (2 * 60 * 60 * 1000);

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                // document.getElementById("demo").innerHTML = "EXPIRED";
            } else {
                $('#day').text(days);
                $('#hours').text(hours);
                $('#minutes').text(minutes);
                $('#second').text(seconds);
            }
        }, 1000);

        $('.copyToClipboard').on('click', function() {
            var element = this;
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            // alert("Copied the text: " + $(element).text());
            // $(this).attr('data-original-title',`Copied the text : ${$(element).text()}`)
            // $('[data-toggle="tooltip"]').tooltip()
            $temp.remove();
        })

        $('[data-toggle="tooltip"]').tooltip()

        $('.select2-single').select2();
    });
</script>
@endsection

@section('addtional-disclaimer','PT. Maxco Futures tidak menjual, memfasilitasi maupun menjamin keuntungan
/profit dalam penggunaan segala jenis ROBOT, EA, Copy Trade dan strategi-strategi trading lainnya. Website ini memberikan edukasi kepada nasabah
maupun calon nasabah dalam informasi mencakup resiko dan strategi-strategi
trading')
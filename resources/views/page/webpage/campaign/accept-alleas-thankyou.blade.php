@extends('page.template.ads.layout')

@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','Aplikasi Trading Forex Terbaik')
@section('og-image','/web/images/campign/getsmartmaxcoapps.png')
@section('og-description','Memperkenalkan Smart Maxco App, sebuah platform all-in-one app untuk kebutuhan investasi anda. Sebuah terobosan yang memudahkan segala urusan trading forex, mulai dari pembukaan akun sesuai dengan peraturan Bappebti, deposit, withdrawal, hingga trading dalam satu platform.')

@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

@endsection

@section('facebookPixel')
@if(isset($fbqid))
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '{{$fbqid}}');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id={{$fbqid}}&ev=PageView
        &noscript=1" />
</noscript>
@endif
@endsection

@section('fbtrack')
@if(isset($fbqid))
<script>
    fbq('track', 'ViewContent');
</script>
@endif
@endsection

@section('cssonpage')
<style>

</style>
@endsection

@section('content')
<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Terima Kasih Telah Melakukan Registrasi</strong></span>
                    </h2>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content text-center pix-padding-top-150 pix-radius-3 pix-margin-v-20" style="background-image: url({{url('/')}}/web/images/campign/expert_advisor2.jpg);background-size: cover;background-position: center;">
                    <h2 class="pix-white pix-no-margin-top secondary-font">
                        <!-- <span class="pix_edit_text"><strong>The Best Marketing In The World.</strong></span> -->
                    </h2>
                    <h6 class="pix-light-white pix-margin-bottom-30">
                        <!-- <span class="pix_edit_text">From logo design to website designs and developers are ready to complete perfect your custom jobs.</span> -->
                    </h6>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-40">
                    <h6 class="h6ix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Trading di Smart Maxco bebas menggunakan segala tipe Expert Advisor. Login sekarang</span>
                    </h6>
                </div>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
                <div class="pix-content ">
                    <a href="{{env('CABINET_URL')}}login?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" class="btn bg-blue-panin btn-lg pix-white pix-margin-bottom-10">
                        <span class="pix_edit_text">
                            <strong>Login Sekarang</strong>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('jsonpage')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="{{url('/')}}/data/nationality.js"></script>

<script>
    // $(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
    $("#registerForm").validate({
        ignore: [],
        // errorElement: "div",
        rules: {
            Nationality: {
                required: true
            },
            Realname: {
                required: true
            },
            AreaCodeSelect: {
                required: true
            },
            IDNO: {
                required: true
            },
            areaCodeTxt: {
                required: true
            },
            Mobile: {
                required: true,
                number: true
            },
            Email: {
                required: true,
                email: true
            },
            Password: {
                required: true,
                minlength: 5,
                maxlength: 30,
                regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
            },
            ConfirmPassword: {
                required: true,
                // minlength: 6,
                equalTo: "[name='Password']"
            },

        },
        messages: {
            rulesPassword: {
                // minlength: "Password must be at least {0} characters!"
            },
            ConfirmPassword: {
                equalTo: "Passwords must match!",
                // minlength: "Password must be at least {0} characters!"
            }
        },
        // submitHandler: function(form) {
        //     $.ajax({
        //         /* the route pointing to the post function */
        //         url: "{{route('postapiregistermaxco')}}",
        //         type: 'POST',
        //         /* send the csrf-token and the input to the controller */
        //         data: {
        //             // 'name': $('[name="name"]').val(),
        //             'email': $('[name="email"]').val(),
        //             // 'phone': $('[name="phone"]').val(),
        //             // 'parent': $('[name="parent"]').val(),
        //             // 'origin': $('[name="origin"]').val(),
        //             // 'campaign': $('[name="campaign"]').val(),
        //         },
        //         dataType: 'JSON',
        //         /* remind that 'data' is the response of the AjaxController */
        //         success: function(massage,data) {
        //             // if (data.success) {
        //             //     $("#opt-in").hide();
        //             //     setTimeout(function() {
        //             //         $(".thankyou-alert").show();
        //             //         window.open(`https://api.whatsapp.com/send?phone={{Cookie::get('salesWhatsapp') ? Cookie::get('salesWhatsapp') : '6285218895459'}}&text=halo nama saya ${data.name}, ingin penjelasan mengenai cara memakai Smart Maxco Apps`, "_blank", 'location=yes,menubar=no,height=570,width=520,scrollbars=yes,status=yes');
        //             //     }, 1000)
        //             // }
        //             // $(".writeinfo").append(data.msg);
        //         }
        //     });
        // }
    });
    // });

    var optionAreaCd = "";
    var NationalityDt = "";
    var areaCode = "";
    nationalityDt.map(function(item) {
        const dt = `<option value="${item.value}">${item.text}</option>`;
        const codeDt = `<option value="${item.AreaCode}">${item.AreaCode}</option>`;
        areaCode += codeDt;
        NationalityDt += dt;
    });
    $('#Nationality').append(NationalityDt)
    $('#AreaCodeSelect').append(areaCode)

    $('#AreaCodeSelect').val('+ 62');
    $('#AreaCode').val('62');


    $('#AreaCodeSelect').on('change', function(event) {
        var value = event.currentTarget.value;
        value = value.replace("+ ", "");
        $('#AreaCode').val(value);
    })
    // Set the date we're counting down to
    var countDownDate = new Date().getTime();
    countDownDate = countDownDate + (2 * 60 * 60 * 1000);

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            // document.getElementById("demo").innerHTML = "EXPIRED";
        } else {
            $('#day').text(days);
            $('#hours').text(hours);
            $('#minutes').text(minutes);
            $('#second').text(seconds);
        }
    }, 1000);

    $('.copyToClipboard').on('click', function() {
        var element = this;
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        // alert("Copied the text: " + $(element).text());
        // $(this).attr('data-original-title',`Copied the text : ${$(element).text()}`)
        // $('[data-toggle="tooltip"]').tooltip()
        $temp.remove();
    })

    $('[data-toggle="tooltip"]').tooltip()

    $('.select2-single').select2();
</script>
@endsection
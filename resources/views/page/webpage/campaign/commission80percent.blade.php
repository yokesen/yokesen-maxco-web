@extends('page.template.ads.layout')

@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','LESS COST BRINGS MORE PROFIT')
@section('og-image',url('/').'/web/images/campign/diskon80.jpeg')
@section('og-description','Biaya transaksi yang rendah, transaksi akan semakin mudah profit.Trading Forex di Maxco Futures aja!')

@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

@endsection

@section('facebookPixel')
@if(isset($fbqid))
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '{{$fbqid}}');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id={{$fbqid}}&ev=PageView
        &noscript=1" />
</noscript>
@endif
@endsection

@section('fbtrack')
@if(isset($fbqid))
<script>
    fbq('track', 'ViewContent');
</script>
@endif
@endsection

@section('cssonpage')
<style>

</style>
@endsection

@section('content')
@if(!isset($viewVerifiCode))

<div class="pix_section pix-padding-v-30 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Diskon 80% Commission</strong></span>
                    </h1>
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top">
                        <span class="pix_edit_text"><strong>SELAMAT!</strong></span>
                    </h2>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10 col-xs-12 text-center">
                <div class="pix-content pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/disc_80_commission_copy.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 pix-margin-top-30">
                <div class="pix-content">
                    <h6 class="h6ix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Kalau Anda sudah sampai di halaman ini, artinya Anda berhak mendapatkan diskon komisi hingga 80% dari komisi normal.</span>
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <img src="{{url('/')}}/web/images/campign/diskon80.jpeg" class="img-responsive">
            </div>
            <div class="col-md-12 col-xs-12 pix-margin-top-40 text-center">
                <a href="#claimtoday" class="btn bg-red-panin btn-lg pix-white pix-margin-top-10">
                    <span class="pix_edit_text">
                        <strong>DAFTAR SEKARANG</strong>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-xs-12 text-center">
                <img src="{{url('/')}}/web/images/campign/pengganti_pohon_uang_ori.png" alt="" class="img-responsive">
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="pix-content">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="pix-content">
                                <div class="media ic">
                                    <div class="media-body">
                                        <h4 class="pix-black-gray-dark secondary-font pix-margin-bottom-30">
                                            <span class="pix_edit_text"><strong>LESS COST BRINGS MORE PROFIT</strong></span>
                                        </h4>
                                        <p class="pix-black-gray-light pix-margin-bottom-30 text-justify">
                                            <span class="pix_edit_text">Di Maxco, kami hanya menghasilkan uang dari komisi trading, bukan dari kegagalan nasabah. Kami disini untuk membantu nasabah mendapatkan kentungan dan membantu dengan komisi rendah hanya $10 per standard lot.
                                                Kami mendahulukan fokus kepada kebutuhan nasabah secara detail untuk memberikan pengalaman layanan nasabah kelas dunia. Kami disini untuk membantu kesuksesan nasabah.
                                                Kesimpulan sederhananya adalah dengan biaya transaksi yang rendah, transaksi akan semakin mudah profit.
                                                Mau mudah profit? Gabung bersama Smart Trader di
                                            </span>
                                        </p>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <a href="https://www.instagram.com/smartmaxco" class="small-social" target="_blank">
                                                    <div class="media">
                                                        <div class="media-left media-middle">
                                                            <i class="fa fa-instagram big-icon-50 color-ig"></i>
                                                        </div>
                                                        <div class="media-body media-middle">
                                                            <h6 class="media-heading secondary-font">
                                                                <span class="pix_edit_text">
                                                                    Smartmaxco
                                                                </span>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <a href="https://www.facebook.com/smartmaxco" class="small-social" target="_blank">
                                                    <div class="media">
                                                        <div class="media-left media-middle">
                                                            <i class="fa fa-facebook big-icon-50 color-fb"></i>
                                                        </div>
                                                        <div class="media-body media-middle">
                                                            <h6 class="media-heading secondary-font">
                                                                <span class="pix_edit_text">
                                                                    Smartmaxco
                                                                </span>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <a href="https://www.youtube.com/channel/UC69yjQ0hynhMXffy4K-XpWw" class="small-social" target="_blank">
                                                    <div class="media">
                                                        <div class="media-left media-middle">
                                                            <i class="fa fa-youtube big-icon-50 color-yt"></i>
                                                        </div>
                                                        <div class="media-body media-middle">
                                                            <h6 class="media-heading secondary-font">
                                                                <span class="pix_edit_text">Smartmaxco</span>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Type Account</strong></span>
                    </h2>
                    <!-- <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">From logo design to website designs and developers are ready to complete perfect your custom jobs.</span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/mini_acct.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>MINI ACCOUNT</strong></span>
                    </h5>
                    <p class="pix-black-gray-light">
                        <span class="pix_edit_text"><strong>Forex & Gold & Index</strong></span>
                    </p>
                    <p class="pix-black-gray-dark pix-red">
                        <span class="pix_edit_text"><strong>$500</strong></span>
                    </p>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Leverage hingga 1:100 dengan fitur fixed rate Rp 10.000 per dolar AS. Anda dapat melakukan trading mulai dari 0.1 lot atau sama dengan 100.000 contract size.</span>
                    </p>
                    <a href="#claimtoday" class="btn btn-md pix-white bg-red-panin">
                        <span class="pix_edit_text">
                            <strong>Claim Today</strong>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/pro_acc.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>PROFESSIONAL ACCOUNT</strong></span>
                    </h5>
                    <p class="pix-black-gray-light">
                        <span class="pix_edit_text"><strong>Forex & Gold & Index</strong></span>
                    </p>
                    <p class="pix-black-gray-dark pix-red">
                        <span class="pix_edit_text"><strong>$10.000</strong></span>
                    </p>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Leverage hingga 1:100 dengan fitur fixed rate Rp 10.000 per dolar AS. Anda dapat melakukan trading mulai dari 1 lot atau sama dengan 100.000 contract size.</span>
                    </p>
                    <a href="#claimtoday" class="btn btn-md pix-white bg-red-panin">
                        <span class="pix_edit_text">
                            <strong>Claim Today</strong>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/gold_acc.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>USD ACCOUNT</strong></span>
                    </h5>
                    <p class="pix-black-gray-light">
                        <span class="pix_edit_text"><strong>Forex & Gold & Index</strong></span>
                    </p>
                    <p class="pix-black-gray-dark pix-red">
                        <span class="pix_edit_text"><strong>$10.000</strong></span>
                    </p>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Leverage hingga 1:100 dengan fitur USD Floating Rate. Anda dapat melakukan trading mulai dari 1 lot atau sama dengan 100.000 contract size.</span>
                    </p>
                    <a href="#claimtoday" class="btn btn-md pix-white bg-red-panin">
                        <span class="pix_edit_text">
                            <strong>Claim Today</strong>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-v-10">
                    <div class="media ic">
                        <div class="media-body">
                            <h4 class="pix-black-gray-dark secondary-font pix-margin-bottom-30 text-center">
                                <span class="pix_edit_text"><strong>TRADING DENGAN POTONGAN UP-TO 90% DARI SPREAD NORMAL</strong></span>
                            </h4>
                            <h6 class="pix-black-gray-light pix-margin-bottom-10 text-justify">
                                <span class="pix_edit_text">Kejutan lainnya untuk Anda yang sudah masuk ke halaman ini. Khusus Anda yang memang ingin bertransaksi forex, kami berikan Diskon Tambahan!
                                </span>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10 col-xs-12 text-center">
                <div class="pix-content pix-radius-3">
                    <img src="{{url('/)}}/web/images/campign/disc_90_spread_copy.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10 col-xs-12 text-center pix-margin-top-30">
                <h6 class=" pix-margin-bottom-10 text-center">
                    <span class="pix_edit_text">Ya kami berikan Diskon untuk SPREAD juga. Diskon yang kami berikan hingga <strong>90%</strong>
                    </span>
                </h6>
            </div>
        </div>
    </div>
</div>
<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 text-center">
                <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                    <img src="{{url('/')}}/web/images/campign/hp_low_fee.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content">
                    <div class="pix_section inner_section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/variablespread.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>VARIABLE SPREAD</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Salah satu ciri broker bandar adalah fixed spread yang tidak menigkuti market. Smart Maxco menawarkan variable. Spread mengikuti market dunia.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5" style="padding: 0px;">
                                                    <img src="{{url('/')}}/web/images/campign/lowspread.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>LOW SPREAD</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Sebagai bentuk kemitmen pelayanan kepada nasabah. Smart Maxco memberikan Anda low spread.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/5digitasset.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-bargraph big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>5 DIGITS ASSET</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Untuk mewujudkan low variable spread, kini Smart Maxco hair dengan asset 5 digit.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="pix_section pix-padding-v-100 gray-bg" id="claimtoday">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-12 padding-countdown">
                <div class="pix-content text-center">
                    <div>
                        <h4 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                            <span class="pix_edit_text"><strong>KHUSUS UNTUK ANDA</strong></span>
                        </h4>
                        <p class="pix-black-gray-light big-text pix-margin-bottom-20">
                            <span class="pix_edit_text">kami berikan penawaran ini, berlaku jika Anda submit pendaftaran dalam waktu 2 jam.</span>
                        </p>
                    </div>
                    <div class="pix-margin-bottom-40">
                        <div class="row">
                            @if($agent->isMobile())
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-days pix-count-num" id="day">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Days</strong></span>
                                </h5>
                            </div>
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-hours pix-count-num" id="hours">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Hours</strong></span>
                                </h5>
                            </div>
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-min pix-count-num" id="minutes">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Minutes</strong></span>
                                </h5>
                            </div>
                            <div class="col-xs-3">
                                <h1 class="pix-black-gray-dark pix-no-margin-bottom">
                                    <strong><span class="pix-count-sec pix-count-num" id="second">0</span></strong>
                                </h1>
                                <h5 class="pix-orange pix-no-margin-top small-text">
                                    <span class="pix_edit_text"><strong>Seconds</strong></span>
                                </h5>
                            </div>
                            @else
                            <div class="hidden-xs col-md-3 pix-inner-col">
                                <div class="pix-content pix-margin-v-20 text-center secondary-font">
                                    <h1 class="pix-black-gray-dark pix-no-margin-bottom big-text-60">
                                        <strong><span class="pix-count-days pix-count-num" id="day">0</span></strong>
                                    </h1>
                                    <h4 class="pix-orange pix-no-margin-top">
                                        <span class="pix_edit_text"><strong>Days</strong></span>
                                    </h4>
                                </div>
                            </div>
                            <div class="hidden-xs col-md-3 pix-inner-col">
                                <div class="pix-content pix-margin-v-20 text-center secondary-font">
                                    <h1 class="pix-black-gray-dark pix-no-margin-bottom big-text-60">
                                        <strong><span class="pix-count-hours pix-count-num" id="hours">0</span></strong>
                                    </h1>
                                    <h4 class="pix-orange pix-no-margin-top">
                                        <span class="pix_edit_text"><strong>Hours</strong></span>
                                    </h4>
                                </div>
                            </div>
                            <div class="hidden-xs col-md-3 pix-inner-col">
                                <div class="pix-content pix-margin-v-20 text-center secondary-font">
                                    <h1 class="pix-black-gray-dark pix-no-margin-bottom big-text-60">
                                        <strong><span class="pix-count-min pix-count-num" id="minutes">0</span></strong>
                                    </h1>
                                    <h4 class="pix-orange pix-no-margin-top">
                                        <span class="pix_edit_text"><strong>Minutes</strong></span>
                                    </h4>
                                </div>
                            </div>
                            <div class="hidden-xs col-md-3 pix-inner-col">
                                <div class="pix-content pix-margin-v-20 text-center secondary-font">
                                    <h1 class="pix-black-gray-dark pix-no-margin-bottom big-text-60">
                                        <strong><span class="pix-count-sec pix-count-num" id="second">0</span></strong>
                                    </h1>
                                    <h4 class="pix-orange pix-no-margin-top">
                                        <span class="pix_edit_text"><strong>Seconds</strong></span>
                                    </h4>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row pix-margin-bottom-10 pix-padding-h-10">
                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                        <div class="row bg-blue-panin" style="color:white;border-radius:3px; padding:10px;">
                            <div class="col-xs-6">
                                <h6 for="max80">KODE PROMO</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6 class="form-control text-center" style="margin-top: 6px;margin-bottom: 0px;"><strong>MAX80</strong></h6>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @if(isset($viewVerifiCode))
            <div class="col-md-5 col-xs-12">
                <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
                    <h4 class="pix-black-gray-dark pix-margin-bottom-15 pix-no-margin-top secondary-font text-center">
                        <span class="pix_edit_text"><strong>Verification Code - Register !</strong></span>
                    </h4>
                    <form method="post" action="{{route('postRegisterVerification')}}" class="pix-form-style pixfort-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="VerifyCode" class="form-control" placeholder="Verify Code">
                            @if (session('ErrorMessage') || isset($ErrorMessage))
                            <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                            @endif
                        </div>
                        <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block"><strong>REGISTER</strong></button>
                    </form>
                </div>
            </div>
            @else
            <div class="col-md-5 col-xs-12">
                <!-- <div class="pix-content"> -->
                <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
                    <form id="registerForm" class="pix-form-style pixfort-form" method="POST" action="{{route('postapiregistermaxco')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="Realname" class="form-control" placeholder="Full Name" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Realname'):''}}">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="Email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="IDNO" class="form-control" placeholder="No ID" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.IDNO'):''}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="Password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password" required="">
                                </div>
                            </div>
                            <div style="display: inline-flex;">
                              <div class="col-md-3 col-xs-3" style="padding-right:5px;">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone" value="+62" disabled>
                                  </div>
                              </div>
                              <div class="col-md-9 col-xs-9" style="padding-left:5px;">
                                  <div class="form-group">
                                      <input type="text" name="Mobile" class="form-control" placeholder="Phone" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Mobile'):''}}">
                                  </div>
                                  <label id="Mobile-error" class="error" style="display: none;" for="Mobile">This field is required.</label>
                              </div>
                            </div>
                        </div>
                        @if (session('ErrorMessage') || isset($ErrorMessage))
                        <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}.</label>
                        @endif
                        <input type="hidden" name="Nationality" value="Indonesia">
                        <input type="hidden" name="originRegister" value="commission80percent">
                        <input type="hidden" name="ParentId" class="form-control" placeholder="Code Reference" value="{{Input::get('ref') ? Input::get('ref') : Cookie::get('ref') }}">
                        <!-- {{Input::get('ref') ? Input::get('ref') : Cookie::get('ref') }} -->
                        <input class="" type="hidden" id="AreaCode" name="AreaCode" value="62" />
                        <input class="" type="hidden" id="InfoType" name="InfoType" value="0" />
                        <input type="hidden" name="ref" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
                        <input type="hidden" name="so" value="<?php if (isset($_GET['so'])) {
                                                                    echo $_GET['so'];
                                                                } else {
                                                                    echo '';
                                                                } ?>" />
                        <input type="hidden" name="campaign" value="<?php if (isset($_GET['campaign'])) {
                                                                        echo $_GET['campaign'];
                                                                    } else {
                                                                        echo '';
                                                                    } ?>" />
                        <button type="submit" class="btn bg-red-panin pix-white btn-lg small-text btn-block"><strong>Daftar Sekarang</strong></button>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>


@endsection

@section('jsonpage')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="{{url('/')}}/data/nationality.js"></script>

<script>
    // $(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Gunakan minimal 5 karakter dengan campuran, huruf, angka dan simbol."
    );
    $("#registerForm").validate({
        ignore: [],
        // errorElement: "div",
        rules: {
            Nationality: {
                required: true
            },
            Realname: {
                required: true
            },
            AreaCodeSelect: {
                required: true
            },
            IDNO: {
                required: true
            },
            areaCodeTxt: {
                required: true
            },
            Mobile: {
                required: true,
                number: true
            },
            Email: {
                required: true,
                email: true
            },
            Password: {
                required: true,
                minlength: 5,
                maxlength: 30,
                regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
            },
            ConfirmPassword: {
                required: true,
                // minlength: 6,
                equalTo: "[name='Password']"
            },

        },
        messages: {
            rulesPassword: {
                // minlength: "Password must be at least {0} characters!"
            },
            ConfirmPassword: {
                equalTo: "Passwords must match!",
                // minlength: "Password must be at least {0} characters!"
            }
        },
        // submitHandler: function(form) {
        //     $.ajax({
        //         /* the route pointing to the post function */
        //         url: "{{route('postapiregistermaxco')}}",
        //         type: 'POST',
        //         /* send the csrf-token and the input to the controller */
        //         data: {
        //             // 'name': $('[name="name"]').val(),
        //             'email': $('[name="email"]').val(),
        //             // 'phone': $('[name="phone"]').val(),
        //             // 'parent': $('[name="parent"]').val(),
        //             // 'origin': $('[name="origin"]').val(),
        //             // 'campaign': $('[name="campaign"]').val(),
        //         },
        //         dataType: 'JSON',
        //         /* remind that 'data' is the response of the AjaxController */
        //         success: function(massage,data) {
        //             // if (data.success) {
        //             //     $("#opt-in").hide();
        //             //     setTimeout(function() {
        //             //         $(".thankyou-alert").show();
        //             //         window.open(`https://api.whatsapp.com/send?phone={{Cookie::get('salesWhatsapp') ? Cookie::get('salesWhatsapp') : '6285218895459'}}&text=halo nama saya ${data.name}, ingin penjelasan mengenai cara memakai Smart Maxco Apps`, "_blank", 'location=yes,menubar=no,height=570,width=520,scrollbars=yes,status=yes');
        //             //     }, 1000)
        //             // }
        //             // $(".writeinfo").append(data.msg);
        //         }
        //     });
        // }
    });
    // });

    var optionAreaCd = "";
    var NationalityDt = "";
    var areaCode = "";
    nationalityDt.map(function(item) {
        const dt = `<option value="${item.value}">${item.text}</option>`;
        const codeDt = `<option value="${item.AreaCode}">${item.AreaCode}</option>`;
        areaCode += codeDt;
        NationalityDt += dt;
    });
    $('#Nationality').append(NationalityDt)
    $('#AreaCodeSelect').append(areaCode)

    $('#AreaCodeSelect').val('+ 62');
    $('#AreaCode').val('62');


    $('#AreaCodeSelect').on('change', function(event) {
        var value = event.currentTarget.value;
        value = value.replace("+ ", "");
        $('#AreaCode').val(value);
    })
    // Set the date we're counting down to
    var countDownDate = new Date().getTime();
    countDownDate = countDownDate + (2 * 60 * 60 * 1000);

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            // document.getElementById("demo").innerHTML = "EXPIRED";
        } else {
            $('#day').text(days);
            $('#hours').text(hours);
            $('#minutes').text(minutes);
            $('#second').text(seconds);
        }
    }, 1000);

    $('.copyToClipboard').on('click', function() {
        $(this).attr('data-original-title');
        var element = this;
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        // alert("Copied the text: " + $(element).text());
        // $(this).attr('data-original-title',`Copied the text : ${$(element).text()}`)
        // $('[data-toggle="tooltip"]').tooltip()
        $temp.remove();
    })

    $('[data-toggle="tooltip"]').tooltip()

    $('.select2-single').select2();
</script>
@endsection

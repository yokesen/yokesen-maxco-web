@extends('page.template.ads.layout')

@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','Accept All Expert Advisor')
@section('og-image',url('/').'/web/images/campign/robot_ai.png')
@section('og-description','Trading di Smart Maxco bebas menggunakan segala tipe Expert Advisor')

@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection


@section('facebookPixel')
@if(isset($fbqid))
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '{{$fbqid}}');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id={{$fbqid}}&ev=PageView
        &noscript=1" />
</noscript>
@endif
@endsection

@section('fbtrack')
@if(isset($fbqid))
<script>
    fbq('track', 'ViewContent');
</script>
@endif
@endsection

@section('cssonpage')
<style>

</style>
@endsection

@section('content')
@if(!isset($viewVerifiCode))
<div class="pix_section pix-padding-v-30 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Selamat datang di Industry 4.0</strong></span>
                    </h2>
                    <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            Era di mana segala sesuatunya bisa dibuat menjadi otomatis.
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <!-- <img src="{{url('/')}}/web/images/campign/era_4.0.png" alt="" class="img-responsive"> -->
                    <img src="{{url('/')}}/web/images/campign/robot_ai.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-40">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text">Termasuk trading forex, bagi Anda yang sudah memiliki sistem atau pola trading yang teruji sekarang bisa juga dibikin otomatis dengan menggunakan Expert Advisor.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Keuntungan menggunakan Expert Advisor</strong></span>
                    </h1>
                    <!-- <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            From logo design to website designers and develope are ready to complete perfect your custom jobs.
                        </span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-5 col-xs-12 text-center">
                <div class="pix-content pix-padding-v-10 pix-margin-v-30">
                    <img src="{{url('/')}}/web/images/campign/advisor_1.png" alt="" class="img-responsive" style="width:75%;margin:5px auto!important;">
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="pix-content">
                    <div class="pix_section inner_section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/24hoours.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>24 jam peluang trading</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Dengan menggunakan EA Anda tidak perlu setiap saat berada di depan layar monitor untuk mencari peluang trading.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/millionaire.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>Asisten terhandal</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Selain mencarikan peluang trading, robot trading juga bisa diperintahkan untuk menutup posisi pada level tertentu (TP/SL).</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                                        <div class="media ic">
                                            <div class="media-left pix-icon-area text-center pix-padding-20">
                                                <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                                                    <img src="{{url('/')}}/web/images/campign/variablespread.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                                                    <!-- <i class="pixicon-lightbulb big-icon-50 pix-white"></i> -->
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                                                    <span class="pix_edit_text"><strong>Canggih dan Cepat</strong></span>
                                                </h4>
                                                <p class="pix-black-gray-light pix-margin-bottom-30">
                                                    <span class="pix_edit_text">Segala perhitungan indikator yang mungkin membutuhkan waktu berjam-jam dapat diselesaikan perhitungannya oleh robot dalam hitungan detik.</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content">
                    <p class="pix-black-gray big-text text-center">
                        <span class="pix_edit_text">Menyambut hal ini Smart Maxco mempersilahkan trading dengan menggunakan Expert Advisor.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h3 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Trading di Smart Maxco bebas menggunakan segala tipe Expert Advisor</strong></span>
                    </h3>
                    <!-- <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            From logo design to website designers and develope are ready to complete perfect your custom jobs.
                        </span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-offset-1 col-md-10 col-xs-12">
                <div class="pix-content text-center pix-radius-3">
                    <img src="{{url('/')}}/web/images/campign/expert_advisor2.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/)}}/web/images/campign/secure.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Anti Kecurangan</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Di saat broker lain melarang penggunaan Expert Advisor, kami justru mendeklarasikan silahkan menggunakan segala tipe Ea dan tidak akan ada kecurangan.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/)}}/web/images/campign/bank.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Kekuatan Financial Terbesar</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Hal ini tentunya karena kami bukan broker bandar, kami adalah pialang bergengsi dengan kekuatan keuangan terbesar di Indonesia.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/)}}/web/images/campign/balance.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Regulasi Bappebti</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Selain itu tentunya akan lebih nyaman jika Anda bertransaksi di broker yang terdaftar dan diawasi oleh Pemerintah melalui Bappebti.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Maxco Futures</strong></span>
                    </h2>
                    <p class="pix-black-gray-light big-text text-justify">
                        <span class="pix_edit_text">MaxcoFutures adalah perusahaan pialang berjangka dengan kekuatan keuangan terbesar di Indonesia, dimana kami merupakan perusahaan afiliasi Panin Group. Dengan legalitas dan diawasi langsung oleh Pemerintah, keinginan kami agar anda dapat bertransaksi dengan perasaan aman dan nyaman, tanpa perlu kuatir tidak dibayar. Berapapun profit anda, pasti kami bayar.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="pix_section pix-padding-v-50 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-12 pix-padding-h-30 text-center">
                <div class="pix-content pix-padding-v-40 pix-margin-v-10 content-center">
                    <img src="{{url('/')}}/web/images/campign/free_trial.png" alt="" class="img-responsive">
                </div>
                <div class="pix-content text-center">
                    <div>
                        <h4 class="h4ix-black-gray-light big-text pix-margin-bottom-20">
                            <span class="pix_edit_text">Dapatkan <span style="font-size: 28px;line-height: 45px;" class="pix-red-panin"><strong>Modal Demo $10.000</strong></span> untuk mencoba Expert Advisor Anda di platform handal Smart Maxco.</span>
                        </h4>
                        <!-- <h2 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                            <span class="pix_edit_text"><strong>DAFTAR SEKARANG</strong></span>
                        </h2> -->
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xs-12 {{isset($viewVerifiCode)?'pix-margin-top-100':''}}" style="border-radius:3px;">
                <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
                    @if(isset($viewVerifiCode))
                    <h4 class="pix-black-gray-dark pix-margin-bottom-15 secondary-font text-center">
                        <span class="pix_edit_text"><strong>Verification Code - Register !</strong></span>
                    </h4>
                    <form method="post" action="{{route('postRegisterVerification')}}" class="pix-form-style pixfort-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="VerifyCode" class="form-control" placeholder="Verify Code" autocomplete="off">
                            @if (session('ErrorMessage') || isset($ErrorMessage))
                            <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                            @endif
                        </div>
                        <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block"><strong>REGISTER</strong></button>
                    </form>

                    @else
                    <div class="pix-content text-center">
                        <h3 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                            <span class="pix_edit_text"><strong>DAFTAR SEKARANG</strong></span>
                        </h3>
                    </div>

                    <form id="registerForm" class="pix-form-style pixfort-form" method="POST" action="{{route('postapiregistermaxco')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="Realname" class="form-control" placeholder="Full Name" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Realname'):''}}">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="Email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="IDNO" class="form-control" placeholder="No ID" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.IDNO'):''}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="Password" class="form-control" placeholder="Password" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password" required="">
                                </div>
                            </div>
                            <div style="display: inline-flex;">

                              <div class="col-md-3 col-xs-3" style="padding-right:5px;">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone" value="+62" disabled>
                                  </div>
                              </div>
                              <div class="col-md-9 col-xs-9" style="padding-left:5px;">
                                  <div class="form-group">
                                      <input type="text" name="Mobile" class="form-control" placeholder="Phone" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Mobile'):''}}">
                                  </div>
                                  <label id="Mobile-error" class="error" style="display: none;" for="Mobile">This field is required.</label>
                              </div>
                            </div>
                            <input type="hidden" name="Nationality" value="Indonesia">
                            <input type="hidden" name="originRegister" value="acceptallea">
                            <input type="hidden" name="ParentId" class="form-control" placeholder="Code Reference" value="{{Input::get('ref') ? Input::get('ref') : Cookie::get('ref') }}">
                            <input class="" type="hidden" id="AreaCode" name="AreaCode" value="62"/>
                            <input class="" type="hidden" id="InfoType" name="InfoType" value="0" />
                            <input type="hidden" name="ref" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
                            <input type="hidden" name="so" value="<?php if (isset($_GET['so'])) {
                                                                        echo $_GET['so'];
                                                                    } else {
                                                                        echo '';
                                                                    } ?>" />
                            <input type="hidden" name="campaign" value="<?php if (isset($_GET['campaign'])) {
                                                                            echo $_GET['campaign'];
                                                                        } else {
                                                                            echo '';
                                                                        } ?>" />
                        </div>
                        @if (session('ErrorMessage') || isset($ErrorMessage))
                        <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                        @endif
                        <button type="submit" class="btn bg-red-panin pix-white btn-lg small-text btn-block"><strong>Daftar Sekarang</strong></button>
                    </form>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsonpage')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="{{url('/')}}/data/nationality.js"></script>

<script>
    // $(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Gunakan minimal 5 karakter dengan campuran, huruf, angka dan simbol."
    );
    $("#registerForm").validate({
        ignore: [],
        // errorElement: "div",
        rules: {
            Nationality: {
                required: true
            },
            Realname: {
                required: true
            },
            // AreaCodeSelect: {
            //     required: true
            // },
            IDNO: {
                required: true
            },
            areaCodeTxt: {
                required: true
            },
            Mobile: {
                required: true,
                number: true
            },
            Email: {
                required: true,
                email: true
            },
            Password: {
                required: true,
                minlength: 5,
                maxlength: 30,
                regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
            },
            ConfirmPassword: {
                required: true,
                // minlength: 6,
                equalTo: "[name='Password']"
            },

        },
        messages: {
            rulesPassword: {
                // minlength: "Password must be at least {0} characters!"
            },
            ConfirmPassword: {
                equalTo: "Passwords must match!",
                // minlength: "Password must be at least {0} characters!"
            }
        },
        // submitHandler: function(form) {
        //     $.ajax({
        //         /* the route pointing to the post function */
        //         url: "{{route('postapiregistermaxco')}}",
        //         type: 'POST',
        //         /* send the csrf-token and the input to the controller */
        //         data: {
        //             // 'name': $('[name="name"]').val(),
        //             'email': $('[name="email"]').val(),
        //             // 'phone': $('[name="phone"]').val(),
        //             // 'parent': $('[name="parent"]').val(),
        //             // 'origin': $('[name="origin"]').val(),
        //             // 'campaign': $('[name="campaign"]').val(),
        //         },
        //         dataType: 'JSON',
        //         /* remind that 'data' is the response of the AjaxController */
        //         success: function(massage,data) {
        //             // if (data.success) {
        //             //     $("#opt-in").hide();
        //             //     setTimeout(function() {
        //             //         $(".thankyou-alert").show();
        //             //         window.open(`https://api.whatsapp.com/send?phone={{Cookie::get('salesWhatsapp') ? Cookie::get('salesWhatsapp') : '6285218895459'}}&text=halo nama saya ${data.name}, ingin penjelasan mengenai cara memakai Smart Maxco Apps`, "_blank", 'location=yes,menubar=no,height=570,width=520,scrollbars=yes,status=yes');
        //             //     }, 1000)
        //             // }
        //             // $(".writeinfo").append(data.msg);
        //         }
        //     });
        // }
    });
    // });

    // var optionAreaCd = "";
    // var NationalityDt = "";
    // var areaCode = "";
    // nationalityDt.map(function(item) {
    //     const dt = `<option value="${item.value}">${item.text}</option>`;
    //     const codeDt = `<option value="${item.AreaCode}">${item.AreaCode}</option>`;
    //     areaCode += codeDt;
    //     NationalityDt += dt;
    // });
    // $('#Nationality').append(NationalityDt)
    // $('#AreaCodeSelect').append(areaCode)

    // $('#AreaCodeSelect').val('+ 62');
    // $('#AreaCode').val('62');


    $('#AreaCodeSelect').on('change', function(event) {
        var value = event.currentTarget.value;
        value = value.replace("+ ", "");
        $('#AreaCode').val(value);
    })
    // Set the date we're counting down to
    var countDownDate = new Date().getTime();
    countDownDate = countDownDate + (2 * 60 * 60 * 1000);

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            // document.getElementById("demo").innerHTML = "EXPIRED";
        } else {
            $('#day').text(days);
            $('#hours').text(hours);
            $('#minutes').text(minutes);
            $('#second').text(seconds);
        }
    }, 1000);

    $('.copyToClipboard').on('click', function() {
        var element = this;
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        // alert("Copied the text: " + $(element).text());
        // $(this).attr('data-original-title',`Copied the text : ${$(element).text()}`)
        // $('[data-toggle="tooltip"]').tooltip()
        $temp.remove();
    })

    $('[data-toggle="tooltip"]').tooltip()

    $('.select2-single').select2();
</script>
@endsection

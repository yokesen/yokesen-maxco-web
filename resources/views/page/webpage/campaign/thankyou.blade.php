@extends('page.template.ads.layout')

@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','Aplikasi Trading Forex Terbaik')
@section('og-image','/web/images/campign/getsmartmaxcoapps.png')
@section('og-description','Memperkenalkan Smart Maxco App, sebuah platform all-in-one app untuk kebutuhan investasi anda. Sebuah terobosan yang memudahkan segala urusan trading forex, mulai dari pembukaan akun sesuai dengan peraturan Bappebti, deposit, withdrawal, hingga trading dalam satu platform.')

@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


@section('facebookPixel')
@if(isset($fbqid))
<!-- Facebook Pixel Code -->
<script>
  ! function(f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function() {
      n.callMethod ?
        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
  }(window, document, 'script',
    'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '{{$fbqid}}');
</script>
<noscript>
  <img height="1" width="1" src="https://www.facebook.com/tr?id={{$fbqid}}&ev=PageView
        &noscript=1" />
</noscript>
@endif
@endsection

@section('fbtrack')
@if(isset($fbqid))
<script>
  fbq('track', 'ViewContent');
</script>
@endif
@endsection

@section('cssonpage')
<style>

</style>
@endsection

@section('content')

<div class="pix_section pix-padding-top-60">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="pix-content text-center pix-padding-v-75 pix-marketing-smartmaxco pix-radius-3 pix-margin-v-20">
          <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Terima kasih, Anda telah mengunduh aplikasi Smart Maxco App.</strong></span>
          </h2>
          <h6 class="pix-black-gray-dark pix-margin-bottom-30">
            <span class="pix_edit_text">Untuk menikmati kemudahan dalam trading silahkan Open Account.</span>
          </h6>
          <a href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" class="btn green-bg btn-lg pix-white bg-blue-panin">
            <span class="pix_edit_text">
              <strong>Open Account</strong>
            </span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@section('jsonpage')

@endsection
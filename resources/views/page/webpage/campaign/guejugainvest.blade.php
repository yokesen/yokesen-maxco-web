@extends('page.template.ads.layout')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

<?php $image='/web/images/campign/guejugainvest.jpeg';?>

@section('og-title','Aplikasi Trading Forex Terbaik')
@section('og-image',$image)
@section('og-description','Memperkenalkan Smart Maxco App, sebuah platform all-in-one app untuk kebutuhan investasi anda. Sebuah terobosan yang memudahkan segala urusan trading forex, mulai dari pembukaan akun sesuai dengan peraturan Bappebti, deposit, withdrawal, hingga trading dalam satu platform.')


@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('cssonpage')

@endsection

@section('content')
<div class="pix_section pix-padding-v-30 ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
        <div class="pix-content pix-padding-bottom-30">
          <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Gue Mahasiswa Tapi GW Punya Bisnis Sendiri #GUEJUGAINVEST</strong></span>
          </h2>
          <!-- <p class="pix-slight-white pix-no-margin-top big-text">
            <span class="pix_edit_text">
              Di era Industri 4.0 ini hidup kita ga pernah lepas dari dunia digital
            </span>
          </p> -->
        </div>
      </div>
      <div class="col-md-12 col-xs-12">
        <div class="pix-content text-center pix-radius-3">
          <img src="{{url('/').$image}}" alt="" class="img-responsive">
        </div>
      </div>
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
        <div class="pix-content pix-padding-v-40">
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Kaum mahasiswa adalam kaum intelektual, generasi penerus bangsa, dan masa depan kita semua. Tapi tidak semua dalam hidup ini bisa berjalan mulus, ada kalanya seorang mahasiswa bisa kekurangan biaya untuk melanjutkan kuliahnya. Bagi beberapa orang yang punya jiwa pejuang akan langsung mencari pekerjaan ataupun bisnis. Kendalanya jika bekerja, kadang studi jadi terganggu waktunya, maka bisnis adalah pilihan yang paling tepat.</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">SmartMaxco peduli akan hal ini, dan untuk mewujudkan hal tersebut, kami membuka kesempatan untuk mahasiswa maupun generasi millennial yang kurang beruntung yang tidak bisa kuliah.</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Mulai sekarang kamu bisa bilang :</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text"><strong>“Gue mahasiswa tapi gw punya bisnis sendiri #guejugainvest”</strong> atau <br><strong>“Gue millennial tapi gw punya bisnis sendiri #guejugainvest”</strong>
          </span>
          </p>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="pix_section pix-padding-v-30 ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
        <div class="pix-content pix-padding-bottom-30">
          <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Very Simple Marketing 4.0 For Your Passive Income</strong></span>
          </h2>
          <p class="pix-slight-white pix-no-margin-top big-text">
            <span class="pix_edit_text">
              Di era Industri 4.0 ini hidup kita ga pernah lepas dari dunia digital
            </span>
          </p>
        </div>
      </div>
      <div class="col-md-12 col-xs-12">
        <div class="pix-content text-center pix-radius-3">
          <img src="{{url('/')}}/web/images/campign/simplemarketing.jpeg" alt="" class="img-responsive">
        </div>
      </div>
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
        <div class="pix-content pix-padding-v-40">
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Di era Industri 4.0 ini hidup kita ga pernah lepas dari dunia digital. Seperti Anda saat ini sudah trading memakai perangkat digital bukan?</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Tahukah Anda bahwa di dunia digital ini, selain mendapatkan penghasilan dari trading, Anda juga bisa memanfaatkan marketing 4.0 sebagai tambahan penghasilan untuk Anda.</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Dengan memanfaatkan Hypertext Transfer Protocol request kita bisa memanfaatkan digital untuk memberikan tracker terhadap afiliasi yang Anda hasilkan.</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Waduh, rumit ya?</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Mohon maaf, tadi kalimatnya terlalu teknis.</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Di Smart Maxco kami gamau bikin Anda pusing, kami ingin Anda mendapatkan kemudahan sesimple mungkin. Oleh karena itu kami telah membuat sebuah platform Very Simple Marketing 4.0, yaitu sebuah platform yang bisa anda gunakan untuk mencari tambahan penghasilan (passive income).</span>
          </p>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="pix_section pix-padding-v-40">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
        <div class="pix-content pix-padding-bottom-30">
          <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Make More Money With Referral Program</strong></span>
          </h2>
          <p class="pix-black-gray-light big-text text-center">
            <span class="pix_edit_text">Pertanyaannya adalah gimana sih caranya bisa dapat penghasilan tambahan dari SmartMaxco ?</span>
          </p>
        </div>
        <div class="pix-content pix-padding-v-40">
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Berikut penjelasan bagaimana model bisnis yang kami tawarkan :</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Dari infografis di atas saya rangkumkan berikut :</span>
          </p>
        </div>

      </div>
      <div class="col-md-6 col-xs-12 text-center">
        <img src="{{url('/')}}/web/images/campign/pohon_uang.jpg" alt="" class="img-responsive" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);">
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="pix-content">
          <div class="pix_section inner_section">
            <div class="container">
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                    <div class="media ic">
                      <div class="media-left pix-icon-area text-center">
                        <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                          <img src="{{url('/')}}/web/images/campign/budgeting.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                        </div>
                      </div>
                      <div class="media-body">
                        <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                          <span class="pix_edit_text"><strong>Daftar</strong></span>
                        </h4>
                        <p class="pix-black-gray-light pix-margin-bottom-30">
                          <span class="pix_edit_text">Cukup daftar dengan klik <a href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" class="pix-orange"><strong>DISINI.</strong></a></span>
                          <!-- (anchor to call to action page, funnel pendaftaran referral) -->
                        </p>
                      </div>
                    </div>
                  </div>
                  <span class="x-feature-box-connector lower" style="font-size: 60px;left: 60px;right: calc(100% - 60px);border-left: 3px solid rgba(1, 125, 199, 1);position: absolute;z-index: -1;height: calc(50% - 39px);top: calc(50% + 39px);"></span>
                </div>
                <div class="col-md-12 col-xs-12">
                  <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                    <div class="media ic">
                      <div class="media-left pix-icon-area text-center">
                        <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                          <img src="{{url('/')}}/web/images/campign/mobile_banking.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                        </div>
                      </div>
                      <div class="media-body">
                        <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                          <span class="pix_edit_text"><strong>Konfirmasi</strong></span>
                        </h4>
                        <p class="pix-black-gray-light pix-margin-bottom-30">
                          <span class="pix_edit_text">Konfirmasi nomor whatsapp Anda dan Login.</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <span class="x-feature-box-connector lower" style="font-size: 60px;left: 60px;right: calc(100% - 60px);border-left: 3px solid rgba(1, 125, 199, 1);position: absolute;z-index: -1;height: 100%;top: 0px;"></span>
                </div>
                <div class="col-md-12 col-xs-12">
                  <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                    <div class="media ic">
                      <div class="media-left pix-icon-area text-center">
                        <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                          <img src="{{url('/')}}/web/images/campign/computer.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                        </div>
                      </div>
                      <div class="media-body">
                        <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                          <span class="pix_edit_text"><strong>Dapatkan Link</strong></span>
                        </h4>
                        <p class="pix-black-gray-light pix-margin-bottom-30">
                          <span class="pix_edit_text">Dapatkan link share campaign.</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <span class="x-feature-box-connector lower" style="font-size: 60px;left: 60px;right: calc(100% - 60px);border-left: 3px solid rgba(1, 125, 199, 1);position: absolute;z-index: -1;height: 100%;top: 0px;"></span>
                </div>
                <div class="col-md-12 col-xs-12">
                  <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                    <div class="media ic">
                      <div class="media-left pix-icon-area text-center">
                        <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                          <img src="{{url('/')}}/web/images/campign/transfer.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                        </div>
                      </div>
                      <div class="media-body">
                        <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                          <span class="pix_edit_text"><strong>Share Campaign</strong></span>
                        </h4>
                        <p class="pix-black-gray-light pix-margin-bottom-30">
                          <span class="pix_edit_text">Share campaign ke network Anda.</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <span class="x-feature-box-connector lower" style="font-size: 60px;left: 60px;right: calc(100% - 60px);border-left: 3px solid rgba(1, 125, 199, 1);position: absolute;z-index: -1;height: 100%;top: 0px;"></span>
                </div>
                <div class="col-md-12 col-xs-12">
                  <div class="pix-content pix-padding-v-10 pix-margin-v-10">
                    <div class="media ic">
                      <div class="media-left pix-icon-area text-center">
                        <div class="pix-cirlce-5 border-bluepanin-3px pix-margin-right-5 big-icon-50" style="padding: 0px !important">
                          <img src="{{url('/')}}/web/images/campign/piggy_bank.png" alt="" class="img-responsive" style=" border-radius: 50%;">
                        </div>
                      </div>
                      <div class="media-body">
                        <h4 class="pix-black-gray-dark secondary-font pix-no-margin-bottom">
                          <span class="pix_edit_text"><strong>Income</strong></span>
                        </h4>
                        <p class="pix-black-gray-light pix-margin-bottom-30">
                          <span class="pix_edit_text">Tunggu hasilnya.</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <span class="x-feature-box-connector lower" style="font-size: 60px;left: 60px;right: calc(100% - 60px);border-left: 3px solid rgba(1, 125, 199, 1);position: absolute;z-index: -1;height: 50%;top: 0px;"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="pix_section pix-padding-v-40 gray-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-xs-12">
        <div class="pix-content pix-padding-v-10 content-center">
          <img src="{{url('/')}}/web/images/campign/whatsapp.png" alt="" class="img-responsive">
        </div>
        <h4 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
          <span class="pix_edit_text"><strong>Masih bingung dengan program ini?<br>
              Chatting dengan referral Anda sekarang
            </strong></span>
        </h4>
        @if(!$agent->isMobile())
        <div class="pix-content text-center pix-margin-v-20 ">
          <h5 class="pix-black-gray-dark secondary-font">
            <span class="pix_edit_text"><strong>Atau</strong></span>
          </h5>
          <a href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" class="btn bg-blue-panin pix-white btn-lg" style="margin-bottom:5px;width: 50%;">Daftar Sekarang</a>
        </div>
        @endif
      </div>
      <div class="col-md-5 col-xs-12" style="margin-top: 25px;">
        <div class="pix-content thankyou-alert pix-padding-20" style="display: none;min-height:300px;">
          <div class="alert alert-success text-center bg-blue-panin pix-padding-20 pix-radius-3 vertical-center" style="display: block;">
            <p style="color:#fff;">Terima kasih, Anda sekarang terhubung dengan </p>
          </div>
        </div>
        <div class="pix-content form-registration bg-blue-panin pix-padding-20 pix-radius-3">
          <h4 class="pix-black-gray-dark pix-no-margin-bottom pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Info Selengkapnya</strong></span>
          </h4>

          <form id="opt-in" class="pix-form-style pixfort-form" novalidate="novalidate">
            <input type="hidden" name="_token" value="I0OgmB5vGM3aQbMIIuiFrh6xqGO5TobvrDGo2ckw"> <input type="hidden" name="parent" value="9999">
            <input type="hidden" name="origin" value="direct">
            <input type="hidden" name="campaign" value="1">
            <input type="hidden" name="media" value="">
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Nama" required="">
              <label id="name-error" class="error pix-red-panin" for="name" style="display: none;">This field is required.</label>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="Email" required="">
              <label id="email-error" class="error pix-red-panin" for="email" style="display: none;">This field is required.</label>

            </div>
            <div class="form-group">
              <input type="text" name="phone" class="form-control" placeholder="Whatsapp" required="">
              <label id="phone-error" class="error pix-red-panin" for="phone" style="display: none;">This field is required.</label>
            </div>
            <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block">Submit</button>
            <button disabled="" class="btn red-bg pix-white btn-lg small-text btn-block register-btn" style="display: none"> <i class="fa fa-spinner fa-spin"></i>
              Registering</button>
          </form>
        </div>
        @if($agent->isMobile())
        <div class="pix-content text-center pix-margin-v-20">
          <h5 class="pix-black-gray-dark secondary-font">
            <span class="pix_edit_text"><strong>Atau</strong></span>
          </h5>
          <a href="{{env('CABINET_URL')}}register?ref={{Input::get('ref') ? Input::get('ref') : cookie::get('ref')}}&so={{Input::get('so') ? Input::get('so') : cookie::get('so')}}&campaign={{Input::get('campaign') ? Input::get('campaign') : cookie::get('campaign')}}&uuid={{Input::get('uuid') ? Input::get('uuid') : cookie::get('uuid')}}" target="_blank" class="btn bg-blue-panin pix-white btn-lg" style="margin-bottom:5px;width: 100%;">Daftar Sekarang</a>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection

@section('jsonpage')
<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('.linkdownload').on('click', function() {
    setTimeout(function() {
      window.location.href = "{{route('thankyou')}}";
    }, 3000);
  });

  $.validator.addMethod(
    "regex",
    function(value, element, regexp) {
      var re = new RegExp(regexp);
      return this.optional(element) || re.test(value);
    },
    "Please check your input."
  );
  $("#opt-in").validate({
    rules: {
      // simple rule, converted to {required:true}
      name: "required",
      // compound rule
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        regex: "([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})"
      }
    },
    submitHandler: function(form) {
      $('[type="submit"]').hide();
      $('.register-btn').show();
      $.ajax({
        /* the route pointing to the post function */
        url: "{{route('postinsertcrm')}}",
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {
          'name': $('[name="name"]').val(),
          'email': $('[name="email"]').val(),
          'phone': $('[name="phone"]').val(),
          'parent': $('[name="parent"]').val(),
          'origin': $('[name="origin"]').val(),
          'campaign': $('[name="campaign"]').val(),
        },
        dataType: 'JSON',
        /* remind that 'data' is the response of the AjaxController */
        success: function(data) {
          if (data.success) {
            $(".form-registration").hide();
            setTimeout(function() {
              $(".thankyou-alert").show();
              $(".alert").show();
              window.open(`https://api.whatsapp.com/send?phone={{Cookie::get('salesWhatsapp') ? Cookie::get('salesWhatsapp') : '6285218895459'}}&text=halo nama saya ${data.name}, ingin penjelasan mengenai cara memakai Smart Maxco Apps`, "_blank", 'location=yes,menubar=no,height=570,width=520,scrollbars=yes,status=yes');
            }, 1000)
          } else {
            $('[type="submit"]').show();
            $('.register-btn').hide();
          }
          // $(".writeinfo").append(data.msg);
        }
      });
    }
  });
</script>
@endsection
@extends('page.template.ads.layout')

@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','Easy Way Access Trading')
@section('og-image',url('/').'/web/images/campign/easywayaccesstrading.jpg')
@section('og-description','Instal Smart Maxco App sekarang dan transaksi dengan perasaan aman, dapatkan akses ke pasar beserta beritanya, pembukaan rekening trading sesuai peraturan Bappebti, Setoran dan Penarikan dana langsung dari gengaman tangan Anda.')

@section('facebookPixel')
@if(isset($fbqid))
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '{{$fbqid}}');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id={{$fbqid}}&ev=PageView
        &noscript=1" />
</noscript>
@endif
@endsection

@section('fbtrack')
@if(isset($fbqid))
<script>
    fbq('track', 'ViewContent');
</script>
@endif
@endsection

@section('csslist')
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
    .slick-prev:before,
    .slick-next:before {
        font-size: 33px !important;
        color: #948a8a;
    }
</style>
@endsection

@section('content')

<div class="pix_section pix-padding-top-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Easy Way Access Trading</strong></span>
                    </h1>
                    <!-- <p class="pix-slight-white pix-no-margin-top big-text">
                        <span class="pix_edit_text">
                            From logo design to website designers and develope are ready to complete perfect your custom jobs.
                        </span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="pix-content pix-margin-bottom-30">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/X4mZ308hgW8?autoplay=1&rel=0"" frameborder=" 0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <div class="pix-content thankyou-alert pix-padding-20" style="display: none;min-height:{{$agent->isMobile()?'115px':'300px'}};">
                    <div class="alert alert-success text-center bg-blue-panin pix-padding-20 pix-radius-3 vertical-center" style="display: block;max-width: 333px;">
                        <p style="color:#fff;" id="thankyou-text">Terima kasih, Anda sekarang terhubung dengan {{Cookie::get('salesName')}}</p>
                    </div>
                </div>
                <div class="pix-content form-registration bg-blue-panin pix-padding-20 pix-radius-3">
                    <h4 class="pix-black-gray-dark pix-no-margin-bottom pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Info Selengkapnya</strong></span>
                    </h4>
                    <!-- <p class="pix-black-gray-light">
                        <span class="pix_edit_text">From logo design to website designers and develope.</span>
                    </p> -->
                    <form id="opt-in" class="pix-form-style pixfort-form">
                        @csrf
                        <input type="hidden" name="parent" value="{{!isset($_GET['ref'])?cookie::get('ref'):$_GET['ref']}}" />
                        <input type="hidden" name="origin" value="{{!isset($_GET['so'])?cookie::get('so'):$_GET['so']}}" />
                        <input type="hidden" name="campaign" value="{{!isset($_GET['campaign'])?cookie::get('campaign'):$_GET['campaign']}}" />
                        <input type="hidden" name="media" value="{{isset($_GET['media'])?$_GET['media']:''}}" />
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Nama" required>
                            <label id="name-error" class="error pix-red-panin" for="name" style="display: none;">This field is required.</label>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                            <label id="email-error" class="error pix-red-panin" for="email" style="display: none;">This field is required.</label>

                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Whatsapp" required>
                            <label id="phone-error" class="error pix-red-panin" for="phone" style="display: none;">This field is required.</label>
                        </div>
                        <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block">Submit</button>
                        <button disabled class="btn red-bg pix-white btn-lg small-text btn-block register-btn" style="display: none"> <i class="fa fa-spinner fa-spin"></i>
                            Registering</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix-padding-v-30"></div>
<div class="pix_section pix-padding-v-30 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Smart All-In-One App For Trading</strong></span>
                    </h2>
                    <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Bagaimana jika investasi forex Anda kami jadikan sangat bergengsi dengan sebuah Smart Platform?</span>
                    </p>
                </div>
            </div>

            <div class="col-md-6 col-xs-12">
                <div class="pix-content">
                    <img src="{{url('/')}}/web/images/campign/dp-maxco-transparent.png" alt="" class="img-responsive" style="margin:5px auto!important;">
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content text-justify pix-margin-v-20 pix-padding-h-10">
                    <p class="pix-gray big-text pix-margin-bottom-20">
                        <span class="pix_edit_text">Memperkenalkan Smart Maxco App, sebuah platform all-in-one app untuk kebutuhan investasi anda. Sebuah terobosan yang memudahkan segala urusan trading forex, mulai dari pembukaan akun sesuai dengan peraturan Bappebti, deposit, withdrawal, hingga trading dalam satu platform</span>
                    </p>
                </div>
                <div class="pix-content text-center pix-margin-v-50">
                    <a href="https://play.google.com/store/apps/details?id=com.smartmaxco.co" target="_blank" class="btn bg-blue-panin btn-lg pix-white linkdownload">
                        <span class="pix_edit_text">
                            <strong>Download Sekarang</strong>
                        </span>
                    </a>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Tentang Maxco Futures</strong></span>
                    </h2>
                    <!-- <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">From logo design to website designs and developers are ready to complete perfect your custom jobs.</span>
                    </p> -->
                </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <!-- <img src="https://via.placeholder.com/336x280" alt="" class="img-responsive"> -->
                    <img src="{{url('/')}}/web/images/campign/sma_336x280.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <p class="pix-gray big-text pix-margin-bottom-30 text-justify">
                    <span class="pix_edit_text">
                        Maxco Futures adalah perusahaan pialang dengan kekuatan keuangan terbesar di Indonesia. Kami berani klaim hal tersebut karena kami adalah afiliasi dari Panin Grup, salah satu institusi keuangan terbesar di Asia Tenggara. Kami ingin agar anda merasakan prestise dalam berinvestasi, oleh karena itu Maxco Futures mempersembahkan Smart Maxco App, sebuah aplikasi Android all-in-one yang memberikan akses tanpa batas ke pasar dan transaksi dengan biaya transaksi yang rendah.
                    </span>
                </p>
                <p class="pix-gray big-text pix-margin-bottom-30 text-justify">
                    <span class="pix_edit_text">
                        Instal Smart Maxco App sekarang dan transaksi dengan perasaan aman, dapatkan akses ke pasar beserta beritanya, pembukaan rekening trading sesuai peraturan Bappebti, Setoran dan Penarikan dana langsung dari gengaman tangan Anda.</span>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Fitur Smart Maxco App</strong></span>
                    </h2>
                    <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Instal Smart Maxco App sekarang dan transaksi dengan perasaan aman, dapatkan akses ke pasar beserta beritanya, pembukaan rekening trading sesuai peraturan Bappebti, Setoran dan Penarikan dana langsung dari gengaman tangan Anda.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/saas.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Always Connected</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Selalu terhubung dengan akses pasar dan transaksi melalui Smart Maxco App.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/mobile_banking.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Pembukaan Akun Transaksi</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Mulai dari pendaftaran profil, kelengkapan dokumen, agreement, sampai dengan persetujuan.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/deposit.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Deposit & Withdrawal</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Konfirmasi deposit dan permintaan withdrawal langsung di tangan anda kapanpun dan dimanapun.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/growth_1.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Pasar</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Akses pantauan langsung ke harga pasar secara real time langsung di satu platform.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/stocks.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Trading</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Kemudahan transaksi dalam satu platform, Open Posisi, Update TP/SL, Close posisi.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/monitoring.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Berita Dan Analisa</strong></span>
                    </h5>
                    <p class="pix-black-gray pix-margin-bottom-20">
                        <span class="pix_edit_text">Notifikasi berita dan analisis terbaru sebagai bahan pertimbangan anda dalam mengelola transaksi.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Why Maxco</strong></span>
                    </h2>
                    <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Keinginan kami agar anda dapat bertransaksi dengan perasaan aman dan nyaman, tanpa perlu kuatir tidak dibayar. Berapapun profit anda, pasti kami bayar.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/billionaire.jpg" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Customer First</strong></span>
                    </h5>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/taxes.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Komisi Rendah</strong></span>
                    </h5>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/balance.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Integritas</strong></span>
                    </h5>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/planning.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Accept All EA</strong></span>
                    </h5>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/identity_theft.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Transparan</strong></span>
                    </h5>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
                    <div class="pix-margin-bottom-20 pix-cirlce-4 border-bluepanin-3px" style="padding: 5px;">
                        <img src="{{url('/')}}/web/images/campign/value.png" alt="" class="img-responsive" style="width: 165px;border-radius: 50%;">
                    </div>
                    <h5 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text"><strong>Unggul Dan Terpercaya</strong></span>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-bottom-30">
                    <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>UNRIVALED FINANCIAL STRENGTH</strong></span>
                    </h2>
                    <p class="pix-black-gray-light big-text text-center">
                        <!-- <span class="pix_edit_text">From logo design to website designs and developers are ready to complete perfect your custom jobs.</span> -->
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content text-center pix-margin-v-30">
                    <div class="pix-margin-bottom-20">
                        <img src="{{url('/')}}/web/images/campign/Panin.png" alt="" class="img-responsive">
                    </div>
                    <h5 class="pix-black-gray pix-no-margin-top pix-no-margin-bottom title-fitur">
                        <span class="pix_edit_text"><strong>Strategic partner DARI PANIN GRUP</strong></span>
                    </h5>
                    <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Maxco Futures adalah perusahaan pialang berjangka dengan kekuatan keuangan terbesar di Indonesia, dimana kami merupakan perusahaan strategic partner Panin Group.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content text-center pix-margin-v-30">
                    <div class="pix-margin-bottom-20" style="height:183px;">
                        <img src="{{url('/')}}/web/images/campign/bappebti.png" alt="" class="img-responsive">
                    </div>
                    <h5 class="pix-black-gray pix-no-margin-top pix-no-margin-bottom title-fitur">
                        <span class="pix_edit_text"><strong>Broker terpercaya</strong></span>
                    </h5>
                    <p class="pix-black-gray-light big-text text-center">
                        <span class="pix_edit_text">Dengan legalitas dan diawasi langsung oleh Pemerintah, keinginan kami agar anda dapat bertransaksi dengan perasaan aman dan nyaman, tanpa perlu kuatir tidak dibayar. Berapapun profit anda, pasti kami bayar.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row row-slick-yt" data-slick='{"slidesToShow": {{$agent->isMobile()?2:4}}, "slidesToScroll": {{$agent->isMobile()?2:4}}}'>
            @foreach($videosYt as $video)
            <div class="col-md-3 col-offset-xs-1 col-xs-10">
                <div class="pix-content pix-margin-v-20">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="1903" height="768" src="{{$video['url']}}?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <p class="pix-light-black pix-margin-top-30"><span class="pix_edit_text"><strong>{{$video['title']}}</strong></span></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="pix_section pix-padding-top-30">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-12">
                <div class="pix-content content-center">
                    <img src="{{url('/')}}/web/images/campign/dp-maxco-transparent-w300.png" alt="" class="img-responsive">
                </div>
                <div class="pix-content pix-margin-bottom-30">
                    <h5 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Hubungi kami untuk cara download, install dan penjelasan lengkap cara memakai SmartMaxco Apps Anda Sekarang</strong></span>
                    </h5>
                </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <div class="pix-content thankyou-alert pix-padding-20" style="display: none;min-height:300px;">
                    <div class="alert alert-success text-center bg-blue-panin pix-padding-20 pix-radius-3 vertical-center" style="display: block;">
                        <p style="color:#fff;">Terima kasih, Anda sekarang terhubung dengan {{Cookie::get('salesName')}}</p>
                    </div>
                </div>
                <div class="pix-content form-registration bg-blue-panin pix-padding-20 pix-radius-3">
                    <h4 class="pix-black-gray-dark pix-no-margin-bottom pix-no-margin-top secondary-font">
                        <span class="pix_edit_text"><strong>Info Selengkapnya</strong></span>
                    </h4>
                    <!-- <p class="pix-black-gray-light">
                        <span class="pix_edit_text">From logo design to website designers and develope.</span>
                    </p> -->
                    <form id="opt-in-footer" class="pix-form-style pixfort-form">
                        @csrf
                        <input type="hidden" name="parent" value="{{!isset($_GET['ref'])?cookie::get('ref'):$_GET['ref']}}" />
                        <input type="hidden" name="origin" value="{{!isset($_GET['so'])?cookie::get('so'):$_GET['so']}}" />
                        <input type="hidden" name="campaign" value="{{!isset($_GET['campaign'])?cookie::get('campaign'):$_GET['campaign']}}" />
                        <input type="hidden" name="media" value="{{isset($_GET['media'])?$_GET['media']:''}}" />
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Nama" required>
                            <label id="name-error" class="error pix-red-panin" for="name" style="display: none;">This field is required.</label>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                            <label id="email-error" class="error pix-red-panin" for="email" style="display: none;">This field is required.</label>

                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Whatsapp" required>
                            <label id="phone-error" class="error pix-red-panin" for="phone" style="display: none;">This field is required.</label>
                        </div>
                        <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block">Submit</button>
                        <button disabled class="btn red-bg pix-white btn-lg small-text btn-block register-btn" style="display: none"> <i class="fa fa-spinner fa-spin"></i>
                            Registering</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsonpage')
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.linkdownload').on('click', function() {
        setTimeout(function() {
            window.location.href = "{{route('thankyou')}}";
        }, 3000);
    });


    var platform = "{{$agent->platform()}}";
    if (platform == "AndroidOS") {
        $('iframe').on("load", function() {

        });
        setTimeout(function() {
            if ($('iframe').length > 0) {
                setTimeout(function() {
                    $('.ytp-large-play-button').click();
                }, 1000)
            }
        }, 1000);
    }

    function apiinsertcrm(elid) {
        $.ajax({
            /* the route pointing to the post function */
            url: "{{route('postinsertcrm')}}",
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {
                'name': $(`#${elid} [name="name"]`).val(),
                'email': $(`#${elid} [name="email"]`).val(),
                'phone': $(`#${elid} [name="phone"]`).val(),
                'parent': $(`#${elid} [name="parent"]`).val(),
                'origin': $(`#${elid} [name="origin"]`).val(),
                'campaign': $(`#${elid} [name="campaign"]`).val(),
            },
            dataType: 'JSON',
            /* remind that 'data' is the response of the AjaxController */
            success: function(data) {
                if (data.success) {
                    $(".form-registration").hide();
                    setTimeout(function() {
                        $(".thankyou-alert").show();
                        $(".alert").show();
                        if (platform == "AndroidOS") {
                            $('#thankyou-text').text('Terima kasih telah mengunduh Smart Maxco App');
                            window.open(`https://play.google.com/store/apps/details?id=com.smartmaxco.co`, "_blank");
                        } else if (platform == "iOS") {

                        } else {
                            window.open(`https://api.whatsapp.com/send?phone={{Cookie::get('salesWhatsapp') ? Cookie::get('salesWhatsapp') : '6285218895459'}}&text=halo nama saya ${data.name}, ingin penjelasan mengenai cara memakai Smart Maxco Apps`, "_blank", 'location=yes,menubar=no,height=570,width=520,scrollbars=yes,status=yes');
                        }
                    }, 1000)
                } else {
                    $('[type="submit"]').show();
                    $('.register-btn').hide();
                }
                // $(".writeinfo").append(data.msg);
            }
        });
    }

    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );

    var rules = {
        // simple rule, converted to {required:true}
        name: "required",
        // compound rule
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            regex: "([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})"
        }
    }

    $("#opt-in").validate({
        rules,
        submitHandler: function(form) {
            $('[type="submit"]').hide();
            $('.register-btn').show();
            var formid = $(form).attr('id');
            apiinsertcrm(formid);
        }
    });

    $("#opt-in-footer").validate({
        rules,
        submitHandler: function(form) {
            $('[type="submit"]').hide();
            $('.register-btn').show();
            var formid = $(form).attr('id');
            apiinsertcrm(formid);
        }
    });
    var autoplay=false;
    @if($agent->isMobile())
        autoplay=true;
    @endif
    $('.row-slick-yt').slick({
        autoplay,
        autoplaySpeed: 2000
    });
</script>
@endsection
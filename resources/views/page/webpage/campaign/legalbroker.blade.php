@extends('page.template.ads.layout')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('og-title','Trading Forex di Smart Maxco App')
@section('og-image',url('/').'/web/images/campign/legalbroker.jpg')
@section('og-description','Trading Forex jangan sembarangan, pastiin profit anda dibayar. Trading Forex di Maxco Futures aja! #GakTakutGakDibayar')


@section('csslist')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('facebookPixel')
@if(isset($fbqid))
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '{{$fbqid}}');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id={{$fbqid}}&ev=PageView
        &noscript=1" />
</noscript>
@endif
@endsection

@section('fbtrack')
    @if(isset($fbqid))
    <script>
        fbq('track', 'ViewContent');
    </script>
    @endif
@endsection

@section('content')
@if(!isset($viewVerifiCode))
<div class="pix_section pix-padding-v-30 ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable text-center">
        <div class="pix-content pix-padding-bottom-30">
          <h2 class="pix-black-gray-dark pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Legal Broker</strong></span>
          </h2>
          <p class="pix-slight-white pix-no-margin-top big-text">
            <span class="pix_edit_text">
              Apakah Anda mengetahui betapa pentingnya legalitas broker dalam Anda bertansaksi forex?
            </span>
          </p>
        </div>
      </div>
      <div class="col-md-12 col-xs-12">
        <div class="pix-content text-center pix-radius-3">
          <img src="{{url('/')}}/web/images/campign/legalbroker.jpg" alt="" class="img-responsive">
        </div>
      </div>
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
        <div class="pix-content pix-padding-v-40">
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Jutaan orang bahkan tidak menyadari bahwa broker forex itu ada yang legal di bawah pengawasan pemeritah dan juga ada yang illegal atau disebut sebagai broker abal-abal.</span>
          </p>
          <p class="pix-black-gray big-text text-justify">
            <span class="pix_edit_text">Berikut ini adalah beberapa hal penting tentang broker :</span>
          </p>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
          <div class="pix-margin-bottom-20 ">
            <img src="{{url('/)}}/public/web/images/campign/bappebti.png" class="img-responsive">
          </div>
          <p class="pix-black-gray pix-margin-bottom-20 text-justify">
            <span class="pix_edit_text">Dengan terdaftar dan diawasi oleh Badan Pengawas Perdagangan Berjangka Komoditi (Bappebti), maka hak-hak Anda sebagai trader dilindungi oleh Negara melalui Undang-Undang.</span>
          </p>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
          <div class="pix-margin-bottom-20">
            <img src="{{url('/)}}/public/web/images/campign/logo_BBJ.png" class="img-responsive">
          </div>
          <p class="pix-black-gray pix-margin-bottom-20 text-justify ">
            <span class="pix_edit_text">Dengan legalitas dari Bappebti dan juga Bursa Berjangka Jakarta, maka transaksi anda jelas dan tercatat oleh Bursa, sehingga Anda tidak perlu kuatir mengenai kecurangan-kecurangan broker.</span>
          </p>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="pix-content text-center pix-margin-v-20 pix-padding-h-10">
          <div class="pix-margin-bottom-20">
            <img src="{{url('/')}}/web/images/campign/logo_KBI.png" class="img-responsive">
          </div>
          <p class="pix-black-gray pix-margin-bottom-20 text-justify ">
            <span class="pix_edit_text">Dengan menjadi anggota Kliring Berjangka Indonesia, maka Anda tidak perlu kuatir dengan keamanan dana investasi Anda, sebab dana tersebut disimpan di rekening terpisah (segregated account) di Bank Penitipan (custodian bank).</span>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="pix_section pix-padding-v-30 ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
        <div class="pix-content pix-padding-bottom-30">
          <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Prestigious Broker</strong></span>
          </h2>
          <p class="pix-black-gray-light big-text text-center">
            <span class="pix_edit_text">Namun SmartMaxco juga bukan hanya sekedar broker legal biasa, sesuai tagline kami Prestigious Global Brokerage yang tentunya bukan hanya sekedar slogan.</span>
          </p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-xs-12 text-center">
        <img src="{{url('/')}}/web/images/campign/unrivaled_financial_strength.jpg" alt="" class="img-responsive" style="margin:5px auto;">
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="pix-content pix-padding-top-30">
          <p class="pix-black-gray pix-margin-bottom-20">
            <span class="pix_edit_text">
              SmartMaxco adalah perusahaan pialang berjangka dengan kekuatan keuangan terbesar di Indonesia, kami merupakan perusahaan afiliasi Panin Group. Dengan legalitas dan diawasi oleh Pemerintah, kami ingin agar anda bertransaksi dengan perasaan aman dan nyaman, tanpa perlu kuatir tidak dibayar. Profit berapapun juga kami pasti bayar.
            </span>
            <h3 class="pix-black-gray-dark pix-no-margin-top text-center">
              <span class="pix_edit_text"><strong>Unrivaled Financial Strength</strong></span>
            </h3>
            <p class="pix-black-gray pix-margin-bottom-20">
              <span class="pix_edit_text">
                Artinya adalah Anda tidak perlu ragu bertransaksi sebesar apapun di Smart Maxco, karena di Indonesia tidak ada broker dengan kekuatan dana sebesar kami.
              </span>
            </p>

        </div>
        <div class="pix-content pix-padding-top-30 text-center">
          <a href="#section-registration" class="btn bg-blue-panin pix-white btn-lg wide">
            <span class="pix_edit_text">
              <strong>Daftar Sekarang</strong>
            </span>
          </a>
        </div>
      </div>
      <!-- <div class="col-md-12 col-xs-12 text-center pix-margin-v-20">
        <a href="#section-registration" class="btn bg-blue-panin pix-white btn-lg wide">
          <span class="pix_edit_text">
            <strong>Daftar Sekarang</strong>
          </span>
        </a>
      </div> -->
    </div>
  </div>
</div>

<div class="pix_section pix-padding-v-40">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
        <div class="pix-content pix-padding-bottom-30">
          <h2 class="pix-black-gray-dark text-center pix-no-margin-top secondary-font">
            <span class="pix_edit_text"><strong>Invest With A Prestigious Company Brings Peace Of Mind</strong></span>
          </h2>
          <p class="pix-black-gray-light big-text text-center">
            <span class="pix_edit_text">Nah, dengan informasi diatas, tentunya Anda bisa menyimpulkan bahwa memang betul kami adalah pialang paling bergengsi di Indonesia.</span>
          </p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-xs-12">
        <img src="{{url('/')}}/web/images/campign/piaece_of_mind_336X280.jpg" alt="" class="img-responsive" style="margin: 5px auto;border: 1px solid #d3d1d1;border-radius: 5px;">
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="pix-content pix-padding-top-30">
          <p class="pix-black-gray pix-margin-bottom-20">
            <span class="pix_edit_text">
              Benefit yang bisa Anda dapatkan adalah kenyamanan dan ketenangan Anda dalam transaksi, tanpa perlu ada kekuatiran tentang keamanan dana Anda.
            </span>
          </p>
          <p class="pix-black-gray pix-margin-bottom-20">
            <span class="pix_edit_text">
              Disaat yang diluaran sana banyak yang frustasi karena dananya dibawa kabur broker atau bahkan sudah profit tapi tidak dibayar, kami menjamin bahwa untung berapapun kami pasti bayar.
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

<div class="pix_section pix-padding-v-50 " id="section-registration">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-xs-12 pix-padding-h-30 text-center">
        <div class="pix-content content-center pix-margin-v-20">
          <img src="{{url('/')}}/web/images/campign/panin_group.png" alt="" class="img-responsive">
        </div>
        <div class="pix-content text-center">
          <div class="pix-margin-v-40">
            <h4 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
              <span class="pix_edit_text"><strong>Segera bergabung dengan perusahaan pialang paling bergengsi di indonesia.</strong></span>
            </h4>
          </div>
        </div>
      </div>
      <div class="col-md-5 col-xs-12 {{isset($viewVerifiCode)?'pix-margin-top-100':''}}" style="border-radius:3px;">
        <div class="pix-content bg-blue-panin pix-padding-20 pix-radius-3">
          @if(isset($viewVerifiCode))
          <h4 class="pix-black-gray-dark pix-margin-bottom-15 secondary-font text-center">
            <span class="pix_edit_text"><strong>Verification Code - Register !</strong></span>
          </h4>
          <form method="post" action="{{route('postRegisterVerification')}}" class="pix-form-style pixfort-form">
            @csrf
            <div class="form-group">
              <input type="text" name="VerifyCode" class="form-control" placeholder="Verify Code" autocomplete="off">
              @if (session('ErrorMessage') || isset($ErrorMessage))
              <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
              @endif
            </div>
            <button type="submit" class="btn red-bg pix-white btn-lg small-text btn-block"><strong>REGISTER</strong></button>
          </form>

          @else
          <div class="pix-content text-center">
            <h3 class="pix-orange pix-small-width-text pix-margin-bottom-20 pix-no-margin-top secondary-font">
              <span class="pix_edit_text"><strong>DAFTAR SEKARANG</strong></span>
            </h3>
          </div>

          <form id="registerForm" class="pix-form-style pixfort-form" method="POST" action="{{route('postapiregistermaxco')}}">
            @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" name="Realname" class="form-control" placeholder="Full Name" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Realname'):''}}">
                </div>
                <div class="form-group">
                  <input type="email" name="Email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" name="IDNO" class="form-control" placeholder="No ID" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.IDNO'):''}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" name="Password" class="form-control" placeholder="Password" required="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password" required="">
                </div>
              </div>
              <div style="display: inline-flex;">
                <div class="col-md-3 col-xs-3" style="padding-right:5px;">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone" value="+62" disabled>
                  </div>
                </div>
                <div class="col-md-9 col-xs-9" style="padding-left:5px;">
                  <div class="form-group">
                    <input type="text" name="Mobile" class="form-control" placeholder="Phone" value="{{session('ErrorMessage') || isset($ErrorMessage)?Session::get('demouser.Mobile'):''}}">
                  </div>
                  <label id="Mobile-error" class="error" style="display: none;" for="Mobile">This field is required.</label>
                </div>
              </div>

              <input type="hidden" name="Nationality" value="Indonesia">
              <input type="hidden" name="originRegister" value="legalbroker">
              <input type="hidden" name="ParentId" class="form-control" placeholder="Code Reference" value="{{Input::get('ref') ? Input::get('ref') : Cookie::get('ref') }}">
              <!-- {{Input::get('ref') ? Input::get('ref') : Cookie::get('ref') }} -->
              <input class="" type="hidden" id="AreaCode" name="AreaCode" value="62" />
              <input class="" type="hidden" id="InfoType" name="InfoType" value="0" />
              <input type="hidden" name="ref" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
              <input type="hidden" name="so" value="<?php if (isset($_GET['so'])) {
                                                      echo $_GET['so'];
                                                    } else {
                                                      echo '';
                                                    } ?>" />
              <input type="hidden" name="campaign" value="<?php if (isset($_GET['campaign'])) {
                                                            echo $_GET['campaign'];
                                                          } else {
                                                            echo '';
                                                          } ?>" />
            </div>
            @if (session('ErrorMessage') || isset($ErrorMessage))
            <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
            @endif
            <button type="submit" class="btn bg-red-panin pix-white btn-lg small-text btn-block"><strong>Daftar Sekarang</strong></button>
          </form>

          @endif
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('jsonpage')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="{{url('/')}}/data/nationality.js"></script>

<script>
  // $(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.validator.addMethod(
    "regex",
    function(value, element, regexp) {
      var re = new RegExp(regexp);
      return this.optional(element) || re.test(value);
    },
    "Gunakan minimal 5 karakter dengan campuran, huruf, angka dan simbol."
  );
  $("#registerForm").validate({
    ignore: [],
    // errorElement: "div",
    rules: {
      Nationality: {
        required: true
      },
      Realname: {
        required: true
      },
      // AreaCodeSelect: {
      //     required: true
      // },
      IDNO: {
        required: true
      },
      areaCodeTxt: {
        required: true
      },
      Mobile: {
        required: true,
        number: true
      },
      Email: {
        required: true,
        email: true
      },
      Password: {
        required: true,
        minlength: 5,
        maxlength: 30,
        regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
      },
      ConfirmPassword: {
        required: true,
        // minlength: 6,
        equalTo: "[name='Password']"
      },

    },
    messages: {
      rulesPassword: {
        // minlength: "Password must be at least {0} characters!"
      },
      ConfirmPassword: {
        equalTo: "Passwords must match!",
        // minlength: "Password must be at least {0} characters!"
      }
    },
    // submitHandler: function(form) {
    //     $.ajax({
    //         /* the route pointing to the post function */
    //         url: "{{route('postapiregistermaxco')}}",
    //         type: 'POST',
    //         /* send the csrf-token and the input to the controller */
    //         data: {
    //             // 'name': $('[name="name"]').val(),
    //             'email': $('[name="email"]').val(),
    //             // 'phone': $('[name="phone"]').val(),
    //             // 'parent': $('[name="parent"]').val(),
    //             // 'origin': $('[name="origin"]').val(),
    //             // 'campaign': $('[name="campaign"]').val(),
    //         },
    //         dataType: 'JSON',
    //         /* remind that 'data' is the response of the AjaxController */
    //         success: function(massage,data) {
    //             // if (data.success) {
    //             //     $("#opt-in").hide();
    //             //     setTimeout(function() {
    //             //         $(".thankyou-alert").show();
    //             //         window.open(`https://api.whatsapp.com/send?phone={{Cookie::get('salesWhatsapp') ? Cookie::get('salesWhatsapp') : '6285218895459'}}&text=halo nama saya ${data.name}, ingin penjelasan mengenai cara memakai Smart Maxco Apps`, "_blank", 'location=yes,menubar=no,height=570,width=520,scrollbars=yes,status=yes');
    //             //     }, 1000)
    //             // }
    //             // $(".writeinfo").append(data.msg);
    //         }
    //     });
    // }
  });
  // });

  $('#AreaCodeSelect').on('change', function(event) {
    var value = event.currentTarget.value;
    value = value.replace("+ ", "");
    $('#AreaCode').val(value);
  })
</script>
@endsection

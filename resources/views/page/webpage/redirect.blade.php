@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>
  .table-structure tr {
    height: 33px;
  }
</style>
@endsection

@section('content')



<!-- Content -->
<div id="content">

  <!-- SERVICES -->
  <section class="padding-top-70 padding-bottom-70" style="min-height: 450px;">

    <div class="container">
        <div class="who-we" style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">
            You will redirect after <span id="second">30</span> Second. If it dooesn't please click <a style="color:blue;" href="{{url('/')}}/{{App::getLocale()}}/{{$pageredirect}}">Link<a/>
        </div>
    </div>
  </section>
</div>

<!-- always on -->
@endsection

@section('jsonpage')
<script>
    $(document).ready(function(){
        var second = 30;
        var inter = setInterval(function(){
            second -= 1;
            rendertext(second);
        },1000);

        function rendertext(second){
            $('#second').text(second);
            if(second===0){
                stopInter();
            }
        }

        function stopInter(){
            clearInterval(inter)
        }
        
    });
</script>
@endsection
@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <!-- <section class="sub-bnr" style="background:url({{url('/')}}/web/images/bg/sub-bnr-bg-2.jpg) no-repeat" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>Forex</h1>
        Breadcrumbs
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Forex</li>
        </ol>
      </div>
    </div>
  </section> -->

  <!-- Content -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/market/FOREX_TRADING.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
  </section>
  <div id="content">
    <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
      <div class="container">
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
          <li class="active">Forex</li>
        </ol>
      </div>
    </section>
    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="row">
          <!-- Revenues Sidebar -->
          <!-- Stories -->
          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Apa itu Forex ?</h3>
              </div>
          </div>
          <div class="col-md-offset-1 col-md-10">
            <div class="col-md-4">
                <img class="img-responsive"
                    src="{{url('/')}}/web/images/webpage/market/forex/Forex.jpg">
            </div>
            <div class="col-md-8">
              <p class="text-justify">
                  FOREX merupakan suatu jenis perdagangan atau
                  transaksi yang memperdagangkan mata uang
                  suatu negara terhadap mata uang negara
                  lainnya (pasangan mata uang/pair) yang
                  melibatkan pasar-pasar uang utama di dunia
                  selama 24 jam secara berkesinambungan.
              </p>
          </div>
          </div>
          <div class="col-md-12">
              <div class="heading text-center">
                  <h3>Apa yang diperdagangkan di forex ?</h3>
              </div>
          </div>

          <div class="col-md-offset-1 col-md-10">

           <div class="justify">
             <p>
               Jawabannya adalah <b>UANG</b>.<br><br>
               Dalam forex-trading ketika anda membeli mata uang, contohnya saja Yen Jepang, Anda pada dasarnya membeli “saham” di perekonomian Jepang.<br><br>
               Anda “bertaruh” kalau perekonomian Jepang sedang dalam kondisi baik dan bahkan akan semakin membaik seiring berjalannya waktu.
               Setelah menjual kembali “saham” tersebut ke pasar, Anda berharap mendaptkan profit.<br><br>
               Secara umum, tingkat nilai tukar sebuah mata uang dengan mata uang lainnya adalah cerminan dari kondisi ekonomi negara tersebut,
               berbanding dengan kondisi ekonomi negara lain.<br><br>
               Saat lulus dari kelas trading ini, Anda akan mulai bersemangat untuk bekerja dengan uang.<br><br>
             </p>
             <br>
             <h5>Mata uang utama</h5>
             <br>
             <p> Meskipun terdapat banyak mata uang yang berpotensi untuk diperdagangkan,
               sebagai trader baru, Anda mungkin perlu memulai trading dengan jenis pasangan mata uang utama. <br>
             <p>
           </div>
           <br>
           <div class="text-center">
              <img
                   src="{{url('/')}}/web/images/webpage/market/forex/Forex.jpg"
                   class="float-left" alt="" title="">
           </div>
           <br>
           <div class="maxco-text-justify" style="display: inline-block;">
             <p> Simbol mata uang selalu diwakili tiga huruf,
               dimana dua huruf pertama adalah nama negara dan huruf ketiga adalah nama mata uang negara tersebut.
             <p>
           </div>
           <br>
           <div class="text-center">
             <img
                 src="{{url('/')}}/web/images/webpage/market/forex/currency-code.png"
                 class="float-left" alt="" title="">
           </div>
           <br>
           <br>
           <div class="text-justify" style="display: inline-block;">
             <p>
               Sebagai contoh, NZD, NZ adalah singkatan dari New Zealand dan D adalah singkatan dari dolar, Gampang kan?<br><br>
               Mata uang yang termasuk dalam tabel di atas adalah “mata uang utama” karena mata uang tersebut lebih banyak diperdagangkan.<br><br>
               Kami juga ingin memberitahu anda kalau “buck” bukanlah satu-satunya sebutan untuk USD.<br><br>
               USD dapat juga disebut: greenbacks, bones, benjis, benjamins, cheddar, paper, loot, scrilla, cheese, bread, moolah, dead presidents, and cash money.<br><br>
               Jadi, jika anda ingin mengatakan, “I have to go to work now.”<br><br>
               Anda dapat mengatakannya dengan cara lain: “Yo, I gotta bounce! Gotta make them benjis, son!”<br><br>
               Fakta: kalau Anda pergi ke Peru, sebutan dolar AS adalah Coco yang memiliki pengertian nama panggilan untuk Jorge?
               (George dalam bahasa Spanyol) gambar George Washington pada nominal uang $ 1 AS.
             <p>
           </div>
        </div>
        <div class="col-md-12">
            <div class="heading  text-center">
              <h3>Karakteristik Forex ?</h3>
            </div>
        </div>
        <div class="col-md-offset-1 col-md-10">

            <p class="text-justify">
              Tidak ada suatu keseragaman dalam pasar
              valuta asing. Dengan adanya transaksi di
              luar bursa perdagangan (over the counter)[3]
              sebagai pasar tradisional dari perdagangan
              valuta asing, banyak sekali pasar valuta
              asing yang saling berhubungan satu sama
              lainnya di mana mata uang yang berbeda
              diperdagangkan, sehingga secara tidak
              langsung artinya bahwa "tidak ada kurs
              tunggal mata uang dollar melainkan kurs yang
              berbeda-beda tergantung pada bank mana atau
              pelaku pasar mana yang bertransaksi". Namun
              dalam praktiknya perbedaan tersebut
              seringkali sangat tipis.
          </p>
          <!-- 6 Peringkat Teratas Mata Uang Yang
                Diperdagangkan
                Peringkat Mata uang ISO 4217 Kode Simbol
                1 United States dollar USD $
                2 Eurozone euro EUR €
                3 Japanese yen JPY ¥
                4 British pound sterling GBP £
                5 Swiss franc CHF -
                6 Australian dollar AUD $ -->
          <p class="text-justify">
              Pusat perdagangan utama adalah di London,
              New York, Tokyo dan Singapura namun
              bank-bank diseluruh dunia menjadi
              pesertanya. Perdagangan valuta asing terjadi
              sepanjang hari. Apabila pasar Asia berakhir
              maka pasar Eropa mulai dibuka dan pada saat
              pasar Eropa berakhir maka pasar Amerika
              dimulai dan kembali lagi ke pasar Asia,
              terkecuali di akhir pekan.
          </p>
          <p class="text-justify">
              Sangat sedikit atau bahkan tidak ada
              "perdagangan orang dalam" atau informasi
              "orang dalam" (Insider trading) [4] yang
              terjadi dalam pasar valuta asing. Fluktuasi
              kurs nilai tukar mata uang biasanya
              disebabkan oleh gejolak aktual moneter
              sebagaimana juga halnya dengan ekspektasi
              pasar terhadap gejolak moneter yang
              disebabkan oleh perubahan dalam pertumbuhan
              Produk Domestik Bruto (PDB/GDP), inflasi,
              suku bunga, rancangan anggaran dan defisit
              perdagangan atau surplus perdagangan,
              penggabungan dan akuisisi serta kondisi
              makro ekonomi lainnya. Berita utama selalu
              dipublikasikan untuk umum, sehingga banyak
              orang dapat mengakses berita tersebut pada
              saat yang bersamaan. Namun bank yang besar
              memiliki nilai lebih yang penting yaitu
              mereka dapat melihat arus pergerakan
              "pesanan" mata uang dari nasabahnya.
          </p>
          <p class="text-justify">
              Mata uang diperdagangkan satu sama lainnya
              dan setiap pasangan mata uang merupakan
              suatu produk tersendiri seperti misalnya
              EUR/USD, USD/JPY, GBP/USD dan lain-lain.
              Faktor pada salah satu mata uang misalnya
              USD akan memengaruhi nilai pasar pada
              USD/JPY dan GBP/USD, ini adalah merupakan
              korelasi antara USD/JPY dan GBP/USD.
          </p>
          <p class="text-justify">
              Pada pasar spot, menurut penelitian yang
              dilakukan oleh Bank for Internasional
              Settlement (BIS)[5], produk yang paling
              sering diperdagangkan adalah
              <p class="text-justify">
                  <ul class="dt-sc-fancy-list orange arrow">
                      <li>EUR/USD - 28 %</li>
                      <li>USD/JPY - 18 %</li>
                      <li>GBP/USD (also called sterling or cable) - 14%</li>
                  </ul>
                  <p class="text-justify">
                      dan mata uang US dollar "terlibat" dalam 89%
                      dari transaksi yang dilakukan, kemudian
                      diikuti oleh mata uang Euro (37%), Yen (20%)
                      dan Pound Sterling (17%).</p>

                  <p class="text-justify"> Walaupun perdagangan dalam mata uang Euro
                      meningkat secara cepat sejak mata uang
                      tersebut diterbitkan pada January 1999,
                      US dollar masih mendominasi pasar valuta
                      asing. Sebagai contoh misalnya dalam
                      perdagangan antara Euro dan mata uang non
                      Eropa (XXX), biasanya selalu melibatkan dua
                      jenis perdagangan yaitu EUR/USD dan USD/XXX,
                      pengecualiannya hanya pada perdagangan
                      EUR/JPY yang merupakan pasangan mata uang
                      yang secara tetap diperdagangkan di pasar
                      spot antar bank.</p>

          </div>
          <div class="col-md-12">
            <div class="heading  text-center">
              <h3>Cara Transaksi Forex ?</h3>
              <br>
            </div>
          </div>
          <div class="col-md-offset-1 col-md-10">
            <ul class="dt-sc-fancy-list orange arrow">
               <li>Membaca dan memahami dengan baik isi dari Buku Perjanjian Nasabah.</li>
               <li>Menanda-tangani Buku Perjanjian Nasabah beserta semua lampiran yang ada.	</li>
               <li>Menyerahkan fotocopy kartu identitas yang masih berlaku (KTP/SIM/Passport)	</li>
               <li>Menyetor Margin ke Rekening Bank segera a/n PT.MAXCO FUTURES	</li>
               <li>Menerima no account trading (Online Trading : Menerima no login dan password.)</li>
               <li>Siap melakukan transaksi.	</li>
             </ul>
          </div>
        </div>
      </div>
    </section>
  </div>

<!-- always on -->
@include('page.template.always_on')

@endsection

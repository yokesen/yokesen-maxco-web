@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<!-- <section class="sub-bnr" style="background:url({{url('/')}}/web/images/bg/sub-bnr-bg-2.jpg) no-repeat" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>Index</h1>
        Breadcrumbs
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Index</li>
        </ol>
      </div>
    </div>
  </section> -->

<!-- Content -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/market/INDEX_TRADING.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
</section>
<div id="content">
  <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Index</li>
      </ol>
    </div>
  </section>

  <!-- Revenues -->
  <section class="revenues padding-top-70 padding-bottom-70">
    <div class="container">

      <!-- Apa itu Index ? -->
      <div class="tabs-frame-content planning-tab">
        <div class="special-features">
          <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
            <!-- {{url('/')}}/web/images/webpage/market/index/Index.jpg -->
            <img src="{{url('/')}}/web/images/webpage/market/index/Index_dfad.jpg" class="float-left" alt="" title="">
          </div>
          <div class="dt-sc-two-third column last">
            <div class="hr-invisible-very-small"> </div>
            <div class="heading text-center">
              <h3>Index Trading</h3>
            </div>
            <p>Index harga saham adalah suatu indikator yang menunjukkan pergerakan harga saham. Index saham ini berfungsi sebagai indikator trend pasar, artinya pergerakan index menggambarkan kondisi pasar pada suatu waktu, apakah pasar sedang dalam kondisi uptrend atau downtrend.</p>
          </div>

        </div>
      </div>
      <!-- Karakteristik Index -->
      <div class="tabs-frame-content">
        <div class="special-features">
          <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
            <img src="{{url('images/maxco/market/index/Index.jpg')}}" class="float-left" alt="" title="">
          </div>
          <div class="dt-sc-two-third column last">
            <div class="hr-invisible-very-small"> </div>
            <h5>Apa saja yang diperdagangkan.</h5>
            <br>
            <div class="text-justify">
              <p>
                Index Tradng tidak memiliki asset dalam melakukan transaksi, maka nilai kontrak merupakan acuan utamanya.
                Besar kecilnya nilai kontrak dalam perdagangan index ditentukan berdasarkan satuan unit yang biasa dikenal lot. dan setiap Negara mempunyai aturan dan tata caranya tersendiri dalam mengatur dan memperdagangkan pasar index .
              </p>
              <p>
                Pasar modal utama index sendiri dibagi menjadi dua pasar modal utama yang biasa diperdagangkan,
                yaitu pasar modal Amerika dan Asia beserta waktu market dan sesi perdagangannya.
              </p>
              <div class="small-gap"> </div>
              <!-- <div class="margin-left-20">
                <h6>Amerika</h6>
                <ul class="dt-sc-fancy-list red arrow margin-top-30 text-justify">
                  <li>
                    <p>DJIA Dow Jones Industrial average (00:00-24:00)</p>
                  </li>
                  <li>
                    <p>Nasdaq Composite (00:00-24:00)</p>
                  </li>
                  <li>
                    <p>S&P 500 (00:00-23:30)</p>
                  </li>
                </ul>
                <div class="small-gap"> </div>
                <h6>Asia</h6>
                <ul class="dt-sc-fancy-list red arrow margin-top-30 text-justify">
                  <li>
                    <p>NIKKEI (Jepang-Tokyo) dengan dua sesi perdagangan (01.45-08.15) dan (09.15-21.00)</p>
                  </li>
                  <li>
                    <p>Hangseng (Hongkong) dengan dua sesi perdagangan (03.15-06.00) dan (07.30-10.15)</p>
                  </li>
                  <li>
                    <p>Kospi (Korea) (02:00-08:45)</p>
                  </li>
                </ul>
              </div> -->

            </div>
          </div>

        </div>
      </div>
      <div class="tabs-frame-content">
        <div class="special-features">
          <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
            <img src="{{url('images/maxco/market/index/Index.jpg')}}" class="float-left" alt="" title="">
          </div>
          <div class="dt-sc-two-third column last">
          <h5>Karakteristik Index</h5>
            <div class="text-justify">
              <p>
                Indeks merupakan instrument investasi yang memiliki kinerja terbaik,
                bahkan 97% melebihi kinerja investasi seluruh reksa dana aktif selama 45 tahun terakhir.</p>
              <p>
                Seperti halnya perdagangan futures lainnya, trading indeks saham tidak memerlukan dana penuh,
                hanya memerlukan sebagian kecil dana dari nilai kontraknya atau yang lebih dikenal dengan system leverage.
                Dengan penggunaan leverage dan system margin memungkinkan setiap trader mendapatkan ROI lebih besar dari nilai realnya.
              </p>
            </div>
          </div>

        </div>
      </div>
      <div class="tabs-frame-content">
        <div class="special-features">
          <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
            <img src="{{url('images/maxco/market/index/Index.jpg')}}" class="float-left" alt="" title="">
          </div>
          <div class="dt-sc-two-third column last">
            <h5>Cara transaksi index :</h5>
            <div class="text-justify">
            <p>
              Untuk dapat melakukan jual beli index, Anda perlu mendaftarkan diri Anda pada broker yang memiliki fasilitas perdagangan Indeks.
              PT. Maxco adalah salah satu broker legal terbaik di Indonesia yang menyediakan layanan ini.</p><p>
              Mirip dengan perdagangan forex, pada transaksi indeks, Anda dapat menempatkan posisi jual atau beli terhadap pergerakan harga indeks.
              Dengan demikian, Anda dapat mencetak profit pada kondisi market apapun, baik sedang naik, maupun turun.</p><p>
              Sebagai contoh, jika Anda ingin memperdagangkan indeks Hang Seng Hongkong pada harga 25.050,
              maka Anda memiliki dua opsi untuk membuka posisi BUY atau SELL.
              Bergantung pada besar spread yang Anda gunakan, broker akan menawarkan bid/offer pada angka tertentu seperti 25.049/25.051.</p><p>
              Dalam satu hari, indeks Hang Seng dapat bergerak antara 300 - 700 pip, dengan harga per pip sebesar $5.
              Jika Anda membuka posisi buy pada level 25.050 dan kemudian menutupnya pada saat harga indeks naik ke angka 25.250,
              maka potensi profit yang Anda dapatkan sebesar ((25.250 - 25.050) * 5) = $1.000.
            </p>

            </div>
            
            <!-- <ul class="dt-sc-fancy-list orange arrow">
                       <li>Membaca dan memahami dengan baik isi dari Buku Perjanjian Nasabah yang memuat	</li>
                       <li>Menanda-tangani Buku Perjanjian Nasabah beserta semua lampiran yang ada.	</li>
                       <li>Menyerahkan fotocopy kartu identitas yang masih berlaku (KTP/SIM/Passport)	</li>
                       <li>Menyetor Margin ke Rekening Bank segera a/n PT.MAXCO FUTURES	</li>
                       <li>Menerima no account trading (Online Trading : Menerima no login dan password.)</li>
                       <li>Siap melakukan transaksi.	</li>
                     </ul> -->

          </div>

        </div>
      </div>

    </div>
    <section>
</div>
<!-- always on -->
@include('page.template.always_on')

@endsection
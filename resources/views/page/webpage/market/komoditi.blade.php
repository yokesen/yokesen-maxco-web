@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
  <section class="sub-bnr" style="background:url({{url('/')}}/web/images/bg/sub-bnr-bg-2.jpg) no-repeat" data-stellar-background-ratio="0.5">
    <div class="position-center-center">
      <div class="container">
        <h1>Komoditi</h1>
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Komoditi</li>
        </ol>
      </div>
    </div>
  </section>

  <!-- Content -->
  <div id="content">

    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
      <div class="container">
        <!-- Apa itu Komoditi -->
            <div class="tabs-frame-content planning-tab">
                <div class="container">
                    <div class="special-features">
                        <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
                            <img src="{{url('images/maxco/market/komoditi/Komoditi.jpg')}}" class="float-left" alt=""
                                title="">
                        </div>
                        <div class="dt-sc-two-third column last">
                            <div class="hr-invisible-very-small"> </div>
                            <p class="maxco-text-justify">Komoditi adalah sesuatu benda nyata yang relatif mudah diperdagangkan, dapat diserahkan
                                secara fisik, dapat disimpan untuk suatu jangka waktu tertentu dan dapat dipertukarkan
                                dengan produk lainnya dengan jenis yang sama, yang biasanya dapat dibeli atau dijual
                                oleh investor melalui bursa berjangka. Secara lebih umum, komoditas adalah suatu produk
                                yang diperdagangkan, termasuk valuta asing, instrumen keuangan dan indeks.</p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Apa saja yang diperdagangkan -->
            <div class="tabs-frame-content">
                <div class="container">
                    <div class="special-features">
                        <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
                            <img src="{{url('images/maxco/market/komoditi/Komoditi.jpg')}}" class="float-left" alt=""
                                title="">
                        </div>
                        <div class="dt-sc-two-third column last">
                            <div class="hr-invisible-very-small"> </div>
                            <h4>Jenis Komoditas</h4>
                            <div class="hr-invisible-very-small"> </div>
                            <div class="dt-sc-toggle-frame-set">
                                <div class="dt-sc-toggle-frame">
                                    <p class="dt-sc-toggle-accordion active">
                                      <a href="#" title=""> Makanan </a>
                                    </p>
                                    <div class="dt-sc-toggle-content">
                                        <div class="column dt-sc-one-two">
                                            <ul class="dt-sc-fancy-list orange
                                                arrow">
                                                <li>Kopi (Robusta)	</li>
                                                <li>Kopi (Arabika)	</li>
                                                <li>Gula	</li>
                                                <li>Gula	</li>
                                                <li>Coklat	</li>
                                                <li>coklat	</li>
                                                <li>maizena (jagung)	</li>
                                                <li>beras	</li>
                                                <li>kedelai	</li>
                                                <li>kacang kedelai	</li>
                                                <li>minyak kedelai	</li>
                                                <li>gandum	</li>
                                                <li>gandum	</li>
                                                <li>bunga matahari (Minyak sayur) (EU)	</li>
                                                <li>minyak biji rami (EU)	</li>
                                                <li>minyak kedelai (EU)	</li>
                                                <li>oleinturunan CPO("hsw" 17 Juli 2012 07.53 (UTC))	</li>
                                                <li>bunga	</li>
                                                <li>barley	</li>
                                                <li>jus jeruk	</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-toggle-frame">
                                    <p class="dt-sc-toggle-accordion">
                                      <a href="#" title=""> Ternak hidup & Daging </a>
                                    </p>
                                    <div class="dt-sc-toggle-content">
                                        <div class="column dt-sc-one-two">
                                            <ul class="dt-sc-fancy-list orange arrow">
                                              <li>	Babi tanpa lemak	</li>
                                              <li>	Perut babi	</li>
                                              <li>	Ternak lembu/sapi	</li>
                                              <li>	Makanan ternak	</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-toggle-frame">
                                    <p class="dt-sc-toggle-accordion">
                                      <a href="#" title=""> Bahan bakar </a>
                                    </p>
                                    <div class="dt-sc-toggle-content">
                                        <div class="column dt-sc-one-two">
                                            <ul class="dt-sc-fancy-list orange arrow">
                                                <li>	Batubara	</li>
                                                <li>	Light, Sweet Crude Oil	</li>
                                                <li>	Bensin tanpa timbal	</li>
                                                <li>	Diesel (max. 0.035% sulfur)	</li>
                                                <li>	Bensin (max. 0.2% sulfur)	</li>
                                                <li>	Bensin (max. 1.0% sulfur)	</li>
                                                <li>	Bensin(max. 3.5% sulfur)	</li>
                                                <li>	Brent crude oil	</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-toggle-frame">
                                    <p class="dt-sc-toggle-accordion">
                                      <a href="#" title=""> Logam </a>
                                    </p>
                                    <div class="dt-sc-toggle-content">
                                        <div class="column dt-sc-one-two">
                                            <ul class="dt-sc-fancy-list orange arrow">
                                              <li>	Emas	</li>
                                              <li>	Emas	</li>
                                              <li>	Platinum	</li>
                                              <li>	Palladium	</li>
                                              <li>	Silver	</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-toggle-frame">
                                    <p class="dt-sc-toggle-accordion">
                                      <a href="#" title=""> Logam industri </a>
                                    </p>
                                    <div class="dt-sc-toggle-content">
                                        <div class="column dt-sc-one-two">
                                            <ul class="dt-sc-fancy-list orange arrow">
                                              <li>	Tembaga	</li>
                                              <li>	Timah	</li>
                                              <li>	Seng(Zinc)	</li>
                                              <li>	Tin	</li>
                                              <li>	Aluminium	</li>
                                              <li>	Aluminium campuran(alloy)	</li>
                                              <li>	Nikel	</li>
                                              <li>	Aluminium campuran(alloy)	</li>
                                              <li>	Baja daur ulang	</li>
                                              <li>	Germanium	</li>
                                              <li>	Indium	</li>
                                              <li>	Cadmium	</li>
                                              <li>	Cobalt	</li>
                                              <li>	Chromium	</li>
                                              <li>	Magnesium	</li>
                                              <li>	Manganese (?)	</li>
                                              <li>	Molybdenum	</li>
                                              <li>	Silicon	</li>
                                              <li>	Rhodium	</li>
                                              <li>	Selenium	</li>
                                              <li>	Titanium	</li>
                                              <li>	Vanadium	</li>
                                              <li>	Wolframit	</li>
                                              <li>	Wolfram (ferro) or Tungsten	</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-toggle-frame">
                                    <p class="dt-sc-toggle-accordion">
                                      <a href="#" title=""> Lain - lain </a>
                                    </p>
                                    <div class="dt-sc-toggle-content">
                                        <div class="column dt-sc-one-two">
                                            <ul class="dt-sc-fancy-list orange arrow">
                                              <li>	Ethanol	</li>
                                              <li>	Katun	</li>
                                              <li>	Karet	</li>
                                              <li>	Minyak sawit	</li>
                                              <li>	Wol	</li>
                                              <li>	Jerami	</li>
                                              <li>	Polipropilen	</li>
                                              <li>	Polietilen	</li>
                                              <li>	Olein	</li>
                                              <li>	Ethanol	</li>
                                              <li>	Katun	</li>
                                              <li>	Karet	</li>
                                              <li>	Minyak sawit	</li>
                                              <li>	Wol	</li>
                                              <li>	Jerami	</li>
                                              <li>	Polipropilen	</li>
                                              <li>	Polietilen	</li>
                                              <li>	Olein	</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
            <!-- Karakteristik Komoditi -->
            <div class="tabs-frame-content">
                    <div class="special-features">
                        <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
                            <img src="{{url('images/maxco/market/komoditi/Komoditi.jpg')}}" class="float-left" alt="" title="">
                        </div>
                        <div class="dt-sc-two-third column last">
                            <div class="hr-invisible-very-small"> </div>
                            <p class="maxco-text-justify">Karakteristik dari Komoditas yaitu harga adalah ditentukan oleh penawaran dan permintaan pasar bukannya ditentukan oleh penyalur ataupun penjual dan harga tersebut adalah berdasarkan perhitungan harga masing-masing pelaku Komoditas contohnya adalah (namun tidak terbatas pada): mineral dan produk pertanian seperti bijih besi, minyak, ethanol, gula, kopi, aluminium, beras, gandum, emas, berlian atau perak, tetapi juga ada yang disebut produk "commoditized" (tidak lagi dibedakan berdasarkan merek) seperti komputer</p>
                        </div>

                    </div>
            </div>
            <!-- Cara Transaksi  Komoditi -->
            <div class="tabs-frame-content">
                <div class="special-features">
                    <div class="dt-sc-one-third column animate" data-animation="bounceInLeft" data-delay="100">
                        <img src="{{url('images/maxco/market/komoditi/Komoditi.jpg')}}" class="float-left" alt=""
                            title="">
                    </div>
                    <div class="dt-sc-two-third column last">
                        <div class="hr-invisible-very-small"> </div>
                        <h5>Cara transaksi komoditi :</h5>
                        &nbsp;
                        <ul class="dt-sc-fancy-list orange arrow">
                          <li>Membaca dan memahami dengan baik isi dari Buku Perjanjian Nasabah yang memuat	</li>
                          <li>Menanda-tangani Buku Perjanjian Nasabah beserta semua lampiran yang ada.	</li>
                          <li>Menyerahkan fotocopy kartu identitas yang masih berlaku (KTP/SIM/Passport)	</li>
                          <li>Menyetor Margin ke Rekening Bank segera a/n PT.MAXCO FUTURES	</li>
                          <li>Menerima no account trading (Online Trading : Menerima no login dan password.)</li>
                          <li>Siap melakukan transaksi.	</li>
                        </ul>
                    </div>

                </div>
            </div>

      </div>
    </section>
  </div>
  
<!-- always on -->
@include('page.template.always_on')

@endsection

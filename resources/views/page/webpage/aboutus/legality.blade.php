@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/aboutus/pentingnya_legalitas_banner.jpg) no-repeat!important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

</section>
<!-- Content -->
<div id="content">
  <section style="
  background: #f5f5f5;
  padding: 2px;
  ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">{{trans('page.home-legality')}}</li>
      </ol>
    </div>
  </section>

    <!-- Revenues -->
    <section class="revenues padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h3>{{trans('page.menu-legality-subtitle')}}</h3>
                    </div>
                </div>
                <div class="col-md-4">
                  <img class="img-responsive"
                      src="{{url("/")}}/web/images/webpage/aboutus/pentingnya_legalitas.jpg">
                </div>
                <div class="col-md-8">
                    <p class="text-justify">
                        {{trans('page.legality-detail')}}
                        <br>
                        <br>
                    </p>
                  </div>
                  <div class="col-md-12">
                    <div class="text-center">
                      <div id="ajax-work-filter" class="cbp-l-filters-buttonCenter filter-style-2">
                        <div data-filter="*" class="cbp-filter-item"> {{trans('page.all')}}
                        </div>
                        <div data-filter=".perijinan" class="cbp-filter-item cbp-filter-item-active"> {{trans('page.license')}}
                        </div>
                        <div data-filter=".sertifikat" class="cbp-filter-item"> {{trans('page.certificate')}}
                        </div>
                      </div>
                    </div>
                    <section class="portfolio padding-top-0 padding-bottom-20">

                        <!-- PORTFOLIO ROW -->
                        <div class="ajax-work">
                            <!-- ITEMS -->
                            <div class="cbp-item frame-thin col-sm-3 perijinan">
                                <article class="item"><img class="img-responsive"
                                        src="{{url('/')}}/web/images/webpage/aboutus/perijinan/certificate_of_membership.png"
                                        alt="">
                                    <!-- Hover -->
                                    <div class="over-detail">
                                        <!-- Link -->
                                        <div class="top-detail">
                                            <a  style="display:none;" href="{{url('/')}}/web/images/webpage/aboutus/perijinan/certificate_of_membership.png"
                                                class="cbp-lightbox top-detail" data-title=""><i class="icon-magnifier"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>

                            <!-- ITEMS -->
                            <div class="cbp-item frame-thin col-sm-3 perijinan">
                                <article class="item"><img class="img-responsive"
                                        src="{{url('/')}}/web/images/webpage/aboutus/perijinan/izin_usaha_pialang_berjangka.png"
                                        alt="">
                                    <!-- Hover -->
                                    <div class="over-detail">
                                        <!-- Link -->
                                        <div class="top-detail">
                                            <a style="display:none;" href="{{url('/')}}/web/images/webpage/aboutus/perijinan/izin_usaha_pialang_berjangka.png"
                                                class="cbp-lightbox" data-title="">
                                                <i class="icon-magnifier"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>

                            <!-- ITEMS -->
                            <div class="cbp-item frame-thin col-sm-3 perijinan">
                                <article class="item"><img class="img-responsive"
                                        src="{{url('/')}}/web/images/webpage/aboutus/perijinan/bapebti_maxco.png"
                                        alt="">
                                    <!-- Hover -->
                                    <div class="over-detail">
                                        <!-- Link -->
                                        <div class="top-detail">
                                            <a  style="display:none;"  href="{{url('/')}}/web/images/webpage/aboutus/perijinan/bapebti_maxco.png"
                                                class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>


                            <!-- ITEMS -->
                            <div class="cbp-item frame-thin col-sm-3 perijinan">
                                <article class="item"><img class="img-responsive"
                                        src="{{url('/')}}/web/images/webpage/aboutus/perijinan/perubahan_nama_2.png"
                                        alt="">
                                    <!-- Hover -->
                                    <div class="over-detail">
                                        <!-- Link -->
                                        <div class="top-detail">
                                            <a  style="display:none;" href="{{url('/')}}/web/images/webpage/aboutus/perijinan/perubahan_nama_2.png"
                                                class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>


                            <!-- ITEMS -->
                            <div class="cbp-item frame-thin col-sm-3 sertifikat">
                                <article class="item"><img class="img-responsive"
                                        src="{{url('/')}}/web/images/webpage/aboutus/perijinan/perijinan_1.jpg"
                                        alt="">
                                    <!-- Hover -->
                                    <div class="over-detail">
                                        <!-- Link -->
                                        <div class="top-detail">
                                            <a  style="display:none;" href="{{url('/')}}/public/web/images/webpage/aboutus/perijinan/surat_persetujuan_anggota_bursa_(SPAB).png"
                                                class="cbp-lightbox" data-title=""><i class="icon-magnifier"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</div>


<!-- always on -->
@include('page.template.always_on')

@endsection

@section("jsonpage")
<script>
$(".over-detail").on('click',function(){
    const child1 = $(this).children();
    $(child1).children()[0].click()
});
</script>
@endsection

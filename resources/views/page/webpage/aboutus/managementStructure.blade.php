@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->
<section class="sub-bnr"
  style="background:url({{url('/')}}/web/images/webpage/aboutus/header_profil_perusahaan_1.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;"
  data-stellar-background-ratio="0.5">

</section>

<!-- Content -->
<div id="content">
  <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Struktur Pengurus</li>
      </ol>
    </div>
  </section>
  <!-- WHO WE ARE -->
  <section class="team team-wrap padding-top-70 padding-bottom-70">
    <div class="container">
      <div class="">
        <ul class="row">
          <li class="col-md-3">
            <article class="text-left"> <img class="img-responsive lazyload"
                data-src="{{url('/')}}/web/images/maxco/management/daniel.jpg" alt="">
              <h5>Muhammad Yanwar Pohan</h5>
              <span>Direktur Utama</span>
            </article>
          </li>
          <li class="col-md-3">
            <article class="text-left"> <img class="img-responsive lazyload"
                data-src="{{url('/')}}/web/images/maxco/management/christian.jpg" alt="">
              <h5>Ade Manuel Ortega</h5>
              <span>Direktur</span>
            </article>
          </li>
          <li class="col-md-3">
            <article class="text-left"> <img class="img-responsive lazyload"
                data-src="{{url('/')}}/web/images/webpage/management/shutterstock_1068240440.jpg" alt="">
              <h5>Rudi Anto, S, Sos</h5>
              <span>Direktur Kepatuhan</span>
            </article>
          </li>
          <li class="col-md-3">
            <article class="text-left"> <img class="img-responsive lazyload"
                data-src="{{url('/')}}/web/images/webpage/management/shutterstock_271171541.jpg" alt="">
              <h5>Ronald H. Lalamentik</h5>
              <span>Komisaris Utama</span>
            </article>
          </li>
          <li class="col-md-3">
            <article class="text-left"> <img class="img-responsive lazyload"
                data-src="{{url('/')}}/web/images/webpage/management/shutterstock_435796384.jpg" alt="">
              <h5>Tofvin</h5>
              <span>Komisaris</span>
            </article>
          </li>
          <li class="col-md-3">
            <article class="text-left"> <img class="img-responsive lazyload"
                data-src="{{url('/')}}/web/images/webpage/management/shutterstock_271171541.jpg" alt="">
              <h5>Ronald H. Lalamentik</h5>
              <span>Pemegang Saham</span>
            </article>
          </li>
        </ul>
      </div>
    </div>
  </section>
</div>


<!-- always on -->
@include('page.template.always_on')

@endsection
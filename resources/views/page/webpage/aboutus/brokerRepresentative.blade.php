@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')

@endsection

@section('content')
<!-- SUB BANNER -->

<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/aboutus/header_profil_perusahaan_1.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

</section>

  <!-- Content -->
  <div id="content">
    <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
      <div class="container">
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
          <li class="active">Daftar Wakil Pialang</li>
        </ol>
      </div>
    </section>

    <!-- WHO WE ARE -->
    <section class="team team-wrap padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="">
          <ul class="row">
            <li class="col-md-3">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/rudi_anto.jpg" alt="">
                <h5>Muhammad Yanwar Pohan</h5>
                <span>Pialang 1</span>
              </article>
            </li>
            <li class="col-md-3">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/ferucha.jpg" alt="">
                <h5>Euis Ferawati</h5>
                <span>Pialang 2</span>
              </article>
            </li>
            <li class="col-md-3">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/prayoga_subagja.jpg" alt="">
                <h5>Irwan Julian</h5>
                <span>Pialang 3</span>
              </article>
            </li>
            <li class="col-md-3">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/broker_representative/martha_barker.jpg" alt="">
                <h5>Irvan Sutanto</h5>
                <span>Pialang 4</span>
              </article>
            </li>
            <li class="col-md-3">
          </li>
            <li class="col-md-3">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/management/deandra.jpg" alt="">
                <h5>Widiastuti</h5>
                <span>Pialang 5</span>
              </article>
            </li>
            <li class="col-md-3">
              <article class="text-left"> <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/maxco/management/raisa.jpg" alt="">
                <h5>Melina Octovia</h5>
                <span>Pialang 6</span>
              </article>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </div>

  <!-- always on -->
  @include('page.template.always_on')
@endsection

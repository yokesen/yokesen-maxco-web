@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

<!-- SUB BANNER -->
<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/aboutus/header_profil_perusahaan_1.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

</section>

<!-- Content -->
<div id="content">
  <section style="
   background: #f5f5f5;
   padding: 2px;
   ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">Home</a></li>
        <li class="active">Lokasi Kantor</li>
      </ol>
    </div>
  </section>
  <section class="padding-bottom-70">
    <div class="contact-info padding-top-100 padding-bottom-100" style="background:url({{url('/')}}/web/images/webpage/aboutus/lokasi_kantor.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">
      <div class="container">

        <!-- Heading -->
        <div class="heading white text-center">
          <h3 style="color: white;">Kantor Pusat Kami</h3><br><br>
          <!-- <span>We trust in longlasting partnerships with the most important brands on the market</span> </div> -->
          <ul class="row">
            <!-- australia -->
            <li class="col-sm-offset-2 col-sm-4">
              <article>
                <h5><b>Jakarta</b></h5>
                <p>Panin Bank Centre - Ground Floor</p>
                <p>Jl. Jend. Sudirman Kav-1 Senayan Jakarta Selatan 10270</p>
                <p>DKI Jakarta, Indonesia </p>
                <span class="margin-top-30">
                  <p><b><i class="fa fa-phone-square"></i> +62 21 720-5868 (Hunting)</b></p>
                </span>
                <span class="">
                  <p><b><i class="fa fa-fax"></i> +62 21 571-0974</b></p>
                </span>
                <span class="primary-color" style="text-transform: inherit;">
                  <i class="fa fa-envelope"></i> info@maxcofutures.co.id
                </span>
              </article>
            </li>

            <!-- australia -->
            <li class="col-sm-4">
              <article>
                <h5><b>Surabaya</b></h5>
                <p>Jln. Dharmahusada No.60A, Mojo, Gubeng
                  <p>Surabaya 60285</p>
                  <p></p>
                  <p>Jawa Timur, Indonesia </p>
                  <span class="margin-top-30">
                    <p><b><i class="fa fa-phone-square"></i> +62 31 599-3178/ 599-3184</b></p>
                  </span>
                  <span class="">
                    <p><b><i class="fa fa-fax"></i> +62 31 599-3208</b></p>
                  </span>
                  <span class="primary-color" style="text-transform: inherit;">
                    <i class="fa fa-envelope"></i> cs@maxcofutures.co.id
                  </span>
              </article>
            </li>

          </ul>
        </div>
      </div>

    </div>
    <div class="container">

      <!-- Heading -->
      <div class="heading text-left margin-top-70 margin-bottom-30">
        <h3>drop us a line</h3>
      </div>
      <div class="row">
        <div class="col-md-6">
          <!-- CONTACT FORM -->
          <div class="contact-form">
            <!-- Success Msg -->
            <div id="contact_message" class="success-msg"> <i class="fa fa-paper-plane-o"></i>Thank You. Your Message has been Submitted</div>

            <!-- FORM -->
            <form role="form" id="maxco_contact_form" class="contact-form" method="post" onSubmit="return false">
              <ul class="row">
                <li class="col-sm-12">
                  <label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>
                    <input type="text" class="form-control" name="company" id="company" placeholder="Subject">
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>
                    <textarea class="form-control" name="message" id="message" rows="5" placeholder="Message"></textarea>
                  </label>
                </li>
                <li class="col-sm-12">
                  <button type="submit" value="submit" class="btn btn-1" id="btn_submit">Send Message <i class="fa fa-caret-right"></i></button>
                </li>
              </ul>
            </form>
          </div>
        </div>

        <!-- MAP -->
        <div class="col-md-6">
          <div id="map"></div>
        </div>
      </div>
    </div>
  </section>
</div>


<!-- always on -->
@include('page.template.always_on')

@endsection

@section('jsonpage')
<script src="{{url('/')}}/web/js/jquery.validate.min.js"></script>
<script src="{{url('/')}}/web/js/vendor.js"></script>
<!-- Begin Map Script-->
<script type="text/javascript">
  /*==========  Map  ==========*/
  var map;

  function initialize_map() {
    if ($('#map').length) {
      var officeLocs = [
        ['Jakarta', -6.223572, 106.800704],
        ['Surabaya', -7.265986, 112.764355]
      ]
      var myLatLng = new google.maps.LatLng(-6.223572, 106.800704);
      var zoomMap = 4.9;
      if (screen.width < 600) {
        zoomMap = 3;
      }
      var mapOptions = {
        zoom: zoomMap,
        center: myLatLng,
        scrollwheel: false,
        panControl: false,
        zoomControl: true,
        scaleControl: false,
        mapTypeControl: false,
        streetViewControl: false
      };
      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      var marker;
      for (i = 0; i < officeLocs.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(officeLocs[i][1], officeLocs[i][2]),
          map: map
        });
      }
    } else {
      return false;
    }
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCegZEASP1XiDIQI-ob2qLEgga0OJiaSBs&callback=initialize_map" async defer></script>
@endsection
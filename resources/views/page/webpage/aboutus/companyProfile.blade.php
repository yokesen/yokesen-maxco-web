@extends('page.template.master')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
<style>
  .table-structure tr {
    height: 33px;
  }
</style>
@endsection

@section('content')

<section class="sub-bnr" style="background:url({{url('/')}}/web/images/webpage/aboutus/header_profil_perusahaan_1.jpg) no-repeat !important;background-size: cover !important; background-position-y: bottom; background-position-x: center;" data-stellar-background-ratio="0.5">

</section>

<!-- Content -->
<div id="content">
  <section style="
    background: #f5f5f5;
    padding: 2px;
    ">
    <div class="container">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="/">{{trans('page.menu-home')}}</a></li>
        <li class="active">{{trans('page.menu-company-profile')}}</li>
      </ol>
    </div>
  </section>
  <!-- SERVICES -->
  <section class="padding-top-70 padding-bottom-70">

    <div class="container">
      <div class="row">
        <!-- Revenues Sidebar -->
        <div class="col-md-3">
          <div class="side-bar-revenues">
            <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('company-profile')" aria-expanded="false"><i class="fa fa-building"></i> {{trans('page.menu-company-profile')}}</a></h6>
            <!-- <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('vision-mision')" aria-expanded="false"><i class="fa fa-eye"></i> {{trans('page.menu-vision-mission')}}</a></h6> -->
            <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('value-history')" aria-expanded="false"><i class="fa fa-history"></i> {{trans('page.menu-valuable-history')}}</a></h6>
            <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('achievement')" aria-expanded="false"><i class="fa fa-space-shuttle"></i> {{trans('page.menu-achievements')}}</a></h6>
            <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('our-excelent')" aria-expanded="false"><i class="fa fa-trophy"></i> {{trans('page.menu-our-advantages')}}</a></h6>
            <h6 class="head"><a data-toggle="collapse" onclick="toggleGhost('management-structure')" aria-expanded="false"><i class="fa fa-trophy"></i> {{trans('page.menu-management-structure')}}</a></h6>

            <!-- Brochures -->
            @include('page.template.footer_sidebar')
          </div>
        </div>

        <div class="col-md-9">
          <div class="col-md-12 toggleGhost company-profile">
            <div class="heading text-center">
              <h3>{{trans('page.menu-company-profile')}}</h3>
            </div>
            <div class="row">
              <div class="col-md-6" style="min-height:{{$agent->isMobile()?'150px':'288px'}};position: relative;background: url({{url('/')}}/web/images/webpage/home-slide/easy_trading_back.png) 0% 0% / cover no-repeat !important;">
                <img class="img-responsive lazyload" data-src="{{url('/')}}/web/images/web/logo_300.png" style="margin: 0;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">
              </div>
              <div class="col-md-6">
                <p class="text-justify">
                  {{trans('page.menu-compro-detail')}}
                </p>
              </div>
              <div class="col-md-12 text-justify">
                <p>
                  <br>
                  {{trans('page.menu-compro-detail-2')}}
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-12 hidden toggleGhost vision-mision" tag="vision-mision">
            <div class="heading text-center">
              <h3>{{trans('page.menu-vision-mission')}}</h3>
            </div>
            <!-- Visi Misi -->
            <div class="row">
              <div class="col-md-4">
                <img class="img-responsive lazyload" data-src="{{url("/")}}\web\images\webpage\aboutus\visi_misi_1.jpg">
              </div>
              <div class="col-md-8">
                <div class="text-center">
                  <h6>{{trans('page.menu.vision-title')}}</h6>
                </div>
                <p class="text-justify">
                  {{trans('page.menu-vision-detail')}}
                </p>
              </div>
              <div class="col-md-12">
                <div class="small-gap"></div>
              </div>
              <div class="col-md-4">
                <img class="img-responsive lazyload" data-src="{{url("/")}}\web\images\webpage\aboutus\visi_misi_2.jpg">
              </div>
              <div class="col-md-8">
                <div class="small-gap"></div>
                <div class="text-center">
                  <h6>{{trans('page.menu.mission-title')}}</h6>
                </div>
                <p class="text-justify">
                  <ul class="dt-sc-fancy-list red arrow text-justify">
                    <li>
                      <p>{{trans('page.menu-mission-detail-1')}}</p>
                    </li>
                    <li>
                      <p>{{trans('page.menu-mission-detail-2')}}</p>
                    </li>
                    <li>
                      <p>{{trans('page.menu-mission-detail-3')}}</p>
                    </li>
                    <li>
                      <p>{{trans('page.menu-mission-detail-4')}}</p>
                    </li>
                  </ul>
                </p>
              </div>
            </div>


          </div>
          <div class="col-md-12 hidden toggleGhost value-history">
            <div class="heading text-center">
              <h3>{{trans('page.menu-valuable-history')}}</h3>
            </div>
            <div class="row">
              <!-- Nilai dan Sejarah -->
              <div class="col-md-5">
                <img class="img-responsive lazyload" data-style="width: 268px !important;" data-src="{{url("/")}}/web/images/webpage/profil-perusahaan/nilai_dan_sejarah.jpg">
              </div>
              <div class="col-md-7">
                <div class="text-justify">
                  <p>{{trans('page.valuable-history-detail-1')}}</p>
                </div>
              </div>
              <div class="col-md-12">
                <div class="text-justify">
                  <p>
                    <br>
                    {{trans('page.valuable-history-detail-2')}}
                    <br>
                    <br>
                    {{trans('page.valuable-history-detail-3')}}
                    <br>
                    <br>
                    {{trans('page.valuable-history-detail-4')}}
                    <br>
                  </p>
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-12 hidden toggleGhost achievement">
            <div class="heading text-center">
              <h3>{{trans('page.menu-achievements')}}</h3>
            </div>
            <div class="row">
              <div class=" col-md-5">
                <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/profil-perusahaan/pencapaian.jpg" style="margin-top: 23px;">
              </div>
              <div class="col-md-7">
                <div class="text-justify">
                  <p>{{trans('page.achievements-subtitle')}}
                    <p>

                      <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                        <li>
                          <p>{{trans('page.achievements-detail-1')}}</p>
                        </li>
                        <li>
                          <p>{{trans('page.achievements-detail-2')}}</p>
                        </li>
                        <li>
                          <p>{{trans('page.achievements-detail-3')}} (1)</p>
                        </li>
                        <li>
                          <p>{{trans('page.achievements-detail-4')}} (2)</p>
                        </li>
                        <li>
                          <p>{{trans('page.achievements-detail-5')}}</p>
                        </li>
                      </ul>
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-12 hidden toggleGhost our-excelent">
            <div class="heading text-center">
              <h3>{{trans('page.menu-our-advantages')}}</h3>
            </div>
            <div class="row">

              <div class="col-md-5 text-center">
                <img class="img-responsive lazyload" data-src="{{url("/")}}/web/images/webpage/profil-perusahaan/keunggulan_kami.jpg">
              </div>
              <div class="col-md-7 text-justify">
                <p>{{trans('page.our-advantages-subtitle')}}</p>
              </div>
              <div class="col-md-12">
                <br>
                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">{{trans('page.our-advantage-subtitle-available-tool')}}</a>
                      </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                          <li>
                            <p>{{trans('page.available-tool-detail-1')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-2')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-3')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-4')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-5')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-6')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-7')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-8')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-9')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-10')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-11')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-12')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-13')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-14')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-15')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.available-tool-detail-16')}}</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">{{trans('page.our-advantage-subtitle-low-commission')}}</a>
                      </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                          <li>
                            <p>{{trans('page.low-commission-detail-1')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.low-commission-detail-2')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.low-commission-detail-3')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.low-commission-detail-4')}}</p>
                          </li>
                        </ul>
                        <p class="text-justify">{{trans('page.low-commission-detail-footer')}}</p>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">{{trans('page.our-advantage-subtitle-best-support')}}</a>
                      </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                          <li>
                            <p>{{trans('page.best-support-detail-1')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.best-support-detail-2')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.best-support-detail-3')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.best-support-detail-4')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.best-support-detail-5')}}</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">{{trans('page.our-advantage-subtitle-licensed-broker')}}</a>
                      </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul class="dt-sc-fancy-list red arrow margin-top-30 padding-left-50">
                          <li>
                            <p>{{trans('page.licensed-broker-detail-1')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.licensed-broker-detail-2')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.licensed-broker-detail-3')}}</p>
                          </li>
                          <li>
                            <p>{{trans('page.licensed-broker-detail-4')}}</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 hidden toggleGhost management-structure">

            <div class="heading text-center">
              <h3>{{trans('page.menu-management-structure')}}</h3>
            </div>
            <div class="row">


              <div class="col-md-12">
                <table class="table-structure" style="margin-left: auto;margin-right: auto">
                  <tr>
                    <td style="width:180px;">{{trans('page.president-director')}}</td>
                    <td>Muhammad Yanwar Pohan</td>
                  </tr>
                  <tr>
                    <td style="width:180px;">{{trans('page.director')}}</td>
                    <td>Ade Manuel Ortega</td>
                  </tr>
                  <tr>
                    <td style="width:180px;">{{trans('page.director-of-compliance')}}</td>
                    <td>Rudi Anto, S.Sos</td>
                  </tr>
                  <tr>
                    <td style="width:180px;">{{trans('page.president-commissioner')}}</td>
                    <td>Ronald H. Lalamentik</td>
                  </tr>
                  <tr>
                    <td style="width:180px;">{{trans('page.commissioner')}}</td>
                    <td>Tofvin</td>
                  </tr>
                  <tr>
                    <td style="width:180px;">{{trans('page.shareholders')}}</td>
                    <td>Ronald H. Lalamentik</td>
                  </tr>
                </table>
              </div>
            </div>
            <!-- <div class="heading text-center">
              <h3>{{trans('page.menu-broker-representatives')}}</h3>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table class="table-structure" style="margin-left: auto;margin-right: auto">
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 1</td>
                    <td>Muhammad Yanwar Pohan</td>
                  </tr>
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 2</td>
                    <td>Euis Ferawati</td>
                  </tr>
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 3</td>
                    <td>Irwan Julian</td>
                  </tr>
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 4</td>
                    <td>Irvan Santoso</td>
                  </tr>
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 5</td>
                    <td>Widiastuti</td>
                  </tr>
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 6</td>
                    <td>Melina Octovia</td>
                  </tr>
                  <tr> 
                    <td style="width:180px;">{{trans('page.broker')}} 7</td>
                    <td>Suluh Adil Wicaksono</td>
                  </tr>
                </table>
              </div>
            </div> -->
          </div>
        </div>
      </div>
  </section>
</div>

<!-- always on -->
@include('page.template.always_on')
@endsection

@section('jsonpage')
<script>
  function toggleGhost(target) {
    $('.toggleGhost').each(function() {
      $(this).hide();
      $(`.${target}`).removeClass('hidden');
      $(`.${target}`).show();

    })
  }
</script>
@endsection
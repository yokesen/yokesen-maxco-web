@extends('page.template.master-campaign')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('cssonpage')
  <style media="screen">
    .bg-red{
        background-color: red !important;
    }

    .bg-blue{
        background-color: blue !important;
    }

    .bg-green{
        background-color: green !important;
    }
  </style>
@endsection

@section('facebookPixel')
  <!-- Facebook Pixel Code -->
  <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '242949443388541');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=242949443388541&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

@endsection

@section('content')

  <section class="sub-bnr" style="background:url(/web/images/campaigns/battle-of-robots/header.jpg) no-repeat;" data-stellar-background-ratio="0.5">

    <div class="position-center-center">
      <div class="container">
        <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/logo-botr.png" width="65%" alt="logo kompetisi trading battle of the robot">
      </div>
    </div>
  </section>

  <section>
    <div class="padding-top-50 padding-bottom-50 bg-paninblue">
      <div class="container">
        <!-- Heading -->

        <div class="row">
          <div class="col-md-6">
            <div class="text-center">
              <h6 style="color:white">Diperbolehkan Semua Tipe Robot (Expert Advisor)</h6>
              <p style="color:white">Bahkan jika anda punya Robot terbaik yang bisa profit ribuan persen!</p>
            </div>
            <div class="text-center">
              <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/allow-all-ea.jpg" width="80%">
            </div>
          </div>

          <div class="col-md-6" id="daftarSekarang">
            <div class="heading text-left margin-bottom-30">
              <h4 style="color:white">Daftar Sekarang!</h4>
            </div>
            <!-- CONTACT FORM -->
            <div class="contact-form">
              <!-- Success Msg -->
              @if(Session::get('insertsuccess') == 'true')
              <div class="heading text-center">
                <h3 style="color:white">Terima kasih</h3>
                <h3 style="color:white">Account Executive kami akan segera menghubungi Anda melalui Whatsapp.</h3>
              </div>
              <script>
                  fbq('track', 'Lead');
              </script>

              @else
              <!-- FORM -->
              <form method="post" action="{{route('funnelCampaignPost')}}">
                <ul class="row">
                  <li class="col-sm-12">
                    <label>
                      <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" required>
                    </label>
                  </li>
                  <li class="col-sm-12">
                    <label>
                      <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </label>
                  </li>
                  <li class="col-sm-3">
                    <label>
                      <input type="text" class="form-control" value="+62" disabled>
                    </label>
                  </li>
                  <li class="col-sm-9">
                    <label>
                      <input type="text" class="form-control" name="phone" placeholder="Nomor Whatsapp" required>
                    </label>
                  </li>
                  <hr>
                  <li class="col-sm-12">
                    <label>
                      <input type="text" class="form-control" name="username" placeholder="Username / Nama Robot / Kode Robot" required>
                    </label>
                  </li>
                  <input type="hidden" name="parent" value="{{Input::get('ref')? Input::get('ref') : Cookie::get('ref') }}" />
                  <input type="hidden" name="origin" value="<?php if (isset($_GET['so'])) {
                    echo $_GET['so'];
                  } else {
                    echo 'direct';
                  } ?>" />
                  <input type="hidden" name="campaign" value="79" />

                  @if (session('ErrorMessage') || isset($ErrorMessage))
                  <label class="error">{{ session('ErrorMessage')?session('ErrorMessage'):$ErrorMessage }}</label>
                  @endif
                  <!--end form-group -->
                  @csrf()
                  <li class="col-sm-12">
                    <button type="submit" value="submit" class="btn btn-1 bg-paninred-dark">Daftar Kompetisi <i class="fa fa-caret-right"></i></button>
                  </li>
                </ul>
              </form>
            @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="presentation padding-top-70 padding-bottom-70">
      <div class="container">
        <div class="logo" style="position:fixed!important;top:20px!important;left:20px!important;">
          <a href="{{route('index')}}">
            <img  class="img-responsive lazyload" data-src="{{url('/')}}/web/images/web/logo_300.png" alt="" style="<?php if(session('isMobile')){echo 'height:46px;';}else{echo 'height:49px;';}?>">
          </a>
        </div>
        <!-- Heading -->
        <div class="heading text-center">
          <span>Kompetisi Trading Real Account</span>
          <h1>Battle of the Robots</h1>
          <h5>Buktikan Robot kamu yang terbaik! Dan raih total hadiah <strong>Ratusan Juta Rupiah!</strong></h5>
        </div>


        <div class="single-slide">
          <div class="item"><img class="img-responsive" src="{{url('/')}}/web/images/campaigns/battle-of-robots/1.jpg"></div>
          <div class="item"><img class="img-responsive" src="{{url('/')}}/web/images/campaigns/battle-of-robots/2.jpg"></div>
          <div class="item"><img class="img-responsive" src="{{url('/')}}/web/images/campaigns/battle-of-robots/3.jpg"></div>
          <div class="item"><a href="#daftarSekarang"><img class="img-responsive" src="{{url('/')}}/web/images/campaigns/battle-of-robots/4.jpg"></a></div>
        </div>
      </div>
    </section>

  <!-- Plan -->
    <section class="light-gray-bg padding-top-70 padding-bottom-70">
      <div class="container">

        <!-- Heading -->
        <div class="heading text-left">
          <h4>Jadwal Kompetisi Trading Battle of the Robots</h4>
        </div>

        <!-- Start Timeline -->
        <div id="timeline" class="col-lg-10 col-md-12 col-sm-12">
          <ul id="dates" class="custom-list">
            <li><a href="#date1"><span>1 Okt 2020</span></a></li>
            {{-- <li><a href="#date2"><span>31 Okt 2020</span></a></li> --}}
            <li><a href="#date3"><span>2 Nov 2020</span></a></li>
            <li><a href="#date4"><span>30 Nov 2020</span></a></li>
            <li><a href="#date5"><span>4 Des 2020</span></a></li>
            <li><a href="#date6"><span>16 Des 2020</span></a></li>
          </ul>
          <ul id="issues" class="custom-list">
            <li id="date1">
              <div class="row">
                <div class="history-thumbnail col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 hidden-xs"> <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/jadwal-1.jpg" alt="jadwal kompetisi trading battle of the robot 2020"> </div>
                <div class="history-content col-lg-5 col-md-5 col-sm-5 col-xs-7"> <span class="date">1 Oktober 2020</span>
                  <h5 class="title">Pendaftaran dibuka</h5>
                  <p>Minimum pendaftaran untuk syarat masuk kompetisi trading Battle of the Robot adalah $1000 (atau setara Rp 10juta).</p>
                </div>
              </div>
            </li>
            {{-- <li id="date2">
              <div class="row">
                <div class="history-thumbnail col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 hidden-xs"> <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/jadwal-2.jpg" alt="jadwal kompetisi trading battle of the robot 2020" width="100%"> </div>
                <div class="history-content col-lg-5 col-md-5 col-sm-5 col-xs-7"> <span class="date">31 Oktober 2020</span>
                  <h5 class="title">Pendaftaran ditutup</h5>
                  <p>Pendaftaran ditutup dan kompetisi akan dimulai tanggal 2 November 2020. Akun kompetisi tidak diperbolehkan untuk mengambil posisi sampai pada penutupan pendaftaran.</p>
                </div>
              </div>
            </li> --}}
            <li id="date3">
              <div class="row">
                <div class="history-thumbnail col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 hidden-xs"> <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/jadwal-3.jpg" alt="jadwal kompetisi trading battle of the robot 2020" width="100%"> </div>
                <div class="history-content col-lg-5 col-md-5 col-sm-5 col-xs-7"> <span class="date">2 November 2020</span>
                  <h5 class="title">Kompetisi dimulai</h5>
                  <p>Kompetisi efektif dimulai per open market hari Senin pagi tanggal 2 November 2020, peserta dipersilahkan untuk memulai pengambilan posisi di akun kompetisi.</p>
                </div>
              </div>
            </li>
            <li id="date4">
              <div class="row">
                <div class="history-thumbnail col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 hidden-xs"> <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/jadwal-4.jpg" alt="jadwal kompetisi trading battle of the robot 2020" width="100%"> </div>
                <div class="history-content col-lg-5 col-md-5 col-sm-5 col-xs-7"> <span class="date">30 November 2020</span>
                  <h5 class="title">Kompetisi selesai</h5>
                  <p>Kompetisi berakhir di penutupan pasar tanggal 30 November 2020, yaitu pada subuh tanggal 1 Desember 2020. Seluruh perhitungan kompetisi di-cut off tetapi akun trading diperbolehkan untuk melanjutkan trading.</p>
                </div>
              </div>
            </li>
            <li id="date5">
              <div class="row">
                <div class="history-thumbnail col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 hidden-xs"> <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/jadwal-5.jpg" alt="jadwal kompetisi trading battle of the robot 2020" width="100%"> </div>
                <div class="history-content col-lg-5 col-md-5 col-sm-5 col-xs-7"> <span class="date">4 Desember 2020</span>
                  <h5 class="title">Pengumuman pemenang</h5>
                  <p>Berdasarkan perhitungan panitia, pemenang kompetisi akan diumumkan pada tanggal 4 Desember 2020. Akan ada dua kategori pemenang yaitu peraih ROI tertinggi dan peraih jumlah transaksi terbanyak.</p>
                </div>
              </div>
            </li>
            <li id="date6">
              <div class="row">
                <div class="history-thumbnail col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 hidden-xs"> <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/jadwal-6.jpg" alt="jadwal kompetisi trading battle of the robot 2020" width="100%"> </div>
                <div class="history-content col-lg-5 col-md-5 col-sm-5 col-xs-7"> <span class="date">16 Desember 2020</span>
                  <h5 class="title">Pembagian Hadiah</h5>
                  <p>Hadiah akan dibagikan dengan cara dikirimkan ke akun trading masing-masing pemenang.</p>
                </div>
              </div>
            </li>
          </ul>

        </div>
        <!-- End Timeline -->

      </div>
    </section>

    <!-- Plan -->
    <section class="padding-top-70 padding-bottom-70">
      <div class="container">

        <!-- Heading -->
        <div class="heading text-center">
          <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/podium.png" width="40%">
          <h4>Dua Podium untuk Diperebutkan</h4>
        </div>

        <!-- Plan -->
        <div class="plan">
          <ul class="row">
            <!-- starter -->
            <li class="col-md-6">
              <article>
                <div class="price">
                  <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/throphy.png" width="40%">
                  <h6>Kategori A</h6>
                  <h6>BEST PROFIT AWARD</h6>
                  <span class="currency">$</span>9.750<span class="period">(Total Hadiah)</span>
                </div>
                <p class="light padding-10">* Pemenangnya dinilai berdasarkan <strong>prosentase keuntungan tertinggi</strong> pada cut off akhir bulan, barangsiapa memperoleh return tertinggi akan memenangkan kategori ini</p>
                <p>* Profit dihitung untuk posisi tertutup pada cut off kompetisi trading</p>
                <p class="light ">* P/L dari posisi hedging masuk dalam hitungan kompetisi</p>
                <p>* Klasemen akan diumumkan harian</p>
                <p class="light ">* Juara akan diumumkan pada tanggal 4 Desember 2020</p>
                <h6>Juara 1 : <span>$ 5.000</span></h6>
                <h6 class="light">Juara 2 : <span>$ 3.000</span></h6>
                <h6>Juara 3 : <span>$ 1.000</span></h6>
                <h6 class="light">Juara 4 : <span>$ 500</span></h6>
                <h6>Juara 5 : <span>$ 250</span></h6>
                <p class="light ">* Hadiah akan dibagikan pada tanggal 16 Desember 2020</p>
              </article>
            </li>
            <!-- ultra -->
            <li class="col-md-6">
              <article>
                <div class="price">
                  <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/medal.png" width="40%">
                  <h6>Kategori B</h6>
                  <h6>HIGH FREQUENCY TRADER AWARD</h6>
                  <span class="currency">$</span>9.750<span class="period">(Total Hadiah)</span>
                </div>
                <p class="light padding-10">* Pemenang dinilai berdasarkan <strong>jumlah lot trading tertinggi</strong> pada cut off akhir bulan, barangsiapa memiliki jumlah transaksi volume terbanyak akan memenangkan kategori ini</p>
                <p>* Perhitungan hanya pada transaksi yang sudah ditutup</p>
                <p class="light ">* Klasemen akan diumumkan harian</p>
                <p>* Juara akan diumumkan pada tanggal 4 Desember 2020</p>
                <h6 class="light">Juara 1 : <span>$ 5.000</span></h6>
                <h6>Juara 2 : <span>$ 3.000</span></h6>
                <h6 class="light">Juara 3 : <span>$ 1.000</span></h6>
                <h6>Juara 4 : <span>$ 500</span></h6>
                <h6 class="light">Juara 5 : <span>$ 250</span></h6>
                <p>* Hadiah akan dibagikan pada tanggal 16 Desember 2020</p>
              </article>
            </li>
          </ul>
        </div>
      </div>
    </section>


    <!-- Counter -->
    <section class="counter padding-top-50 padding-bottom-50">
      <div class="container">

        <!-- Team Member -->
        <ul class="row">
          <li class="col-md-3">
            <div class="count margin-bottom-20"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="10" data-to="30" data-from="0"></span> </span>
              <h5>Hari Kompetisi</h5>
            </div>
          </li>

          <!-- Line Of Codes -->
          <li class="col-md-3">
            <div class="count margin-bottom-20"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="10" data-to="19500" data-from="0"></span> </span>
              <h5>Dollar Hadiah</h5>
            </div>
          </li>

          <!-- Satisfied Client -->
          <li class="col-md-3">
            <div class="count margin-bottom-20"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="10" data-to="92" data-from="0"></span> </span>
              <h5>Peserta</h5>
            </div>
          </li>

          <!-- PSD file included -->
          <li class="col-md-3">
            <div class="count margin-bottom-20"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="10" data-to="10" data-from="0"></span> </span>
              <h5>Pemenang</h5>
            </div>
          </li>
        </ul>
      </div>
    </section>

    <section class="padding-bottom-70">
      <div class="contact-info padding-top-100 padding-bottom-100" data-stellar-background-ratio="0.95">
        <div class="container">

          <!-- Heading -->
          <div class="heading white text-center">
            <h4>Ketentuan Kompetisi</h4>
            <span>Berikut beberapa hal berkaitan dengan syarat dan ketentuan kompetisi trading ini :</span>
          </div>
          <ul class="row">
            <li class="col-sm-4">
              <article class="margin-bottom-20">
                <h5>Deposit</h5>
                <p>Mininum deposit adalah $1.000 (rate 1$ = Rp 10.000,-)<p>
                <span class="margin-top-30">
                  <i class="fa fa-check-square"></i> Setara 10 juta rupiah
                </span>
              </article>
            </li>
            <li class="col-sm-4">
              <article class="margin-bottom-20">
                <h5>Expert Advisor</h5>
                <p>Minimum 50% dari total transaksi harus menggunakan Robot<p>
                <span class="margin-top-30">
                  <i class="fa fa-check-square"></i> diperbolehkan segala jenis robot.
                </span>
              </article>
            </li>
            <li class="col-sm-4">
              <article class="margin-bottom-20">
                <h5>Pendaftaran</h5>
                <p>Wajib melakukan pendaftaran melalui halaman ini<p>
                <span class="margin-top-30">
                  <i class="fa fa-check-square"></i> Daftar Sekarang!
                </span>
              </article>
            </li>
          </ul>
          <ul class="row">
            <li class="col-sm-4 margin-bottom-20">
              <article class="margin-bottom-20">
                <h5>Kategori</h5>
                <p>Terdapat 2 kategori pemenang: Best Profit Award dan High Frequency Trader Award. Jika seorang nasabah memenangkan dua kategori maka wajib memilih hadiah hanya di satu kategori saja<p>
                <span class="margin-top-30">
                  <i class="fa fa-check-square"></i> 1 nasabah hanya 1 kategori
                </span>
              </article>
            </li>
            <li class="col-sm-4">
              <article class="margin-bottom-20">
                <h5>Jumlah Trade</h5>
                <p>Semua peserta wajib melakukan trade minimal 10 kali untuk syarat kualifikasi pemenang<p>
                <span class="margin-top-30">
                  <i class="fa fa-check-square"></i> 1 trade per 2 hari
                </span>
              </article>
            </li>
            <li class="col-sm-4">
              <article class="margin-bottom-20">
                <h5>Jumlah Akun</h5>
                <p>Satu peserta hanya dapat mengikuti kompetisi dengan satu akun. Jika peserta memiliki akun nasabah lebih dari satu maka diwajibkan memilih satu akun yang akan diikut sertakan dalam kompetisi ini<p>
                <span class="margin-top-30">
                  <i class="fa fa-check-square"></i> 1 peserta per 1 akun
                </span>
              </article>
            </li>
          </ul>
          <div class="row margin-bottom-30">
            <div class="col-md-4 col-md-offset-4 text-center">
              <a href="#daftarSekarang" class="btn btn-lg btn-1 bg-paninred">Daftar Sekarang!<i class="fa fa-caret-right"></i></a>
            </div>
          </div>
        </div>
      </div>

    </section>


@endsection

@section('jslist')
  <script src="{{url('/')}}/web/js/jquery.timelinr-0.9.54.js"></script>
@endsection

@section('jsonpage')
  <script type="text/javascript">
  $(function(){
    $().timelinr({
      orientation: 'horizontal',
      // value: horizontal | vertical, default to horizontal
      containerDiv: '#timeline',
      // value: any HTML tag or #id, default to #timeline
      datesDiv: '#dates',
      // value: any HTML tag or #id, default to #dates
      datesSelectedClass: 'selected',
      // value: any class, default to selected
      datesSpeed: 'fast',
      // value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to normal
      issuesDiv : '#issues',
      // value: any HTML tag or #id, default to #issues
      issuesSelectedClass: 'selected',
      // value: any class, default to selected
      issuesSpeed: 'normal',
      // value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to fast
      issuesTransparency: 0.1,
      // value: integer between 0 and 1 (recommended), default to 0.2
      issuesTransparencySpeed: 500,
      // value: integer between 100 and 1000 (recommended), default to 500 (normal)
      prevButton: '#prev',
      // value: any HTML tag or #id, default to #prev
      nextButton: '#next',
      // value: any HTML tag or #id, default to #next
      arrowKeys: 'false',
      // value: true/false, default to false
      startAt: 1,
      // value: integer, default to 1 (first)
      autoPlay: 'true',
      // value: true | false, default to false
      autoPlayDirection: 'forward',
      // value: forward | backward, default to forward
      autoPlayPause: 10000
      // value: integer (1000 = 1 seg), default to 2000 (2segs)< });
    });
  });
  </script>
@endsection

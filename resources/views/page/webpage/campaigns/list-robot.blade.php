<section class="job padding-bottom-70">
  <div class="container">
    <div class="heading text-center">
      <img src="{{url('/')}}/web/images/campaigns/battle-of-robots/co-driver.png" width="40%">
      <h4>Beberapa Robot (Expert Advisor) yang bisa dipakai untuk Anda yang belum memiliki Robot.</h4>
    </div>
    <!-- Job Content -->
    <div id="accordion">
      <!-- Job Section -->
      <div class="row">
        @foreach ($robots as $n => $robot)
          <div class="col-md-4">
            <div class="job-content job-post-page margin-top-20">
              <div class="panel panel-default">
                <!-- Save -->
                <div class="star-save">
                  <a href="#."> <i class="fa fa-angle-down"></i></a>
                </div>
                <!-- PANEL HEADING -->
                <div class="panel-heading">
                  <a data-toggle="collapse" href="#{{$robot->robotSlugName}}">
                  <div class="job-tittle">
                    <div class="media-left">
                      <div class="date"> {{$robot->robotAttribute1 + $robot->robotAttribute2 + $robot->robotAttribute3}} <span>Power</span> </div>
                    </div>
                    <div class="media-body">
                      <h5>{{$robot->robotLongName}}</h5>
                      <span>{{$robot->robotAttribute5}} | </span>
                      <span>{{$robot->typeLongName}}</span>
                    </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                        <img src="{{env('APP_IMG_SEWAROBOT').'/'.$robot->robotImage}}" width="100%" alt="Robot {{$robot->robotLongName}}">
                      </div>
                    </div>
                  </a>

                </div>

                <!-- ADD INFO HERE -->
                <div id="{{$robot->robotSlugName}}" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>

                      {!!$robot->robotDescription!!}

                      <!-- Required Skills -->
                      <h5 class="margin-top-50">Robot Strength</h5>
                      <div class="job-skills">

                        <!-- Logo Design -->
                        <ul class="row">
                          <li class="col-sm-3">
                            <h6>Attack</h6>
                          </li>
                          <li class="col-sm-9">
                            <div class="progress">
                              <div class="progress-bar bg-red" role="progressbar" aria-valuenow="{{$robot->robotAttribute1}}" aria-valuemin="0" aria-valuemax="10" style="width: {{$robot->robotAttribute1*10}}%;"> </div>
                            </div>
                          </li>
                        </ul>

                        <!-- Logo Design -->
                        <ul class="row">
                          <li class="col-sm-3">
                            <h6>Defense</h6>
                          </li>
                          <li class="col-sm-9">
                            <div class="progress">
                              <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="{{$robot->robotAttribute2}}" aria-valuemin="0" aria-valuemax="10" style="width: {{$robot->robotAttribute2*10}}%;"> </div>
                            </div>
                          </li>
                        </ul>

                        <!-- UI / UX -->
                        <ul class="row">
                          <li class="col-sm-3">
                            <h6>Speed</h6>
                          </li>
                          <li class="col-sm-9">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="{{$robot->robotAttribute3}}" aria-valuemin="0" aria-valuemax="10" style="width: {{$robot->robotAttribute3*10}}%;"> </div>
                            </div>
                          </li>
                        </ul>

                        <ul class="row">
                          <li class="col-sm-4">
                            <h6>Ratio</h6>
                          </li>
                          <li class="col-sm-8">
                            <p>TP:SL {{$robot->robotAttribute4}}</p>
                          </li>
                        </ul>

                        <ul class="row">
                          <li class="col-sm-4">
                            <h6>Timeframe</h6>
                          </li>
                          <li class="col-sm-8">
                            <p>{{$robot->robotAttribute5}}</p>
                          </li>
                        </ul>

                        <!-- Additional Requirements -->
                        <h5 class="margin-top-50">Additional Requirements</h5>
                        <ul class="tags dall margin-bottom-40">
                          <li><a href="#.">Smart Maxco Competition Account</a></li>
                          <li><a href="#.">VPS</a></li>
                          <li><a href="#.">Lisence</a></li>
                        </ul>

                        <!-- Share -->
                        <div class="row share-info">
                          <div class="col-md-12"> <a href="#daftarSekarang" target="_blank" class="btn btn-1">Download Robot<i class="ion-ios-arrow-right"></i></a> </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        @endforeach
      </div>
    </div>
  </div>
</section>

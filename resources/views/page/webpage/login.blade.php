@extends('page.template.master_homepage')
@section('title','Maxco Futures | Prestigious Global Brokerage House')

@section('csslist')

@endsection

@section('content')
<section class="padding-bottom-70">

    <div class="container">

        <!-- Heading -->
        <div class="heading text-center margin-top-70 margin-bottom-30">
            <h4>Login</h4>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <!-- CONTACT FORM -->
                <div class="contact-form">
                    <!-- Success Msg -->
                    <div id="contact_message" class="success-msg"> <i class="fa fa-paper-plane-o"></i>Thank You. Your Message has been Submitted</div>

                    <!-- FORM -->
                    <form role="form" id="contact_form" class="contact-form" method="post" action="{{route('postlogin')}}">
                        @csrf
                        <ul class="row">
                            <li class="col-sm-12">
                                <label>
                                    <input type="text" class="form-control" name="login" id="login" placeholder="Login" required value="{{old('login') ? old('login') : ''}}" autofocus>
                                </label>
                            </li>
                            <li class="col-sm-12">
                                <label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                </label>
                            </li>
                            <li class="col-sm-6">
                                <!-- <button type="submit" value="submit" class="btn btn-1" id="btn_submit" >Login <i class="fa fa-caret-right"></i></button> -->
                                <a href="{{route('forgetpassword')}}">Forget Password ?</a>
                            </li>
                            <li class="col-sm-6 text-right">
                                <button type="submit" value="submit" class="btn btn-1" id="btn_submit" >Login <i class="fa fa-caret-right"></i></button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h4>Add Products</h4>
        </div>
        <div class="box-body">
          <form action="{{route('productUploadSave')}}" method="post" name="addProduct" id="addProduct" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="Brand">Brand ID</label>
              <select class="form-control" name="Brand">
                @foreach($brands as $brand)
                  <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="ProductName">Product Name</label>
              <input type="text" name="productName" class="form-control"  placeholder="Product Name" >
            </div>
            <div class="form-group">
              <label for="Product Name">Product Description</label>
              <textarea name="productDescription" class="form-control" form="addProduct" rows="4" placeholder="Product Name" ></textarea>
            </div>
            <div class="form-group">
              <label for="Product Function">Product Function</label>
              <select class="form-control" name="ProductFunction">
                @foreach($functions as $function)
                  <option value="{{$function->id}}">{{$function->functionName}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="Product Name">Product Display (can attach more than one):</label>
              <br />
              <input type="file" class="form-control" name="photos[]" multiple/>
            </div>

            <div class="form-group">
              <label for="Product Video">Product Detail  (can attach more than one)</label>
              <input type="file" class="form-control" name="videos[]" multiple />
            </div>

            <div class="form-group">
              <label for="Product Function">Product Status</label>
              <select class="form-control" name="productStatus">
                <option value="active">Active</option>
                <option value="inactive">InActive</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product Price</label>
              <input type="text" name="productPrice" class="form-control" placeholder="Product Price" >
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary" value="Simpan" />

            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
@endsection

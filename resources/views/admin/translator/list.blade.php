@extends("crudbooster::admin_template")

@section("content")
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Table With Full Features</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered table-striped datatables-simple">
            <thead>
              <tr>
                <th>Group</th>
                <th>Item</th>
                <th>Locale</th>
                <th>Text</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($lists as $list)
                <?php
                  $detail = DB::table('translator_translations')->where('item',$list->item)->first();
                ?>
                <tr>
                  <td>Page</td>
                  <td>{{$list->item}}</td>
                  <td>{{$detail->locale}}</td>
                  <td>{{$detail->text}}</td>
                  <td> <a href="{{route('translateEdit',$list->item)}}" class="btn btn-primary btn-block">Edit</a> </td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Group</th>
                <th>Item</th>
                <th>Locale</th>
                <th>Text</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
@endsection

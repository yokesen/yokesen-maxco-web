@extends("crudbooster::admin_template")

@section("content")
<div class="row">
  @foreach ($data as $isi)
    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h4>{{$locale}}</h4>
        </div>
        <div class="box-body">

        </div>
        <div class="box-footer">

        </div>
      </div>
    </div>
  @endforeach
</div>
@endsection

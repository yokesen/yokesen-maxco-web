@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h4>Add Products</h4>
        </div>
        <div class="box-body">
          <form action="{{route('productUploadSave')}}" method="post" name="addProduct" id="addProduct" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="Brand">Brand ID</label>
              <select class="form-control" name="Brand">
                @foreach($brands as $brand)
                  <option value="{{$brand->id}}" {{$product->brand_id == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="ProductName">Product Name</label>
              <input type="text" name="productName" class="form-control"  placeholder="Product Name" value="{{$product->productName}}">
            </div>
            <div class="form-group">
              <label for="Product Name">Product Description</label>
              <textarea name="productDescription" class="form-control" form="addProduct" rows="4" placeholder="Product Name" >{{$product->productDescription}}</textarea>
            </div>
            <div class="form-group">
              <label for="Product Function">Product Function</label>
              <select class="form-control" name="ProductFunction">
                @foreach($functions as $function)
                  <option value="{{$function->id}}" {{$product->productFunction == $function->id ? 'selected' : ''}}>{{$function->functionName}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="Product Function">Product Status</label>
              <select class="form-control" name="productStatus">
                <option value="active" {{$product->productStatus == 'active' ? 'selected' : ''}}>Active</option>
                <option value="inactive" {{$product->productStatus == 'inactive' ? 'selected' : ''}}>InActive</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product Price</label>
              <input type="text" name="productPrice" class="form-control" placeholder="Product Price" value="{{$product->productPrice}}">
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary" value="Simpan" />

            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto display</h4>
            </div>
            <div class="box-body">
              <?php
                $displays = unserialize($product->productImages);
              ?>
              <div class="row">
                @if($displays)
                  @foreach ($displays as $n => $display)
                    <div class="col-md-4">
                      <p><img src="{{$url}}/uploads/{{$display}}" alt="" width="100%"></p>
                      <p><a href="{{route('productImgDelete',[$product->id,$n])}}" class="btn btn-danger btn-xs btn-block">delete</a></p>
                      <hr>
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
            <div class="box-footer">
              <form action="{{route('productAddDisplay',$product->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Name">Product Display (can attach more than one):</label>
                  <br />
                  <input type="file" class="form-control" name="photos[]" multiple/>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto detail</h4>
            </div>
            <div class="box-body">
              <?php
                $details = unserialize($product->productVideos);
              ?>
              <div class="row">
                @if($details)
                  @foreach ($details as $n => $detail)
                    <div class="col-md-4">
                      <p><img src="{{$url}}/uploads/{{$detail}}" alt="" height="100px"></p>
                      <p><a href="{{route('productDtlDelete',[$product->id,$n])}}" class="btn btn-danger btn-xs btn-block">delete</a></p>
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
            <div class="box-footer">
              <form action="{{route('productAddDetail',$product->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Video">Product Detail  (can attach more than one)</label>
                  <input type="file" class="form-control" name="videos[]" multiple />
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection

<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Input;
use Cookie;
use Jenssegers\Agent\Agent;
use URL;
use Session;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class visitordata
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $agent = new Agent();

      $browser = $agent->browser();
      $version_browser = $agent->version($browser);

      $platform = $agent->platform();
      $version_platform = $agent->version($platform);

      $device = $agent->device();

      $isMobile = $agent->isMobile();
      $request->session()->put('isMobile', $isMobile);

      $client = new Client();

      $ip = request()->ip();
      $time = time();
      $date = date('H:i d-m-Y',$time);

      if(Input::get('ref')){
        Cookie::queue('ref', Input::get('ref'), 3243200);
        $ref = Input::get('ref');
      }else{
        if(empty(Cookie::get('ref'))){
          if(env('APP_ENV')=='dev'){
            $ref = '3';
          }else{
            $ref = '10037';
          }
          Cookie::queue('ref', $ref , 3243200);
        }else{
          $ref = Cookie::get('ref');
        }
      }

      $sales_request = $client->get(env('CRM_URL').'/referall_owner?id='.$ref,[
              'headers' => [
                "X-Authorization-Time" => time(),
                "X-Authorization-Token" => env('CRM_TOKEN'),
                "useragent" => $_SERVER['HTTP_USER_AGENT']
              ]
            ]);
      $sales = json_decode($sales_request->getBody());

      if(empty($sales->whatsapp)){
        Cookie::queue('salesWhatsapp', 'none', 3243200);
      }else{
        Cookie::queue('salesWhatsapp', $sales->whatsapp, 3243200);
      }

      if(empty($sales->photo)){
        Cookie::queue('salesPhoto', url('/').'/web/images/web/IMG_1795.JPG', 3243200);
      }else{
        Cookie::queue('salesPhoto', $sales->photo, 3243200);
      }

      Cookie::queue('salesName', $sales->name, 3243200);


      if(Input::get('uuid')){
        Cookie::queue('uuid', Input::get('uuid'), 3243200);
        $uuid = Input::get('uuid');
      }else{

        if(!Cookie::get('uuid')){
          $uuid = md5($ip.$time);
          Cookie::queue('uuid', $uuid , 3243200);
        }else{
          $uuid = Cookie::get('uuid');
        }
      }

      if(Input::get('campaign')){
        Cookie::queue('campaign', Input::get('campaign'), 3243200);
        $campaign = Input::get('campaign');
      }else{
        if(empty(Cookie::get('campaign'))){
          Cookie::queue('campaign', '1', 3243200);
          $campaign = '1';
        }else{
          $campaign = Cookie::get('campaign');
        }
      }

      $interest_request = $client->get(env('CRM_URL').'/campaign_detail?id='.$campaign,[
              'headers' => [
                "X-Authorization-Time" => time(),
                "X-Authorization-Token" => env('CRM_TOKEN'),
                "useragent" => $_SERVER['HTTP_USER_AGENT']
              ]
            ]);
      $interest = json_decode($interest_request->getBody());

      if(empty($interest->title)){
        Cookie::queue('campaignTitle', 'none', 3243200);
      }else{
        Cookie::queue('campaignTitle', $interest->title, 3243200);
      }

      if(Input::get('so')){
        Cookie::queue('so', Input::get('so'), 3243200);
        $so = Input::get('so');
      }else{
        if(empty(Cookie::get('so'))){
          Cookie::queue('so', 'direct', 3243200);
          $so = 'direct';
        }else{
          $so = Cookie::get('so');
        }
      }

        $agent = new Agent();

        $browser = $agent->browser();
        $version_browser = $agent->version($browser);

        $platform = $agent->platform();
        $version_platform = $agent->version($platform);

        $device = $agent->device();

        $url = URL::current();
        $before = URL::previous();

        $analytic_report = $client->post(env('CRM_URL').'/insert_analytic',[
                'headers' => [
                  "X-Authorization-Time" => time(),
                  "X-Authorization-Token" => env('CRM_TOKEN'),
                  "useragent" => $_SERVER['HTTP_USER_AGENT']
                ],
                'form_params' => [
                  "uuid"=>$uuid,
                  "timecode"=>$time,
                  "ipaddress"=>$ip,
                  "ref"=>$ref,
                  "campaign"=>$campaign,
                  "so"=>$so,
                  "browser"=>$browser,
                  "version_browser"=>$version_browser,
                  "platform"=>$platform,
                  "version_platform"=>$version_platform,
                  "device"=>$device,
                  "url_current"=>$url,
                  "url_from"=>$before,
                  "flag"=>1
                ]
              ]);
        $analytic = json_decode($analytic_report->getBody());

        $insert = DB::table('analytics')->insert([
          "uuid"=>$uuid,
          "timecode"=>$time,
          "ipaddress"=>$ip,
          "ref"=>$ref,
          "campaign"=>$campaign,
          "so"=>$so,
          "browser"=>$browser,
          "version_browser"=>$version_browser,
          "platform"=>$platform,
          "version_platform"=>$version_platform,
          "device"=>$device,
          "url_current"=>$url,
          "url_from"=>$before,
          "flag"=>1
          ]);

        $journey = DB::table('analytics')->where('uuid',$uuid)->count();
        Session::put('journey',$journey);

        if(empty(Session::get('robot'))){
          $robot = 1;
          Session::put('robot',$robot);
          //dd('disini');
        }else{
          $robot = Session::get('robot');
          $newRobot = $robot + 1;
          Session::put('robot',$newRobot);
          //dd('disana');
        }

        $response = $next($request);
        return $response->withCookie(cookie()->forever('ref', $ref));
    }
}

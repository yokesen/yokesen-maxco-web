<?php

namespace App\Http\Middleware;

use Closure;
use DB;


class Nav
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $NavBrands = DB::table('brand')->orderby('name','asc')->where('status','active')->get();
        $NavFunction = DB::table('functions')->orderby('functionName','asc')->where('functionStatus','active')->get();
        return $next($request,compact('NavBrands','NavFunction'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Cookie;
use Jenssegers\Agent\Agent;
use URL;
use Alert;

class FormController extends Controller
{
    public function register(Request $request){
      $requestData = $request->all();
      $whatsapp = $requestData['whatsapp'];
      $whatsapp = str_replace('-','',$whatsapp);
      $whatsapp = str_replace(' ','',$whatsapp);
      $whatsapp = str_replace(' ','',$whatsapp);
      $whatsapp = str_replace('.','',$whatsapp);
      $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
      $check_number = str_split($whatsapp);
      $new_number = "62";

      if($check_number[0]=='0'){
        foreach($check_number as $n => $number){
          if($n > 0){
            if($check_number[1]=='8'){
              $new_number .= $number;
            }else{
              $new_number = '0';

            }
          }
        }
      }else{
        if($check_number[0]=='8'){
          $new_number = "62".$whatsapp;
        }elseif($check_number[0]=='6'){
          $new_number = $whatsapp;
        }elseif($check_number[0]=='+'){
          foreach($check_number as $n => $number){
            if($n > 2){
                $new_number .= $number;
            }
          }
        }else{
          $new_number = '0';
        }
      }
      $request['whatsapp'] = $new_number ;

      $validatedData = $request->validate([
        'email' => 'required|max:255|unique:crm.register',
        'whatsapp' => 'required|unique:crm.register',
        'password' => 'required|confirmed'
      ]);

      $agent = new Agent();

      $insert = DB::connection('crm')->table('register')->insert([
        'name' => '',
        'password' => bcrypt($request->password),
        'email' => $request->email,
        'whatsapp' => $request->whatsapp,
        'parent' => Cookie::get('ref'),
        'origin' => Cookie::get('so'),
        'campaign' => Cookie::get('campaign'),
        'ipaddress' => request()->ip(),
        'desktop' => $agent->platform(),
        'device' => $agent->device(),
        'lang' => serialize($agent->languages()),
        'previous_url' => $request->lastURL,
        'uuid' => Cookie::get('uuid')
      ]);

    }

    public function login(Request $request){

      $whatsapp = $request->credential;
      $whatsapp = str_replace('-','',$whatsapp);
      $whatsapp = str_replace(' ','',$whatsapp);
      $whatsapp = str_replace(' ','',$whatsapp);
      $whatsapp = str_replace('.','',$whatsapp);
      $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
      $check_number = str_split($whatsapp);
      $new_number = "62";

      if($check_number[0]=='0'){
        foreach($check_number as $n => $number){
          if($n > 0){
            if($check_number[1]=='8'){
              $new_number .= $number;
            }else{
              $new_number = '0';

            }
          }
        }
      }else{
        if($check_number[0]=='8'){
          $new_number = "62".$whatsapp;
        }elseif($check_number[0]=='6'){
          $new_number = $whatsapp;
        }elseif($check_number[0]=='+'){
          foreach($check_number as $n => $number){
            if($n > 2){
                $new_number .= $number;
            }
          }
        }else{
          $new_number = '0';
        }
      }
      $whatsapp = $new_number ;

      $password = $request->password;

      $checkEmail = DB::connection('crm')->table('register')->where('email',$request->credential)->count();

      if($checkEmail > 0){
        $users = DB::connection('crm')->table('register')->where('email',$request->credential)->first();
        if(\Hash::check($password,$users->password)){
          dd($users);
        }else{
          alert()->error('Password You provided does not match Our Record', 'Oooops! is that realy You?')->persistent('Close');
          return redirect()->route('login');
        }
      }elseif($checkEmail == 0){
        $checkWhatsapp = DB::connection('crm')->table('register')->where('whatsapp',$whatsapp)->count();

        if($checkWhatsapp > 0){
          $users = DB::connection('crm')->table('register')->where('whatsapp',$whatsapp)->first();
          if(\Hash::check($password,$users->password)){
            dd($users);
          }else{
            alert()->error('Password You provided does not match Our Record', 'Oooops! is that realy You?')->persistent('Close');
            return redirect()->route('login');
          }
        }else{
          alert()->error('We could not find your account', 'Please Register!')->persistent('Close');
          return redirect()->route('login');
        }
      }


    }

    public function cookie(){
      dd(Cookie::get());
    }
}

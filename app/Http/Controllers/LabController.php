<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LabController extends Controller
{
    public function email(){
      $email_email = 'yoke.endarto@gmail.com';
      $name_email = 'Yoke Endarto';

      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('email.lab-sunny', [
        'name' => 'Namanya Yoke',
        'email' => 'emailnya Endarto',
        'time' => 'waktunya kerja'
      ],
        function($message) use ($name_email, $email_email)
      {
          $message
            ->from('noreply@yokesen.com','Smart Maxco Service')
              ->to($email_email,$name_email)
                ->subject('Ini Template yng benar Sunny');
      });

      if($beautymail){
        dd($beautymail);
      }
    }
}

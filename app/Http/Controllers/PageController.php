<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Session;
use Jenssegers\Agent\Agent;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Validator;
use Alert;
use URL;
use Cookie;

class PageController extends Controller
{

  public function login()
  {

    return view('page.webpage.login');
  }

  public function postlogin(Request $request)
  {

    $validator = Validator::make(
      $request->all(),
      [
        'login' => 'required|email'
      ]
    );
    if ($validator->fails()) {
      if (strlen($request->login) > 6) {
        $InputType = 'phone';
      } else {
        $InputType = 'id';
      }
    } else {
      $InputType = 'email';
    }

    $client = new Client();
    $urllogin = env('API_URL') . 'api/account/listmatchusers';
    $client_login = $client->post($urllogin, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' =>  [
        'Username' => $request->login,
        'Password' => $request->password,
        'InputType' => $InputType,
        'AreaCode' => '62'
      ]
    ]);
    $accounts = json_decode($client_login->getBody());
    // dd($accounts);
    // jika berhasil login
    if ($accounts->Code === 0) {
      $header = $client_login->getHeaders();

      if (count($header['Set-Cookie']) > 1) {
        $cfuid = $header['Set-Cookie'][0];
        Cookie::queue('__cfduid', substr($cfuid, 9));
        $cookie = $header['Set-Cookie'][1];
      } else {
        $cookie = $header['Set-Cookie'][0];
      }

      $cookie = explode(";", $cookie);
      $session = trim($cookie[0]);
      Cookie::queue('NBCRMWEBAPI.SESSION', substr($session, 20));
      Session::put('user.tokencabinet', substr($session, 20));
      Session::put('user.token', $session);
      Session::put('user.id', $accounts->Data->User->UserId);
      Session::put('user.parent', $accounts->Data->User->ParentId);
      Session::put('user.type', $accounts->Data->User->UserType);
      Session::put('user.email', $accounts->Data->User->Email);
      Session::put('user.mobile', $accounts->Data->User->Mobile);
      Session::put('user.gender', $accounts->Data->User->Gender);
      Session::put('user.name', $accounts->Data->IdCard->RealName);
      Session::put('user.IDNO', $accounts->Data->IdCard->IDNO);
      Session::put('user.PlaceOfBirth', $accounts->Data->User->PlaceOfBirth);
      Session::put('user.DateOfBirth', $accounts->Data->User->DateOfBirth);
      Session::put('user.Address', $accounts->Data->User->Address);
      Session::put('user.KycStatus', $accounts->Data->User->KycStatus);
      Session::put('user.Currency', $accounts->Data->User->Currency);
      return redirect()->route('profile-trading-tool');
    } else {
      alert()->info('Info Message.', $accounts->Message);
      return redirect()->back()->withInput();
    }
  }

  public function forgetpassword(Request $request)
  {
    return view('page.webpage.forgetpassword');
  }

  public function sendVerificationCodeForgotPass(Request $request)
  {
    $validator = Validator::make(
      $request->all(),
      [
        'login' => 'required|email'
      ]
    );
    $dataset=[];
    if ($validator->fails()) {
      $dataset['ValidateType'] = 43;
      $dataset['InfoType']=0;
      $dataset['AreaCode']="62";
      $dataset['Mobile']=$request->login;
      Session::put('forgetpassword.ValidateType',43);
      Session::put('forgetpassword.InfoType',0);
      Session::put('forgetpassword.AreaCode',"62");
      Session::put('forgetpassword.Mobile',$request->login);
      $sendby="sms";
    } else {
      $dataset['ValidateType'] = 34;
      $dataset['InfoType']=1;
      $dataset['Email']=$request->login;
      $sendby="email";
      Session::put('forgetpassword.ValidateType',34);
      Session::put('forgetpassword.InfoType',1);
      Session::put('forgetpassword.Email',$request->login);
    }
    $api_url = env('API_URL');
    $url = $api_url . 'api/message/sendvalidatecaptcha';
    $client = new Client();
    // ValidateType
    // 34 => Email forgot password verification code
    // 43 => SMS forgot password verification code
    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => $dataset
    ]);
    $response = json_decode($requestPost->getBody());
    if($response->Code==0){
      alert()->success('Success', 'Anda telah dikirimkan kode verifikasi melalui '.$sendby)->persistent('Close');
      return redirect()->route('forgetpasswordchange');
    }else{
      alert()->info('Info', $response->Message)->persistent('Close');
      return redirect()->back()->withInput();
    }
  }

  public function forgetpasswordchange(Request $request){
    return view('page.webpage.forgetpasswordchange');
  }

  public function forgetpasswordnew(Request $request){

    $UserId = Session::get('user.id');
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/resetpassword';
    $client = new Client();
    $jsonDt = array(
      'VerifyCode' => $request->VerifyCode,
      'InfoType' => Session::get('forgetpassword.InfoType'),
      'UserId' => $UserId,
      'NewPassword' => $request->Password,
    );
    if (Session::get('forgetpassword.InfoType') === 0) {
      $jsonDt = array_add($jsonDt, 'AreaCode', Session::get('forgetpassword.AreaCode'));
      $jsonDt = array_add($jsonDt, 'Mobile', Session::get('forgetpassword.Mobile'));
    } else {
      $jsonDt = array_add($jsonDt, 'Email',Session::get('forgetpassword.Email'));
    }

    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => $jsonDt
    ]);
    $response = json_decode($requestPost->getBody());

    if ($response->Code === 0) {
      Session::forget('forgetpassword.ValidateType');
      Session::forget('forgetpassword.InfoType');
      Session::forget('forgetpassword.AreaCode');
      Session::forget('forgetpassword.Mobile');
      Session::forget('forgetpassword.Email');
      alert()->success('Success', 'Password berhasil dirubah')->persistent('Close');
      return redirect()->route('login');
    } else {
      alert()->info('Info', $response->Message)->persistent('Close');
      return redirect()->back();
    }
  }

  public function logout()
  {
    Session::forget('user.token');
    // Session::forget('user.id');
    Session::forget('user.parent');
    Session::forget('user.type');
    Session::forget('user.email');
    Session::forget('user.mobile');
    Session::forget('user.gender');
    Session::forget('user.name');
    Session::forget('user.IDNO');
    Session::forget('user.PlaceOfBirth');
    Session::forget('user.DateOfBirth');
    Session::forget('user.Address');
    Session::forget('user.KycStatus');
    Session::forget('user.Currency');

    return redirect()->route('login');
  }

  public function index()
  {
    $latest = DB::table('blogs')->where('blogStatus', 'active')->orderby('release_date', 'desc')->limit(9)->get();
    $navMenu = "index";
    $agent = new Agent();
    return view('page.webpage.homepage', compact('latest', 'navMenu', 'agent'));
  }
  public function logo()
  {
    return view('page.webpage.logo', ["navMenu" => "logos"]);
  }

  public function companyProfile()
  {
    $agent = new Agent();
    $navMenu = "about-us";
    $dropdownNav = "company-profile";
    return view('page.webpage.aboutus.companyProfile', compact('agent', 'navMenu', 'dropdownNav'));
  }
  public function legality()
  {
    return view('page.webpage.aboutus.legality', ["navMenu" => "about-us", "dropdownNav" => "company-profile"]);
  }
  public function managementStructure()
  {
    return view('page.webpage.aboutus.managementStructure', ["navMenu" => "about-us", "dropdownNav" => "company-profile"]);
  }
  public function brokerRepresentative()
  {
    return view('page.webpage.aboutus.brokerRepresentative', ["navMenu" => "about-us", "dropdownNav" => "company-profile"]);
  }
  public function officeLocation()
  {
    return view('page.webpage.aboutus.officeLocation', ["navMenu" => "about-us", "dropdownNav" => "company-profile"]);
  }
  // futures
  public function futuresTrading()
  {
    return view('page.webpage.futures.futuresTrading', ["navMenu" => "futures", "dropdownNav" => "company-profile"]);
  }
  public function futuresRules()
  {
    return view('page.webpage.futures.futuresRules', ["navMenu" => "futures", "dropdownNav" => "company-profile"]);
  }

  // plarform
  public function platformDownload()
  {
    return view('page.webpage.platform.download', ["navMenu" => "platform", "dropdownNav" => "company-profile"]);
  }
  public function platformIntroduction()
  {
    return view('page.webpage.platform.introduction', ["navMenu" => "platform", "dropdownNav" => "company-profile"]);
  }
  public function platformHowToUse()
  {
    return view('page.webpage.platform.howtouse', ["navMenu" => "platform", "dropdownNav" => "company-profile"]);
  }
  public function maxcoMobile()
  {
    return view('page.webpage.platform.maxco_mobile', ["navMenu" => "platform", "dropdownNav" => "company-profile"]);
  }


  // Market
  public function marketForex()
  {
    return view('page.webpage.market.forex', ["navMenu" => "market", "dropdownNav" => "company-profile"]);
  }
  public function marketKomoditi()
  {
    return view('page.webpage.market.komoditi', ["navMenu" => "market", "dropdownNav" => "company-profile"]);
  }
  public function marketIndex()
  {
    return view('page.webpage.market.indexMarket', ["navMenu" => "market", "dropdownNav" => "company-profile"]);
  }
  public function tradingAccountTtype()
  {
    return view('page.webpage.futures.tradingAccountTtype', ["navMenu" => "market", "dropdownNav" => "company-profile"]);
  }

  public function tradinghours()
  {
    return view('page.webpage.futures.tradinghours', ["navMenu" => "market", "dropdownNav" => "company-profile"]);
  }
  // expert
  public function expertAnalyzeMarket()
  {
    return view('page.webpage.expert.analyzeMarket', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function expertCandlestick()
  {
    return view('page.webpage.expert.candlestick', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function expertTechnicalAnalysis()
  {
    return view('page.webpage.expert.technicalAnalysis', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function expertFundamentalAnalysis()
  {
    return view('page.webpage.expert.fundamentalAnalysis', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function expertMarketNews()
  {
    return view('page.webpage.expert.marketNews', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function expertTips()
  {
    return view('page.webpage.expert.expertTips', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function tradingTricks()
  {
    return view('page.webpage.expert.tradingTricks', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function news()
  {
    return view('page.webpage.expert.news', ["navMenu" => "expert-area", "dropdownNav" => "company-profile"]);
  }
  public function privacyPolicy()
  {
    return view('page.webpage.privacypolicy');
  }
  public function redirect(Request $request)
  {
    $pageredirect = $request->pageurl;
    // dd($pageredirect);
    return view('page.webpage.redirect', compact('pageredirect'));
  }
  public function company()
  {
    $histories = db::table('histories')->where('status', 'active')->get();
    $brand = DB::table('brand')->orderby('name', 'asc')->get();
    $company = DB::table('company')->orderby('id', 'asc')->get();
    $video = DB::table('videoaboutus')->where('status', 'active')->first();
    //dd($company);
    return view('pages.about.company', compact('histories', 'brand', 'company', 'video'));
  }

  public function blogIndex(Request $request)
  {
    $news = DB::table('blogs')->where('blogStatus', 'active');
    if (!empty($request->query('search'))) {
      $search = $request->query('search');
      $news = $news->where(function ($query) use ($search) {
        $query->where('blogTitle', 'like', '%' . $search . '%')
          ->orWhere('blogSlug', 'like', '%' . $search . '%')
          ->orWhere('blogContent', 'like', '%' . $search . '%');
      });
    }
    $news = $news->orderby('id', 'desc')->paginate(5);
    $latest = DB::table('blogs')->where('blogStatus', 'active')->orderby('id', 'asc')->limit(5)->get();
    // dd($news);
    return view('page.webpage.blogs.list', compact('news', 'latest'));
  }

  public function blogSingle($slug)
  {
    $isi = DB::table('blogs')->where('blogSlug', $slug)
      ->join('cms_users', 'blogs.blogAuthor', '=', 'cms_users.id')
      ->first();
    $list = DB::table('blogs')->where('blogStatus', 'active')->orderby('id', 'desc')->limit(5)->get();
    $update = DB::table('blogs')->where('blogSlug', $slug)->update([
      'blogRead' => $isi->blogRead + 1
    ]);
    // dd($isi);
    return view('page.webpage.blogs.single', compact('isi', 'list'));
  }

  public function getsmartmaxcoapps()
  {
    $fbqid = "242949443388541";
    if (isset($fbqid)) {
      Cookie::queue('fbqid', $fbqid, 60);
    }
    Cookie::queue('fbqtrack', 'CompleteRegistration', 60);
    $agent = new Agent();
    return view('page.webpage.campaign.getsmartmaxcoapps', compact('fbqid', 'agent'));
  }

  public function easywayaccesstrading()
  {
    // $fbqid = "242949443388541";
    $videosYt = array(
      [
        'url' => 'https://www.youtube.com/embed/X4mZ308hgW8',
        'title' => 'Introduction Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/S3YXFMHWlNM',
        'title' => 'Fitur dan Keunggulan Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/yZcUHFz0I4o',
        'title' => 'Cara Mendaftar Akun Demo dan Login di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/Ce5i3kpAE6g',
        'title' => 'Cara Mendaftar Akun Live di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/0MpiKfTK6oA',
        'title' => 'Cara Setor Dana di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/_JZGzyirWys',
        'title' => 'Cara Tarik Dana di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/BgldwvO9yOE',
        'title' => 'Pengertian One Click Trading di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/DUOBgZnEv6Y',
        'title' => 'Fitur Livechat di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/EFJTOhWU3mg',
        'title' => 'Cara Menyimpan Aset Trading Favorit di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/k5PHqGgeROU',
        'title' => 'Cara Melihat Grafik dan Mengubah Timeline di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/OCNidiHdRXA',
        'title' => 'Cara Mengubah Orientasi Grafik di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/R10uohyEtIQ',
        'title' => 'Cara Melihat Detail Aset Trading di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/3zaWia9nudY',
        'title' => 'Cara Melakukan Open Position di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/GtP5d2KXJko',
        'title' => 'Cara Mengubah Lot Size di Smar Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/PPvx_CQ8Qmo',
        'title' => 'Pengertian Market Order dan Entry Order di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/EpKAPuy_w80',
        'title' => 'Cara Melihat Riwayat Transaksi Anda di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/FZph2OBkEXM',
        'title' => 'Cara Memodifikasi Transaksi di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/sTWtf48TStc',
        'title' => 'Cara Beralih dari Akun Demo ke Akun Live di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/j7EsraqcV78',
        'title' => 'Cara Membaca Kondisi Keuangan Trading di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/eMtS4Uj72Sk',
        'title' => 'Hal yang Perlu Diperhatikan Sebelum Bertransaksi di Smart Maxco App',
      ],
      [
        'url' => 'https://www.youtube.com/embed/smzNPSDLwYk',
        'title' => 'Cara Membaca Menu Pasar di Smart Maxco App',
      ],
    );
    // dd($videosYt);
    $agent = new Agent();
    return view('page.webpage.campaign.easywayaccesstrading', compact('fbqid', 'agent', 'videosYt'));
  }

  public function legalbroker()
  {
    // $fbqid = "242949443388541";
    return view('page.webpage.campaign.legalbroker');
  }

  public function prestigious()
  {
    // $fbqid = "242949443388541";
    return view('page.webpage.campaign.prestigious');
  }

  public function commission80percent()
  {
    // $fbqid = "242949443388541";
    // if (isset($fbqid)) {
    //   Cookie::queue('fbqid', $fbqid, 60);
    // }
    $agent = new Agent();
    // Cookie::queue('fbqtrack', 'CompleteRegistration', 60);
    return view('page.webpage.campaign.commission80percent', compact('agent'));
  }

  public function simplemarketing()
  {
    // $fbqid = "242949443388541";
    $agent = new Agent();
    return view('page.webpage.campaign.simplemarketing', compact('agent'));
  }

  public function guejugainvest()
  {
    // $fbqid = "242949443388541";
    $agent = new Agent();
    return view('page.webpage.campaign.guejugainvest', compact('agent'));
  }

  public function bantusuamidarirumah()
  {
    // $fbqid = "242949443388541";
    $agent = new Agent();
    return view('page.webpage.campaign.bantusuamidarirumah', compact('agent'));
  }

  public function acceptalleas()
  {
    // $fbqid = "242949443388541";
    return view('page.webpage.campaign.accept-all-ea');
  }

  public function artikel10()
  {
    return view('page.webpage.campaign.artikel-10');
  }

  public function robotDemo()
  {
    return view('page.webpage.campaign.robot-trading-demo');
  }

  public function thankyou()
  {
    $fbqid = Cookie::get('fbqid');
    $fbqtrack = Cookie::get('fbqtrack');
    // dd($fbqtrack);
    return view('page.webpage.campaign.thankyou', compact('fbqid', 'fbqtrack'));
  }

  public function tradingrules()
  {
    $agent = new Agent();
    return view('page.webpage.tradingrules', compact('agent'));
  }

  public function history()
  {
    $histories = db::table('histories')->get();
    $brand = DB::table('brand')->orderby('name', 'asc')->get();
    return view('pages.about.history', compact('histories', 'brand'));
  }

  public function visionMission()
  {

    return view('pages.about.vision-mission');
  }

  public function value()
  {
    $brand = DB::table('brand')->orderby('name', 'asc')->get();

    return view('pages.about.values', compact('brand'));
  }

  public function apiinsertcrm(Request $request)
  {
    // dd($request);
    $whatsapp = $request->phone;
    $whatsapp = str_replace('-', '', $whatsapp);
    $whatsapp = str_replace(' ', '', $whatsapp);
    $whatsapp = str_replace(' ', '', $whatsapp);
    $whatsapp = str_replace('.', '', $whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if ($check_number[0] == '0') {
      foreach ($check_number as $n => $number) {
        if ($n > 0) {
          if ($check_number[1] == '8') {
            $new_number .= $number;
          } else {;
            $new_number = '-';
          }
        }
      }
    } else {
      if ($check_number[0] == '8') {
        $new_number = "62" . $whatsapp;
      } elseif ($check_number[0] == '6') {
        $new_number = $whatsapp;
      } elseif ($check_number[0] == '+') {
        foreach ($check_number as $n => $number) {
          if ($n > 2) {
            $new_number .= $number;
          }
        }
      } else {
        $new_number = '-';
      }
    }

    if ($new_number == '-') {
      Alert::warning('Nomor Whatsapp anda salah!', 'Error')->persistent('Close');
      return redirect()->back()->withInput();
    }

    $agent = new Agent();
    $client = new Client();
    // dd(env('CRM_TOKEN'));
    $insertcrm = $client->post(env('CRM_URL') . '/insert_client', [
      'headers' => [
        "X-Authorization-Time" => time(),
        "X-Authorization-Token" => env('CRM_TOKEN'),
        "useragent" => $_SERVER['HTTP_USER_AGENT']
      ],
      'form_params' => [
        'name' => $request->name,
        'email' => $request->email,
        'phone' => $request->phone,
        'ktp' => '',
        'parent' => $request->parent,
        'origin' => $request->origin,
        'campaign' => $request->campaign,
        'media_iklan' => $request->media,
        'previous_url' => URL::previous(),
        'ipaddress' => $request->ip(),
        'desktop' => $agent->platform(),
        'device' => $agent->device(),
        'lang' => $agent->languages()[0],
        'uuid' => Cookie::get('uuid'),
      ]
    ]);
    $responsecrm = json_decode($insertcrm->getBody());
    // dd($responsecrm);
    if ($responsecrm->api_status === 1) {
      return response()->json(['success' => true, 'name' => $request->name]);
    }

    // return redirect()->back()->with(['name'=>$request->name,'insertsuccess'=>true]);
  }

  public function registerCRM(Request $request)
    {
      //dd($request->all());
      $whatsapp = $request->phone;
      $whatsapp = str_replace('-', '', $whatsapp);
      $whatsapp = str_replace(' ', '', $whatsapp);
      $whatsapp = str_replace(' ', '', $whatsapp);
      $whatsapp = str_replace('.', '', $whatsapp);
      $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
      $check_number = str_split($whatsapp);
      $new_number = "62";

      if ($check_number[0] == '0') {
        foreach ($check_number as $n => $number) {
          if ($n > 0) {
            if ($check_number[1] == '8') {
              $new_number .= $number;
            } else {;
              $new_number = '-';
            }
          }
        }
      } else {
        if ($check_number[0] == '8') {
          $new_number = "62" . $whatsapp;
        } elseif ($check_number[0] == '6') {
          $new_number = $whatsapp;
        } elseif ($check_number[0] == '+') {
          foreach ($check_number as $n => $number) {
            if ($n > 2) {
              $new_number .= $number;
            }
          }
        } else {
          $new_number = '-';
        }
      }

      if ($new_number == '-') {
        Alert::warning('Nomor Whatsapp anda salah!', 'Error')->persistent('Close');
        return redirect()->back()->withInput();
      }

      $agent = new Agent();
      $client = new Client();
      // dd(env('CRM_TOKEN'));
      $insertcrm = $client->post(env('CRM_URL') . '/insert_client', [
        'headers' => [
          "X-Authorization-Time" => time(),
          "X-Authorization-Token" => env('CRM_TOKEN'),
          "useragent" => $_SERVER['HTTP_USER_AGENT']
        ],
        'form_params' => [
          'username' => $request->username,
          'name' => $request->name,
          'email' => $request->email,
          'phone' => $request->phone,
          'ktp' => '',
          'parent' => $request->parent,
          'origin' => $request->origin,
          'campaign' => $request->campaign,
          'media_iklan' => $request->media,
          'previous_url' => URL::previous(),
          'ipaddress' => $request->ip(),
          'desktop' => $agent->platform(),
          'device' => $agent->device(),
          'lang' => $agent->languages()[0],
          'uuid' => Cookie::get('uuid'),
        ]
      ]);
      $responsecrm = json_decode($insertcrm->getBody());
      alert()->success('Account Executive kami akan segera menghubungi Anda melalui Whatsapp.', 'Pendaftaran Berhasil!')->autoclose(3500);
      return redirect()->back()->with(['name'=>$request->name,'insertsuccess'=>'true']);
  }

  public function robotTradingTester(){
    return view('page.webpage.campaign2.robot-trading-tester');
  }

  public function apiregistermaxco(Request $request)
  {
    Session::put('demouser.Password', $request->Password);
    Session::put('demouser.ConfirmPassword', $request->ConfirmPassword);
    Session::put('demouser.Email', $request->Email);
    Session::put('demouser.Realname', $request->Realname);
    Session::put('demouser.AreaCode', $request->AreaCode);
    Session::put('demouser.Mobile', $request->Mobile);
    Session::put('demouser.IDNO', $request->IDNO);
    Session::put('demouser.Nationality', $request->Nationality);
    Session::put('demouser.VerifyCode', $request->VerifyCode);
    Session::put('demouser.ParentId', $request->ParentId);
    Session::put('demouser.InfoType', $request->InfoType);
    Session::put('demouser.originRegister', $request->originRegister);

    return redirect()->route('getRegisterVerification');
  }

  public function savedataregister(Request $request)
  {
    try {
      Session::put('demouser.Password', $request->Password);
      Session::put('demouser.ConfirmPassword', $request->ConfirmPassword);
      Session::put('demouser.Email', $request->Email);
      Session::put('demouser.Realname', $request->Realname);
      Session::put('demouser.AreaCode', $request->AreaCode);
      Session::put('demouser.Mobile', $request->Mobile);
      Session::put('demouser.IDNO', $request->IDNO);
      Session::put('demouser.Nationality', $request->Nationality);
      Session::put('demouser.VerifyCode', $request->VerifyCode);
      Session::put('demouser.ParentId', $request->ParentId);
      Session::put('demouser.InfoType', $request->InfoType);
      Session::put('demouser.originRegister', $request->originRegister);

      $api_url = env('API_URL');
      $url = $api_url . 'api/message/sendvalidatecaptcha';
      $client = new Client();
      $requestCode = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en'
        ],
        'json' => [
          'InfoType' => Session::get('demouser.InfoType'),
          'ValidateType' => "1",
          'AreaCode' => Session::get('demouser.AreaCode'),
          'Mobile' => Session::get('demouser.Mobile'),
          'Email' => Session::get('demouser.Email')
        ]
      ]);
      $response = json_decode($requestCode->getBody());

      // if($response->Code==21004){
      //   alert()->info('Please Login.', 'This mobile number has already been used to register.');
      //   return redirect()->route('login');
      // }
      return response()->json([
        'code' => $response->Code,
        'message' => $response->Message,
        'response' => $response,
        'IDNO' => $request->IDNO
      ]);
    } catch (Exception $e) {
      dd($e->message);
    }
  }



  public function sendVerificationCodeRegistration()
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/message/sendvalidatecaptcha';
    $client = new Client();
    $requestCode = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'InfoType' => Session::get('demouser.InfoType'),
        'ValidateType' => "1",
        'AreaCode' => Session::get('demouser.AreaCode'),
        'Mobile' => Session::get('demouser.Mobile'),
        'Email' => Session::get('demouser.Email')
      ]
    ]);
    $response = json_decode($requestCode->getBody());
    $originRegister = Session::get('demouser')['originRegister'];
    $agent = new Agent();
    // dd($response,$originRegister);
    if ($response->Code === 0) {
      $viewVerifiCode = 1;
      if ($originRegister == 'commission80percent') {
        return view('page.webpage.campaign.commission80percentv2', compact('viewVerifiCode', 'agent'));
      } else if ($originRegister == 'acceptallea') {
        return view('page.webpage.campaign.accept-all-ea', compact('viewVerifiCode'));
      } else if ($originRegister == 'legalbroker') {
        return view('page.webpage.campaign.legalbroker', compact('viewVerifiCode'));
      } else if ($originRegister == 'prestigious') {
        return view('page.webpage.campaign.prestigious', compact('viewVerifiCode'));
      }
    } else {
      if ($originRegister == 'commission80percent') {
        return redirect()->route('commission80percent')->with('ErrorMessage', $response->Message);
      } else if ($originRegister == 'acceptallea') {
        return redirect()->route('acceptalleas')->with('ErrorMessage', $response->Message);
      } else if ($originRegister == 'legalbroker') {
        return redirect()->route('legalbroker')->with('ErrorMessage', $response->Message);
      } else if ($originRegister == 'prestigious') {
        return redirect()->route('prestigious')->with('ErrorMessage', $response->Message);
      }
    }
  }

  public function adddemouser(Request $request)
  {
    $dataDemoUser = Session::get('demouser');
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/adddemouser';
    $client = new Client();
    $req_register = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'Password' => $dataDemoUser['Password'],
        'ConfirmPassword' => $dataDemoUser['ConfirmPassword'],
        'Email' => $dataDemoUser['Email'],
        'Realname' => $dataDemoUser['Realname'],
        'AreaCode' => $dataDemoUser['AreaCode'],
        'Mobile' => $dataDemoUser['Mobile'],
        'IDNO' => $dataDemoUser['IDNO'],
        'Nationality' => $dataDemoUser['Nationality'],
        'VerifyCode' => $request->VerifyCode,
        'ParentId' => $dataDemoUser['ParentId'],
        'RegChannel' => 1
      ]
    ]);
    $response = json_decode($req_register->getBody());
    // dd($response);
    if ($response->Code === 0) {
      $agent = new Agent();
      $client = new Client();

      $insertcrm = $client->post(env('CRM_URL') . '/insert_client', [
        'headers' => [
          "X-Authorization-Time" => time(),
          "X-Authorization-Token" => env('CRM_TOKEN'),
          "useragent" => $_SERVER['HTTP_USER_AGENT']
        ],
        'form_params' => [
          'name' => $dataDemoUser['Realname'],
          'email' => $dataDemoUser['Email'],
          'phone' => $dataDemoUser['AreaCode'] . $dataDemoUser['Mobile'],
          'ktp' => $dataDemoUser['IDNO'],
          'parent' => Cookie::get('ref'),
          'origin' => Cookie::get('so'),
          'campaign' => Cookie::get('campaign'),
          'media_iklan' => 'none',
          'previous_url' => URL::previous(),
          'ipaddress' => request()->ip(),
          'desktop' => $agent->platform(),
          'device' => $agent->device(),
          'lang' => $agent->languages()[0],
          'uuid' => Cookie::get('uuid')
        ]
      ]);
      $responsecrm = json_decode($insertcrm->getBody());

      Session::forget('demouser.Password');
      Session::forget('demouser.ConfirmPassword');
      Session::forget('demouser.Email');
      Session::forget('demouser.Realname');
      Session::forget('demouser.AreaCode');
      Session::forget('demouser.Mobile');
      Session::forget('demouser.IDNO');
      Session::forget('demouser.Nationality');
      Session::forget('demouser.VerifyCode');
      Session::forget('demouser.ParentId');
      Session::forget('demouser.InfoType');
      return redirect()->route('thank-you');
    } else {
      $viewVerifiCode = 1;
      $originRegister = Session::get('demouser')['originRegister'];
      $ErrorMessage = $response->Message;
      if ($response->Code == 21003) {
        unset($viewVerifiCode);
        alert()->info('Info Message.', $ErrorMessage);
        if ($originRegister == 'commission80percent') {
          return redirect()->route('commission80percent')->with('ErrorMessage', $response->Message);
        } else if ($originRegister == 'acceptallea') {
          return redirect()->route('acceptalleas')->with('ErrorMessage', $response->Message);
        } else if ($originRegister == 'legalbroker') {
          return redirect()->route('legalbroker')->with('ErrorMessage', $response->Message);
        } else if ($originRegister == 'prestigious') {
          return redirect()->route('prestigious')->with('ErrorMessage', $response->Message);
        }
      }

      if ($originRegister == 'commission80percent') {
        return view('page.webpage.campaign.commission80percent', compact('viewVerifiCode', 'ErrorMessage'))->withInput();
      } else if ($originRegister == 'acceptallea') {
        return view('page.webpage.campaign.accept-all-ea', compact('viewVerifiCode', 'ErrorMessage'))->withInput();
      } else if ($originRegister == 'legalbroker') {
        return view('page.webpage.campaign.legalbroker', compact('viewVerifiCode', 'ErrorMessage'))->withInput();
      } else if ($originRegister == 'prestigious') {
        return view('page.webpage.campaign.prestigious', compact('viewVerifiCode', 'ErrorMessage'))->withInput();
      }
    }
  }

  public function apiadddemouser(Request $request)
  {
    $dataDemoUser = Session::get('demouser');
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/adddemouser';
    $client = new Client();
    $req_register = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'Password' => $dataDemoUser['Password'],
        'ConfirmPassword' => $dataDemoUser['ConfirmPassword'],
        'Email' => $dataDemoUser['Email'],
        'Realname' => $dataDemoUser['Realname'],
        'AreaCode' => $dataDemoUser['AreaCode'],
        'Mobile' => $dataDemoUser['Mobile'],
        'IDNO' => $dataDemoUser['IDNO'],
        'Nationality' => $dataDemoUser['Nationality'],
        'VerifyCode' => $request->VerifyCode,
        'ParentId' => $dataDemoUser['ParentId'],
        'RegChannel' => 1
      ]
    ]);
    $response = json_decode($req_register->getBody());
    // dd($response);
    if ($response->Code === 0) {
      $agent = new Agent();
      $client = new Client();

      $insertcrm = $client->post(env('CRM_URL') . '/insert_client', [
        'headers' => [
          "X-Authorization-Time" => time(),
          "X-Authorization-Token" => env('CRM_TOKEN'),
          "useragent" => $_SERVER['HTTP_USER_AGENT']
        ],
        'form_params' => [
          'name' => $dataDemoUser['Realname'],
          'email' => $dataDemoUser['Email'],
          'phone' => $dataDemoUser['AreaCode'] . $dataDemoUser['Mobile'],
          'ktp' => $dataDemoUser['IDNO'],
          'parent' => Cookie::get('ref'),
          'origin' => Cookie::get('so'),
          'campaign' => Cookie::get('campaign'),
          'media_iklan' => 'none',
          'previous_url' => URL::previous(),
          'ipaddress' => request()->ip(),
          'desktop' => $agent->platform(),
          'device' => $agent->device(),
          'lang' => $agent->languages()[0],
          'uuid' => Cookie::get('uuid')
        ]
      ]);
      $responsecrm = json_decode($insertcrm->getBody());

      // otomatis login saat berhasil registrasi
      $urllogin = $api_url . 'api/account/listmatchusers';
      $client_login = $client->post($urllogin, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en'
        ],
        'json' =>  [
          'Username' => $dataDemoUser['Email'],
          'Password' => $dataDemoUser['Password'],
          'InputType' => 'email'
        ]
      ]);
      $accounts = json_decode($client_login->getBody());

      // jika berhasil login
      if ($accounts->Code === 0) {
        $header = $client_login->getHeaders();

        if (count($header['Set-Cookie']) > 1) {
          $cfuid = $header['Set-Cookie'][0];
          Cookie::queue('__cfduid', substr($cfuid, 9));
          $cookie = $header['Set-Cookie'][1];
        } else {
          $cookie = $header['Set-Cookie'][0];
        }

        $cookie = explode(";", $cookie);
        $session = trim($cookie[0]);
        Cookie::queue('NBCRMWEBAPI.SESSION', substr($session, 20));
        Session::put('user.token', $session);
        Session::put('user.tokencabinet', substr($session, 20));
        Session::put('user.id', $accounts->Data->User->UserId);
        Session::put('user.parent', $accounts->Data->User->ParentId);
        Session::put('user.type', $accounts->Data->User->UserType);
        Session::put('user.email', $accounts->Data->User->Email);
        Session::put('user.mobile', $accounts->Data->User->Mobile);
        Session::put('user.gender', $accounts->Data->User->Gender);
        Session::put('user.name', $accounts->Data->IdCard->RealName);
        Session::put('user.IDNO', $accounts->Data->IdCard->IDNO);
        Session::put('user.PlaceOfBirth', $accounts->Data->User->PlaceOfBirth);
        Session::put('user.DateOfBirth', $accounts->Data->User->DateOfBirth);
        Session::put('user.Address', $accounts->Data->User->Address);
        Session::put('user.KycStatus', $accounts->Data->User->KycStatus);
        Session::put('user.Currency', $accounts->Data->User->Currency);

        Session::forget('demouser.Password');
        Session::forget('demouser.ConfirmPassword');
        Session::forget('demouser.Email');
        Session::forget('demouser.Realname');
        Session::forget('demouser.AreaCode');
        Session::forget('demouser.Mobile');
        Session::forget('demouser.IDNO');
        Session::forget('demouser.Nationality');
        Session::forget('demouser.VerifyCode');
        Session::forget('demouser.ParentId');
        Session::forget('demouser.InfoType');

        // return redirect()->route('profile-trading-tool');
        return response()->json([
          'code' => $accounts->Code,
          'response' => $accounts
        ]);
      } else {

        return response()->json([
          'code' => $accounts->Code,
          'message' => $accounts->Message,
          'datademo' => $dataDemoUser
        ]);
      }

      // Session::forget('demouser.Password');
      // Session::forget('demouser.ConfirmPassword');
      // Session::forget('demouser.Email');
      // Session::forget('demouser.Realname');
      // Session::forget('demouser.AreaCode');
      // Session::forget('demouser.Mobile');
      // Session::forget('demouser.IDNO');
      // Session::forget('demouser.Nationality');
      // Session::forget('demouser.VerifyCode');
      // Session::forget('demouser.ParentId');
      // Session::forget('demouser.InfoType');
      // return response()->json([
      //   'code' => $response->Code,
      //   'response' => $response
      // ]);
      // // return redirect()->route('thank-you');
    } else {

      return response()->json([
        'code' => $response->Code,
        'message' => $response->Message,
        'datademo' => $dataDemoUser
      ]);
    }
  }

  public function acceptalleasthankyou()
  {
    return view('page.webpage.campaign.accept-alleas-thankyou');
  }

  public function thankyou2()
  {
    $originRegister = Session::get('demouser')['originRegister'];
    if ($originRegister == 'commission80percent') {
      return view('page.webpage.campaign.commission80percent-thankyou');
    } else if ($originRegister == 'acceptallea') {
      return view('page.webpage.campaign.accept-alleas-thankyou');
    }
  }

  public function redeembonus500dolar(){
    return view('page.webpage.campaign2.dapatkanbonus500dolar');
  }

  public function redeembonus500dolarV2(){
    return view('page.webpage.campaign2.dapatkanbonus500dolar-v2');
  }


  // ======================melandas

  public function whyMelandas()
  {

    $brand = DB::table('brand')->orderby('name', 'asc')->get();
    return view('pages.about.why-melandas', compact('brand'));
  }

  public function career()
  {

    return view('pages.about.career');
  }

  public function ourStore()
  {
    $brand = DB::table('brand')->orderby('name', 'asc')->get();
    return view('pages.about.history-our-places', compact('brand'));
  }

  public function ourBrands()
  {
    $brand = DB::table('brand')->orderby('name', 'asc')->get();
    return view('pages.about.history-our-brand', compact('brand'));
  }

  public function brandNicoleti(Request $request)
  {
    $id = 1;

    $hasil = DB::table('products')->join('functions', 'functions.id', 'products.productFunction')->where('products.brand_id', $id)->where('products.productStatus', 'active')->orderby('products.productName', 'asc')->get();

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $col = new Collection($hasil);
    $perPage = 30;
    $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $products = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
    if ($request->ajax()) {
      $view = view('pages.product.container', compact('products'))->render();
      return response()->json(['html' => $view]);
    }

    $brand = DB::table('brand')->where('id', $id)->first();
    $functions = DB::table('products')->where('brand_id', $id)->join('functions', 'functions.id', 'products.productFunction')->distinct()->select('products.productFunction')->get('functions.functionName');
    return view('pages.collections.brand', compact('brand', 'products', 'functions'));
  }

  public function brandLazboy(Request $request)
  {
    $id = 2;

    $hasil = DB::table('products')->join('functions', 'functions.id', 'products.productFunction')->where('products.brand_id', $id)->where('products.productStatus', 'active')->orderby('products.productName', 'asc')->get();

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $col = new Collection($hasil);
    $perPage = 30;
    $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $products = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
    if ($request->ajax()) {
      $view = view('pages.product.container', compact('products'))->render();
      return response()->json(['html' => $view]);
    }

    $brand = DB::table('brand')->where('id', $id)->first();
    $functions = DB::table('products')->where('brand_id', $id)->join('functions', 'functions.id', 'products.productFunction')->distinct()->select('products.productFunction')->get('functions.functionName');
    return view('pages.collections.brand', compact('brand', 'products', 'functions'));
  }

  public function brandCattelan(Request $request)
  {
    $id = 3;

    $hasil = DB::table('products')->join('functions', 'functions.id', 'products.productFunction')->where('products.brand_id', $id)->where('products.productStatus', 'active')->orderby('products.productName', 'asc')->get();

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $col = new Collection($hasil);
    $perPage = 30;
    $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $products = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
    if ($request->ajax()) {
      $view = view('pages.product.container', compact('products'))->render();
      return response()->json(['html' => $view]);
    }

    $brand = DB::table('brand')->where('id', $id)->first();
    $functions = DB::table('products')->where('brand_id', $id)->join('functions', 'functions.id', 'products.productFunction')->distinct()->select('products.productFunction')->get('functions.functionName');
    return view('pages.collections.brand', compact('brand', 'products', 'functions'));
  }

  public function brandEichholtz(Request $request)
  {
    $id = 4;

    $hasil = DB::table('products')->join('functions', 'functions.id', 'products.productFunction')->where('products.brand_id', $id)->where('products.productStatus', 'active')->orderby('products.productName', 'asc')->get();

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $col = new Collection($hasil);
    $perPage = 30;
    $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $products = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
    if ($request->ajax()) {
      $view = view('pages.product.container', compact('products'))->render();
      return response()->json(['html' => $view]);
    }

    $brand = DB::table('brand')->where('id', $id)->first();
    $functions = DB::table('products')->where('brand_id', $id)->join('functions', 'functions.id', 'products.productFunction')->distinct()->select('products.productFunction')->get('functions.functionName');
    return view('pages.collections.brand', compact('brand', 'products', 'functions'));
  }

  public function brandNatuzzie(Request $request)
  {
    $id = 5;

    $hasil = DB::table('products')->join('functions', 'functions.id', 'products.productFunction')->where('products.brand_id', $id)->where('products.productStatus', 'active')->orderby('products.productName', 'asc')->get();

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $col = new Collection($hasil);
    $perPage = 30;
    $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
    $products = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
    if ($request->ajax()) {
      $view = view('pages.product.container', compact('products'))->render();
      return response()->json(['html' => $view]);
    }

    $brand = DB::table('brand')->where('id', $id)->first();
    $functions = DB::table('products')->where('brand_id', $id)->join('functions', 'functions.id', 'products.productFunction')->distinct()->select('products.productFunction')->get('functions.functionName');
    return view('pages.collections.brand', compact('brand', 'products', 'functions'));
  }

  public function productFunction($id)
  {
    $products = DB::table('products')->join('brand', 'brand.id', 'products.brand_id')->where('products.productFunction', $id)->where('products.productStatus', 'active')->inRandomOrder()->limit(50)->get();
    $function = DB::table('functions')->where('id', $id)->first();
    $brand = DB::table('brand')->inRandomOrder()->first();
    return view('pages.collections.function', compact('products', 'function', 'brand'));
  }

  public function functionDinningTable()
  {
    $products = DB::table('products')->join('brand', 'brand.id', 'products.brand_id')->where('products.productFunction', '1')->where('products.productStatus', 'active')->inRandomOrder()->limit(50)->get();
    $function = DB::table('functions')->where('id', '1')->first();
    $brand = DB::table('brand')->inRandomOrder()->first();
    return view('pages.collections.function', compact('products', 'function', 'brand'));
  }

  public function functionCoffeeTable()
  {

    return view('pages.collections.function');
  }

  public function functionChairs()
  {

    return view('pages.collections.function');
  }

  public function functionRecliners()
  {

    return view('pages.collections.function');
  }

  public function functionSofa()
  {

    return view('pages.collections.function');
  }

  public function newThisMonth($id)
  {
    $date = date('Y-m-d H:i:s');
    $trimonth = strtotime($date . ' - 3 months');
    $products = DB::table('products')->join('brand', 'brand.id', 'products.brand_id')->where('products.productFunction', $id)->where('products.productStatus', 'active')->where('products.created_at', '>', $trimonth)->limit(50)->get();
    $function = DB::table('functions')->where('id', $id)->first();
    $brand = DB::table('brand')->inRandomOrder()->first();
    return view('pages.collections.this-month', compact('products', 'function', 'brand'));
  }

  public function newUpcoming()
  {

    return view('pages.collections.upcoming');
  }

  public function newIndent()
  {

    return view('pages.collections.indent');
  }

  public function clearStore()
  {

    return view('pages.collections.clear-store');
  }

  public function clearEvents()
  {

    return view('pages.collections.clear-event');
  }

  public function clearStock()
  {

    return view('pages.collections.clear-stock');
  }

  public function promotion()
  {
    $promotions = DB::table('promotion')->orderby('id', 'desc')->where('promotionStatus', 'active')->get();
    return view('pages.news-events.promotion', compact('promotions'));
  }

  public function newsEvent()
  {
    $news = DB::table('blogs')->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')->select(
      'cms_users.name',
      'cms_users.photo',
      'cms_users.id',
      'blogs.id',
      'blogs.blogTitle',
      'blogs.blogSlug',
      'blogs.blogContent',
      'blogs.blogImage1',
      'blogs.blogMetaDescription',
      'blogs.blogAuthor',
      'blogs.created_at'
    )->where('blogStatus', 'active')->orderby('blogs.id', 'desc')->get();
    return view('pages.news-events.index', compact('news'));
  }

  public function storeByLocation($loc)
  {
    $stores = db::table('stores')->where('storeCity', $loc)->get();
    $brand = DB::table('brand')->inRandomOrder()->first();
    return view('pages.stores.location', compact('stores', 'brand'));
  }

  public function storeNicoleti()
  {
    $stores = db::table('stores')->where('brand_id', '1')->get();
    $brand = DB::table('brand')->where('id', '1')->first();
    return view('pages.stores.nicoletti', compact('stores', 'brand'));
  }

  public function storeLazboy()
  {
    $stores = db::table('stores')->where('brand_id', '2')->get();
    $brand = DB::table('brand')->where('id', '2')->first();
    return view('pages.stores.lazboy', compact('stores', 'brand'));
  }

  public function storeCattelan()
  {
    $stores = db::table('stores')->where('brand_id', '3')->get();
    $brand = DB::table('brand')->where('id', '3')->first();
    return view('pages.stores.cattelan', compact('stores', 'brand'));
  }

  public function storeEichholtz()
  {
    $stores = db::table('stores')->where('brand_id', '4')->get();
    $brand = DB::table('brand')->where('id', '4')->first();
    return view('pages.stores.eichholtz', compact('stores', 'brand'));
  }

  public function storeNatuzzie()
  {
    $stores = db::table('stores')->where('brand_id', '5')->get();
    $brand = DB::table('brand')->where('id', '5')->first();
    return view('pages.stores.natuzzie', compact('stores', 'brand'));
  }

  public function storeDetail($id)
  {
    $store = db::table('stores')->whereId($id)->first();
    return view('pages.stores.single', compact('store'));
  }

  public function customerService()
  {

    return view('pages.contact.customer-service');
  }

  public function contactYou()
  {

    return view('pages.contact.contact-you');
  }

  public function faq()
  {

    return view('pages.general.faq');
  }



  public function register()
  {

    return view('pages.general.register');
  }

  public function lostPassword()
  {

    return view('pages.general.lost-password');
  }



  public function termCondition()
  {

    return view('pages.general.tac');
  }

  public function productSingle($brand, $slug)
  {
    $brpr = DB::table('brand')->where('id', $brand)->first();
    $product = DB::table('products')->where('productSlug', $slug)->first();
    $function = DB::table('functions')->where('id', $product->productFunction)->first();
    $images = unserialize($product->productImages);
    $details = unserialize($product->productVideos);
    $brand = DB::table('brand')->orderby('name', 'asc')->get();

    return view('pages.product.single', compact('brpr', 'product', 'function', 'images', 'details', 'brand'));
  }


  public function career_experienced_professional()
  {
    $works = DB::table('job_vacancies')->where('jobLevel', 'Experienced Professional')->get();
    return view('pages.general.job-1', compact('works'));
  }

  public function career_recent_graduated()
  {
    $works = DB::table('job_vacancies')->where('jobLevel', 'Recent Graduates')->get();
    return view('pages.general.job-2', compact('works'));
  }

  public function promotionSingle($slug)
  {
    $promo = DB::table('promotion')->where('promotionSlug', $slug)->first();
    $words = str_word_count($promo->promotionTitle);
    $mod = $words / 3;
    $title = explode(" ", $promo->promotionTitle);
    $median = floor($words / $mod);
    return view('pages.news-events.promo-single', compact('promo', 'title', 'mod', 'median'));
  }


  public function bountyprogram()
  {
    return view('page.webpage.promo.bounty');
  }

  public function tradingToolProfile()
  {
    $tools = DB::table('tools')->get();
    return view('page.webpage.trading-tool.profile', compact('tools'));
  }

  public function robotTrading()
  {
    $tools = DB::table('tools')->get();
    return view('page.webpage.trading-tool.robot-trading', compact('tools'));
  }

  public function sitemap(){
    $blogs = DB::table('blogs')->where('blogStatus', 'active')->orderby('release_date', 'desc')->get();
    return response()->view('page.webpage.sitemap', [
      'blogs' => $blogs,
    ])->header('Content-Type', 'text/xml');
  }

  public function sitemaphtml(){
    $blogs = DB::table('blogs')->where('blogStatus', 'active')->limit(15)->orderby('release_date', 'desc')->get();

    return response()->view('page.webpage.sitemaphtml',compact('blogs'));
    // dd('sitemap html');
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PageAdminController extends Controller
{
    public function productUpload(){
      $brands = DB::table('brand')->get();
      $functions = DB::table('functions')->get();
      return view('admin.productUpload',compact('brands','functions'));
    }

    public function productEdit($id){
      $product = DB::table('products')->where('id',$id)->first();
      $brands = DB::table('brand')->get();
      $functions = DB::table('functions')->get();
      //dd($product);
      return view('admin.productEdit',compact('brands','functions','product'));
    }

    public function storeEdit($id){
      $product = DB::table('stores')->where('id',$id)->first();
      $brands = DB::table('brand')->get();
      $functions = DB::table('functions')->get();
      //dd($product);
      return view('admin.productEdit',compact('brands','functions','product'));
    }
}

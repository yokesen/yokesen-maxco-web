<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use CRUDBooster;

class CampaignController extends Controller
{
    public function battleOfTheRobotsPromo(){
      $robots = DB::connection('sewarobot')->table('robots')->join('robottype','robottype.id','robots.robotType')->get();

      return view('page.webpage.campaigns.battle-of-robot-promo',compact('robots'));
    }
}

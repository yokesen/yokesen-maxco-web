<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Image;
use Storage;
use CRUDBooster;

class FormAdminController extends Controller
{
    public function productUpload(Request $request){
      //dd($request->all());
      if(!empty($request->photos)){
        foreach($request->photos as $photo){
          $image = $photo;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayName[] = $imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      if(!empty($request->videos)){
        foreach($request->videos as $video){
          $image = $video;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayVideo[] = $imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      $videoSave = serialize($arrayVideo);
      $imageSave = serialize($arrayName);
      $slug = str_slug($request->productName);
      $insert = DB::table('products')->insert([
        'brand_id' => $request->Brand,
        'productName' => $request->productName,
        'productDescription' => $request->productDescription,
        'productFunction' => $request->ProductFunction,
        'productStatus' => $request->productStatus,
        'productPrice' => $request->productPrice,
        'productImages' => $imageSave,
        'productVideos' => $videoSave,
        'productSlug' => $slug
      ]);

      CRUDBooster::redirect('melandas-web-admin/products' ,    'Produk berhasil disimpan!', 'success');
    }

    public function productUpdating(Request $request,$id){
      dd($id,$request->all());
    }

    public function displayDelete($productID,$imageID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productImages);
      array_splice($images, $imageID, 1);
      $image_save = serialize($images);
      $update = DB::table('products')->where('id',$productID)->update([
        'productImages' => $image_save
      ]);
      //dd($product,$productID,$imageID,$images);
      return redirect()->back();
    }

    public function detailDelete($productID,$imageID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productVideos	);
      array_splice($images, $imageID, 1);
      $image_save = serialize($images);
      $update = DB::table('products')->where('id',$productID)->update([
        'productVideos' => $image_save
      ]);
      //dd($product,$productID,$imageID,$images);
      return redirect()->back();
    }

    public function displayAdd(Request $request,$productID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productImages	);

      if(!empty($request->photos)){
        foreach($request->photos as $photo){
          $image = $photo;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayName[] = $imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      if(count($images)>0){
        foreach($arrayName as $baru){
          array_push($images,$baru);
        }
      }else{
        $images = $arrayName;
      }
      $saveimg = serialize($images);
      $product = DB::table('products')->where('id',$productID)->update([
        'productImages' => $saveimg
      ]);

      return redirect()->back();
    }

    public function detailAdd(Request $request,$productID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productVideos	);
      if(!empty($request->videos)){
        foreach($request->videos as $video){
          $image = $video;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayVideo[] = $imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }
      if(count($images)>0){
        foreach($arrayVideo as $baru){
          array_push($images,$baru);
        }
      }else{
        $images = $arrayVideo;
      }
      $saveimg = serialize($images);
      $product = DB::table('products')->where('id',$productID)->update([
        'productVideos' => $saveimg
      ]);
      return redirect()->back();
    }
}

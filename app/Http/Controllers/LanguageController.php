<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Crudbooster;
use UriLocalizer;
use \Waavi\Translation\Repositories\LanguageRepository;

class LanguageController extends Controller
{
    public function list(){
      $lists = DB::table('translator_translations')->where('group','page')->distinct()->select('item')->get();
      return view('admin.translator.list',compact('lists'));
    }

    public function edit($item){
      $data = DB::table('translator_translations')->where('item',$item)->get();
      return view('admin.translator.edit',compact('data'));
    }
}

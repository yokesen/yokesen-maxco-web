<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#137dc1',
        'button'    => '#d62027',

    ],

    'view' => [
        'senderName'  => 'Smart Maxco',
        'reminder'    => 'Email ini dikirim karena Anda menginput data di web '.env('APP_URL'),
        'unsubscribe' => 'Untuk unsubsciber mohon hubungi nomor telpon',
        'address'     => 'Panin Bank Centre - Ground Floor
Jl. Jend. Sudirman Kav-1 Senayan Jakarta Selatan 10270
DKI Jakarta, Indonesia',

        'logo'        => [
            'path'   => '%PUBLIC%/web/images/web/logo_300.png',
            'width'  => '',
            'height' => '49',
        ],

        //'twitter'  => 'https://www.instagram.com/smartmaxco/',
        'facebook' => 'smartmaxco',
      //  'flickr'   => 'https://www.youtube.com/channel/UC69yjQ0hynhMXffy4K-XpWw',
    ],

];
